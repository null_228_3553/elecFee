<%@ page language="java" contentType="text/html; charset=utf-8"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="" />
	<meta name="decription" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
	<title>校园宣讲会</title>		
<link href="css/icon.css"  rel="stylesheet" type="text/css" />
<link href="css/style1.css"  rel="stylesheet" type="text/css" />
<link href="css/color_02.css"  rel="stylesheet" type="text/css" />
<link href="css/bootstrap-3.3.7-dist/css/bootstrap.css"  rel="stylesheet" type="text/css" />
<link href="css/switch.css"  rel="stylesheet" type="text/css" />
    <link href="css/mui.css" rel="stylesheet"/>
	<link href="css/mui.picker.css" rel="stylesheet" />
	<link href="css/mui.poppicker.css" rel="stylesheet" />
<link href="css/mobiscroll.css" rel="stylesheet" />
<link href="css/mobiscroll_date.css" rel="stylesheet" />
<link href="css/scrollbar5.css" rel="stylesheet" />
<style type="text/css">
</style>
</head>
<body>

<div class="container-fluid"  id="divBody">
	<div id="qrcode">

	</div>
</div>

<div class="bottom_box_02">
	<div class="bottom_box_inside">
		<span style="width:233px;" id="bottom_p_02">
			智慧华中大&nbsp;&nbsp;数据提供：大学生就业信息网
		</span>
	</div>
</div>

<script src="js/vue.js" type="text/javascript"></script>
<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/layer.js" type="text/javascript"></script>
<script src="https://pass.hust.edu.cn/cas/js/qrcode.js" type="text/javascript"></script>
<script src="https://pass.hust.edu.cn/cas/js/login-qrcode.js" type="text/javascript"></script>
<script type="text/javascript">
var db=new Vue({
	el:"#divBody",
	mounted:function(){
		var lqrcode = new loginQRCode("qrcode",153,153);
		lqrcode.generateLoginQRCode(function(token){
			window.location.href="/elecFee/qrLogin.do?token="+token;
		});
	},
	computed:{
	},
	watch: {
	},
	methods:{
	},
	data:{
	}
});
</script>
</body>
</html>