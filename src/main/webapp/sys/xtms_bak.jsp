<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript">
$(document).ready(function(){
	$("#zxq").click(function(){
		if($(this).attr("class")==undefined)
			$("#z401").click();
		$(this).attr("class","kctab2_select");
		$("#dxq").removeAttr("class");
		$("#zTitle").show();
		$("#dTitle").hide();
	});
	$("#dxq").click(function(){
		if($(this).attr("class")==undefined)
			$("#d201").click();
		$(this).attr("class","kctab2_select");
		$("#zxq").removeAttr("class");
		$("#dTitle").show();
		$("#zTitle").hide();
	});
	$("#z401").click(function(){
		$(this).attr("class","kctab_select");
		$("#z403").removeAttr("class");
		$("#z208").removeAttr("class");
		$("#z401div").show();
		$("#z403div").hide();
		$("#z208div").hide();
		$("#d201div").hide();
		$("#d202div").hide();
		$("#d301div").hide();
	});
	$("#z403").click(function(){
		$(this).attr("class","kctab_select");
		$("#z401").removeAttr("class");
		$("#z208").removeAttr("class");
		$("#z401div").hide();
		$("#z403div").show();
		$("#z208div").hide();
		$("#d201div").hide();
		$("#d202div").hide();
		$("#d301div").hide();
	});
	$("#z208").click(function(){
		$(this).attr("class","kctab_select");
		$("#z403").removeAttr("class");
		$("#z401").removeAttr("class");
		$("#z401div").hide();
		$("#z403div").hide();
		$("#z208div").show();
		$("#d201div").hide();
		$("#d202div").hide();
		$("#d301div").hide();
	});
	$("#d201").click(function(){
		$(this).attr("class","kctab_select");
		$("#d202").removeAttr("class");
		$("#d206").removeAttr("class");
		$("#d301").removeAttr("class");
		$("#z401div").hide();
		$("#z403div").hide();
		$("#z208div").hide();
		$("#d201div").show();
		$("#d202div").hide();
		$("#d301div").hide();
	});
	$("#d202").click(function(){
		$(this).attr("class","kctab_select");
		$("#d201").removeAttr("class");
		$("#d206").removeAttr("class");
		$("#d301").removeAttr("class");
		$("#z401div").hide();
		$("#z403div").hide();
		$("#z208div").hide();
		$("#d201div").hide();
		$("#d202div").show();
		$("#d301div").hide();
	});
	$("#d206").click(function(){
		$(this).attr("class","kctab_select");
		$("#d201").removeAttr("class");
		$("#d202").removeAttr("class");
		$("#d301").removeAttr("class");
		$("#z401div").hide();
		$("#z403div").hide();
		$("#z208div").hide();
		$("#d201div").hide();
		$("#d202div").show();
		$("#d301div").hide();
	});
	$("#d301").click(function(){
		$(this).attr("class","kctab_select");
		$("#d201").removeAttr("class");
		$("#d202").removeAttr("class");
		$("#d206").removeAttr("class");
		$("#z401div").hide();
		$("#z403div").hide();
		$("#z208div").hide();
		$("#d201div").hide();
		$("#d202div").hide();
		$("#d301div").show();
	});
	$(".text").hide();
	$(".kc_week_con").click(function(){
		$(".text").slideUp();
		if($(this).next().is(":hidden"))
			$(this).next().slideDown();
	});
});
</script>
<div class="div_title key_color" >系统描述</div>
<div class="main_03">
	<div style="margin:0 10px" class="systab kc_tab2">
		<a href="javascript:;" id="zxq" class="kctab2_select">主校区</a>
		<a href="javascript:;" id="dxq" >东校区</a>
	</div>
	<div id="zTitle" style="margin:0 10px 15px;" class="systab kc_tab" >
		<a href="javascript:;" id="z208" class="kctab_select">208机房</a>
		<a href="javascript:;" id="z401">401机房</a>
		<a href="javascript:;" id="z403">403机房</a>
	</div>
	<div id="dTitle" style="margin:0 10px 15px;display:none" class="systab kc_tab">
		<a href="javascript:;" id="d201" class="kctab_select" style="width:25%">201机房</a>
		<a href="javascript:;" id="d202" style="width:25%">202机房</a>
		<a href="javascript:;" id="d206" style="width:25%">206机房</a>
		<a href="javascript:;" id="d301" style="width:25%">301机房</a>
	</div>
	
<div id="z208div" class="kc_list" style="display:block">
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">1、网络实验(Windows Server 2003)</p>
	</div>
	<div class="text">
		Cisco Packet Tracer<br/>
		IIS 6.0<br/>
		Microsoft Office 2010<br/>
		VMware Workstation 8.0<br/>
		ServUAdmin<br/>
		Wireshark<br/>
	</div>
</div>
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">2、图形设计xp(Windows XP)</p>
	</div>
	<div class="text">
		3DMax2010<br/>
		ArcGIS<br/>
		AutoCAD 2007<br/>
		AutoCAD Mechanical 2010<br/>
		CATIA P3, DreamweaverCS6<br/>
		DWG TrueView 2014<br/>
		Google SketchUp 8<br/>
		Inventor2014<br/>
		MATLAB 7.1<br/>
		Microsoft Office 2007<br/>
		Microsoft Visual Studio 2005<br/>
		Model CHECK<br/>
		OSFMount<br/>
		ProENGINEER<br/>
		Protel 99 SE<br/>
		PTC<br/>
		SolidWorks2012<br/>
		UGS NX 4.0<br/>
		天喻CAD<br/>
	</div>
</div>
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">3、程序设计XP(Windows XP)</p>
	</div>
	<div class="text">
		Altium SP2<br/>
		AdobeDreamweave CS3,<br/>
		Adobe Firework CS3<br/>
		Adobe Flash CS3<br/>
		Adobe Photoshop CS3<br/>
		Compaq Visual Fortran 6<br/>
		DXP 2004 SP2<br/>
		MapInfo<br/>
		Microsoft office 2003(含Visio)<br/>
		Microsoft SQL Server 2005<br/>
		Microsoft Visual Basic 6.0<br/>
		Microsoft Visual C++ 6.0<br/>
		Microsoft Visual FoxPro 6.0<br/>
		Microsoft Visual Studio 2005<br/>
		OSFMount<br/>
		Processing<br/>
		SPSS for Windows<br/>
		Sybase<br/>
	</div>
</div>
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">4、Cn-Win7(Windows 7)</p>
	</div>
	<div class="text">
		Adobe PhotoshopCS4<br/>
		Adobe Flash CS4<br/>
		Cisco Packet Tracer<br/>
		Dreamweaver CS5<br/>
		Google Chrome 浏览器<br/>
		Microsoft Office 2010<br/>
		Microsoft Project2010<br/>
		Microsoft Visual C++ 6.0<br/>
		Microsoft Visual Studio2010<br/>
		Microsoft SQLServer 2008<br/>
		PGPDesktop 10.0.3<br/>
		Qt5.4.0<br/>
		VMware<br/>
		考试系统<br/>
		英语自主学习综合平台<br/>
		神机妙算软件<br/>
	</div>
</div>
</div>

<div id="z401div" class="kc_list" style="display:none;">
<div class="xxcx_row">
		<p style="line-height:30px;color:red;">主校区开放实验室本学期因安全原因停用整改，请同学们到东校区实训中心二楼机房上机！</p>
</div>
<!-- <div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">1、LINUX</p>
	</div>
	<div class="text">
		Fedora release 13<br/>
	</div>
</div>
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">2、图形设计(Windows 7)</p>
	</div>
	<div class="text">
		360极速浏览器<br/>
		Adobe Bridge CS6 (64bit)<br/>
		Adobe ExtendScript Toolkit CS6<br/>
		Adobe Extension Manager CS6<br/>
		Adobe Photoshop CS3<br/>
		Adobe Photoshop CS6 (64 Bit)<br/>
		Adobe Reader XI<br/>
		ANSYS 16.0<br/>
		AutoCAD 2014 - 简体中文 (Simplified Chinese)<br/>
		Autodesk 3ds Max 2012 64-bit - Simplified Chinese<br/>
		Autodesk Data Management<br/>
		Autodesk FBX Plug-ins<br/>
		Autodesk Inventor 2014<br/>
		Autodesk DWG TrueView 2014<br/>
		Compaq Visual Fortran 6<br/>
		DAEMON Tools Lite<br/>
		Google SketchUp 8<br/>
		Google Chrome浏览器<br/>
		IBM SPSS Statistics<br/>
		Internet Explorer浏览器<br/>
		Macromedia <br/>
		MATLAB 7.0<br/>
		Microsoft Office 2013<br/>
		Microsoft Visual Basic 6.0 中文版<br/>
		Microsoft Visual C++ 6.0<br/>
		Microsoft Visual FoxPro 6.0<br/>
		Microsoft Visual Studio 2005<br/>
		Protel 99<br/>
		SolidWorks 2012<br/>
		XPS Viewer<br/>
		暴风软件<br/>
		美图<br/>
		联想商用驱动管理器<br/>
		腾讯软件<br/>
		迅雷软件<br/>
	</div>
</div>
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">3、WIN7_64(Windows 7)</p>
	</div>
	<div class="text">
		Adobe Audition 3.0<br/>
		Adobe Dreamweaver CS3<br/>
		Adobe Fireworks CS3<br/>
		Adobe Flash CS3<br/>
		Adobe Reader XI<br/>
		Altium Designer<br/>
		Autodesk DWF Viewer - Simplified Chinese<br/>
		Inventor Fusion 2013<br/>
		AutoCAD 2007 - Simplified Chinese<br/>
		AutoCAD 2013 – 简体中文 (Simplified Chinese)<br/>
		Cisco Packet Tracer<br/>
		Citrix Receiver<br/>
		CloudCompare<br/>
		Google Chrome浏览器<br/>
		Internet Explorer 浏览器<br/>
		Java<br/>
		Java Development Kit<br/>
		MPLAB C32<br/>
		MPLAB IDE v8.30<br/>
		Microsoft Office 2010<br/>
		Microsoft Silverlight 3 SDK - 中文(简体)<br/>
		Microsoft SQL Server 2008 R2<br/>
		Microsoft Sync Framework<br/>
		Microsoft Visual Basic 6.0 中文版<br/>
		Microsoft Visual C++ 6.0<br/>
		Microsoft Visual FoxPro 6.0<br/>
		Mozilla Firefox浏览器<br/>
		National Instruments<br/>
		National Instruments LabVIEW 2012 （32位）<br/>
		Photoshop CS6<br/>
		PGP 8<br/>
		PTV Vision 2015<br/>
		TC2.0<br/>
		UltraISO<br/>
		VMware<br/>
		Weka 3.8.0<br/>
		WinPcap<br/>
		Wireshark<br/>
		XPS Viewer<br/>
		大学英语精品课程<br/>
		暴风软件<br/>
		美图<br/>
		联想商用驱动管理器<br/>
		腾讯软件<br/>
		迅雷软件<br/>
		考试系统<br/>
	</div>
</div>
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">4、Win 10(Windows 10)</p>
	</div>
	<div class="text">
		360极速浏览器<br/>
		Adobe Reader XI<br/>
		Google Chrome浏览器<br/>
		Microsoft Office 2013<br/>
		暴风软件<br/>
		美图<br/>
		腾讯软件<br/>
		迅雷软件<br/>
	</div>
</div> -->
</div>

<div id="z403div" class="kc_list" style="display:none;">
<div class="xxcx_row">
		<p style="line-height:30px;color:red;">主校区开放实验室本学期因安全原因停用整改，请同学们到东校区实训中心二楼机房上机！</p>
</div>
<!-- <div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">1、LINUX</p>
	</div>
	<div class="text">
		Fedora release 17<br/>
	</div>
</div>
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">2、图形设计(Windows 7 64-bit)</p>
	</div>
	<div class="text">
		Adobe Bridge CS6 (64bit)<br/>
		Adobe Extension Manager CS6<br/>
		Adobe Photoshop CS6<br/>
		Mechanical APDL Product Launcher 16.0<br/>
		Autodesk AutoCAD 2013<br/>
		Autodesk 3ds Max 2012 64-bit - Simplified Chinese<br/>
		Autodesk FBX Plug-ins<br/>
		Autodesk Inventor 2014<br/>
		Autodesk DWG TrueView 2014<br/>
		Compaq Visual Fortran 6<br/>
		DAEMON Tools Lite<br/>
		Google Chrome浏览器<br/>
		Google SketchUp 8<br/>
		IBM SPSS Statistics<br/>
		Macromedia Authorware 7.0<br/>
		MATLAB 7.0<br/>
		Microsoft Office 2013<br/>
		Microsoft Visual Basic 6.0 中文版<br/>
		Microsoft Visual C++ 6.0<br/>
		Microsoft Visual FoxPro 6.0<br/>
		Microsoft Visual Studio 2005<br/>
		Pro ENGINEERWildfire 5.0<br/>
		Protel 99<br/>
		SolidWorks 2012<br/>
		暴风软件<br/>
		美图<br/>
		腾讯QQ<br/>
		迅雷软件<br/>
	</div>
</div>
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">3、WINXP(Windows XP)</p>
	</div>
	<div class="text">
		Adobe Reader XI<br/>
		AutoCAD 2012 - Simplified Chinese<br/>
		Compaq Visual Fortran 6<br/>
		Google Chrome浏览器<br/>
		Microsoft Office 2010<br/>
		Microsoft SQL Server 2003<br/>
		Microsoft SQL Server 2008<br/>
		Microsoft Visual Studio 2010<br/>
		Protel 99 SE<br/>
		UltraISO<br/>
		WinRAR<br/>
		暴风软件<br/>
		电工实习自制PCB库文件<br/>
		美图<br/>
		腾讯QQ轻聊版<br/>
		微信<br/>
		迅雷软件<br/>
		用友U8 v10.1<br/>
	</div>
</div>
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">4、WIN 7_64(Windows 7 64-bit)</p>
	</div>
	<div class="text">
		Adobe Audition 3.0<br/>
		Adobe Dreamweaver CS3<br/>
		Adobe Fireworks CS3<br/>
		Adobe Flash CS3<br/>
		Adobe Photoshop CS6<br/>
		Autodesk AutoCAD 2007 - Simplified Chinese<br/>
		AutoCAD 2013 –简体中文 (Simplified Chinese)<br/>
		Autodesk Inventor Fusion 2013<br/>
		Cisco Packet Tracer<br/>
		Eclipse<br/>
		Google Chrome浏览器<br/>
		Java<br/>
		Java Development Kit<br/>
		Microsoft Office 2010<br/>
		Microsoft SQL Server 2008 R2<br/>
		Microsoft Visual Basic 6.0 中文版<br/>
		Microsoft Visual C++ 6.0<br/>
		Microsoft Visual FoxPro 6.0、<br/>
		Mozilla Firefox<br/>
		PGPDesktop 10.0.3<br/>
		TC2.0<br/>
		UltraISO<br/>
		VMware Workstation 12<br/>
		Wireshark<br/>
		大学英语自主学习综合平台<br/>
		暴风软件<br/>
		美图<br/>
		腾讯QQ<br/>
		万维全自动网络考试系统<br/>
	</div>
</div>
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">5、win10(Windows 10企业版 64-bit)</p>
	</div>
	<div class="text">
		Adobe Reader XI<br/>
		Microsoft Office 2013<br/>
		Skype<br/>
		暴风软件<br/>
		美图<br/>
	</div>
</div> -->
</div>

<div id="d201div" class="kc_list" style="display:none;">
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">1、程序设计XP(Windows XP)</p>
	</div>
	<div class="text">
		Adobe Dreamweaver/Flash/FireWorks CS3<br/>
		Adobe Photoshop CS3<br/>
		ANSYS 10.0<br/>
		DXP 2004 sp2 <br/>
		Eclipse 6.5<br/>
		Foxpro 6.0<br/>
		Mapinfor Professional7.0<br/>
		Microsoft MySQL 5.0<br/>
		PowerBuilder 9.0<br/>
		Microsoft SQL Server2005<br/>
		SPSS 13.0<br/>
		TCOS光学自动设计<br/>
		VB 6.0<br/>
		VC++ 6.0<br/>
		VMware Workstation 8.02(包含CentOS 5.6)<br/>
	</div>
</div>
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">2、图形设计XP(Windows XP)</p>
	</div>
	<div class="text">
		AutoCAD 2007<br/>
		Autodesk 3ds MAX 2010<br/>
		Autodesk Inventor Professional 2014<br/>
		CATIA P3 V5R19<br/>
		MATLAB 7.1<br/>
		Protel 99SE<br/>
		ProE 4.0(wildfire)<br/>
		SolidWorks 2012<br/>
		UGS NX4.0<br/>
		天喻CAD<br/>
	</div>
</div>
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">3、大学英语(Windows XP)</p>
	</div>
	<div class="text">
		主要提供学生直接登陆大学英语学习平台及所需插件<br/>
	</div>
</div>
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">4、CN-WIN7(中文win7)</p>
	</div>
	<div class="text">
		Adobe Dreamweaver CS5<br/>
		Adobe Flash CS4<br/>
		Adobe Audition 3.0<br/>
		CISCO Packet Tracer<br/>
		Microsoft Office 2010<br/>
		PGP Desktop 10.0.3<br/>
		Microsoft SQL Server 2008<br/>
		Microsoft Visual Studio 2010旗舰版(VB/VC++/VC#)<br/>
		万维网全自动网络考试系统<br/>
	</div>
</div>
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">5、EN-WIN7(英文win7)</p>
	</div>
	<div class="text">
		AutoCAD 2004（英文版）<br/>
		Adobe Photoshop CS4（英文版）<br/>
		ProE 4.0（英文版）<br/>
	</div>
</div>
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">6、软件实训XP(Windows XP)</p>
	</div>
	<div class="text">
		Apache Tomact 6.0<br/>
		MyEclipse 8.5<br/>
		MySQL 5.0<br/>
		Microsoft Office2003/2010<br/>
		Microsoft SQL Server 2005<br/>
		Microsoft SQL Server 2008 R2<br/>
		Microsoft Visual Studio 2005<br/>
		Microsoft Visual Studio 2010<br/>
	</div>
</div>
</div>

<div id="d202div" class="kc_list" style="display:none;">
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">1、图形设计（Windows7 64位）</p>
	</div>
	<div class="text">
		IE浏览器11.0<br/>
		360 极速浏览器<br/>
		Google Chrome 60.0.3112<br/>
		Adobe Photoshop CS3<br/>
		IBM SPSS Statistics 19.0<br/>
		ANSYS R16.0<br/>
		Autodesk AutoCAD 2014<br/>
		Autodesk 3ds MAX 2012<br/>
		Autodesk Inventor professsional 2014<br/>
		Autodesk Vault Basic 2014<br/>
		Google SketchUP 8.0<br/>
		MATLAB 7.0<br/>
		PKPM<br/>
		Autodesk REVIT 2016<br/>
		SolidWorks 2012<br/>
		Altium Designer 15.0<br/>
		Pro/Engineer 5.0<br/>
		UGS NX 4.0<br/>
		CATIA P3V5-6R2014<br/>
		Protel 99SE<br/>
		Compaq Visual Fortran 6.0<br/>
		ENVI 4.4<br/>
		Micromedia Authorware 7.0<br/>
		Office 2013<br/>
		Microsoft Visual Basic 6.0<br/>
		Microsoft Visual C++ 6.0<br/>
		Microsoft Visual Foxpro 6.0<br/>
	</div>
</div>
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">2、WIN7_x64_1（Windows7 64位）</p>
	</div>
	<div class="text">
		Office 2010<br/>
		Microsoft SQL Server 2008<br/>
		Microsoft Visual Studio 2010<br/>
		HFSS 15.0<br/>
		LabView 2012<br/>
		NI MAX<br/>
		Xilinx ISE 6.0<br/>
		用友U8 10.1<br/>
		ANSYS 18.2<br/>
		IE浏览器 11.0<br/>
		360浏览器<br/>
		Google Chrome<br/>
		Gephi 0.8.2 beta<br/>
		ArcGIS 10.2<br/>
	</div>
</div>
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">3、WIN7_x64_2（Windows7 64位）</p>
	</div>
	<div class="text">
		Office 2010<br/>
		IE浏览器 11.0<br/>
		FireFox浏览器 57.0.2<br/>
		Google Chrome 56.0.2924<br/>
		Adobe Photoshop CS6<br/>
		Adobe Dreamweaver CS3<br/>
		Adobe FireWorks CS3<br/>
		Adobe Flash CS3<br/>
		Adobe Audition 3.0<br/>
		Autodesk AutoCAD 2007<br/>
		Chivox Pepper Service<br/>
		Cisco Packet Tracer 5.3<br/>
		Compaq Visual Fortran 6.0<br/>
		Java Development Kit 5.5<br/>
		Microsoft SQl Server 2008R2<br/>
		Microsoft Visual Basic 6.0<br/>
		Microsoft Visual C++ 6.0<br/>
		Microsoft Visual Foxpro 6.0<br/>
		Microsoft Visual Studio 2010<br/>
		LabView 2012<br/>
		PTV Vissim 8.0<br/>
		Vmware Workstation 12.0<br/>
		Weka 3.8.0<br/>
		万维网全自动网络考试系统<br/>
		TC 2.0 for dos 2.0<br/>
		Eclips 4.4.1<br/>
		MPLAB IDE 8.30<br/>
		PGP desktop 10.03<br/>
	</div>
</div>
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">4、WIN10（Windows10 64位）
</p>
	</div>
	<div class="text">
		Office 2013<br/>
		IE浏览器 11.0<br/>
		360极速浏览器<br/>
		Google Chrome 60.0.3112.90<br/>
		微信电脑版<br/>
		QQ<br/>
	</div>
</div>
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">5、软件实训（Windows7 64位）</p>
	</div>
	<div class="text">
		Office 2013<br/>
		Visio 2010<br/>
		IE浏览器 11.0<br/>
		FireFox浏览器<br/>
		Apache Tomcat 6.0<br/>
		Daum Potplayer 1.6.56209<br/>
		Java Development Kit 5.5<br/>
		MyEclipse 8.5<br/>
		My SQL Server 5.0<br/>
		Notepad++<br/>
		Navicat for MySQL 8.0<br/>
		QT 5.5.0<br/>
		Tortoise SVN<br/>
		Microsoft Visual Studio 2015<br/>
		Windows Kits<br/>
	</div>
</div>
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">6、Linux</p>
	</div>
	<div class="text">
		Ubuntu 16.04.4<br/>
	</div>
</div>
</div>

<div id="d206div" class="kc_list" style="display:none;">
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">1、图形设计（Windows7 64位）</p>
	</div>
	<div class="text">
		IE浏览器11.0<br/>
		360 极速浏览器<br/>
		Google Chrome 60.0.3112<br/>
		Adobe Photoshop CS3<br/>
		IBM SPSS Statistics 19.0<br/>
		ANSYS R16.0<br/>
		Autodesk AutoCAD 2014<br/>
		Autodesk 3ds MAX 2012<br/>
		Autodesk Inventor professsional 2014<br/>
		Autodesk Vault Basic 2014<br/>
		Blender 2.79<br/>
		Google SketchUP 8.0<br/>
		MATLAB 7.0<br/>
		PKPM<br/>
		Autodesk REVIT 2016<br/>
		SolidWorks 2012<br/>
		Altium Designer 15.0<br/>
		Pro/Engineer 5.0<br/>
		UGS NX 4.0<br/>
		CATIA P3V5-6R2014<br/>
		Protel 99SE<br/>
		Compaq Visual Fortran 6.0<br/>
		ENVI 4.4<br/>
		Micromedia Authorware 7.0<br/>
		Office 2013<br/>
		Microsoft Visual Basic 6.0<br/>
		Microsoft Visual C++ 6.0<br/>
		Microsoft Visual Foxpro 6.0<br/>
		Gephi 0.9.2<br/>
		ArcGIS 10.5<br/>
		Solidthinking 2017<br/>
	</div>
</div>
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">2、WIN7_x64_1（Windows7 64位）</p>
	</div>
	<div class="text">
		Office 2010<br/>
		Microsoft SQL Server 2008<br/>
		Microsoft Visual Studio 2010<br/>
		HFSS 15.0<br/>
		LabView 2012<br/>
		NI MAX<br/>
		Xilinx ISE 6.0<br/>
		用友U8 10.1<br/>
		ANSYS 18.2<br/>
		IE浏览器 11.0<br/>
		360浏览器<br/>
		Google Chrome<br/>
		Gephi 0.8.2 beta<br/>
		ArcGIS 10.2<br/>
	</div>
</div>
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">3、WIN7_x64_2（Windows7 64位）</p>
	</div>
	<div class="text">
		Office 2010<br/>
		IE浏览器 11.0<br/>
		FireFox浏览器 57.0.2<br/>
		Google Chrome 56.0.2924<br/>
		Adobe Photoshop CS6<br/>
		Adobe Dreamweaver CS3<br/>
		Adobe FireWorks CS3<br/>
		Adobe Flash CS3<br/>
		Adobe Audition 3.0<br/>
		Autodesk AutoCAD 2007<br/>
		Chivox Pepper Service<br/>
		Cisco Packet Tracer 5.3<br/>
		Compaq Visual Fortran 6.0<br/>
		Java Development Kit 5.5<br/>
		Microsoft SQl Server 2008R2<br/>
		Microsoft Visual Basic 6.0<br/>
		Microsoft Visual C++ 6.0<br/>
		Microsoft Visual Foxpro 6.0<br/>
		Microsoft Visual Studio 2010<br/>
		LabView 2012<br/>
		PTV Vissim 8.0<br/>
		Vmware Workstation 12.0<br/>
		Weka 3.8.0<br/>
		万维网全自动网络考试系统<br/>
		TC 2.0 for dos 2.0<br/>
		Eclips 4.4.1<br/>
		MPLAB IDE 8.30<br/>
		PGP desktop 10.03<br/>
	</div>
</div>
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">4、WIN10（Windows10 64位）
</p>
	</div>
	<div class="text">
		Office 2013<br/>
		IE浏览器 11.0<br/>
		360极速浏览器<br/>
		Google Chrome 60.0.3112.90<br/>
		微信电脑版<br/>
		QQ<br/>
	</div>
</div>
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">5、软件实训（Windows7 64位）</p>
	</div>
	<div class="text">
		Office 2013<br/>
		Visio 2010<br/>
		IE浏览器 11.0<br/>
		FireFox浏览器<br/>
		Apache Tomcat 6.0<br/>
		Daum Potplayer 1.6.56209<br/>
		Java Development Kit 5.5<br/>
		MyEclipse 8.5<br/>
		My SQL Server 5.0<br/>
		Notepad++<br/>
		Navicat for MySQL 8.0<br/>
		QT 5.5.0<br/>
		Tortoise SVN<br/>
		Microsoft Visual Studio 2015<br/>
		Windows Kits<br/>
	</div>
</div>
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">6、Linux</p>
	</div>
	<div class="text">
		Ubuntu 16.04.4<br/>
	</div>
</div>
</div>

<div id="d301div" class="kc_list" style="display:none;">
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">1、图形设计（Windows7 64位）</p>
	</div>
	<div class="text">
		IE浏览器11.0<br/>
		360 极速浏览器<br/>
		Google Chrome 60.0.3112<br/>
		Adobe Photoshop CS3<br/>
		IBM SPSS Statistics 19.0<br/>
		ANSYS R16.0<br/>
		Autodesk AutoCAD 2014<br/>
		Autodesk 3ds MAX 2012<br/>
		Autodesk Inventor professsional 2014<br/>
		Autodesk Vault Basic 2014<br/>
		Google SketchUP 8.0<br/>
		MATLAB 7.0<br/>
		PKPM<br/>
		Autodesk REVIT 2016<br/>
		SolidWorks 2012<br/>
		Altium Designer 15.0<br/>
		Pro/Engineer 5.0<br/>
		UGS NX 4.0<br/>
		CATIA P3V5-6R2014<br/>
		Protel 99SE<br/>
		Compaq Visual Fortran 6.0<br/>
		ENVI 4.4<br/>
		Micromedia Authorware 7.0<br/>
		Office 2013<br/>
		Microsoft Visual Basic 6.0<br/>
		Microsoft Visual C++ 6.0<br/>
		Microsoft Visual Foxpro 6.0
	</div>
</div>
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">2、WIN7_x64_1（Windows7 64位）</p>
	</div>
	<div class="text">
		Office 2010<br/>
		Microsoft SQL Server 2008<br/>
		Microsoft Visual Studio 2010<br/>
		HFSS 15.0<br/>
		LabView 2012<br/>
		NI MAX<br/>
		Xilinx ISE 6.0<br/>
		用友U8 10.1<br/>
		ANSYS 18.2<br/>
		IE浏览器 11.0<br/>
		360浏览器<br/>
		Google Chrome<br/>
		Gephi 0.8.2 beta<br/>
		ArcGIS 10.2<br/>
	</div>
</div>
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">3、WIN7_x64_2（Windows7 64位）</p>
	</div>
	<div class="text">
		Office 2010<br/>
		IE浏览器 11.0<br/>
		FireFox浏览器 57.0.2<br/>
		Google Chrome 56.0.2924<br/>
		Adobe Photoshop CS6<br/>
		Adobe Dreamweaver CS3<br/>
		Adobe FireWorks CS3<br/>
		Adobe Flash CS3<br/>
		Adobe Audition 3.0<br/>
		Autodesk AutoCAD 2007<br/>
		Chivox Pepper Service<br/>
		Cisco Packet Tracer 5.3<br/>
		Compaq Visual Fortran 6.0<br/>
		Java Development Kit 5.5<br/>
		Microsoft SQl Server 2008R2<br/>
		Microsoft Visual Basic 6.0<br/>
		Microsoft Visual C++ 6.0<br/>
		Microsoft Visual Foxpro 6.0<br/>
		Microsoft Visual Studio 2010<br/>
		LabView 2012<br/>
		PTV Vissim 8.0<br/>
		Vmware Workstation 12.0<br/>
		Weka 3.8.0<br/>
		万维⽹全⾃动⽹络考试系统<br/>
		TC 2.0 for dos 2.0<br/>
		Eclips 4.4.1<br/>
		MPLAB IDE 8.30<br/>
		PGP desktop 10.03<br/>
		Fluent 6.3.26<br/>
		Gambit 2.4.6
	</div>
</div>
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">4、WIN10（Windows10 64位）
</p>
	</div>
	<div class="text">
		Office 2013<br/>
		IE浏览器 11.0<br/>
		360极速浏览器<br/>
		Google Chrome 60.0.3112.90<br/>
		微信电脑版<br/>
		QQ<br/>
	</div>
</div>
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">5、软件实训（Windows7 64位）</p>
	</div>
	<div class="text">
		Office 2013<br/>
		Visio 2010<br/>
		IE浏览器 11.0<br/>
		FireFox浏览器<br/>
		Apache Tomcat 6.0<br/>
		Daum Potplayer 1.6.56209<br/>
		Java Development Kit 5.5<br/>
		MyEclipse 8.5<br/>
		My SQL Server 5.0<br/>
		Notepad++<br/>
		Navicat for MySQL 8.0<br/>
		QT 5.5.0<br/>
		Tortoise SVN<br/>
		Microsoft Visual Studio 2015<br/>
		Windows Kits<br/>
	</div>
</div>
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">6、Linux</p>
	</div>
	<div class="text">
		Ubuntu 16.04.4<br/>
	</div>
</div>
</div>

	</div>
<div class="button_02_outside button_color">
	<input type="button" onclick="sysMain();" value="返回" class="button_02">
</div>
