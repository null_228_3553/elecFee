<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script src="../js/layer.js"></script>
<script type="text/javascript">
(function($){
	var uploadDiv='<div id="progressNumber"></div>';
	var msgLayer;
	var obj;
	$.fn.upload = function(url,param,callback) {
		if(callback==undefined){
			callback=param;
		}
		obj=this;
		var file=obj[0].files[0];
		if (file) {
			msgLayer=layer.msg(uploadDiv, {
				time: 0, //永远存在
			});
			var fd = new FormData();
			fd.append("file", file);
			//判断是否传入param
			if(param.constructor==Object){
				for(i in param){
					fd.append(i,param[i])
				}
			}
			var xhr = new XMLHttpRequest();
			xhr.upload.addEventListener("progress", uploadProgress, false);
			xhr.addEventListener("load", uploadComplete.bind(event,callback), false);
			xhr.addEventListener("error", uploadFailed, false);
//			xhr.addEventListener("abort", uploadCanceled, false);
			xhr.open("POST", url);
			xhr.send(fd);
		}
	}
	function uploadProgress(evt) {
		if (evt.lengthComputable) {
			var percentComplete = Math.round(evt.loaded * 100 / evt.total);
			document.getElementById('progressNumber').innerHTML = percentComplete.toString() + '%';
		}
	}
	//关闭layer，清空input=file
	function uploadComplete(callback,evt) {
		if(msgLayer){ layer.close(msgLayer); }
		if(obj){ obj.val(""); }
		var data=JSON.parse(evt.target.responseText);
		if(data.retcode=="0000"){
			callback(data.body);
		}else{
			layer.alert(data.errMsg);
		}
	}
	function uploadFailed(evt) {
		if(msgLayer){ layer.close(msgLayer); }
		layer.alert("上传文件发生了错误,请重新尝试");
	}
}(jQuery));
$(document).ready(function(){
	$("#addNew").click(function(){
		$("#uploadFile").click();
	});
	$("#uploadFile").change(function(e){
        //formData上传文件
        var file = e.target.files[0];
        if (!file.name.endsWith('docx')) {
            alert('只能上传docx文件');
            return;
        }
        $("#uploadFile").upload("jxapAdd.do", function(data) {
    		$("#pwdSpan").html("上传成功");
    		$("#pwdDiv").dialog("open");
        	setTimeout(function(){ 
        		jxap();
           	}, 1000);
        });
	});
});
function jxapDelete(jxapId){
	var a=confirm("确认要删除吗？");
	if(a){
		post2SRV("jxapDelete.do",{jxapId:jxapId},function(data){
			$("#pwdSpan").html("删除成功");
			$("#pwdDiv").dialog("open");
	    	setTimeout(function(){
	    		jxap();
	       	}, 1000);
		},"json");
	}
}
</script>
<div class="div_title key_color">上机实验课表</div>
<div class="barss" >
<div style="font-size:14px;">以下信息仅供参考，具体请与任课老师联系</div>
<hr/>
<c:forEach items="${list2}" var="xx">
	<p class="dfcx_bar_03" onclick="jxapDetail2('${xx.JXAP_ID}');">
	<span class="black" >
		${xx.FILE_NAME}&nbsp;
	</span>
	<br/>
		发布时间：${xx.CREATE_TIME}
	</p>
<c:if test="${sessionScope.userId=='2007210041'}">
	<a style="float: right;padding-right: 10px;" href="javascript:;"
		onclick="jxapDelete('${xx.JXAP_ID}')">删除</a><br/>
</c:if>	
	<hr style="width:70%;margin-left:0px;">
</c:forEach>
</div>
<c:if test="${sessionScope.userId=='2007210041'}">
<div class="button_02_outside button_color" >
	<input type="file" name="file" id="uploadFile" style="display:none" />
	<input type="button" id="addNew" value="新增" class="button_02" style="display:inline-block;">
</div>
</c:if>
<div class="button_02_outside button_color" >
	<input type="button" onclick="sysMain();" value="返回" class="button_02">
</div>

