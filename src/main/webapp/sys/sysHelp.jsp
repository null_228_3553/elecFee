<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="" />
	<meta name="decription" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
	<title></title>		
<link href="/elecFee/css/icon.css" media="screen" rel="stylesheet" type="text/css" />
<link href="/elecFee/css/style1.css" media="screen" rel="stylesheet" type="text/css" />
<link href="/elecFee/css/color_02.css" media="screen" rel="stylesheet" type="text/css" />
<link href="/elecFee/css/sys.css" media="screen" rel="stylesheet" type="text/css" />
</head>
<body>
<jsp:include page="sysHeader.jsp" flush="true">
	<jsp:param name="title" value="帮助" /> 
</jsp:include>
<div class="main_02">
	<p class="help_title_01">机房信息查询-帮助</p>
	<p class="help_title_02">介绍：</p>
	<p class="helep_content_01" style="text-indent:2em">“机房信息查询”应用是帮助用户熟悉上机流程，及时知晓上机账号和密码，查询费用余额，了解各个机房的计算机硬软件配置、实验教学安排和机位空闲率。</p>
	<p class="help_title_02">建议与反馈：</p>
	<p class="helep_content_01" style="text-indent:2em">该应用如有不足之处，或有好的意见和建议，请通过电子邮件liliuqun@hust.edu.cn或电话15327312456反馈，谢谢！</p>

	<div class="button_02_outside button_color">
		<input type="button" onclick="window.history.go(-1)" value="返回" class="button_02">
	</div>
</div>


<jsp:include page="../footer.jsp">
	<jsp:param name="source" value="网络与计算中心" /> 
</jsp:include>
</body>
</html>