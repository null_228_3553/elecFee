<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript">
$(document).ready(function(){
	var xtData;
	post2SRV("xtms.do",function(data){
		xtData=data;
		$("#z208").click();
	},"json");
	$("#zxq").click(function(){
		if($(this).attr("class")==undefined)
			$("#z401").click();
		$(this).attr("class","kctab2_select");
		$("#dxq").removeAttr("class");
		$("#zTitle").show();
		$("#dTitle").hide();
	});
	$("#dxq").click(function(){
		if($(this).attr("class")==undefined)
			$("#d201").click();
		$(this).attr("class","kctab2_select");
		$("#zxq").removeAttr("class");
		$("#dTitle").show();
		$("#zTitle").hide();
	});
	var selected;
	var userId="${sessionScope.userId}";
	$(".kc_tab a").click(function(){
		//标签样式
		$(".kc_tab a").removeAttr("class");
		$(this).attr("class","kctab_select");
		//获取id
		var id=$(this).attr("id");
		selected=id;
		if(id=="z401"|| id=="z403"){
			var str="<div class='xxcx_row'>"
				+"<p style='line-height:30px;color:red;'>主校区开放实验室本学期因安全原因停用整改，请同学们到东校区实训中心二楼机房上机！</p>"
				+"</div>";
			$("#kcdiv").html(str);
		}else{
			var c=xtData[id];
			var str="";
			for(var i=0;i<c.length;i++){
				if(userId=="2007210041"){
					str+="<div class='xxcx_row'>"
							+"<div class='kc_week_con'>"
								+"<p class='kc_year_mini key_color'>"+c[i].TITLE+"</p>"
							+"</div>"
							+"<div class='text'>"
							// \n回车符换成<br/>
							+c[i].CONTENT.replace(/\n/g,'<br/>')
								+"<div style='color:red'>"
									+"<a class='editPre' data_id='"+c[i].XTMS_ID+"' data_title='"+c[i].TITLE+"' data_content='"+c[i].CONTENT+"'>编辑</a>"
									+"<a style='padding-left:15px;' class='delete' data_id='"+c[i].XTMS_ID+"'>删除</a>"
								+"</div>"
							+"</div>"
						+"</div>";
				}else{
					str+="<div class='xxcx_row'>"
						+"<div class='kc_week_con'>"
							+"<p class='kc_year_mini key_color'>"+c[i].TITLE+"</p>"
						+"</div>"
						+"<div class='text'>"
						// \n回车符换成<br/>
						+c[i].CONTENT.replace(/\n/g,'<br/>')
						+"</div>"
					+"</div>";
				}
			}
			$("#kcdiv").html(str);
			//添加事件
			$(".text").hide();
			$(".kc_week_con").click(function(){
				$(".text").slideUp();
				if($(this).next().is(":hidden"))
					$(this).next().slideDown();
			});
			$(".editPre").click(function(){
				//div
				$("#kcdiv").hide();
				$("#adddiv").show();
				//塞值
				var xtmsId=$(this).attr("data_id");
				var title=$(this).attr("data_title");
				var content=$(this).attr("data_content");
				$("#xtmsId").val(xtmsId);
				$("#title").val(title);
				$("#content").val(content);
				//按钮
				$("#addPre").hide();
				$("#add").hide();
				$("#update").show();
			});
			$(".delete").click(function(){
				if(confirm("确认删除么？")){
					var xtmsId=$(this).attr("data_id");
					post2SRV("xtmsDelete.do",{xtmsId:xtmsId},function(data){
						$("#pwdSpan").html("删除成功");
						$("#pwdDiv").dialog("open");
				    	setTimeout(function(){
				    		xtms();
				       	}, 1000);
					},"json");
				}
			});
		}
		//按钮
		$("#addPre").show();
		$("#add").hide();
		$("#update").hide();
		//div
		$("#kcdiv").show();
		$("#adddiv").hide();
	});
	$("#addPre").click(function(){
		//按钮
		$("#addPre").hide();
		$("#add").show();
		$("#update").hide();
		//div
		$("#kcdiv").hide();
		$("#adddiv").show();
	});
	$("#add").click(function(){
		if(selected!="z401" && selected!="z403"){
			var param={			
				room:selected,
				title:$("#title").val(),
				content:$("#content").val(),
			};
			post2SRV("xtmsAdd.do",param,function(data){
				$("#pwdSpan").html("添加成功");
				$("#pwdDiv").dialog("open");
		    	setTimeout(function(){
		    		xtms();
		       	}, 1000);
			},"json");
		}
	});
	$("#update").click(function(){
		var xtmsId=$("#xtmsId").val();
		var title=$("#title").val();
		var content=$("#content").val();
		post2SRV("xtmsUpdate.do",{xtmsId:xtmsId,title:title,content:content},function(data){
			$("#pwdSpan").html("更新成功");
			$("#pwdDiv").dialog("open");
	    	setTimeout(function(){
	    		xtms();
	       	}, 1000);
		},"json");
	});
});
</script>
<div class="div_title key_color" >系统描述</div>
<div class="main_03">
	<div style="margin:0 10px" class="systab kc_tab2">
		<a href="javascript:;" id="zxq" class="kctab2_select">主校区</a>
		<a href="javascript:;" id="dxq" >东校区</a>
	</div>
	<div id="zTitle" style="margin:0 10px 15px;" class="systab kc_tab" >
		<a href="javascript:;" id="z208" class="kctab_select">208机房</a>
		<a href="javascript:;" id="z401">401机房</a>
		<a href="javascript:;" id="z403">403机房</a>
	</div>
	<div id="dTitle" style="margin:0 10px 15px;display:none" class="systab kc_tab">
		<a href="javascript:;" id="d201" class="kctab_select" style="width:25%">201机房</a>
		<a href="javascript:;" id="d202" style="width:25%">202机房</a>
		<a href="javascript:;" id="d206" style="width:25%">206机房</a>
		<a href="javascript:;" id="d301" style="width:25%">301机房</a>
	</div>
	
	<div id="kcdiv" class="kc_list" style="display:block">
		
	</div>
	<div id="adddiv" class="kc_list" style="display:none;font-size:16px;">
		<input type="hidden" id="xtmsId" />
		<div style="height:50px">
			标题：<input type="text" id="title" style="width:100%;height:22px;"/>
		</div>
		<div>
			内容：<textarea rows="10" style="width:100%" id="content"></textarea>
		</div>
	</div>

</div>
<c:if test="${sessionScope.userId=='2007210041'}">
<div class="button_02_outside button_color" >
	<input type="button" id="addPre" value="新增" class="button_02">
	<input type="button" id="add" value="确定" class="button_02">
	<input type="button" id="update" value="确定" class="button_02">
</div>
</c:if>
<div class="button_02_outside button_color">
	<input type="button" onclick="sysMain();" value="返回" class="button_02">
</div>
