<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<style>
input{
	height: 25px;
    width: 200px;
    font-size: 16px;
}
</style>
<script type="text/javascript">
$(document).ready(function(){
	var yjData;
	post2SRV("yjms.do",function(data){
		yjData=data;
		$("#z208").click();
	},"json");
	$("#zxq").click(function(){
		if($(this).attr("class")==undefined)
			$("#z208").click();
		$(this).attr("class","kctab2_select");
		$("#dxq").removeAttr("class");
		$("#zTitle").show();
		$("#dTitle").hide();
	});
	$("#dxq").click(function(){
		if($(this).attr("class")==undefined)
			$("#d201").click();
		$(this).attr("class","kctab2_select");
		$("#zxq").removeAttr("class");
		$("#dTitle").show();
		$("#zTitle").hide();
	});
	var selected;
	$(".kc_tab a").click(function(){
		$(".kc_tab a").removeAttr("class");
		$(this).attr("class","kctab_select");
		var id=$(this).attr("id");
		selected=id;
		if(id=="z401"|| id=="z403"){
			var str="<div class='xxcx_row'>"
				+"<p style='line-height:30px;color:red;'>主校区开放实验室本学期因安全原因停用整改，请同学们到东校区实训中心二楼机房上机！</p>"
				+"</div>";
			$("#kcdiv").html(str);
		}else{
			var c=yjData[id];
			var str="<div class='xxcx_row'>"
						+"<div class='kc_week_con'>"
						+"<p class='kc_year_mini key_color'>"+c.COMPUTER+"</p>"
						+"<p class='kc_week_mini key_color'>"+c.USE_TIME+"</p>"
					+"</div>"
				+"</div>"
				+"<div class='xxcx_row'>"
					+"<table style='width:100%;'>"
						+"<tr style='width:100%;'>"
							+"<td class='xxcx_left_box'>CPU：</td>"
							+"<td class='xxcx_right_box'>"+c.CPU+"</td>"
						+"<tr>"
					+"</table>"
				+"</div>"
				+"<div class='xxcx_row'>"
					+"<table style='width:100%;'>"
						+"<tr style='width:100%;'>"
							+"<td class='xxcx_left_box'>内存：</td>"
							+"<td class='xxcx_right_box'>"+c.MEMORY+"</td>"
						+"<tr>"
					+"</table>"
				+"</div>"
				+"<div class='xxcx_row'>"
					+"<table style='width:100%;'>"
						+"<tr style='width:100%;'>"
							+"<td class='xxcx_left_box'>芯片组：</td>"
							+"<td class='xxcx_right_box'>"+c.CHIP+"</td>"
						+"<tr>"
					+"</table>"
				+"</div>"
				+"<div class='xxcx_row'>"
					+"<table style='width:100%;'>"
						+"<tr style='width:100%;'>"
							+"<td class='xxcx_left_box'>硬盘：</td>"
							+"<td class='xxcx_right_box'>"+c.DISK+"</td>"
						+"<tr>"
					+"</table>"
				+"</div>"
				+"<div class='xxcx_row'>"
					+"<table style='width:100%;'>"
						+"<tr style='width:100%;'>"
							+"<td class='xxcx_left_box'>显示器：</td>"
							+"<td class='xxcx_right_box'>"+c.MONITOR+"</td>"
						+"<tr>"
					+"</table>"
				+"</div>";
			$("#kcdiv").html(str);
		}
		$("#updatePre").show();
		$("#doIt").hide();
	});
	$("#updatePre").click(function(){
		if(selected!="z401" && selected!="z403"){
			var c=yjData[selected];
			var str="<div class='xxcx_row'>"
						+"<div class='kc_week_con'>"
						+"<p class='kc_year_mini key_color'><input id='computer' value='"+c.COMPUTER+"'></p>"
						+"<p class='kc_week_mini key_color'><input id='useTime' value='"+c.USE_TIME+"'></p>"
					+"</div>"
				+"</div>"
				+"<div class='xxcx_row'>"
					+"<table style='width:100%;'>"
						+"<tr style='width:100%;'>"
							+"<td class='xxcx_left_box'>CPU：</td>"
							+"<td class='xxcx_right_box'><input id='cpu' value='"+c.CPU+"'></td>"
						+"<tr>"
					+"</table>"
				+"</div>"
				+"<div class='xxcx_row'>"
					+"<table style='width:100%;'>"
						+"<tr style='width:100%;'>"
							+"<td class='xxcx_left_box'>内存：</td>"
							+"<td class='xxcx_right_box'><input id='memory' value='"+c.MEMORY+"'></td>"
						+"<tr>"
					+"</table>"
				+"</div>"
				+"<div class='xxcx_row'>"
					+"<table style='width:100%;'>"
						+"<tr style='width:100%;'>"
							+"<td class='xxcx_left_box'>芯片组：</td>"
							+"<td class='xxcx_right_box'><input id='chip' value='"+c.CHIP+"'></td>"
						+"<tr>"
					+"</table>"
				+"</div>"
				+"<div class='xxcx_row'>"
					+"<table style='width:100%;'>"
						+"<tr style='width:100%;'>"
							+"<td class='xxcx_left_box'>硬盘：</td>"
							+"<td class='xxcx_right_box'><input id='disk' value='"+c.DISK+"'></td>"
						+"<tr>"
					+"</table>"
				+"</div>"
				+"<div class='xxcx_row'>"
					+"<table style='width:100%;'>"
						+"<tr style='width:100%;'>"
							+"<td class='xxcx_left_box'>显示器：</td>"
							+"<td class='xxcx_right_box'><input id='monitor' value='"+c.MONITOR+"'></td>"
						+"<tr>"
					+"</table>"
				+"</div>";
			$("#kcdiv").html(str);
		}
		$("#updatePre").hide();
		$("#doIt").show();
	});
	$("#doIt").click(function(){
		var param={
			room:selected,
			computer:$("#computer").val(),
			useTime:$("#useTime").val(),
			cpu:$("#cpu").val(),
			memory:$("#memory").val(),
			chip:$("#chip").val(),
			disk:$("#disk").val(),
			monitor:$("#monitor").val(),
		};
		post2SRV("yjmsUpdate.do",param,function(data){
			$("#pwdSpan").html("修改成功");
			$("#pwdDiv").dialog("open");
	    	setTimeout(function(){
	    		yjms();
	       	}, 1000);
		},"json");
	});
});
</script>
<div class="div_title key_color" id="yjms">硬件描述</div>
<div class="main_03">
	<div style="margin:0 10px" class="systab kc_tab2">
		<a href="javascript:;" id="zxq" class="kctab2_select">主校区</a>
		<a href="javascript:;" id="dxq" >东校区</a>
	</div>
	<div id="zTitle" style="margin:0 10px 15px;" class="systab kc_tab" >
		<a href="javascript:;" id="z208" class="kctab_select">208机房</a>
		<a href="javascript:;" id="z401" >401机房</a>
		<a href="javascript:;" id="z403" >403机房</a>
	</div>
	<div id="dTitle" style="margin:0 10px 15px;display:none" class="systab kc_tab">
		<a href="javascript:;" id="d201" class="kctab_select" style="width:25%">201机房</a>
		<a href="javascript:;" id="d202" style="width:25%">202机房</a>
		<a href="javascript:;" id="d206" style="width:25%">206机房</a>
		<a href="javascript:;" id="d301" style="width:25%">301机房</a>
	</div>
			
	<div id="kcdiv" class="kc_list" style="display:block;">
	
	</div>
</div>
<c:if test="${sessionScope.userId=='2007210041'}">
<div class="button_02_outside button_color" >
	<input type="button" id="updatePre" value="编辑" class="button_02">
	<input type="button" id=doIt value="确定" class="button_02">
</div>
</c:if>
<div class="button_02_outside button_color">
	<input type="button" onclick="sysMain();" value="返回" class="button_02">
</div>
