<%@ page language="java" contentType="text/html; charset=utf-8"%>
<script type="text/javascript">
$(document).ready(function(){
// 	var href=$("#jxapHref").val();
	var href=$("#jxap_id").val();
	post2SRV("jxapDetail.do",{href:href},function(data){
		$(".kc_week_box").html(data.title);
		var list=data.list;
		$(".kc_tab3 a").click(function(){
			$(this).siblings().removeAttr("class");
			$(this).attr("class","tab_01_select");
			var dayId=$(this).attr("id");
			changeDay(dayId,list);
		}); 
		$(".kc_tab3 a:first").click();
	},"json");
});
function changeDay(dayId,list){
	var day=dayMap[dayId];
	var str=courseStr(day, "1To4", list);
	str+=courseStr(day, "5To8", list);
	str+=courseStr(day, "9To12", list);
	$(".kc_list").html(str);
}
function courseStr(day,courseTime,list){
	var str='<div class="xxcx_row">'
				+'<p class="key_color" style="text-align:center">'
				+courseTimeMap[courseTime]
				+'</p>'
			+'</div>';
	for(var i=0;i<list.length;i++){
		if(list[i].DAY==day && list[i].COURSE_TIME==courseTime){
			if(list[i].COURSE_NAME==undefined){
				list[i].COURSE_NAME="";
			}
			if(list[i].CLASS_NAME==undefined){
				list[i].CLASS_NAME="";
			}
			if(list[i].ROOM_NAME==undefined){
				list[i].ROOM_NAME="";
			}
			if(list[i].REMARK==undefined){
				list[i].REMARK="";
			}
			str+='<div class="xxcx_row">'
				+'<table style="width: 100%;">'
				+'<tr>'
				+'<td class="left_box">课程名称：</td>'
				+'<td class="right_box">'+list[i].COURSE_NAME+'</td>'
				+'</tr>'
				+'<tr>'
				+'<td class="left_box">院系班级：</td>'
				+'<td class="right_box">'+list[i].CLASS_NAME+'</td>'
				+'</tr>'
				+'<tr>'
				+'<td class="left_box">上机教室：</td>'
				+'<td class="right_box">'+list[i].ROOM_NAME+'</td>'
				+'</tr>'
				+'<tr>'
				+'<td class="left_box">备注：</td>'
				+'<td class="right_box">'+list[i].REMARK+'</td>'
				+'</tr>'
				+'</table>'
				+'</div>';
		}
	}
	return str;
}
var dayMap={
	"day1":"星期一",
	"day2":"星期二",
	"day3":"星期三",
	"day4":"星期四",
	"day5":"星期五",
	"day6":"星期六",
	"day7":"星期日"
};
var courseTimeMap={
	"1To4":"1-----4节",
	"5To8":"5-----8节",
	"9To12":"9-----12节"
};
</script>
<div class="div_title key_color" >上机实验课表</div>
<div class="main_03">
	<div class="kc_week_box" style="font-size:16px;color:#666;text-align:center"></div>
	<div style="margin: 0 10px 15px;" class="tab_01 kc_tab3">
		<a id="day1" href="javascript:;" style="border-radius:3px 0 0 3px;">一</a> 
		<a id="day2" href="javascript:;" >二</a> 
		<a id="day3" href="javascript:;" >三</a>
		<a id="day4" href="javascript:;" >四</a>
		<a id="day5" href="javascript:;" >五</a> 
		<a id="day6" href="javascript:;" >六</a> 
		<a id="day7" href="javascript:;" style="border-radius:0 3px 3px 0;">日</a>
	</div>

	<div class="kc_list" style="display:block">
	</div>
</div>
<div class="button_02_outside button_color">
	<input type="button" onclick="jxap();" value="返回" class="button_02">
</div>
