<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="" />
	<meta name="decription" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
	<title></title>		
<link href="/elecFee/css/icon.css" media="screen" rel="stylesheet" type="text/css" />
<link href="/elecFee/css/style1.css" media="screen" rel="stylesheet" type="text/css" />
<link href="/elecFee/css/color_02.css" media="screen" rel="stylesheet" type="text/css" />
<link href="/elecFee/css/sys.css" media="screen" rel="stylesheet" type="text/css" />
<link href="/elecFee/css/kcdz.css" media="screen" rel="stylesheet" type="text/css" />
<link href="/elecFee/css/jquery-ui.css" media="screen" rel="stylesheet" >
<link href="/elecFee/css/verification.css" media="screen" rel="stylesheet" >
<script src="/elecFee/js/jquery.js" type="text/javascript"></script>
<script src="/elecFee/js/jquery-ui.js"></script>
<script src="/elecFee/js/sync.js"></script>
<script src="/elecFee/js/resetAndroidFontsize.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#pwdDiv").dialog({
		autoOpen : false,
		resizable : false,
		height : "auto",
		width : "90%",
		modal : true,
		show : {
			effect : "blind",
			duration : 300
		},
	});
	$("#layer1").dialog({
		autoOpen : false,
		resizable : false,
		modal : true,
		show : {
			effect : "blind",
			duration : 500
		},
	});
	$("#pwdClose").click(function() {
		$("#pwdSpan").html("");
		$("#pwdDiv").dialog("close");
	});
	$("#qryJxPwd").click(function(){
		post2SRV("qryJxPwd.do",function(data){
			if(data.success){
				$("#pwdSpan").html("您的教学账号的密码为：<br/>"+data.passwd);
			}else{
				$("#pwdSpan").html("系统中没有您的教学账号的密码<br/>请联系管理员");
			}
			$("#pwdDiv").dialog("open");
		},"json");
	});
	$("#qryZfPwd").click(function(){
		$("#layer1").dialog("open");
	});
	$("#confirm").click(function(){
		if($("#passwd").val()==""){
			$("#errSpan").html("密码不能为空");
			return;
		}
		$("#errSpan").html("");
		post2SRV("qryZfPwd.do",{passwd:$("#passwd").val()},function(data){
			if(data.success){
				$("#layer1").dialog("close");
				$("#pwdSpan").html("您的自费账号的密码为：<br/>"+data.passwd);
				$("#pwdDiv").dialog("open");
			}else{
				$("#errSpan").html(data.errMsg);
			}
			$("#passwd").val("");
		},"json");
	});
	$("#back").click(function(){
		$("#errSpan").html("");
		$("#layer1").dialog("close");
	});
	$("#qryFee").click(function(){
		if($("#warn2").html()!=undefined){
			$("#pwdSpan").html("您的自费账号不存在<br/>请到机房管理办公室开通");
			$("#pwdDiv").dialog("open");
		}else{
			post2SRV("qryZfFee.do",function(data){
				if(data.success){
					$("#pwdSpan").html("您的自费账号的余额为：<br/>"+data.leftFee+"元");
				}else{
					$("#pwdSpan").html(data.errMsg);
				}
				$("#pwdDiv").dialog("open");
			},"json");
		}
	});
	window.addEventListener("popstate", function() {
        if(history.state==null){
        	sysMain();
//         }else if(history.state=='jxap'){
//         	post2SRV("jxap.do",function(data){
//         		$("#list_div").html(data);
//         		hideHead();
//         	},"text");
        }else if(history.state!=null){
        	gotoUrl(""+history.state+".do");
        }
    });
});
function gotoUrl(url,callback){
	post2SRV(url,function(data){
		$("#list_div").html(data);
		if(callback) callback();
		hideHead();
	},"text");
}
function sjxz(){
	gotoUrl("sjxz.do",function(){
		history.pushState("sjxz", "上机须知", "#!/sjxz");
	});
}
function sblyl(){
	gotoUrl("sblyl.do",function(){
		history.pushState("sblyl", "机位空闲率", "#!/sblyl");
	});
}
function jxap(){
	gotoUrl("jxap.do",function(){
		history.pushState("jxap", "教学安排", "#!/jxap");
	});
}
function yjms(){
	gotoUrl("yjms.jsp",function(){
		history.pushState("yjms", "硬件描述", "#!/yjms");
	});
}
function xtms(){
	gotoUrl("xtms.jsp",function(){
		history.pushState("xtms", "系统描述", "#!/xtms");
	});
}
/* function jxapDetail(href){
	post2SRV("jxapDetail.jsp",function(data){
		$("#list_div").html("");
		$("#list_div").append('<input type="hidden" id="jxapHref" value="'+href+'">');
		$("#list_div").append(data);
		history.pushState("jxapDetail", "教学安排", "#!/jxapDetail");
	});
} */
function jxapDetail2(jxap_id){
	post2SRV("jxapDetail.jsp",function(data){
		$("#list_div").html("");
		$("#list_div").append('<input type="hidden" id="jxap_id" value="'+jxap_id+'">');
		$("#list_div").append(data);
		history.pushState("jxapDetail", "教学安排", "#!/jxapDetail");
	});
}
function sysMain(){
	showLoading();
// 	window.location.href='/elecFee/sysMain.do';
	window.location.href='sysMain.do';
}
</script>
</head>
<body>
<jsp:include page="sysHeader.jsp" flush="true">
	<jsp:param name="title" value="开放式计算机实验室" /> 
</jsp:include>
<div id="pwdDiv" title="信息提示">
  <p id="pwdSpan" style="padding-top:5px;line-height:30px;font-size:20px; text-align:center">
  	
  </p>
  <div  style="text-align:center; font-size:18px;">
  <input type="button" class="button_01 button_color" id="pwdClose" value="关闭" /> 
  </div>
</div>
<div id="overlay" class="overlay"></div>
<div id="showbox" class="showbox">
	<div style="width:180px;height:50px;line-height:50px;border:2px solid #D6E7F2;background:#fff;">
		<img style="margin:10px 15px;float:left;display:inline;"
			src="/elecFee/images/waiting.gif">加载中，请稍候...
	</div>
</div>
<div class="layui-layer layer-anim layui-layer-page " id="layer1" title="请输入统一身份认证密码"
	style="z-index: 19891015; width: 95%; height: 405px;">
	<div id="" class="layui-layer-content" style="height: 362px;">
		<div class="main">
			<div class="main_up"
				style="background-position: center center; 
				background-image: url(/elecFee/images/logo.png); height: 60px; margin: 10px 0;"></div>
			<div class="login_input_box" style="height: auto">
				<div class="login_input_box_1">
					<input type="text" style="color: #000;font-size:18px;" 
						readonly="readonly" value="${sessionScope.userId }"
						class="login_input" id="userid">
				</div>
				<div class="login_input_box_2">
					<input type="password" class="login_input" id="passwd">
				</div>
			</div>
			<div id="errSpan" style="color:red; font-size:16px; text-align:center;"></div>
			<div style="text-align:center;font-size:20px;">
				<input type="button" class="button_01" id="confirm" value="确定"/>
				<input type="button" class="button_01" id="back" value="返回" >
			</div>
			<div class="verification_notice">
				<span class="verification_tips">
					说明：您正要查看的信息为个人敏感信息，为保护您的信息安全，请输入您的统一身份认证系统的密码。 </span>
			</div>
			<div class="login_bottom_bar">
				<div class="lbb_inside"></div>
			</div>
		</div>
	</div>
	<span class="layui-layer-setwin"><a
		class="layui-layer-ico layui-layer-close layui-layer-close1"
		href="javascript:;"></a></span>
</div>
<div class="main_02">
	<div class="dfcx_box_02">
		<p class="dfcx_bar_03">
		姓　　名：<span class="black"><c:out value="${name}"/></span>
		</p>
		<p class="dfcx_bar_03">
		教学账号：
		<c:if test="${jxStu.stdno ne null}">
			<span class="black"><c:out value="${jxStu.stdno}"/></span>&nbsp;
			<a style="text-decoration:none;" id="qryJxPwd" href="javascript:;">查询密码</a>
		</c:if>
		<c:if test="${jxStu.stdno eq null}">
			<span class="black">您的教学账号不存在，请联系管理员</span>
		</c:if>
		</p>
		<p class="dfcx_bar_03">
		自费账号：
		<c:if test="${zfStu.stdno ne null}">
			<span class="black"><c:out value="${zfStu.stdno}"/></span>&nbsp;
			<a style="text-decoration:none;" id="qryZfPwd" href="javascript:;">查询密码</a>
		</c:if>
		<c:if test="${zfStu.stdno eq null}">
			<span class="black" id="warn2">您的自费账号不存在，请到机房管理办公室开通</span>
		</c:if>
		</p>
	</div>
	<div class="list_div" id="list_div">
	<ul class="demo_list">
        <li id="sjxz" onclick="sjxz();">
            <a >
				<span class="pp_a015 key_color icon_40"></span>
	            <strong class="">上机须知</strong>
            </a>
        </li>
        <li id="qryFee">
            <a >
				<span class="pp_b009 key_color icon_40"></span>
	            <strong class="">费用查询</strong>
            </a>
        </li>
        <li id="sblyl" onclick="sblyl();">
            <a >
				<span class="pp_a009 key_color icon_40"></span>
	            <strong class="">机位空闲率</strong>
            </a>
        </li>
		<li id="jxap" onclick="jxap();">
			<a >
				<span class="pp_a013 key_color icon_40"></span>
	            <strong class="">实验教学安排</strong>
	        </a>
        </li>
		<li id="yjms" onclick="yjms();">
	        <a >
				<span class="pp_a014 key_color icon_40"></span>
	            <strong class="">硬件描述</strong>
	        </a>
        </li>
		<li id="xtms" onclick="xtms();">
	        <a >
				<span class="pp_a017 key_color icon_40"></span>
	            <strong class="">系统描述</strong>
	        </a>
        </li>
    </ul>	
	</div>
</div>
<jsp:include page="../footer.jsp">
	<jsp:param name="source" value="网络与计算中心" /> 
</jsp:include>
</body>
</html>