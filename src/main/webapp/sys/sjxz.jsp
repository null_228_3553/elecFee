<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript">
	$(document).ready(function() {
	});
</script>
<div class="div_title key_color">上机须知</div>
<p>
	<br />
</p>
<div class="main_03" style="padding: 0 5%; width:90%">
	<blockquote id="m139" class="yead_editor yead-selected" data-author="易点微信编辑器" data-url="http://wxedit.yead.net" style="font-family: Simsun; white-space: normal; widows: 1; border: 0px; padding: 0px; margin: 5px auto;">
	    <section class="yead_bdc" style="margin: 0px; padding: 0px; border: 0px rgb(0, 187, 236); font-family: inherit; font-size: 1em; line-height: 25px; box-sizing: border-box; max-width: 100%; text-align: center; text-decoration: inherit; color: rgb(255, 255, 255); word-wrap: break-word !important;">
	        <section class="yead_bgc" style="margin: 0px auto; padding: 0px; border: 0px; box-sizing: border-box; max-width: 100%; width: 2em; height: 2em; border-radius: 100%; word-wrap: break-word !important; background-color: rgb(0, 187, 236);">
	            <section style="margin: 0px; padding: 0px; border: 0px; display: inline-block; box-sizing: border-box; max-width: 100%; font-size: 1em; line-height: 2; font-family: inherit; word-wrap: break-word !important;">
	                <section style="margin: 0px; padding: 0px; border: 0px; box-sizing: border-box; max-width: 100%; word-wrap: break-word !important;">
	                    <span style="font-family: 微软雅黑, &#39;Microsoft YaHei&#39;;">1</span>
	                </section>
	            </section>
	        </section>
	        <section style="margin: -1em 0px 1em; padding: 0px; border: 0px; box-sizing: border-box; max-width: 100%; word-wrap: break-word !important;">
	            <section class="yead_bdc" style="margin: 0px; padding: 0px; border-width: 1px 0px 0px; border-top-style: solid; border-color: rgb(0, 187, 236); box-sizing: border-box; max-width: 100%; width: 128.8px; float: left; word-wrap: break-word !important;"></section>
	            <section class="yead_bdc" style="margin: 0px; padding: 0px; border-width: 1px 0px 0px; border-top-style: solid; border-color: rgb(0, 187, 236); box-sizing: border-box; max-width: 100%; width: 128.8px; float: right; word-wrap: break-word !important;"></section>
	        </section>
	    </section>
	</blockquote>
	<p style="margin-top: 0px; margin-bottom: 0px; font-family: Simsun; font-size: 12px; white-space: normal; widows: 1; text-indent: 32px;">
	    <span style="font-size: 16px; font-family: 微软雅黑, &#39;Microsoft YaHei&#39;;">学生上机采用帐号管理模式，帐号与学号绑定，在学号有效期内使用。</span>
	</p>
	<p style="margin-top: 0px; margin-bottom: 0px; font-family: Simsun; font-size: 12px; white-space: normal; widows: 1; text-indent: 32px;">
	    <span style="font-size: 18px; font-family: 微软雅黑, &#39;Microsoft YaHei&#39;;"></span>
	</p>
	<p style="margin-top: 0px; margin-bottom: 0px; font-family: Simsun; font-size: 12px; white-space: normal; widows: 1;">
	    <br/>
	</p>
	<blockquote id="m139" class="yead_editor" data-author="易点微信编辑器" data-url="http://wxedit.yead.net" style="font-family: Simsun; white-space: normal; widows: 1; border: 0px; padding: 0px; margin: 5px auto;">
	    <section class="yead_bdc" style="margin: 0px; padding: 0px; border: 0px rgb(0, 187, 236); font-family: inherit; font-size: 1em; line-height: 25px; box-sizing: border-box; max-width: 100%; text-align: center; text-decoration: inherit; color: rgb(255, 255, 255); word-wrap: break-word !important;">
	        <section class="yead_bgc" style="margin: 0px auto; padding: 0px; border: 0px; box-sizing: border-box; max-width: 100%; width: 2em; height: 2em; border-radius: 100%; word-wrap: break-word !important; background-color: rgb(0, 187, 236);">
	            <section style="margin: 0px; padding: 0px; border: 0px; display: inline-block; box-sizing: border-box; max-width: 100%; font-size: 1em; line-height: 2; font-family: inherit; word-wrap: break-word !important;">
	                <section style="margin: 0px; padding: 0px; border: 0px; box-sizing: border-box; max-width: 100%; word-wrap: break-word !important;">
	                    <span style="font-family: 微软雅黑, &#39;Microsoft YaHei&#39;;">2</span>
	                </section>
	            </section>
	        </section>
	        <section style="margin: -1em 0px 1em; padding: 0px; border: 0px; box-sizing: border-box; max-width: 100%; word-wrap: break-word !important;">
	            <section class="yead_bdc" style="margin: 0px; padding: 0px; border-width: 1px 0px 0px; border-top-style: solid; border-color: rgb(0, 187, 236); box-sizing: border-box; max-width: 100%; width: 128.8px; float: left; word-wrap: break-word !important;"></section>
	            <section class="yead_bdc" style="margin: 0px; padding: 0px; border-width: 1px 0px 0px; border-top-style: solid; border-color: rgb(0, 187, 236); box-sizing: border-box; max-width: 100%; width: 128.8px; float: right; word-wrap: break-word !important;"></section>
	        </section>
	    </section>
	</blockquote>
	<p style="margin-top: 0px; margin-bottom: 0px; font-family: Simsun; font-size: 12px; white-space: normal; widows: 1; text-indent: 32px;">
	    <span style="font-family: 微软雅黑, &#39;Microsoft YaHei&#39;; font-size: 16px;">每位学生均可拥有2个上机帐号：教学帐号和自费帐号，教学帐号是以“1”开头的8位数字(1XXXXXXX)；自费帐号则以“2”开头的8位数字(2XXXXXXX)，两种帐号的后7位完全相同。&nbsp;<br/></span>
	</p>
	<p style="margin-top: 0px; margin-bottom: 0px; font-family: Simsun; font-size: 12px; white-space: normal; widows: 1; text-indent: 32px;">
	    <span style="font-size: 16px; font-family: 微软雅黑, &#39;Microsoft YaHei&#39;;">(1)教学帐号的机时费用，由实验室按照校教务处的教学计划统一充值；</span>
	</p>
	<p style="margin-top: 0px; margin-bottom: 0px; font-family: Simsun; font-size: 12px; white-space: normal; widows: 1; text-indent: 32px;">
	    <span style="font-size: 16px; font-family: 微软雅黑, &#39;Microsoft YaHei&#39;;">(2)自费帐号的机时费用由学生个人自费充值，帐号的开启与充值可凭一卡通在实验室服务窗口办理。</span>
	</p>
	<p style="margin-top: 0px; margin-bottom: 0px; font-family: Simsun; font-size: 12px; white-space: normal; widows: 1; text-indent: 32px;">
	    <span style="font-size: 16px; font-family: 微软雅黑, &#39;Microsoft YaHei&#39;;"></span>
	</p>
	<p style="margin-top: 0px; margin-bottom: 0px; font-family: Simsun; font-size: 12px; white-space: normal; widows: 1;">
	    <br/>
	</p>
	<blockquote id="m139" class="yead_editor" data-author="易点微信编辑器" data-url="http://wxedit.yead.net" style="font-family: Simsun; white-space: normal; widows: 1; border: 0px; padding: 0px; margin: 5px auto;">
	    <section class="yead_bdc" style="margin: 0px; padding: 0px; border: 0px rgb(0, 187, 236); font-family: inherit; font-size: 1em; line-height: 25px; box-sizing: border-box; max-width: 100%; text-align: center; text-decoration: inherit; color: rgb(255, 255, 255); word-wrap: break-word !important;">
	        <section class="yead_bgc" style="margin: 0px auto; padding: 0px; border: 0px; box-sizing: border-box; max-width: 100%; width: 2em; height: 2em; border-radius: 100%; word-wrap: break-word !important; background-color: rgb(0, 187, 236);">
	            <section style="margin: 0px; padding: 0px; border: 0px; display: inline-block; box-sizing: border-box; max-width: 100%; font-size: 1em; line-height: 2; font-family: inherit; word-wrap: break-word !important;">
	                <section style="margin: 0px; padding: 0px; border: 0px; box-sizing: border-box; max-width: 100%; word-wrap: break-word !important;">
	                    <span style="font-family: 微软雅黑, &#39;Microsoft YaHei&#39;; font-size: 14px;">3</span>
	                </section>
	            </section>
	        </section>
	        <section style="margin: -1em 0px 1em; padding: 0px; border: 0px; box-sizing: border-box; max-width: 100%; word-wrap: break-word !important;">
	            <section class="yead_bdc" style="margin: 0px; padding: 0px; border-width: 1px 0px 0px; border-top-style: solid; border-color: rgb(0, 187, 236); box-sizing: border-box; max-width: 100%; width: 128.8px; float: left; word-wrap: break-word !important;"></section>
	            <section class="yead_bdc" style="margin: 0px; padding: 0px; border-width: 1px 0px 0px; border-top-style: solid; border-color: rgb(0, 187, 236); box-sizing: border-box; max-width: 100%; width: 128.8px; float: right; word-wrap: break-word !important;"></section>
	        </section>
	    </section>
	</blockquote>
	<p style="margin-top: 0px; margin-bottom: 0px; font-family: Simsun; font-size: 12px; white-space: normal; widows: 1; text-indent: 32px;">
	    <span style="font-family: 微软雅黑, &#39;Microsoft YaHei&#39;; font-size: 16px;">账号登录与退出<br/></span>
	</p>
	<p style="margin-top: 0px; margin-bottom: 0px; font-family: Simsun; font-size: 12px; white-space: normal; widows: 1; text-indent: 32px;">
	    <span style="font-size: 16px; font-family: 微软雅黑, &#39;Microsoft YaHei&#39;;">(1)开启或重启计算机电源，屏幕出现操作系统的选择菜单界面，选中想进入的操作系统，屏幕出现“华中科技大学计算机开放实验室账号管理系统界面”(如图1所示)。</span>
	</p>
	<p>
	    <img src="/elecFee/images/pic1.png" style="width:100%; font-family: Simsun; font-size: 12px; white-space: normal; widows: 1;"/><span style="font-family: 微软雅黑, &#39;Microsoft YaHei&#39;; font-size: 12px; widows: 1; background-color: rgb(255, 255, 255);"></span>
	</p>
	<p style="margin-top: 0px; margin-bottom: 0px; font-family: Simsun; font-size: 12px; white-space: normal; widows: 1; text-align: center;">
	    <span style="font-size: 14px; color: rgb(127, 127, 127); font-family: 微软雅黑, &#39;Microsoft YaHei&#39;;">图1 账号管理系统</span>
	</p>
	<p style="margin-top: 0px; margin-bottom: 0px; font-family: Simsun; font-size: 12px; white-space: normal; widows: 1;">
	    <span style="font-size: 14px; color: rgb(127, 127, 127); font-family: 微软雅黑, &#39;Microsoft YaHei&#39;;"><br/></span>
	</p>
	<p style="margin-top: 0px; margin-bottom: 0px; font-family: Simsun; font-size: 12px; white-space: normal; widows: 1; text-indent: 32px;">
	    <span style="font-size: 18px; font-family: 微软雅黑, &#39;Microsoft YaHei&#39;;"><span style="font-size: 16px; font-family: 微软雅黑, &#39;Microsoft YaHei&#39;;">(2)账号登录，请按数字键“</span><strong><span style="font-size: 16px; font-family: 微软雅黑, &#39;Microsoft YaHei&#39;; color: rgb(255, 0, 0);">1</span></strong><span style="font-size: 16px; font-family: 微软雅黑, &#39;Microsoft YaHei&#39;;">”，屏幕跳转为图2，在Username输入帐号并回车，Password输入密码并回车后，计费系统开始认证，当帐号和密码均正确且费用余额大于零时，进入选择的操作系统，计费也随即开始。</span></span>
	</p>
	<p>
	    <img src="/elecFee/images/pic2.png" style="width:100%; font-family: Simsun; font-size: 12px; white-space: normal; widows: 1;"/><span style="font-family: 微软雅黑, &#39;Microsoft YaHei&#39;; font-size: 12px; widows: 1; background-color: rgb(255, 255, 255);"></span>
	</p>
	<p style="margin-top: 0px; margin-bottom: 0px; font-family: Simsun; font-size: 12px; white-space: normal; widows: 1; text-indent: 32px;">
	    <span style="font-size: 18px; font-family: 微软雅黑, &#39;Microsoft YaHei&#39;;"></span>
	</p>
	<p style="margin-top: 0px; margin-bottom: 0px; font-family: Simsun; font-size: 12px; white-space: normal; widows: 1; text-align: center;">
	    <span style="font-size: 14px; color: rgb(127, 127, 127); font-family: 微软雅黑, &#39;Microsoft YaHei&#39;;">图2 输入账号和密码</span>
	</p>
	<p style="margin-top: 0px; margin-bottom: 0px; font-family: Simsun; font-size: 12px; white-space: normal; widows: 1;">
	    <span style="font-size: 14px; color: rgb(127, 127, 127); font-family: 微软雅黑, &#39;Microsoft YaHei&#39;;"><br/></span>
	</p>
	<p style="margin-top: 0px; margin-bottom: 0px; font-family: Simsun; font-size: 12px; white-space: normal; widows: 1; text-indent: 32px;">
	    <span style="font-size: 18px; font-family: 微软雅黑, &#39;Microsoft YaHei&#39;;"></span>
	</p>
	<p style="margin-top: 0px; margin-bottom: 0px; font-family: Simsun; font-size: 12px; white-space: normal; widows: 1; text-indent: 32px;">
	    <span style="font-size: 16px; font-family: 微软雅黑, &#39;Microsoft YaHei&#39;;">(3)帐号退出，请重新启动计算机，进入图1界面后按数字键“<strong><span style="font-family: 微软雅黑, &#39;Microsoft YaHei&#39;; color: rgb(255, 0, 0);">2</span></strong>”，同样输入帐号和密码，计费系统对帐号进行注销操作，稍等片刻后，屏幕跳转为图3所示，当看见“<strong><span style="font-family: 微软雅黑, &#39;Microsoft YaHei&#39;; color: rgb(0, 176, 80);">您已经成功退出开放实验室系统</span></strong>，<strong><span style="font-family: 微软雅黑, &#39;Microsoft YaHei&#39;; color: rgb(255, 0, 0);">欢迎下次光临</span></strong>”提示信息后，表示账号成功退出，计费已终止，此时方可离开机房；否则仍在计费，继续扣减帐号里的费用。</span>
	</p>
	<p>
	    <img src="/elecFee/images/pic3.png" style="width:100%; font-family: Simsun; font-size: 12px; white-space: normal; widows: 1;"/><span style="font-family: 微软雅黑, &#39;Microsoft YaHei&#39;; font-size: 12px; widows: 1; background-color: rgb(255, 255, 255);"></span>
	</p>
	<p style="margin-top: 0px; margin-bottom: 0px; font-family: Simsun; font-size: 12px; white-space: normal; widows: 1; text-align: center;">
	    <span style="font-size: 14px; color: rgb(127, 127, 127); font-family: 微软雅黑, &#39;Microsoft YaHei&#39;;">图3 账号退出后的提示信息</span>
	</p>
	<p style="margin-top: 0px; margin-bottom: 0px; font-family: Simsun; font-size: 12px; white-space: normal; widows: 1; text-indent: 32px;">
	    <span style="font-size: 18px; font-family: 微软雅黑, &#39;Microsoft YaHei&#39;;"></span>
	</p>
	<p style="margin-top: 0px; margin-bottom: 0px; font-family: Simsun; font-size: 12px; white-space: normal; widows: 1;">
	    <br/>
	</p>
	<blockquote id="m139" class="yead_editor" data-author="易点微信编辑器" data-url="http://wxedit.yead.net" style="font-family: Simsun; white-space: normal; widows: 1; border: 0px; padding: 0px; margin: 5px auto;">
	    <section class="yead_bdc" style="margin: 0px; padding: 0px; border: 0px rgb(0, 187, 236); font-family: inherit; font-size: 1em; line-height: 25px; box-sizing: border-box; max-width: 100%; text-align: center; text-decoration: inherit; color: rgb(255, 255, 255); word-wrap: break-word !important;">
	        <section class="yead_bgc" style="margin: 0px auto; padding: 0px; border: 0px; box-sizing: border-box; max-width: 100%; width: 2em; height: 2em; border-radius: 100%; word-wrap: break-word !important; background-color: rgb(0, 187, 236);">
	            <section style="margin: 0px; padding: 0px; border: 0px; display: inline-block; box-sizing: border-box; max-width: 100%; font-size: 1em; line-height: 2; font-family: inherit; word-wrap: break-word !important;">
	                <section style="margin: 0px; padding: 0px; border: 0px; box-sizing: border-box; max-width: 100%; word-wrap: break-word !important;">
	                    <span style="font-family: 微软雅黑, &#39;Microsoft YaHei&#39;;">4</span>
	                </section>
	            </section>
	        </section>
	        <section style="margin: -1em 0px 1em; padding: 0px; border: 0px; box-sizing: border-box; max-width: 100%; word-wrap: break-word !important;">
	            <section class="yead_bdc" style="margin: 0px; padding: 0px; border-width: 1px 0px 0px; border-top-style: solid; border-color: rgb(0, 187, 236); box-sizing: border-box; max-width: 100%; width: 128.8px; float: left; word-wrap: break-word !important;"></section>
	            <section class="yead_bdc" style="margin: 0px; padding: 0px; border-width: 1px 0px 0px; border-top-style: solid; border-color: rgb(0, 187, 236); box-sizing: border-box; max-width: 100%; width: 128.8px; float: right; word-wrap: break-word !important;"></section>
	        </section>
	    </section>
	</blockquote>
	<p style="margin-top: 0px; margin-bottom: 0px; font-family: Simsun; font-size: 12px; white-space: normal; widows: 1; text-indent: 32px;">
	    <span style="font-family: 微软雅黑, &#39;Microsoft YaHei&#39;; font-size: 16px;">每个帐号只能同时登录一台计算机，若更换计算机，必须在原登录的计算机上退出帐号后，才能在其它机器上登录该账号。<br/></span>
	</p>
	<p style="margin-top: 0px; margin-bottom: 0px; font-family: Simsun; font-size: 12px; white-space: normal; widows: 1; text-indent: 32px;">
	    <span style="font-size: 16px; font-family: 微软雅黑, &#39;Microsoft YaHei&#39;;"><br/></span>
	</p>
	<p style="margin-top: 0px; margin-bottom: 0px; font-family: Simsun; font-size: 12px; white-space: normal; widows: 1; text-indent: 32px;">
	    <span style="font-size: 16px; font-family: 微软雅黑, &#39;Microsoft YaHei&#39;;"></span>
	</p>
	<blockquote id="m139" class="yead_editor" data-author="易点微信编辑器" data-url="http://wxedit.yead.net" style="font-family: Simsun; white-space: normal; widows: 1; border: 0px; padding: 0px; margin: 5px auto;">
	    <section class="yead_bdc" style="margin: 0px; padding: 0px; border: 0px rgb(0, 187, 236); font-family: inherit; font-size: 1em; line-height: 25px; box-sizing: border-box; max-width: 100%; text-align: center; text-decoration: inherit; color: rgb(255, 255, 255); word-wrap: break-word !important;">
	        <section class="yead_bgc" style="margin: 0px auto; padding: 0px; border: 0px; box-sizing: border-box; max-width: 100%; width: 2em; height: 2em; border-radius: 100%; word-wrap: break-word !important; background-color: rgb(0, 187, 236);">
	            <section style="margin: 0px; padding: 0px; border: 0px; display: inline-block; box-sizing: border-box; max-width: 100%; font-size: 1em; line-height: 2; font-family: inherit; word-wrap: break-word !important;">
	                <section style="margin: 0px; padding: 0px; border: 0px; box-sizing: border-box; max-width: 100%; word-wrap: break-word !important;">
	                    <span style="font-family: 微软雅黑, &#39;Microsoft YaHei&#39;; font-size: 14px;">5</span>
	                </section>
	            </section>
	        </section>
	        <section style="margin: -1em 0px 1em; padding: 0px; border: 0px; box-sizing: border-box; max-width: 100%; word-wrap: break-word !important;">
	            <section class="yead_bdc" style="margin: 0px; padding: 0px; border-width: 1px 0px 0px; border-top-style: solid; border-color: rgb(0, 187, 236); box-sizing: border-box; max-width: 100%; width: 128.8px; float: left; word-wrap: break-word !important;"></section>
	            <section class="yead_bdc" style="margin: 0px; padding: 0px; border-width: 1px 0px 0px; border-top-style: solid; border-color: rgb(0, 187, 236); box-sizing: border-box; max-width: 100%; width: 128.8px; float: right; word-wrap: break-word !important;"></section>
	        </section>
	    </section>
	</blockquote>
	<p style="margin-top: 0px; margin-bottom: 0px; font-family: Simsun; font-size: 12px; white-space: normal; widows: 1; text-indent: 32px;">
	    <span style="font-family: 微软雅黑, &#39;Microsoft YaHei&#39;; font-size: 16px;">使用计算机时，文件可以暂时保存在硬盘的最后一个逻辑盘(只当天有效)，其它逻辑盘已为“写保护”，若重启计算机后系统自动还原为初始状态。<br/></span>
	</p>
	<p style="margin-top: 0px; margin-bottom: 0px; font-family: Simsun; font-size: 12px; white-space: normal; widows: 1; text-indent: 32px;">
	    <span style="font-size: 16px; font-family: 微软雅黑, &#39;Microsoft YaHei&#39;;"><br/></span>
	</p>
	<p style="margin-top: 0px; margin-bottom: 0px; font-family: Simsun; font-size: 12px; white-space: normal; widows: 1; text-indent: 32px;">
	    <span style="font-size: 16px; font-family: 微软雅黑, &#39;Microsoft YaHei&#39;;"></span>
	</p>
	<blockquote id="m139" class="yead_editor" data-author="易点微信编辑器" data-url="http://wxedit.yead.net" style="font-family: Simsun; white-space: normal; widows: 1; border: 0px; padding: 0px; margin: 5px auto;">
	    <section class="yead_bdc" style="margin: 0px; padding: 0px; border: 0px rgb(0, 187, 236); font-family: inherit; font-size: 1em; line-height: 25px; box-sizing: border-box; max-width: 100%; text-align: center; text-decoration: inherit; color: rgb(255, 255, 255); word-wrap: break-word !important;">
	        <section class="yead_bgc" style="margin: 0px auto; padding: 0px; border: 0px; box-sizing: border-box; max-width: 100%; width: 2em; height: 2em; border-radius: 100%; word-wrap: break-word !important; background-color: rgb(0, 187, 236);">
	            <section style="margin: 0px; padding: 0px; border: 0px; display: inline-block; box-sizing: border-box; max-width: 100%; font-size: 1em; line-height: 2; font-family: inherit; word-wrap: break-word !important;">
	                <section style="margin: 0px; padding: 0px; border: 0px; box-sizing: border-box; max-width: 100%; word-wrap: break-word !important;">
	                    <span style="font-family: 微软雅黑, &#39;Microsoft YaHei&#39;; font-size: 14px;">6</span>
	                </section>
	            </section>
	        </section>
	        <section style="margin: -1em 0px 1em; padding: 0px; border: 0px; box-sizing: border-box; max-width: 100%; word-wrap: break-word !important;">
	            <section class="yead_bdc" style="margin: 0px; padding: 0px; border-width: 1px 0px 0px; border-top-style: solid; border-color: rgb(0, 187, 236); box-sizing: border-box; max-width: 100%; width: 128.8px; float: left; word-wrap: break-word !important;"></section>
	            <section class="yead_bdc" style="margin: 0px; padding: 0px; border-width: 1px 0px 0px; border-top-style: solid; border-color: rgb(0, 187, 236); box-sizing: border-box; max-width: 100%; width: 128.8px; float: right; word-wrap: break-word !important;"></section>
	        </section>
	    </section>
	</blockquote>
	<p style="margin-top: 0px; margin-bottom: 0px; font-family: Simsun; font-size: 12px; white-space: normal; widows: 1; text-indent: 32px;">
	    <span style="font-family: 微软雅黑, &#39;Microsoft YaHei&#39;; font-size: 16px;">每天上机安排在实验室的显示屏上显示，请同学们按座位排数入座。<br/></span>
	</p>
	<p style="margin-top: 0px; margin-bottom: 0px; font-family: Simsun; font-size: 12px; white-space: normal; widows: 1; text-indent: 32px;">
	    <span style="font-size: 16px; font-family: 微软雅黑, &#39;Microsoft YaHei&#39;;"><br/></span>
	</p>
	<p style="margin-top: 0px; margin-bottom: 0px; font-family: Simsun; font-size: 12px; white-space: normal; widows: 1; text-indent: 32px;">
	    <span style="font-size: 16px; font-family: 微软雅黑, &#39;Microsoft YaHei&#39;;"></span>
	</p>
	<blockquote id="m139" class="yead_editor" data-author="易点微信编辑器" data-url="http://wxedit.yead.net" style="font-family: Simsun; white-space: normal; widows: 1; border: 0px; padding: 0px; margin: 5px auto;">
	    <section class="yead_bdc" style="margin: 0px; padding: 0px; border: 0px rgb(0, 187, 236); font-family: inherit; font-size: 1em; line-height: 25px; box-sizing: border-box; max-width: 100%; text-align: center; text-decoration: inherit; color: rgb(255, 255, 255); word-wrap: break-word !important;">
	        <section class="yead_bgc" style="margin: 0px auto; padding: 0px; border: 0px; box-sizing: border-box; max-width: 100%; width: 2em; height: 2em; border-radius: 100%; word-wrap: break-word !important; background-color: rgb(0, 187, 236);">
	            <section style="margin: 0px; padding: 0px; border: 0px; display: inline-block; box-sizing: border-box; max-width: 100%; font-size: 1em; line-height: 2; font-family: inherit; word-wrap: break-word !important;">
	                <section style="margin: 0px; padding: 0px; border: 0px; box-sizing: border-box; max-width: 100%; word-wrap: break-word !important;">
	                    <span style="font-family: 微软雅黑, &#39;Microsoft YaHei&#39;; font-size: 14px;">7</span>
	                </section>
	            </section>
	        </section>
	        <section style="margin: -1em 0px 1em; padding: 0px; border: 0px; box-sizing: border-box; max-width: 100%; word-wrap: break-word !important;">
	            <section class="yead_bdc" style="margin: 0px; padding: 0px; border-width: 1px 0px 0px; border-top-style: solid; border-color: rgb(0, 187, 236); box-sizing: border-box; max-width: 100%; width: 128.8px; float: left; word-wrap: break-word !important;"></section>
	            <section class="yead_bdc" style="margin: 0px; padding: 0px; border-width: 1px 0px 0px; border-top-style: solid; border-color: rgb(0, 187, 236); box-sizing: border-box; max-width: 100%; width: 128.8px; float: right; word-wrap: break-word !important;"></section>
	        </section>
	    </section>
	</blockquote>
	<p style="margin-top: 0px; margin-bottom: 0px; font-family: Simsun; font-size: 12px; white-space: normal; widows: 1; text-indent: 32px;">
	    <span style="font-family: 微软雅黑, &#39;Microsoft YaHei&#39;; font-size: 16px;">上机时间为教学周的周一至周六开放，如遇特殊情况预先通知。<br/></span>
	</p>
	<p>
	    <span style="font-size: 18px; font-family: 微软雅黑, &#39;Microsoft YaHei&#39;;"><br/></span>
	</p>
	<p>
	    <br/>
	</p>
</div>
<div class="button_02_outside button_color">
	<input type="button" onclick="sysMain();" value="返回" class="button_02">
</div>
