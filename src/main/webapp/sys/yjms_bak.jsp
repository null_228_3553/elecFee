<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript">
$(document).ready(function(){
	post2SRV("yjms.do",function(data){
		alert(data);
	},"json");
	$("#zxq").click(function(){
		if($(this).attr("class")==undefined)
			$("#z401").click();
		$(this).attr("class","kctab2_select");
		$("#dxq").removeAttr("class");
		$("#zTitle").show();
		$("#dTitle").hide();
	});
	$("#dxq").click(function(){
		if($(this).attr("class")==undefined)
			$("#d201").click();
		$(this).attr("class","kctab2_select");
		$("#zxq").removeAttr("class");
		$("#dTitle").show();
		$("#zTitle").hide();
	});
	$("#z401").click(function(){
		$(this).attr("class","kctab_select");
		$("#z403").removeAttr("class");
		$("#z208").removeAttr("class");
		$("#z401div").show();
		$("#z403div").hide();
		$("#z208div").hide();
		$("#d201div").hide();
		$("#d202div").hide();
		$("#d301div").hide();
	});
	$("#z403").click(function(){
		$(this).attr("class","kctab_select");
		$("#z401").removeAttr("class");
		$("#z208").removeAttr("class");
		$("#z401div").hide();
		$("#z403div").show();
		$("#z208div").hide();
		$("#d201div").hide();
		$("#d202div").hide();
		$("#d301div").hide();
	});
	$("#z208").click(function(){
		$(this).attr("class","kctab_select");
		$("#z403").removeAttr("class");
		$("#z401").removeAttr("class");
		$("#z401div").hide();
		$("#z403div").hide();
		$("#z208div").show();
		$("#d201div").hide();
		$("#d202div").hide();
		$("#d301div").hide();
	});
	$("#d201").click(function(){
		$(this).attr("class","kctab_select");
		$("#d202").removeAttr("class");
		$("#d206").removeAttr("class");
		$("#d301").removeAttr("class");
		$("#z401div").hide();
		$("#z403div").hide();
		$("#z208div").hide();
		$("#d201div").show();
		$("#d202div").hide();
		$("#d301div").hide();
	});
	$("#d202").click(function(){
		$(this).attr("class","kctab_select");
		$("#d201").removeAttr("class");
		$("#d206").removeAttr("class");
		$("#d301").removeAttr("class");
		$("#z401div").hide();
		$("#z403div").hide();
		$("#z208div").hide();
		$("#d201div").hide();
		$("#d202div").show();
		$("#d301div").hide();
	});
	$("#d206").click(function(){
		$(this).attr("class","kctab_select");
		$("#d201").removeAttr("class");
		$("#d202").removeAttr("class");
		$("#d301").removeAttr("class");
		$("#z401div").hide();
		$("#z403div").hide();
		$("#z208div").hide();
		$("#d201div").hide();
		$("#d202div").show();
		$("#d301div").hide();
	});
	$("#d301").click(function(){
		$(this).attr("class","kctab_select");
		$("#d201").removeAttr("class");
		$("#d202").removeAttr("class");
		$("#d206").removeAttr("class");
		$("#z401div").hide();
		$("#z403div").hide();
		$("#z208div").hide();
		$("#d201div").hide();
		$("#d202div").hide();
		$("#d301div").show();
	});
});
</script>
<div class="div_title key_color" id="yjms">硬件描述</div>
<div class="main_03">
	<div style="margin:0 10px" class="systab kc_tab2">
		<a href="javascript:;" id="zxq" class="kctab2_select">主校区</a>
		<a href="javascript:;" id="dxq" >东校区</a>
	</div>
	<div id="zTitle" style="margin:0 10px 15px;" class="systab kc_tab" >
		<a href="javascript:;" id="z208" class="kctab_select">208机房</a>
		<a href="javascript:;" id="z401" >401机房</a>
		<a href="javascript:;" id="z403" >403机房</a>
	</div>
	<div id="dTitle" style="margin:0 10px 15px;display:none" class="systab kc_tab">
		<a href="javascript:;" id="d201" class="kctab_select" style="width:25%">201机房</a>
		<a href="javascript:;" id="d202" style="width:25%">202机房</a>
		<a href="javascript:;" id="d206" style="width:25%">206机房</a>
		<a href="javascript:;" id="d301" style="width:25%">301机房</a>
	</div>
			
<div id="z401div" class="kc_list">
<div class="xxcx_row">
	<p style="line-height:30px;color:red;">主校区开放实验室本学期因安全原因停用整改，请同学们到东校区实训中心二楼机房上机！</p>
</div>
</div>
<div id="z403div" class="kc_list" >
<div class="xxcx_row">
	<p style="line-height:30px;color:red;">主校区开放实验室本学期因安全原因停用整改，请同学们到东校区实训中心二楼机房上机！</p>
</div>
</div>
<div id="z208div" class="kc_list" style="display:block;">
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">${z208.COMPUTER}</p>
		<p class="kc_week_mini key_color">${z208.USE_TIME}</p>
	</div>
</div>
<div class="xxcx_row">
	<table style="width:100%;">
		<tr style="width:100%;">
			<td class="xxcx_left_box">CPU：</td>
			<td class="xxcx_right_box">${z208.CPU}</td>
		<tr>
	</table>
</div>
<div class="xxcx_row">
	<table style="width:100%;">
		<tr style="width:100%;">
			<td class="xxcx_left_box">内存：</td>
			<td class="xxcx_right_box">${z208.MEMORY}</td>
		<tr>
	</table>
</div>
<div class="xxcx_row">
	<table style="width:100%;">
		<tr style="width:100%;">
			<td class="xxcx_left_box">芯片组：</td>
			<td class="xxcx_right_box">${z208.CHIP}</td>
		<tr>
	</table>
</div>
<div class="xxcx_row">
	<table style="width:100%;">
		<tr style="width:100%;">
			<td class="xxcx_left_box">硬盘：</td>
			<td class="xxcx_right_box">${z208.DISK}</td>
		<tr>
	</table>
</div>
<div class="xxcx_row">
	<table style="width:100%;">
		<tr style="width:100%;">
			<td class="xxcx_left_box">显示器：</td>
			<td class="xxcx_right_box">${z208.MONITOR}</td>
		<tr>
	</table>
</div>
</div>

<div id="d201div" class="kc_list">
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">${d201.COMPUTER}</p>
		<p class="kc_week_mini key_color">${d201.USE_TIME}</p>
	</div>
</div>
<div class="xxcx_row">
	<table style="width:100%;">
		<tr style="width:100%;">
			<td class="xxcx_left_box">CPU：</td>
			<td class="xxcx_right_box">${d201.CPU}</td>
		<tr>
	</table>
</div>
<div class="xxcx_row">
	<table style="width:100%;">
		<tr style="width:100%;">
			<td class="xxcx_left_box">内存：</td>
			<td class="xxcx_right_box">${d201.MEMORY}</td>
		<tr>
	</table>
</div>
<div class="xxcx_row">
	<table style="width:100%;">
		<tr style="width:100%;">
			<td class="xxcx_left_box">芯片组：</td>
			<td class="xxcx_right_box">${d201.CHIP}</td>
		<tr>
	</table>
</div>
<div class="xxcx_row">
	<table style="width:100%;">
		<tr style="width:100%;">
			<td class="xxcx_left_box">硬盘：</td>
			<td class="xxcx_right_box">${d201.DISK}</td>
		<tr>
	</table>
</div>
<div class="xxcx_row">
	<table style="width:100%;">
		<tr style="width:100%;">
			<td class="xxcx_left_box">显示器：</td>
			<td class="xxcx_right_box">${d201.MONITOR}</td>
		<tr>
	</table>
</div>
</div>

<div id="d202div" class="kc_list">
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">${d202.COMPUTER}</p>
		<p class="kc_week_mini key_color">${d202.USE_TIME}</p>
	</div>
</div>
<div class="xxcx_row">
	<table style="width:100%;">
		<tr style="width:100%;">
			<td class="xxcx_left_box">CPU：</td>
			<td class="xxcx_right_box">${d202.CPU}</td>
		<tr>
	</table>
</div>
<div class="xxcx_row">
	<table style="width:100%;">
		<tr style="width:100%;">
			<td class="xxcx_left_box">内存：</td>
			<td class="xxcx_right_box">${d202.MEMORY}</td>
		<tr>
	</table>
</div>
<div class="xxcx_row">
	<table style="width:100%;">
		<tr style="width:100%;">
			<td class="xxcx_left_box">芯片组：</td>
			<td class="xxcx_right_box">${d202.CHIP}</td>
		<tr>
	</table>
</div>
<div class="xxcx_row">
	<table style="width:100%;">
		<tr style="width:100%;">
			<td class="xxcx_left_box">硬盘：</td>
			<td class="xxcx_right_box">${d202.DISK}</td>
		<tr>
	</table>
</div>
<div class="xxcx_row">
	<table style="width:100%;">
		<tr style="width:100%;">
			<td class="xxcx_left_box">显示器：</td>
			<td class="xxcx_right_box">${d202.MONITOR}</td>
		<tr>
	</table>
</div>
</div>

<div id="d301div" class="kc_list">
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">${d301.COMPUTER}</p>
		<p class="kc_week_mini key_color">${d301.USE_TIME}</p>
	</div>
</div>
<div class="xxcx_row">
	<table style="width:100%;">
		<tr style="width:100%;">
			<td class="xxcx_left_box">CPU：</td>
			<td class="xxcx_right_box">${d301.CPU}</td>
		<tr>
	</table>
</div>
<div class="xxcx_row">
	<table style="width:100%;">
		<tr style="width:100%;">
			<td class="xxcx_left_box">内存：</td>
			<td class="xxcx_right_box">${d301.MEMORY}</td>
		<tr>
	</table>
</div>
<div class="xxcx_row">
	<table style="width:100%;">
		<tr style="width:100%;">
			<td class="xxcx_left_box">芯片组：</td>
			<td class="xxcx_right_box">${d301.CHIP}</td>
		<tr>
	</table>
</div>
<div class="xxcx_row">
	<table style="width:100%;">
		<tr style="width:100%;">
			<td class="xxcx_left_box">硬盘：</td>
			<td class="xxcx_right_box">${d301.DISK}</td>
		<tr>
	</table>
</div>
<div class="xxcx_row">
	<table style="width:100%;">
		<tr style="width:100%;">
			<td class="xxcx_left_box">显示器：</td>
			<td class="xxcx_right_box">${d301.MONITOR}</td>
		<tr>
	</table>
</div>
</div>

	</div>
<div class="button_02_outside button_color">
	<input type="button" onclick="sysMain();" value="返回" class="button_02">
</div>
