<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="div_title key_color">机位空闲率</div>
<hr/>
<div class="barss" style="font-size:16px;background-color: #f3f3ff;">
<p style="color:#134c84;font-weight:bold;font-size:18px;line-height:30px;margin-bottom: 5px;">
	主校区:<p/>
	<p></p>
	<%-- ${z401.mc}：使用率
	<font color="#FF6600">${z401.rate}%</font>&nbsp; 空闲
	<font color="#FF6600">${z401.r_num-z401.count}</font>台
	<br>
	<br>${z403.mc}：使用率
	<font color="#FF6600">${z403.rate}%</font>&nbsp; 空闲
	<font color="#FF6600">${z403.r_num-z403.count}</font>台
	<br>
	<br>${z208.mc}：使用率
	<font color="#FF6600">${z208.rate}%</font>&nbsp; 空闲
	<font color="#FF6600">${z208.r_num-z208.count}</font>台--%>
	
	<span style="color:red">主校区开放实验室本学期因安全原因停用整改，请同学们到东校区实训中心二楼机房上机！</span>
	<br> 
	<br>  
</div>
<hr/>
<div class="barss" style="font-size:16px;background-color: #f3f3ff;">
<p style="color:#134c84;font-weight:bold;font-size:18px;line-height:30px;margin-bottom: 5px;">
	东校区: <p/>
	<p></p>
	${d301.mc}：机位空闲率
	<font color="#FF6600">${ (10000-d301.rate*100)/100<0 ? 0 : (10000-d301.rate*100)/100 }%</font>&nbsp; 空闲
	<font color="#FF6600">${ (d301.r_num-d301.count)<0 ? 0 : (d301.r_num-d301.count) }</font>台
	<br>
	<br>${d201.mc}：机位空闲率
	<font color="#FF6600">${ (10000-d201.rate*100)/100<0 ? 0 : (10000-d201.rate*100)/100}%</font>&nbsp; 空闲
	<font color="#FF6600">${ (d201.r_num-d201.count)<0 ? 0 : (d201.r_num-d201.count) }</font>台
	<br>
</div>
<hr/>
<div class="button_02_outside button_color">
	<input type="button" onclick="sysMain();" value="返回" class="button_02">
</div>
