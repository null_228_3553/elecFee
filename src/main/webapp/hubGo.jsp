<%@ page language="java" contentType="text/html; charset=utf-8"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<title>抱歉</title>
<link rel="stylesheet" type="text/css" href="/elecFee/css/error.css">
</head>
<body>
	<div id="wrapper">
		<a class="logo" href="/"></a>
		<div id="main" style="padding-top:20px">
			<header id="header">
				<h1>
					<span class="icon">!</span><span class="sub" style="font-size:30px;">四六级缴费暂停使用</span>
				</h1>
			</header>
			<div id="content">
				<h2 style="padding-bottom:0px">请关注HUB系统微信号，选择教学服务""四六级报名"，完成缴费</h2>
				<p style="padding-top:10px;">
					<img src="/elecFee/images/hub.png">
				</p>
				<div class="utilities" style="padding-top:0px;">
					<a class="button right" href="http://m.hust.edu.cn/wechat/app.jsp">返回首页</a>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>
</html>