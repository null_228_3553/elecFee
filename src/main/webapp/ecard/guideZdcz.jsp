<%@ page language="java" contentType="text/html; charset=utf-8"%>
<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
  <meta name="keywords" content=""> 
  <meta name="decription" content=""> 
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"> 
  <title></title> 
  <link href="/elecFee/css/icon.css" media="screen" rel="stylesheet" type="text/css"> 
  <link href="/elecFee/css/style1.css" media="screen" rel="stylesheet" type="text/css"> 
  <link href="/elecFee/css/color_02.css" media="screen" rel="stylesheet" type="text/css">   
 </head>
 <body style="background-color:#fff;margin:60px 5px 5px 5px">
  <jsp:include page="guideHeader.jsp" flush="true"> 
   <jsp:param name="title" value="校园卡充值指南" /> 
  </jsp:include> 
<blockquote id="m647" class="yead_editor yead-selected" data-author="易点微信编辑器" data-url="http://wxedit.yead.net" style="border: 0px; padding: 0px; margin: 5px auto; white-space: normal;">
    <section style="text-align: center;">
        <section class="yead_color" style="font-size: 14px; color: rgb(120, 128, 200);">
            <p style="margin-top:0;margin-bottom:0;font-size:24px">
                <span style="font-size: 36px;">自动充值</span>
            </p>
        </section>
        <section style="display: inline-block;">
            <section style="font-size: 14px; margin-right: auto; margin-left: auto; width: 15px; height: 15px; border-top-left-radius: 50%; border-top-right-radius: 50%; border-bottom-right-radius: 50%; border-bottom-left-radius: 50%; -webkit-transform: rotate(0deg);">
                <section style="width:15px;height:15px;border-top-left-radius:50%;border-top-right-radius:50%;border-bottom-right-radius:50%;border-bottom-left-radius:50%;display:table-cell;vertical-align:middle">
                    <section class="yead_bgc" style="margin-right:auto;margin-left:auto;width:5px;height:5px;border-top-left-radius:50%;border-top-right-radius:50%;border-bottom-right-radius:50%;border-bottom-left-radius:50%;background-color:#7880c8"></section>
                </section>
            </section>
            <section class="yead_bdtc yead_color" style="margin-top: -8px; padding: 5px 15px; border-top-width: 1px; border-top-style: solid; border-top-color: rgb(120, 128, 200); color: rgb(120, 128, 200);">
                <p style="margin-top: 0px; margin-bottom: 0px;">
                    <span style="font-size: 14px;">Auto Recharge</span>
                </p>
            </section>
        </section>
    </section>
</blockquote>
<blockquote id="m924" class="yead_editor" data-author="易点微信编辑器" data-url="http://wxedit.yead.net" style="font-size:14px;border:0px;padding:0px;margin:5px auto;white-space: normal;">
    <section style="background:#f3f3f3">
        <section style="display:block;padding:10px">
            <section class="yead_bgc" style="float:left;text-align:center;border-top-left-radius:4px;border-top-right-radius:4px;border-bottom-right-radius:4px;border-bottom-left-radius:4px;background-color:#f93;width:30px;height:30px;line-height:30px">
                <span style="color:#fff;font-size:16px">1</span>
            </section>
            <section style="font-size:16px;height:30px;line-height:30px;vertical-align:middle;color:#333;font-weight:700;margin-left:40px">
                <span style="font-size: 20px;">什么是自动充值</span>
            </section>
        </section>
    </section>
</blockquote>
<p>
    <span style="font-family: 微软雅黑, &#39;Microsoft YaHei&#39;; font-size: 18px;">&nbsp;&nbsp;&nbsp;&nbsp;自动充值是华中大校园卡系统的特色服务之一。</span>
</p>
<p>
    <span style="font-family: 微软雅黑, &#39;Microsoft YaHei&#39;; font-size: 18px;">&nbsp;&nbsp;&nbsp;&nbsp;校园卡和银行卡建立绑定关系后，当校园卡内小于一定金额时，自动从绑定的银行卡转入50元。省去了充值这一步骤。</span>
</p>
<p>
    <span style="font-family: 微软雅黑, &#39;Microsoft YaHei&#39;; font-size: 18px;">&nbsp;&nbsp;&nbsp;&nbsp;系统设置为卡钱包余额小于50元，自动从绑定的银行卡转入50元。该限额可在自助服务终端修改。</span>
</p>
<blockquote id="m924" class="yead_editor" data-author="易点微信编辑器" data-url="http://wxedit.yead.net" style="font-size:14px;border:0px;padding:0px;margin:5px auto;white-space: normal;">
    <section style="background:#f3f3f3">
        <section style="display:block;padding:10px">
            <section class="yead_bgc" style="float:left;text-align:center;border-top-left-radius:4px;border-top-right-radius:4px;border-bottom-right-radius:4px;border-bottom-left-radius:4px;background-color:#f93;width:30px;height:30px;line-height:30px">
                <span style="color:#fff;font-size:16px">2</span>
            </section>
            <section style="font-size:16px;height:30px;line-height:30px;vertical-align:middle;color:#333;font-weight:700;margin-left:40px">
                <span style="font-size: 20px;">怎么自动充值</span>
            </section>
        </section>
    </section>
</blockquote>
<p>
    <span style="font-family: 微软雅黑, &#39;Microsoft YaHei&#39;; font-size: 18px;">&nbsp;&nbsp;&nbsp;&nbsp;1、在自助服务终端放入校园卡，选择转账业务。</span><br/>
</p>
<p>
    <span style="font-family: 微软雅黑, &#39;Microsoft YaHei&#39;; font-size: 18px;"></span>
</p>
<p style="text-align: center">
    <img width="100%" src="/elecFee/images/zdcz/zdcz1.jpg" title="1477041730699834.jpg" alt="zdcz1.jpg"/>
</p>
<p>
    <span style="font-family: 微软雅黑, &#39;Microsoft YaHei&#39;; font-size: 18px;">&nbsp;&nbsp;&nbsp;&nbsp;2、选择充值方式设置</span><br/>
</p>
<p style="text-align: center">
    <img width="100%" src="/elecFee/images/zdcz/zdcz2.jpg" title="1477041775327843.jpg" alt="zdcz2.jpg" />
</p>
<p>
    <span style="font-family: 微软雅黑, &#39;Microsoft YaHei&#39;; font-size: 18px;">&nbsp;&nbsp;&nbsp;&nbsp;3、选择打开自动转账功能</span>
</p>
<p style="text-align: center">
    <img width="100%" src="/elecFee/images/zdcz/zdcz3.jpg" title="1477041843906060.jpg" alt="zdcz3.jpg"/>
</p>
<p>
    <span style="font-family: 微软雅黑, &#39;Microsoft YaHei&#39;; font-size: 18px;">&nbsp;&nbsp;&nbsp;&nbsp;4、返回转账业务页，点击自动充值额度</span><br/>
</p>
<p style="text-align: center">
    <img width="100%" src="/elecFee/images/zdcz/zdcz4.jpg" title="1477041880668104.jpg" alt="zdcz4.jpg"/>
</p>
<p>
    <span style="font-family: 微软雅黑, &#39;Microsoft YaHei&#39;; font-size: 18px;">&nbsp;&nbsp;&nbsp;&nbsp;5、输入新的自动转账金额，点击确认。自动转账金额需在50元到200元之间。</span><br/>
</p>
<p style="text-align: center">
    <img width="100%" src="/elecFee/images/zdcz/zdcz5.jpg" title="1477041928242105.jpg" alt="zdcz5.jpg"/>
</p>
<p>
    <span style="font-family: 微软雅黑, &#39;Microsoft YaHei&#39;; font-size: 18px;">&nbsp;&nbsp;&nbsp;&nbsp;至此，您已成功设定好校园卡的自动充值，再也不用去排队充值啦。</span><br/>
</p>
<blockquote id="m924" class="yead_editor" data-author="易点微信编辑器" data-url="http://wxedit.yead.net" style="font-size:14px;border:0px;padding:0px;margin:5px auto;white-space: normal;">
    <section style="background:#f3f3f3">
        <section style="display:block;padding:10px">
            <section class="yead_bgc" style="float:left;text-align:center;border-top-left-radius:4px;border-top-right-radius:4px;border-bottom-right-radius:4px;border-bottom-left-radius:4px;background-color:#f93;width:30px;height:30px;line-height:30px">
                <span style="color:#fff;font-size:16px">3</span>
            </section>
            <section style="font-size:16px;height:30px;line-height:30px;vertical-align:middle;color:#333;font-weight:700;margin-left:40px">
                <span style="font-size: 20px;">自动充值注意事项</span>
            </section>
        </section>
    </section>
</blockquote>
<p>
    <span style="font-family: 微软雅黑, &#39;Microsoft YaHei&#39;; font-size: 18px;">&nbsp;&nbsp;&nbsp;&nbsp;1、设置自动充值前请将校园卡与银行卡绑定，您可以在自助服务终端点击“转账业务”→“银行卡签约”进行自助绑定。</span>
</p>
<p>
    <span style="font-family: 微软雅黑, &#39;Microsoft YaHei&#39;; font-size: 18px;">&nbsp;&nbsp;&nbsp;&nbsp;2、<span style="font-family: 微软雅黑, &#39;Microsoft YaHei&#39;; font-size: 18px;">目前华中大的校园卡只支持</span><span style="font-family: 微软雅黑, &#39;Microsoft YaHei&#39;; font-size: 18px;">中国银行、工商银行和建设银行的借记卡，且必须是本人的。</span></span>
</p>
<p>
    <span style="font-family: 微软雅黑, &#39;Microsoft YaHei&#39;; font-size: 18px;">&nbsp;&nbsp;&nbsp;&nbsp;3、<span style="font-family: 微软雅黑, &#39;Microsoft YaHei&#39;; font-size: 18px;">请确保绑定的银行卡里留有足够的余钱</span>。</span>
</p>
<p>
    <span style="font-family: 微软雅黑, &#39;Microsoft YaHei&#39;; font-size: 18px;">&nbsp;&nbsp;&nbsp;&nbsp;4、<span style="font-family: 微软雅黑, &#39;Microsoft YaHei&#39;; font-size: 18px;">为方便快捷，减轻人工现金充值压力，请尽量采用非现金充值方式。包括网银手机银行的圈存和自动充值</span><span style="font-family: 微软雅黑, &#39;Microsoft YaHei&#39;; font-size: 18px;">。</span></span>
</p>
  <div class="button_02_outside button_color" style="margin-bottom:44px"> 
   <input type="button" onclick="window.history.go(-1);" value="返回" class="button_02"> 
  </div> 
  <jsp:include page="../footer.jsp"> 
   <jsp:param name="source" value="校园卡服务中心" /> 
  </jsp:include>  
 </body>
</html>