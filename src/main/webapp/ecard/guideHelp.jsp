<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="" />
	<meta name="decription" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
	<title></title>		
<link href="/elecFee/css/icon.css" media="screen" rel="stylesheet" type="text/css" />
<link href="/elecFee/css/style1.css" media="screen" rel="stylesheet" type="text/css" />
<link href="/elecFee/css/color_02.css" media="screen" rel="stylesheet" type="text/css" />
<link href="/elecFee/css/sys.css" media="screen" rel="stylesheet" type="text/css" />
</head>
<body>
<jsp:include page="guideHeader.jsp" flush="true">
	<jsp:param name="title" value="帮助" /> 
</jsp:include>
<div class="main_02">
	<p class="help_title_01">校园卡充值指南-帮助</p>
	<p class="help_title_02">介绍：</p>
	<p class="helep_content_01" style="text-indent:2em">该应用只提供移动端的建设银行、工商银行、中国银行使用手机银行对校园卡的圈存方式。如需使用网银，请在校园网环境下登录http://ecard.hust.edu.cn，在菜单使用指南下查看详细流程</p>


	<div class="button_02_outside button_color">
		<input type="button" onclick="window.history.go(-1)" value="返回" class="button_02">
	</div>
</div>


<jsp:include page="../footer.jsp">
	<jsp:param name="source" value="网络与计算中心" /> 
</jsp:include>
</body>
</html>