<%@ page language="java" contentType="text/html; charset=utf-8"%>
<!-- <html style="font-size: 20px;"> -->
<html style="font-size: 23.4375px;" >
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="keywords" content="">
    <meta name="decription" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>2017年华中大校园卡账单</title>
    <style>
    
    @-webkit-keyframes gogogo {
	    0% {
	        -webkit-transform: rotate(0deg);
	    }
	    50% {
	        -webkit-transform: rotate(180deg);
	    }
	    100% {
	        -webkit-transform: rotate(360deg);
	    }
	}
	.param{
		font-weight:bold;
		color:rgb(236, 111, 4);
	}
	img[lazy=loading] {
    	vertical-align:middle;
    	width:50px;
    	height:auto;
  	}
  	.start{
	  	position: absolute;
	    left: 50%;
	    margin-left: -17px;
	    top: 95%;
	    margin-top: -48px;
	    width: 34px;
	    height: 34px;
	    z-index: 99999999999;
  	}
  	.start b{
  		position: absolute;
	    left: 50%;
	    top: 10px;
	    margin-left: -19px;
	    width: 38px;
	    height: 24px;
	    background: url(/elecFee/images/ecard2017/arrow2.png);
	    background-size: 100% 100%;
	    -webkit-animation: start 1.5s infinite ease-in-out;
	    animation: start 1.5s infinite ease-in-out;
  	}
  	@-webkit-keyframes start {
	    0%,30% {
	        opacity: 0.8;
	        -webkit-transform: translate(0,10px);
	    }
	    60% {
	        opacity: 1;
	        -webkit-transform: translate(0,0);
	    }
	    100% {
	        opacity: 0.8;
	        -webkit-transform: translate(0, -5px);
	    }
	}
    </style>
	<link rel="stylesheet" href="/elecFee/css/pageSlider.css">
    <link rel="stylesheet" href="/elecFee/css/ecard2017.css">
<link href='https://cdn.webfont.youziku.com/webfonts/nomal/97360/47073/5a670464f629d90b84eaaf99.css?v=1.1' rel='stylesheet' type='text/css' />
<!-- <script type="text/javascript" src="http://cdn.webfont.youziku.com/wwwroot/js/wf/youziku.api.min.js"></script> -->
</head>
<body style="margin: 0px;font-family:'AaCarriefc7926fb917c50';">
<div id="divBody">
<div class="section sec1">
	<div id="cmp-1" data-tid="1515555131125" class="cmp image " style="width:100%;height:100%;z-index:0;-webkit-animation:;-moz-animation:;animation:;" width="384" height="590">
        <img class="cmp-inner" v-lazy="'../images/ecard2017/1.png'" style="color:black;font-size:1rem;-webkit-transform:rotate(360deg);-moz-transform:rotate(360deg);transform:rotate(360deg);opacity:1;position:absolute;"
        width="384" height="590" crossorigin="anonymous" ondragstart="return false;">
    </div>
    <div id="cmp-2" data-tid="1515554908083" class="cmp text " style="width:78.4375%;height:auto;left:17.1875%;top:19.345238095238095%;z-index:0;-webkit-animation:fadeInDown 1s 0.5s 1 both;-moz-animation:fadeInDown 1s 0.5s 1 both;animation:fadeInDown 1s 0.5s 1 both;">
	    <div class="cmp-inner" style="color: rgb(91, 137, 205);font-size:0.9rem;-webkit-transform:rotate(1deg);-moz-transform:rotate(1deg);transform:rotate(1deg);opacity:1;line-height:0.9rem;">
	        <div style="font-size:0.9rem;color: rgb(245, 91, 46);">
	        	亲爱的 <span class="param">{{xyk.XM}}</span> 
	        	<template v-if="xyk.XB=='男'">小哥哥</template>
	        	<template v-if="xyk.XB=='女'">小姐姐</template>
	        </div>
	    </div>
	</div>
    <div id="cmp-3" data-tid="1515555257798" class="cmp text " style="width:64.375%;height:auto;left:18.4375%;top:27.26624503968254%;z-index:0;-webkit-animation:fadeInDown 1s 0.5s 1 both;-moz-animation:fadeInDown 1s 0.5s 1 both;animation:fadeInDown 1s 0.5s 1 both;">
        <div class="cmp-inner" style="color: rgb(91, 137, 205);font-size:0.9rem;-webkit-transform:rotate(1deg);-moz-transform:rotate(1deg);transform:rotate(1deg);opacity:1;line-height:1.4rem;">
            <div style="font-size: 0.75rem;line-height: 1.55rem;color: rgb(91, 137, 205);">
            	&nbsp; &nbsp; 365个昼夜又从我们紧扣的十指间的缝隙流走，
            	我不想贪婪地苛求我们在一起的每一天都如满月般圆满，
            	我只愿把我在这365天里对你的记忆，
            	用最精致的笔触勾勒出来。
            </div>
        </div>
    </div>
</div>
<div class="section sec2">
    <div id="cmp-4" data-tid="1515555493670" class="cmp image " style="width:100.93749999999999%;height:100%;left:-0.3125%;top:-0.11470734126984128%;z-index:0;-webkit-animation:;-moz-animation:;animation:;" width="378" height="590">
        <img class="cmp-inner" v-lazy="'../images/ecard2017/2.png'" style="color:black;font-size:1rem;opacity:1;position:absolute;"
        width="378" height="590" crossorigin="anonymous" ondragstart="return false;">
    </div>
    <div id="cmp-5" data-tid="1515555526058" class="cmp text " style="width:83.4375%;height:auto;left:22.25%;top:30.83767361111111%;z-index:0;-webkit-animation:fadeInDown 1s 0.5s 1 both;-moz-animation:fadeInDown 1s 0.5s 1 both;animation:fadeInDown 1s 0.5s 1 both;">
        <div class="cmp-inner" style="color:rgb(56, 139, 182);line-height:1.2rem;font-size:0.8rem;">
            <div><span class="param">{{xyk.KHRQ}}</span></div>
            <div>我第一次知晓了</div>
            <div>世人口中的</div>
            <div>“因缘际会”四字何解</div>
        </div>
    </div>
    <div id="cmp-6" data-tid="1515555665070" class="cmp text " style="width:119.0625%;height:auto;left:24%;top:53.456721230158735%;z-index:0;-webkit-animation:fadeInDown 1s 0.5s 1 both;-moz-animation:fadeInDown 1s 0.5s 1 both;animation:fadeInDown 1s 0.5s 1 both;">
        <div class="cmp-inner" style="color:rgb(56, 139, 182);line-height:1rem;font-size:0.8rem;">
            <div>学工号<span class="param">{{xyk.XGH}}</span></div>
            <div>编号<span class="param">{{xyk.ZH}}</span></div>
            <div>你来自<span class="param">{{xyk.DWMC}}</span></div>
            <div>看似毫无关联，但我专属于你</div>
            <div>你是我有且仅有的唯一</div>
        </div>
    </div>
    <div id="cmp-7" data-tid="1515555793570" class="cmp text " style="width:68.75%;height:auto;left:20.1875%;top:77.46465773809523%;z-index:0;-webkit-animation:fadeInDown 1s 0.5s 1 both;-moz-animation:fadeInDown 1s 0.5s 1 both;animation:fadeInDown 1s 0.5s 1 both;">
        <div class="cmp-inner" style="color:rgb(56, 139, 182);line-height:1.4rem;font-size:0.8rem;">
            <template v-if="xyk.SFQC=='开通'">
	            <span>你开通了<span class="param">圈存功能</span></span>
	            <div>曾贫瘠的我</div>
	            <div>因你变得饱满、温软</div>
        	</template>
        	<template v-else>
	            <span>你未开通<span class="param">圈存功能</span></span>
	            <div>新的尝试</div>
	            <div>会让你更深入地了解我</div>
        	</template>
        </div>
    </div>
</div>
<div class="section sec3">
    <div id="cmp-12" data-tid="1515555999980" class="cmp image " style="width:100.93749999999999%;height:100%;left:-0.625%;top:-0.11470734126984128%;z-index:0;-webkit-animation:;-moz-animation:;animation:;" width="378" height="590">
        <img class="cmp-inner" v-lazy="'../images/ecard2017/3.png'" style="color:black;font-size:1rem;opacity:1;position:absolute;"
        width="378" height="590" crossorigin="anonymous" ondragstart="return false;">
    </div>
    <div id="cmp-13" data-tid="1515556149638" class="cmp text " style="width:77.8125%;height:auto;left:11.5625%;top:29.012276785714285%;z-index:0;-webkit-animation:fadeInDown 1s 0.5s 1 both;-moz-animation:fadeInDown 1s 0.5s 1 both;animation:fadeInDown 1s 0.5s 1 both;">
        <div class="cmp-inner" style="color:rgb(91, 137, 205);;font-size:1rem;opacity:1;line-height:1.8rem;">
            <div>我本是冰冷，是铜墙铁壁</div>
            <div>是你耗费了 <span class="param">{{xyk.CZCS}}</span>次充值</div>
            <div>灌注了 <span class="param">{{xyk.CZZE}}</span>元</div>
            <div>才成就了现在的全新的我</div>
            <div>2017年12月31日</div>
            <div>仔细数着存钱罐</div>
            <div>我们<span class="param">{{xyk.ZHYE}}</span>元的跨年积蓄</div>
            <div>应该足以给彼此一份新年礼物了吧</div>
<!--             <div>现在的我不止剩下 <span class="param">{{xyk.ZHYE}}</span>元</div> -->
<!--             <div>(截止统计时间)</div> -->
<!--             <div>还有能望进你眸底的欢喜</div> -->
        </div>
    </div>
</div>
<div class="section sec4">
    <div id="cmp-14" data-tid="1515556341478" class="cmp image " style="width:100.93749999999999%;height:100%;left:-0.625%;top:-0.11470734126984128%;z-index:0;-webkit-animation:;-moz-animation:;animation:;" width="378" height="590">
        <img class="cmp-inner" v-lazy="'../images/ecard2017/4.png'" style="color:black;font-size:1rem;opacity:1;position:absolute;"
        width="378" height="590" crossorigin="anonymous" ondragstart="return false;">
    </div>
    <div id="cmp-15" data-tid="1515556341479" class="cmp text " style="width:86.25%;height:auto;left:7.8125%;top:30.160280257936506%;z-index:0;-webkit-animation:fadeInDown 1s 0.5s 1 both;-moz-animation:fadeInDown 1s 0.5s 1 both;animation:fadeInDown 1s 0.5s 1 both;">
        <div class="cmp-inner" style="color: rgb(91, 137, 205);font-size:1rem;opacity:1;line-height:1.8rem;">
            <div>2017年</div>
            <div>我们被逆流的人潮冲散了<span class="param">{{parseInt(xyk.BBCS)+parseInt(xyk.GSCS)}}</span>次</div>
            <div>有 <span class="param">{{xyk.GSCS}}</span>次你选择挂失</div>
            <div>我听到你内心的自责与内疚</div>
            <div>但我知道意外的分离</div>
            <div>会让我们更好的重逢</div>
            <div>新的一年里，希望你能好好照顾我</div>
            <div>包容我在漫漫长夜里的惶惑迷茫</div>
        </div>
    </div>
</div>
<div class="section sec5">
    <div id="cmp-16" data-tid="1515556501704" class="cmp image " style="width:100.3125%;height:100%;left:-0.3125%;top:-0.11470734126984128%;z-index:0;-webkit-animation:;-moz-animation:;animation:;" width="376" height="590">
        <img class="cmp-inner" src="../images/ecard2017/5.png" style="color:black;font-size:1rem;opacity:1;position:absolute;"
        width="376" height="590" crossorigin="anonymous" ondragstart="return false;">
    </div>
    <div id="cmp-17" data-tid="1515556547486" class="cmp text " style="width:76.5625%;height:auto;left:12.5%;top:16.94878472222222%;z-index:0;-webkit-animation:fadeInDown 1s 0.5s 1 both;-moz-animation:fadeInDown 1s 0.5s 1 both;animation:fadeInDown 1s 0.5s 1 both;">
        <div class="cmp-inner" style="color:rgb(91, 137, 205);font-size:0.9rem;opacity:1;line-height:1.75rem;">
<!--             <div style="text-indent:2em;">如果能把陪你走过的距离用金钱量化，那我想我们一起携手<span class="param">{{xyk.XFCS}}</span>次走过了<span class="param">{{xyk.XFZE}}</span>元的路程，被你握在掌心的次数是<span class="param">{{xyk.LJYKCS}}</span>。</div> -->
<!--             <div style="text-indent:2em;">看看你的生活费都花到哪里去了呀。</div> -->
            <div>2017年里<span class="param">{{xyk.XFCS}}</span>次的消费次数</div>
            <div><span class="param">{{xyk.XFZE}}</span>元的消费总额</div>
<!--             <div>以及我们相遇以来</div> -->
<!--             <div>你<span class="param">{{xyk.LJYKCS}}</span>次的累计用卡次数</div> -->
            <div>都承载着我们</div>
            <div>在倥偬浮生里相聚的记忆</div>
            <div>不知不觉累积</div>
            <div>凝成剔透的琥珀</div>
        </div>
    </div>
</div>
<div class="section sec6">
    <div id="cmp-18" data-tid="1515556744884" class="cmp image " style="width:100.93749999999999%;height:100%;left:-0.625%;top:-0.11470734126984128%;z-index:0;-webkit-animation:;-moz-animation:;animation:;" width="378" height="590">
        <img class="cmp-inner" v-lazy="'../images/ecard2017/6.png'" style="color:black;font-size:1rem;opacity:1;position:absolute;"
        width="378" height="590" crossorigin="anonymous" ondragstart="return false;">
    </div>
    <div id="cmp-19" data-tid="1515556781257" class="cmp text " style="width:94.6875%;height:auto;left:2.5%;top:2.861483134920635%;z-index:0;-webkit-animation:fadeInDown 1s 0.5s 1 both;-moz-animation:fadeInDown 1s 0.5s 1 both;animation:fadeInDown 1s 0.5s 1 both;">
        <div class="cmp-inner" style="color:rgb(56, 139, 182);font-size:1rem;opacity:1;line-height:2rem;">
            <div><span class="param">{{xyk.SCXFSJ}}</span></div>
            <div>初见你是惊鸿一面</div>
            <div>你在<span class="param">{{xyk.SCXFDD}}</span>带着我消费了<span class="param">{{xyk.SCXFJE}}</span>元</div>
            <div>牵手瞬间我心跳如雷</div>
            <div>跌入你温柔漩涡</div>
            <div>我们就这样开启了2017年</div>
        </div>
    </div>
</div>
<div class="section sec7">
    <div id="cmp-20" data-tid="1515556911782" class="cmp image " style="width:100.3125%;height:100%;left:-0.3125%;top:-0.11470734126984128%;z-index:0;-webkit-animation:;-moz-animation:;animation:;" width="376" height="590">
        <img class="cmp-inner" v-lazy="'../images/ecard2017/7.png'" style="color:black;font-size:1rem;opacity:1;position:absolute;"
        width="376" height="590" crossorigin="anonymous" ondragstart="return false;">
    </div>
    <div id="cmp-21" data-tid="1515556936485" class="cmp text " style="width:84.6875%;height:auto;left:8.595390319824219%;top:6.234499007936508%;z-index:0;-webkit-animation:fadeInDown 1s 0.5s 1 both;-moz-animation:fadeInDown 1s 0.5s 1 both;animation:fadeInDown 1s 0.5s 1 both;">
        <div class="cmp-inner" style="color: rgb(91, 137, 205);font-size:0.9rem;opacity:1;line-height:1.4rem;">
            <div style="text-indent:2em;">你包容我的任性、我的迷糊、我的懵懂，把我的朋友当成是自己的朋友。
            	这一年，你 <span class="param">{{xyk.CZXYWCS}}</span> 次充实了我的小伙伴校园网，
            	为它投入了<span class="param">{{xyk.CZXYW}}</span>元。
            </div>
        </div>
    </div>
    <div id="cmp-22" data-tid="1515557078454" class="cmp text " style="width:84.6875%;height:auto;left:8.909530639648438%;top:34.409102182539684%;z-index:0;-webkit-animation:fadeInDown 1s 0.5s 1 both;-moz-animation:fadeInDown 1s 0.5s 1 both;animation:fadeInDown 1s 0.5s 1 both;">
        <div class="cmp-inner" style="color: rgb(91, 137, 205);font-size:0.9rem;opacity:1;line-height:1.4rem;">
            <div style="text-indent:2em;">东来西往，又复南行，
            	你带着我辗转<span class="param">{{xyk.XCXFZE}}</span>次往返在这片好看的森林之海里，
            	车票钱<span class="param">{{xyk.XCXFZE}}</span>元，你说：“你坐好，钱我来付。”
            	每每这时，我总是微笑摆首，总是忍不住想向全世界炫耀你的温柔，
            	炫耀我能遇见你是何其有幸。
            </div>
        </div>
    </div>
</div>
<div class="section sec8">
    <div id="cmp-23" data-tid="1515559464645" class="cmp image " style="width:100.93749999999999%;height:100%;left:-0.625%;top:-0.0992063492063492%;z-index:0;-webkit-animation:;-moz-animation:;animation:;" width="378" height="590">
        <img class="cmp-inner" src="../images/ecard2017/8.png" style="color:black;font-size:1rem;opacity:1;position:absolute;"
        width="378" height="590" crossorigin="anonymous" ondragstart="return false;">
    </div>
    <div id="cmp-24" data-tid="1515559511540" class="cmp text " style="width:81.5625%;height:auto;left:32.625%;top:5.75%;z-index:0;-webkit-animation:fadeInDown 1s 0.5s 1 both;-moz-animation:fadeInDown 1s 0.5s 1 both;animation:fadeInDown 1s 0.5s 1 both;">
        <div class="cmp-inner" style="color: rgb(91, 137, 205);font-size:1.4rem;opacity:1;line-height:1.5rem;">
            <div>各食堂消费</div>
        </div>
    </div>
    <div id="stChart" style="width:100%;position:absolute;top:14.6%;left:2.5%"></div>
</div>
<div class="section sec9">
    <div id="cmp-23" data-tid="1515559464645" class="cmp image " style="width:100.93749999999999%;height:100%;left:-0.625%;top:-0.0992063492063492%;z-index:0;-webkit-animation:;-moz-animation:;animation:;" width="378" height="590">
        <img class="cmp-inner" v-lazy="'../images/ecard2017/9.png'" style="color:black;font-size:1rem;opacity:1;position:absolute;"
        width="378" height="590" crossorigin="anonymous" ondragstart="return false;">
    </div>
    <div id="cmp-24" data-tid="1515559800472" class="cmp text " style="height:auto;left:21.375%;top:25.51190476190477%;z-index:0;-webkit-animation:fadeInDown 1s 0.5s 1 both;-moz-animation:fadeInDown 1s 0.5s 1 both;animation:fadeInDown 1s 0.5s 1 both;">
        <div class="cmp-inner" style="color: rgb(91, 137, 205);font-size:1.2rem;opacity:1;line-height:1.3rem;">
            <div>最受师生欢迎的食堂</div>
        </div>
    </div>
    <div id="cmp-25" data-tid="1515559800472" class="cmp text " style="height:auto;left:39.375%;top:52.51190476190477%;z-index:0;-webkit-animation:fadeInDown 1s 0.5s 1 both;-moz-animation:fadeInDown 1s 0.5s 1 both;animation:fadeInDown 1s 0.5s 1 both;">
        <div class="cmp-inner" style="color: rgb(91, 137, 205);font-size:1rem;opacity:1;line-height:1.3rem;">
            <div>东一食堂</div>
        </div>
    </div>
    <div id="cmp-26" data-tid="1515559800472" class="cmp text " style="height:auto;left:16.375%;top:57.51190476190477%;z-index:0;-webkit-animation:fadeInDown 1s 0.5s 1 both;-moz-animation:fadeInDown 1s 0.5s 1 both;animation:fadeInDown 1s 0.5s 1 both;">
        <div class="cmp-inner" style="color: rgb(91, 137, 205);font-size:1rem;opacity:1;line-height:1.3rem;">
            <div>西一食堂</div>
        </div>
    </div>
    <div id="cmp-26" data-tid="1515559800472" class="cmp text " style="height:auto;left:61.375%;top:59.51190476190477%;z-index:0;-webkit-animation:fadeInDown 1s 0.5s 1 both;-moz-animation:fadeInDown 1s 0.5s 1 both;animation:fadeInDown 1s 0.5s 1 both;">
        <div class="cmp-inner" style="color: rgb(91, 137, 205);font-size:1rem;opacity:1;line-height:1.3rem;">
            <div>韵苑食堂</div>
        </div>
    </div>
</div>
<div class="section sec10">
    <div id="cmp-28" data-tid="1515559925073" class="cmp image " style="width:100.62500000000001%;height:100%;left:-0.625%;top:-0.2976190476190476%;z-index:0;-webkit-animation:;-moz-animation:;animation:;" width="377" height="590">
        <img class="cmp-inner" v-lazy="'../images/ecard2017/10.png'" style="color:black;font-size:1rem;opacity:1;position:absolute;"
        width="377" height="590" crossorigin="anonymous" ondragstart="return false;">
    </div>
    <div id="cmp-29" data-tid="1515559947383" class="cmp text " style="width:76.875%;height:auto;left:12.812499999999998%;top:19.940476190476193%;z-index:0;-webkit-animation:fadeInDown 1s 0.5s 1 both;-moz-animation:fadeInDown 1s 0.5s 1 both;animation:fadeInDown 1s 0.5s 1 both;">
        <div class="cmp-inner" style="color: rgb(91, 137, 205);font-size:0.9rem;opacity:1;line-height:1.3rem;">
            <div style="text-indent:2em;">我们一起去过<span class="param">{{xyk.STXFCS}}</span>次食堂，</div>
            <div><span class="param">{{xyk.STSL}}</span>个食堂都有我们俩的足迹。
            	这一年你一共吃掉了<span class="param">{{xyk.STXFZE}}</span>元的美食，
            	但吃了<span class="param">{{xyk.DBZDSTJE}}</span>元的那一次，应该是一顿饕餮盛宴吧。
            </div>
        </div>
    </div>
    <div id="cmp-30" data-tid="1515560071913" class="cmp text " style="width:87.1875%;height:auto;left:9.6875%;top:58.82936507936508%;z-index:0;-webkit-animation:fadeInDown 1s 0.5s 1 both;-moz-animation:fadeInDown 1s 0.5s 1 both;animation:fadeInDown 1s 0.5s 1 both;">
        <div class="cmp-inner" style="color: rgb(91, 137, 205);font-size:0.9rem;opacity:1;line-height:1rem;">
            <div style="text-indent:4em;"><span class="param">{{xyk.ZZXFRQ}}</span></div>
            <div>你在<span class="param">{{xyk.ZZXFSJ}}</span>就踏入了食堂</div>
        </div>
    </div>
    <div id="cmp-31" data-tid="1515560266896" class="cmp text " style="width:87.1875%;height:auto;left:9.6875%;top:67.15476190476191%;z-index:0;-webkit-animation:fadeInDown 1s 0.5s 1 both;-moz-animation:fadeInDown 1s 0.5s 1 both;animation:fadeInDown 1s 0.5s 1 both;">
        <div class="cmp-inner" style="color: rgb(91, 137, 205);font-size:0.9rem;opacity:1;line-height:1.2rem;">
            <div style="text-indent:2em;"><span class="param">{{xyk.ZWXFRQ}}</span></div>
            <div>你在<span class="param">{{xyk.ZWXFSJ}}</span>才离开食堂</div>
        </div>
    </div>
    <div id="cmp-32" data-tid="1515560338854" class="cmp text " style="width:63.74999999999999%;height:auto;left:8.75%;top:77.85317460317461%;z-index:0;-webkit-animation:fadeInDown 1s 0.5s 1 both;-moz-animation:fadeInDown 1s 0.5s 1 both;animation:fadeInDown 1s 0.5s 1 both;">
        <div class="cmp-inner" style="color: rgb(91, 137, 205);font-size:0.9rem;opacity:1;line-height:1.2rem;">
            <div style="text-indent:2em;">我知道每个人的饮食</div>
            <div style="text-indent:2em;">习惯都不同</div>
            <div style="text-indent:2em;">但我依然希望你</div>
            <div style="text-indent:2em;">好好照顾自己</span></div>
        </div>
    </div>
</div>
<div class="section sec11">
    <div id="cmp-33" data-tid="1515560569376" class="cmp image " style="width:100.93749999999999%;height:100%;left:-0.625%;top:-0.0992063492063492%;z-index:0;-webkit-animation:;-moz-animation:;animation:;" width="378" height="590">
        <img class="cmp-inner" v-lazy="'../images/ecard2017/11.png'" style="color:black;font-size:1rem;opacity:1;position:absolute;"
        width="378" height="590" crossorigin="anonymous" ondragstart="return false;">
    </div>
    <div id="cmp-34" data-tid="1515560588336" class="cmp text " style="width:83.75%;height:auto;left:6.25%;top:5.257936507936508%;z-index:0;-webkit-animation:fadeInDown 1s 0.5s 1 both;-moz-animation:fadeInDown 1s 0.5s 1 both;animation:fadeInDown 1s 0.5s 1 both;">
        <div class="cmp-inner" style="color: rgb(56, 139, 182);font-size:0.9rem;opacity:1;line-height:1.4rem;">
            <div>我很爱和你手挽手逛超市</div>
            <div>超市里的满目琳琅总让我兴奋</div>
            <div>但说到底</div>
            <div>我喜欢的是和你在一起逛超市时</div>
           	<div>自己身上染上的凡间烟火气息</div>
        </div>
    </div>
    <div id="cmp-35" data-tid="1515560678684" class="cmp text " style="width:83.75%;height:auto;left:6.25%;top:35.61507936507937%;z-index:0;-webkit-animation:fadeInDown 1s 0.5s 1 both;-moz-animation:fadeInDown 1s 0.5s 1 both;animation:fadeInDown 1s 0.5s 1 both;">
        <div class="cmp-inner" style="color: rgb(56, 139, 182);;font-size:0.9rem;opacity:1;line-height:1.4rem;">
            <div>这一年</div>
            <div>我们在超市里买了<span class="param">{{xyk.CSXFZE}}</span>元的东西</div>
            <div>你为我刷了<span class="param">{{xyk.CSSKCS}}</span>次卡</div>
            <div>我们并肩走过 <span class="param">{{xyk.CSSL}}</span>个超市</div>
            <div>可你总偏爱 <span class="param">{{xyk.XFZDCS}}</span></div>
            <div>在那里你一共买了 <span class="param">{{xyk.XFZDCSJE}}</span>元的商品&nbsp;</div>
        </div>
    </div>
</div>
<div class="section sec12">
    <div id="cmp-36" data-tid="1515560792075" class="cmp image " style="width:100.3125%;height:100%;left:-0.3125%;top:-0.0992063492063492%;z-index:0;-webkit-animation:;-moz-animation:;animation:;" width="376" height="590">
        <img class="cmp-inner" v-lazy="'../images/ecard2017/12.png'" style="color:black;font-size:1rem;opacity:1;position:absolute;"
        width="376" height="590" crossorigin="anonymous" ondragstart="return false;">
    </div>
    <div id="cmp-37" data-tid="1515560824136" class="cmp text " style="width:70.625%;height:auto;left:15.625%;top:13.392857142857142%;z-index:0;-webkit-animation:fadeInDown 1s 0.5s 1 both;-moz-animation:fadeInDown 1s 0.5s 1 both;animation:fadeInDown 1s 0.5s 1 both;">
        <div class="cmp-inner" style="color: rgb(91, 137, 205);font-size:0.9rem;opacity:1;line-height:1.9rem;">
            <div style="text-indent:2em;">我知道你跌跌撞撞，挨过生活不少耳光，
            	那就对自己好一些吧，你喜欢的东西，我来付就好，算是你带着我旅行的回礼。
            </div>
            <div style="text-indent:2em;">你在自助售货机刷过<span class="param">{{xyk.ZZSHJXFCS}}</span>次卡，
            	买了<span class="param">{{xyk.ZZSHJXFZE}}</span>元的东西，它是我们的新朋友，以后我们别忘了多多找它玩哦。
            </div>
        </div>
    </div>
</div>
<div class="section sec13">
	<template v-if="xyk.TSFKCS!='0'">
	    <div id="cmp-39" data-tid="1515561449419" class="cmp image " style="width:100.93749999999999%;height:100%;left:-0.3125%;top:-0.0992063492063492%;z-index:0;-webkit-animation:;-moz-animation:;animation:;" width="378" height="590">
	        <img class="cmp-inner" src="../images/ecard2017/13.png" style="color:black;font-size:1rem;opacity:1;position:absolute;"
	        width="378" height="590" crossorigin="anonymous" ondragstart="return false;">
	    </div>
	    <div id="cmp-40" data-tid="1515561116060" class="cmp text " style="width:85.3125%;height:auto;left:8.4375%;top:11%;z-index:0;-webkit-animation:fadeInDown 1s 0.5s 1 both;-moz-animation:fadeInDown 1s 0.5s 1 both;animation:fadeInDown 1s 0.5s 1 both;">
	        <div class="cmp-inner" style="color: rgb(91, 137, 205);font-size:0.9rem;opacity:1;line-height:1.75rem;">
	            <div style="text-indent:2em;">你曾经笑我有点笨，有点胖，有点路痴，
	            	可我也知道你的小糗事哦，
	            	不过我才不会把某人在图书馆借的书超期了得交罚款这种事告诉别人听呢。
	            </div>
	            <div style="text-indent:2em;">这一年，你的总图书罚款是<span class="param">{{xyk.TSFK}}</span>元，
	            	图书罚款次数为<span class="param">{{xyk.TSFKCS}}</span>次，最大一笔
	            	罚款金额为<span class="param">{{xyk.TSFKZDJE}}</span>元。新的一年，要记得按时还书哦。
	            </div>
	        </div>
	    </div>
	</template>
	<template v-else>
		<div id="cmp-42" data-tid="1515561529384" class="cmp image " style="width:100.93749999999999%;height:100%;left:-0.3125%;top:-0.0992063492063492%;z-index:0;-webkit-animation:;-moz-animation:;animation:;" width="378" height="590">
	        <img class="cmp-inner" src="../images/ecard2017/13.png" style="color:black;font-size:1rem;opacity:1;position:absolute;"
	        width="378" height="590" crossorigin="anonymous" ondragstart="return false;">
	    </div>
	    <div id="cmp-43" data-tid="1515561529385" class="cmp text " style="width:85.3125%;height:auto;left:8.4375%;top:10.813492063492063%;z-index:0;-webkit-animation:fadeInDown 1s 0.5s 1 both;-moz-animation:fadeInDown 1s 0.5s 1 both;animation:fadeInDown 1s 0.5s 1 both;">
	        <div class="cmp-inner" style="color: rgb(91, 137, 205);font-size:0.9rem;opacity:1;line-height:1.8rem;">
	            <div>我喜欢你的守时和有效率</div>
	            <div>不管是和谁的约定</div>
	            <div>到期前一定会认真完成</div>
	            <div>这一年你没有因超期归还图书</div>
	            <div>而被罚款</div>
	            <div>我相信</div>
	            <div>下一年也不会有的</div>
	            <div>对吧</div>
	        </div>
	    </div>
	</template>
</div>
<div class="section sec14">
	<template v-if="xyk.DPCCZCS=='0' || xyk.DPCCZCS==''">
	    <div id="cmp-44" data-tid="1515562693923" class="cmp image " style="width:101.875%;height:100%;left:-1.5625%;top:0.0992063492063492%;z-index:0;-webkit-animation:;-moz-animation:;animation:;" width="382" height="590">
	        <img class="cmp-inner" v-lazy="'../images/ecard2017/14.png'" style="color:black;font-size:1rem;opacity:1;position:absolute;"
	        width="382" height="590" crossorigin="anonymous" ondragstart="return false;">
	    </div>
	    <div id="cmp-45" data-tid="1515562721955" class="cmp image " style="width:26.5625%;height:7.738095238095238%;left:10.9375%;top:77.67857142857143%;z-index:0;-webkit-animation:;-moz-animation:;animation:;" width="99" height="41">
	        <img class="cmp-inner" v-lazy="'../images/ecard2017/yun1.png'" style="color:black;font-size:1rem;opacity:1;position:absolute;"
	        width="99" height="41" crossorigin="anonymous" ondragstart="return false;">
	    </div>
	    <div id="cmp-46" data-tid="1515562746418" class="cmp image " style="width:22.5%;height:7.738095238095238%;left:10.9375%;top:43.948412698412696%;z-index:0;-webkit-animation:;-moz-animation:;animation:;" width="84" height="41">
	        <img class="cmp-inner" v-lazy="'../images/ecard2017/yun2.png'" style="color:black;font-size:1rem;opacity:1;position:absolute;"
	        width="84" height="41" crossorigin="anonymous" ondragstart="return false;">
	    </div>
	    <div id="cmp-47" data-tid="1515562771155" class="cmp image " style="width:80%;height:8.333333333333332%;left:10.9375%;top:55.05952380952381%;z-index:0;-webkit-animation:;-moz-animation:;animation:;" width="300" height="47">
	        <img class="cmp-inner" v-lazy="'../images/ecard2017/yun3.png'" style="color:black;font-size:1rem;opacity:1;position:absolute;"
	        width="300" height="47" crossorigin="anonymous" ondragstart="return false;">
	    </div>
	    <div id="cmp-48" data-tid="1515562789750" class="cmp image " style="width:80%;height:8.333333333333332%;left:10.9375%;top:65.97222222222221%;z-index:0;-webkit-animation:;-moz-animation:;animation:;" width="300" height="47">
	        <img class="cmp-inner" v-lazy="'../images/ecard2017/yun3.png'" style="color:black;font-size:1rem;opacity:1;position:absolute;"
	        width="300" height="47" crossorigin="anonymous" ondragstart="return false;">
	    </div>
	    <div id="cmp-49" data-tid="1515562400560" class="cmp text " style="width:88.125%;height:auto;left:13.4375%;top:42.55952380952381%;z-index:0;-webkit-animation:fadeInDown 1s 0.5s 1 both;-moz-animation:fadeInDown 1s 0.5s 1 both;animation:fadeInDown 1s 0.5s 1 both;">
	        <div class="cmp-inner" style="color: rgb(91, 137, 205);font-size:0.9rem;opacity:1;line-height:3.3rem;">
	            <div>这一年</div>
	            <div>你没有在充电桩为电动车充过电，</div>
	            <div>或许明年你会入手一辆小电驴带</div>
	            <div>带我去兜风吗？</div>
	        </div>
	    </div>
    </template>
    <template v-else>
	    <div id="cmp-50" data-tid="1515562844425" class="cmp image " style="width:101.875%;height:100%;left:-1.5625%;top:0.0992063492063492%;z-index:0;-webkit-animation:;-moz-animation:;animation:;" width="382" height="590">
	        <img class="cmp-inner" v-lazy="'../images/ecard2017/14.png'" style="color:black;font-size:1rem;opacity:1;position:absolute;"
	        width="382" height="590" crossorigin="anonymous" ondragstart="return false;">
	    </div>
	    <div id="cmp-51" data-tid="1515562848790" class="cmp text " style="width:81.25%;height:auto;left:9.6875%;top:36.21031746031746%;z-index:0;-webkit-animation:fadeInDown 1s 0.5s 1 both;-moz-animation:fadeInDown 1s 0.5s 1 both;animation:fadeInDown 1s 0.5s 1 both;">
	        <div class="cmp-inner" style="color: rgb(91, 137, 205);font-size:0.9rem;opacity:1;line-height:1.4rem;">
	            <div style="text-indent:2em;">我喜欢坐在你的电驴后座，
	            	看周围的树影飞速倒退，风刮在脸颊，
	            	吹乱了头发。你给电动车充过<span class="param">{{xyk.DPCCZCS}}</span>次电，
	            	每次都嘟囔着是因为我太重才那么快就把电耗没了。
	            	我偷看了你的账单，
	            	2017年你总共才为电瓶车充电花了<span class="param">{{xyk.DPCCDZJE}}</span>元，
	            	我只是有点胖，才不重呢。
	            </div>
	        </div>
	    </div>
    </template>
</div>
<div class="section sec15">
    <div id="cmp-52" data-tid="1515562971753" class="cmp image " style="width:100.93749999999999%;height:100%;left:-0.625%;top:-0.0992063492063492%;z-index:0;-webkit-animation:;-moz-animation:;animation:;" width="378" height="590">
        <img class="cmp-inner" v-lazy="'../images/ecard2017/15.png'" style="color:black;font-size:1rem;opacity:1;position:absolute;"
        width="378" height="590" crossorigin="anonymous" ondragstart="return false;">
    </div>
    <div id="cmp-53" data-tid="1515562991318" class="cmp text " style="width:71.25%;height:auto;left:15.625%;top:14.607142857142858%;z-index:0;-webkit-animation:fadeInDown 1s 0.5s 1 both;-moz-animation:fadeInDown 1s 0.5s 1 both;animation:fadeInDown 1s 0.5s 1 both;">
        <div class="cmp-inner" style="color: rgb(91, 137, 205);font-size:1rem;opacity:1;line-height:1.8rem;">
            
            <div style="text-indent:2em;">这一年，我忍着对来苏水的不喜，
            	<span class="param">{{xyk.XYYCS}}</span>次陪你去校医院挂号看病，
            	为你交了<span class="param">{{xyk.XYYJE}}</span>元医药费，
            	我愿你能在新的一年平安喜乐，身体健康，
            	你生病憔悴时的蹙眉太让人心疼。
			</div>
        </div>
    </div>
</div>
<div class="section sec16">
    <div id="cmp-54" data-tid="1515563124274" class="cmp image " style="width:100.62500000000001%;height:100%;left:-0.625%;top:0.0992063492063492%;z-index:0;-webkit-animation:;-moz-animation:;animation:;" width="377" height="590">
        <img class="cmp-inner" src="../images/ecard2017/16.png" style="color:black;font-size:1rem;opacity:1;position:absolute;"
        width="377" height="590" crossorigin="anonymous" ondragstart="return false;">
    </div>
    <div id="cmp-55" data-tid="1515563143450" class="cmp text " style="width:72.875%;height:auto;left:14.374999999999998%;top:28.25%;z-index:0;-webkit-animation:fadeInDown 1s 0.5s 1 both;-moz-animation:fadeInDown 1s 0.5s 1 both;animation:fadeInDown 1s 0.5s 1 both;">
        <div class="cmp-inner" style="color: rgb(91, 137, 205);font-size:0.9rem;opacity:1;line-height:1.35rem;">
            <div><span class="param">{{xyk.ZDXFSJ}}</span>对你来说是不是很重要，
            	那一天你在<span class="param">{{xyk.ZDXFDD}}</span>买了<span class="param">{{xyk.ZDXFJE}}</span>元的东西，
            	如果是很重要的日子，那明年的那一天，无论是阳光还是雨水倾泻下来，
            	我都希望还能和你在一起。
            </div>
        </div>
    </div>
</div>
<div class="section sec17">
    <div id="cmp-56" data-tid="1515563275710" class="cmp image " style="width:100.62500000000001%;height:100%;left:-0.3125%;top:0.0992063492063492%;z-index:0;-webkit-animation:;-moz-animation:;animation:;" width="377" height="590">
        <img class="cmp-inner" v-lazy="'../images/ecard2017/17.png'" style="color:black;font-size:1rem;opacity:1;position:absolute;"
        width="377" height="590" crossorigin="anonymous" ondragstart="return false;">
    </div>
    <div id="cmp-57" data-tid="1515563308327" class="cmp text " style="width:88.5625%;height:auto;left:6.25%;top:9.325396825396826%;z-index:0;-webkit-animation:fadeInDown 1s 0.5s 1 both;-moz-animation:fadeInDown 1s 0.5s 1 both;animation:fadeInDown 1s 0.5s 1 both;">
        <div class="cmp-inner" style="color: rgb(77, 82, 148);font-size:0.9rem;opacity:1;line-height:1.4rem;">
            <div>水仙盛开的一月，你消费了<span class="param">{{xyk.Y1}}</span>元</div>
            <div>茶花盼月的二月，你消费了<span class="param">{{xyk.Y2}}</span>元</div>
            <div>桃花娇艳的三月，你消费了<span class="param">{{xyk.Y3}}</span>元</div>
            <div>蔷薇绽放的四月，你消费了<span class="param">{{xyk.Y4}}</span>元</div>
            <div>栀子花开的五月，你消费了<span class="param">{{xyk.Y5}}</span>元</div>
            <div>荷花初绽的六月，你消费了<span class="param">{{xyk.Y6}}</span>元</div>
            <div>菱花青涩的七月，你消费了<span class="param">{{xyk.Y7}}</span>元</div>
            <div>桂花香起的八月，你消费了<span class="param">{{xyk.Y8}}</span>元</div>
            <div>菊花渐黄的九月，你消费了<span class="param">{{xyk.Y9}}</span>元</div>
            <div>芙蓉鲜嫩的十月，你消费了<span class="param">{{xyk.Y10}}</span>元</div>
            <div>芦花白茫的十一月，你消费了<span class="param">{{xyk.Y11}}</span>元</div>
            <div>腊梅顽强的十二月，你消费了<span class="param">{{xyk.Y12}}</span>元</div>   
        	<div>在我心里，每一朵花，都长成了你的模样。</div>
        </div>
    </div>
</div>
<div class="section sec18">
    <div id="cmp-60" data-tid="1515563701044" class="cmp image " style="width:100.62500000000001%;height:100%;left:-0.3125%;top:0.0992063492063492%;z-index:0;-webkit-animation:;-moz-animation:;animation:;" width="377" height="590">
        <img class="cmp-inner" v-lazy="'../images/ecard2017/18.png'" style="color:black;font-size:1rem;opacity:1;position:absolute;"
        width="377" height="590" crossorigin="anonymous" ondragstart="return false;">
    </div>
    <div id="cmp-61" data-tid="1515563701045" class="cmp text " style="width:90.3125%;height:auto;left:7.8125%;top:8.234126984126984%;z-index:0;-webkit-animation:fadeInDown 1s 0.5s 1 both;-moz-animation:fadeInDown 1s 0.5s 1 both;animation:fadeInDown 1s 0.5s 1 both;">
        <div class="cmp-inner" style="color: rgb(77, 82, 148);font-size:0.9rem;opacity:1;line-height:1.4rem;">
            <div>请屏住呼吸</div>
            <div>2018年</div>
            <div>我为你准备了一份新年惊喜</div>
            <div>进入华中大微校园</div>
            <div>你可以找到一个全新的我</div>
            <div>以后你去超市等地方</div>
            <div>即使你忘了带上我</div>
            <div>只要掏出手机</div>
            <div>使用微校园里的<span class="param">校园卡扫码支付</span>功能</div>
            <div>问题便迎刃而解了</div>
            <div>我在变得更好而努力着</div>
            <div>所以请原谅我有些迟来的新年祝福</div>
            <div>希望你每一天都快乐</div>
            <div>更希望你是因我而快乐</div>
        </div>
    </div>
</div>
<div class="section sec19">
    <div id="cmp-62" data-tid="1515563857313" class="cmp image " style="width:101.25%;height:100.39682539682539%;left:-0.3125%;top:0.0992063492063492%;z-index:0;-webkit-animation:;-moz-animation:;animation:;" width="379" height="590">
        <img class="cmp-inner" v-lazy="'../images/ecard2017/19.png'" style="color:black;font-size:1rem;opacity:1;position:absolute;"
        width="379" height="590" crossorigin="anonymous" ondragstart="return false;">
    </div>
    <div id="cmp-63" data-tid="1515563884731" class="cmp text " style="width:56.25%;height:auto;left:21.875%;top:25.892857142857146%;z-index:0;-webkit-animation:fadeInDown 1s 0.5s 1 both;-moz-animation:fadeInDown 1s 0.5s 1 both;animation:fadeInDown 1s 0.5s 1 both;">
        <div class="cmp-inner" style="color: rgb(91, 137, 205);font-size:0.8rem;opacity:1;line-height:1.3rem;">
            <div>
            	&nbsp; &nbsp; 这封小小的情书，
            	记录的是从2017年1月1日到2017年12月31日里，
            	我们的缱绻时光。情长纸短，就此搁笔。
            	想捕捉所有的萤火，释放在这片森林之海，
            	看你在绚丽的舞台上绽放和盛开，
            	2018年，由你来书写我们全新的篇章。
            </div>
        </div>
    </div>
    <div id="cmp-64" data-tid="1515563884732" class="cmp text " style="width:56.25%;height:auto;left:59.875%;top:64.892857142857146%;z-index:0;-webkit-animation:fadeInDown 1s 0.5s 1 both;-moz-animation:fadeInDown 1s 0.5s 1 both;animation:fadeInDown 1s 0.5s 1 both;">
        <div class="cmp-inner" style="color: rgb(91, 137, 205);font-size:0.7rem;opacity:1;line-height:1.15rem;">
            <div style="padding-left:20%">
            	导演：詹必胜
            </div>
            <div style="padding-left:20%">
            	编剧：熊倩艺
            </div>
            <div>
            	技术支持：网络中心
            </div>
        </div>
    </div>
</div>
</div>
	<div><span class="start"><b></b></span></div>
    <div id="music" style="position: absolute; top: 10px; right: 10px; width: 41px; height: 41px;">
        <img id="yinbiao" src="/elecFee/images/ecard/yinbiao.png" style="-webkit-animation: gogogo 2s infinite linear" />
        <audio id="musicaudio" class="audio" loop="" autoplay="autoplay">
            <source src="c400004cfoi52viyxg.m4a">
<!--             <source src="zbsxr.m4a"> -->
        </audio>
        <div id="control" style="background: url(/elecFee/images/ecard/music_c0fda01.gif); width: 60px; height: 60px; margin-top: -50px; right: -10px; position: absolute;"></div>
    </div>
<script src="/elecFee/js/vue.js"></script>
<script src="/elecFee/js/vue-lazyload.js"></script>
<script src="/elecFee/js/zepto.js"></script>
<script src="/elecFee/js/pageSlider.js"></script>
<script src="/elecFee/js/jquery.js"></script>
<script src="/elecFee/js/highcharts.js"></script>
<script src="/elecFee/js/exporting.js"></script>
<script src="https://res.wx.qq.com/open/js/jweixin-1.2.0.js" type="text/javascript"></script>
<script src="/elecFee/js/resetAndroidFontsize.js" type="text/javascript"></script>
<script>
$.post("/elecFee/addCount.view",{videoName:"xyk2017"});
ElecFee.wxShare({
	title: '校园卡2017小情书',
	desc: '雪花是时间写给冬天的情书，片片晶莹都是倾诉;这是我写给你的情书，点点记忆都是幸福',
	link: window.location.href,
	imgUrl: 'https://m.hust.edu.cn/elecFee/images/ecard2017/xyk_logo.png'
});
//苹果手机没法autoplay
document.addEventListener("WeixinJSBridgeReady", function () {  
	document.getElementById("musicaudio").play();
}, false);  
//懒加载
Vue.use(VueLazyload, {
	error: '/elecFee/images/img_error.png',//这个是请求失败后显示的图片
// 	loading: '/elecFee/images/loading-1.gif',//这个是加载的loading过渡效果
	try: 2 // 这个是加载图片数量
});
var db=new Vue({
	el:"#divBody",
	mounted:function(){
		var height=window.innerHeight;
		var width=window.innerWidth;
		//18:9手机
		if(height/width>1.62){
			$("html").css("font-size",height/30)
		//16:9手机
		}else{
			$("html").css("font-size",height/28);
		}
		var that=this;
		$.post("xyk2017.do",function(data){
			that.xyk=data;
			var diningData=[];
			for(var i=0;i<data.stList.length;i++){
				diningData[i]=[data.stList[i].MC,data.stList[i].JE];
			}
			that.xyk.dinings=diningData;
			/* Vue.nextTick(function(){
				$youziku.load("#divBody", "59408eb4de564629b0b774a3ec80b013", "AaCarrie");
				$youziku.draw();
				$youziku.submit("#divBody");
			}); */
			$(".param").each(function(e){
				var size=$(this).css("font-size");
				
				$(this).css("font-size",size*1.4);
			});
		},"json");
		
	},
	methods:{
		
	},
	data:{
		xyk:{},
	}
});
//数据
var pageSlider = PageSlider.instance({
    startPage: 1,
    loop: true,
    translate3d: true,
    callback: function(page, direction) {
    	if (page==4){
    		if(db.xyk.BBCS=='0'&&db.xyk.GSCS=='0'){
	    		if(direction=="prev")
	    			pageSlider.go(3);
	    		else
	    			pageSlider.go(5);
    		}
    	}
    	if (page==7){
    		if(db.xyk.CZXYWCS=='0'&&db.xyk.XCXFZE=='0'){
	    		if(direction=="prev")
	    			pageSlider.go(6);
	    		else
	    			pageSlider.go(8);
    		}
    	}
        if (page == 8) {
        	$('#stChart').highcharts({
        		chart: {
                    type: 'column',
                    backgroundColor: {
                        linearGradient: [0, 0, 500, 500],
                        stops: [
                            [0, 'rgb(255, 255, 255)'],
                            [1, 'rgb(144, 227, 255)']
                        ]
                    },
                   	borderRadius: 30,
                   	width:window.innerWidth*0.95,
                },
                title: {
        			text: '',
        			style: {
        				fontWeight:'bold',
        				color: '#7B6159',
        				fontFamily: 'DroidSans817c0905317c50'
        			}
                },
        		plotOptions: {
        			series: {
        				color: '#ff5722',
                     }
        		},
        		xAxis: {
        			type: 'category',
        			labels: {
        				rotation: -45,
        				style: {
        					fontSize: '11px',
        					fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    title: {
                        text: '消费金额（元）'
                    },
                	labels: false
                },
                legend: {
                    enabled: false
                },
                series: [{
                    name: '消费',
                    data: 
                    	db.xyk.dinings,
//                     [
//                         ['西一食堂', 280],
//                         ['西二食堂', 555],
//                         ['百景园', 333],
//                         ['喻园', 444],
//                         ['其他', 390]
//                     ],
        			dataLabels: {
                        enabled: true,
                        color: '#000000',
                        align: 'right',
                        format: '{point.y:.1f}', // one decimal
                        x:10,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                }],
                credits: {
                    enabled:false
                },
                exporting: {
                    enabled:false
                }
            });
        }
        if(page==12){
        	if(db.xyk.ZZSHJXFCS=='0'){
        		if(direction=="prev")
	    			pageSlider.go(11);
	    		else
	    			pageSlider.go(13);
        	}
        }
        if(page==15){
        	if(db.xyk.XYYCS=='0'){
        		if(direction=="prev")
	    			pageSlider.go(14);
	    		else
	    			pageSlider.go(16);
        	}
        }
    }
});
$("#control").click(function() {
    if (document.getElementById("musicaudio").paused) {
        document.getElementById("musicaudio").play();
        $("#yinbiao").css("-webkit-animation", "gogogo 2s infinite linear");
        $("#control").css("background", "url(/elecFee/images/ecard/music_c0fda01.gif)");
    } else {
        document.getElementById("musicaudio").pause();
        $("#yinbiao").css("-webkit-animation", "");
        $("#control").css("background", "");
    }
});
</script>
</body>

</html>