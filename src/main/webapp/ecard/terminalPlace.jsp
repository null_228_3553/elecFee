<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="" />
	<meta name="decription" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
	<title></title>		
<link href="/elecFee/css/icon.css" media="screen" rel="stylesheet" type="text/css" />
<link href="/elecFee/css/style1.css" media="screen" rel="stylesheet" type="text/css" />
<link href="/elecFee/css/color_02.css" media="screen" rel="stylesheet" type="text/css" />
<link href="/elecFee/css/sys.css" media="screen" rel="stylesheet" type="text/css" />
<link href="/elecFee/css/kcdz.css" media="screen" rel="stylesheet" type="text/css" />
<link href="/elecFee/css/jquery-ui.css" media="screen" rel="stylesheet" >
<style type="text/css">
.systab a{
	background-color:#fff;
	width:33.3%;
	height:38px;
	line-height:38px;
	text-align:center;
	float:left;
	font-size:15px;
	text-decoration:none;
	}
</style>
<script src="/elecFee/js/jquery.js" type="text/javascript"></script>
<script src="/elecFee/js/jquery-ui.js"></script>
<script src="http://api.map.baidu.com/api?v=2.0&ak=VoD9ZEuwxw6aGw5MPeEqmTn9mhtajQHf" type="text/javascript"></script>
<script src="/elecFee/js/resetAndroidFontsize.js" type="text/javascript"></script>
<script src="/elecFee/ecard/data.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	var placeJson=JSON.parse(places);
	$("#zxq").click(function(){
		$(this).attr("class","kctab2_select");
		$("#dxq").removeAttr("class");
		$("#tjxq").removeAttr("class");
		$("#zxqdiv").show();
		$("#dxqdiv").hide();
		$("#tjxqdiv").hide();
	});
	$("#dxq").click(function(){
		$(this).attr("class","kctab2_select");
		$("#zxq").removeAttr("class");
		$("#tjxq").removeAttr("class");
		$("#zxqdiv").hide();
		$("#dxqdiv").show();
		$("#tjxqdiv").hide();
	});
	$("#tjxq").click(function(){
		$(this).attr("class","kctab2_select");
		$("#zxq").removeAttr("class");
		$("#dxq").removeAttr("class");
		$("#zxqdiv").hide();
		$("#dxqdiv").hide();
		$("#tjxqdiv").show();
	});
	$(".text").hide();
	$(".kc_week_con").click(function(){
		$(".text").slideUp();
		if($(this).next().is(":hidden"))
			$(this).next().slideDown();
	});
	
	$("#container").dialog({
		autoOpen : false,
		resizable : false,
		height : "auto",
		width : "90%",
		modal : true //遮罩
	});
	
	$("td").click(function(){
		var placeName=$(this).html();
		var x=placeJson[placeName].x;
		var y=placeJson[placeName].y;
		if(x!=undefined){
			$("#container").dialog("open");
			var map = new BMap.Map("bdMap");    
			map.enableScrollWheelZoom();   //启用滚轮放大缩小，默认禁用
			map.enableContinuousZoom();    //启用地图惯性拖拽，默认禁用
			var point = new BMap.Point(x, y);//集锦园
			map.centerAndZoom(point,18);  
			var mk = new BMap.Marker(point);
			map.addOverlay(mk);
		}
	});
});
</script>
</head>
<body>
<jsp:include page="guideHeader.jsp" flush="true">
	<jsp:param name="title" value="校园卡充值指南" />
</jsp:include>
<div id="container" title="地图">
	<div id="bdMap"  style="width: 100%; height: 450px;"></div>
</div>
<div class="main_03" style="margin-top:55px;">
	<div class="div_title key_color" >自助终端安放点</div>
	<div style="margin:0 10px" class="systab kc_tab2">
		<a href="javascript:;" id="zxq" class="kctab2_select">主校区</a>
		<a href="javascript:;" id="dxq" >东校区</a>
		<a href="javascript:;" id="tjxq" >同济校区</a>
	</div>
	
<div id="zxqdiv" class="kc_list" style="display:block;">
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">1、宿舍区</p>
	</div>
	<div class="text">
		<table width="100%">
			<tr><td width="50%">东一舍</td><td>东四舍</td></tr>
			<tr><td>东五舍</td><td>东七舍</td></tr>
			<tr><td>东九舍</td><td>东十二舍</td></tr>
			<tr><td>附中主楼</td><td>教七舍</td></tr>
			<tr><td>南二舍</td><td>西二舍</td></tr>
			<tr><td>西七舍</td><td>西九舍</td></tr>
			<tr><td>西十二舍</td><td>西十五舍</td></tr>
			<tr><td>紫崧3栋</td><td>紫崧6栋</td></tr>
			<tr><td>紫崧10栋</td><td>紫崧12栋</td></tr>
		</table>
	</div>
</div>
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">2、食堂</p>
	</div>
	<div class="text">
		<table width="100%">
			<tr><td width="50%" >集锦园</td><td>喻园</td></tr>
			<tr><td>东一食堂</td><td>集贤楼</td></tr>
			<tr><td>百景园</td><td>西一食堂</td></tr>
			<tr><td>西二食堂</td><td>百惠园</td></tr>
		</table>
	</div>
</div>
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">3、公共区</p>
	</div>
	<div class="text">
		<table width="100%">
			<tr><td width="50%">主校区图书馆</td><td>主校区校医院</td></tr>
			<tr><td>主校区校园卡服务中心</td><td></td></tr>
		</table>
	</div>
</div>
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">4、教学楼</p>
	</div>
	<div class="text">
		<table width="100%">
			<tr><td width="50%">东五楼</td><td>东九楼</td></tr>
			<tr><td>大学生活动中心</td><td>管理学院</td></tr>
			<tr><td>先进制造大楼</td><td>南一楼</td></tr>
			<tr><td>南三楼</td><td>南六楼</td></tr>
			<tr><td>西二楼</td><td>西五楼</td></tr>
			<tr><td>西十二楼</td><td></td></tr>
		</table>
	</div>
</div>
</div>

<div id="dxqdiv" class="kc_list" style="display:none;">
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">1、宿舍区</p>
	</div>
	<div class="text">
		<table width="100%">
			<tr><td width="50%">韵苑二栋</td><td>韵苑四栋</td></tr>
			<tr><td>韵苑五栋</td><td>韵苑七栋</td></tr>
			<tr><td>韵苑九栋</td><td>韵苑十二栋</td></tr>
			<tr><td>韵苑十四栋</td><td>韵苑十八栋</td></tr>
			<tr><td>韵苑二十三栋</td><td>韵苑二十四栋</td></tr>
			<tr><td>韵苑二十七栋</td><td></td></tr>
		</table>
	</div>
</div>
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">2、食堂</p>
	</div>
	<div class="text">
		<table width="100%">
			<tr><td width="50%">东教工食堂</td><td>学一食堂</td></tr>
			<tr><td>学二食堂</td><td>韵苑食堂</td></tr>
			<tr><td>东园广场</td><td></td></tr>
		</table>
	</div>
</div>
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">3、公共区</p>
	</div>
	<div class="text">
		<table width="100%">
			<tr><td width="50%">东校区图书馆</td><td>东校区校医院</td></tr>
			<tr><td>东校区校园卡服务中心</td><td></td></tr>
		</table>
	</div>
</div>
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">4、教学楼</p>
	</div>
	<div class="text">
		<table width="100%">
			<tr><td width="50%">生科院</td><td>东十二楼</td></tr>
			<tr><td>光电实验室</td><td></td></tr>
		</table>
	</div>
</div>
</div>

<div id="tjxqdiv" class="kc_list" style="display:none;">
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">1、宿舍区</p>
	</div>
	<div class="text">
		<table width="100%">
			<tr><td width="50%">学生宿舍</td><td></td></tr>
		</table>
	</div>
</div>
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">2、食堂</p>
	</div>
	<div class="text">
		<table width="100%">
			<tr><td width="60%">学生食堂一楼和二楼</td><td>教工一食堂</td></tr>
			<tr><td>教工二食堂</td><td>新食堂</td></tr>
			<tr><td>内招餐厅</td><td>外招餐厅</td></tr>
		</table>
	</div>
</div>
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">3、公共区</p>
	</div>
	<div class="text">
		<table width="100%">
			<tr><td width="65%">同济校区图书馆</td><td></td></tr>
			<tr><td>同济校区校园卡服务中心</td><td></td></tr>
		</table>
	</div>
</div>
<div class="xxcx_row">
	<div class="kc_week_con">
		<p class="kc_year_mini key_color">4、教学楼</p>
	</div>
	<div class="text">
		<table width="100%">
			<tr><td width="50%">1号教学楼</td><td>2号教学楼</td></tr>
		</table>
	</div>
</div>
</div>

	</div>
<div class="button_02_outside button_color">
	<input type="button" onclick="window.history.go(-1)" value="返回" class="button_02">
</div>
<jsp:include page="../footer.jsp">
	<jsp:param name="source" value="校园卡服务中心" /> 
</jsp:include>
</body>
</html>