<%@ page language="java" contentType="text/html; charset=utf-8"%>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
  <meta name="keywords" content=""> 
  <meta name="decription" content=""> 
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"> 
  <meta charset="utf-8">
  <title>2016年华中大校园卡账单</title> 
<style>
.textbg{
	width:100%;
 	height:100%;
	background:url(/elecFee/images/ecard/bg2.png) no-repeat;
	background-size:100% 100%;
}
.textSection{
	color:#24887A;
	background: whitesmoke;
}
.section{
	background:url(/elecFee/images/ecard/bg2.png) no-repeat;
}
.section current{
	display:flex;
}
.chart{
	display: flex;
	align-items: center;
	justify-content: center;
}
@-webkit-keyframes gogogo {
    0%{
		-webkit-transform: rotate(0deg);
    }
    50%{
		-webkit-transform: rotate(180deg);
    }
    100%{
        -webkit-transform: rotate(360deg);
    }
}
</style>
<link href='//cdn.webfont.youziku.com/webfonts/nomal/97360/45824/586f415af629da120cafce31.css' rel='stylesheet' type='text/css' />
<link rel="stylesheet" href="/elecFee/css/pageSlider.css">
	<script src="/elecFee/js/zepto.js"></script>
	<script src="/elecFee/js/pageSlider.js"></script>
<script src="/elecFee/js/jquery.js"></script>
<script src="/elecFee/js/highcharts.js"></script>
<script src="/elecFee/js/exporting.js"></script>
<script type="text/javascript">
var ecard={};
ecard.intro=function(){
	var zxf=xyk.desc.CZZXF;
	var zcz=xyk.desc.CZZJE
	var introSpan="快来一起回顾一下这一年校园卡陪伴你的那些日子吧！2016年，你总共往校园卡里充了<span style='color:red'>"+zcz+"</span>元，"
				+"总共消费了<span style='color:red'>"+zxf+"</span>元！"
	$("#introSpan").html(introSpan);
	ecard.verticalMiddle(winHeight,"introText");
}
ecard.stcs=function(){
	var st=xyk.desc.ST;//食堂
	var cs=xyk.desc.CS;//超市
	$("#stcsSpan").html("2016年，您在食堂吃掉了<span style='color:red'>"+st+"</span>元，"
			+"在超市总共剁手了<span style='color:red'>"+cs+"</span>元！");
	ecard.verticalMiddle(winHeight,"stcsText");
}
ecard.other=function(direction){
	var wf=xyk.desc.WF;//网费
	var xc=xyk.desc.XC;//校车
	var shj=xyk.desc.SHJ;//售货机
	if(wf==0 && xc==0 && shj==0){
		if(direction=="prev")
			page.go(6);
		else
			page.go(8);
	}else{
		var otherSpan="除了食堂和超市，";
		if(wf!=0){
			otherSpan+="你还在用校园卡为校园网充值了<span style='color:red'>"+wf+"</span>元，";
		}
		if(xc!=0){
			otherSpan+="在校车上花了<span style='color:red'>"+xc+"</span>元，";
		}
		if(shj!=0){
			otherSpan+="在自动售货机上消费了<span style='color:red'>"+shj+"</span>元，";
		}
		otherSpan=otherSpan.substring(0,otherSpan.length-1);
		otherSpan+="！哇，你已经发掘了好多校园卡的技能呢！";
		$("#otherSpan").html(otherSpan);
		ecard.verticalMiddle(winHeight,"otherText");
	}
}
ecard.most=function(){
	var max=xyk.desc.MAX;//最大一次金额
	var max_address=xyk.desc.MAX_ADRESS;//最大一次地点
	var max_time=xyk.desc.MAX_TIME;//最大一次时间
	$("#mostSpan").html("快来瞧瞧你最土豪的一天，"+max_time+"，在<span style='color:red'>"+max_address+"</span>的<span style='color:red'>"+max+"</span>元是您的最大一笔消费！");
	ecard.verticalMiddle(winHeight,"mostText");
}
ecard.lost=function(direction){
	var hkcs=xyk.desc.HKCS;//换卡次数
	if(hkcs==0){
		if(direction=="prev")
			page.go(8);
		else
			page.go(10);
	}else{
		$("#lostSpan").html("2016年您更换了<span style='color:red'>"+hkcs+"</span>次校园卡，2017年可要小心保管啦！");
		ecard.verticalMiddle(winHeight,"lostText");
	}
}
ecard.auto=function(){
	var qc=xyk.desc.QC;//最大一次金额
	var autoSpan="";
	if(qc=="已开通"){
		autoSpan="<span style='color:red'>圈存转账</span>的技能点也被你点亮了呀？你真是太棒啦！在自助服务终端就可以为自己的校园卡充值，方便快捷，为你点赞！";
	}else{
		autoSpan="您还未开通校园卡<span style='color:red'>圈存转账</span>功能，该功能可以在自助服务终端上进行充值，方便又快捷，赶快去开通吧！";
	}
	$("#autoSpan").html(autoSpan);
	ecard.verticalMiddle(winHeight,"autoText");
}
ecard.desc=function(){
	var ret=xyk.desc;
	$('#desc').highcharts({
		chart: {
			type: 'line'
		},
		title: {
			text: '每个月的消费情况都在折线图里啦，<br/>快看看哪个月是买买买得最爽的一个月吧！',
			style: {
				fontWeight:'bold',
				color: '#7B6159',
				fontFamily: 'DroidSans817c0905317c50'
			},
		},
		xAxis: {
			categories: ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月']
		},
		yAxis: {
			title: {
				text: '消费金额 (元)'
			},
			labels: false
		},
		legend:{
			enabled:false,
		},
		plotOptions: {
			line: {
				dataLabels: {
					allowOverlap: true,
					align: 'right',
					enabled: true
				},
				enableMouseTracking: false
			}
		},
		series: [{
			data:[ret.JAN,ret.FEB,ret.MAR,ret.APR,ret.MAY,ret.JUN,ret.JULY,ret.AUG,ret.SEP,ret.OCT,ret.NOV,ret.DEC],
			color: '#ff5722'
		}],
		credits: {
			enabled:false
		},
		exporting: {
		enabled:false
		}
	}); 
	ecard.verticalMiddle(winHeight,"descDiv");
}
ecard.markets=function(){
	$('#markets').highcharts({
		chart: {
			plotBackgroundColor: null,
			plotBorderWidth: null,
			plotShadow: false
		},
		title: {
			text: '快来看看你最喜欢在哪个超市刷卡吧，<br/>刷卡虽好，<br/>可别忘了在各个超市“雨露均沾”哦~',
			style: {
				fontWeight:'bold',
				color: '#7B6159',
				fontFamily: 'DroidSans817c0905317c50'
			}
		},
		plotOptions: {
			pie: {
				colors: ['#ea4a17', '#ffeb3b', '#90ed7d', '#f7a35c', '#35ca3b', '#8085e9'],
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: true,
					format: '{point.y:.1f} 元',
					style: {
						color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
					}
				},
				showInLegend: true
			}
		},
		series: [{
			type: 'pie',
			name: '金额',
			data: xyk.markets,
// 			[
// 				['西学超市',   ret.xixue],
// 				['东边超市',   ret.dongbian],
// 				{
// 					name: '喻园教工超市',
// 					y: ret.yuyuanjiaogong,
// 					sliced: true,
// 					selected: true
// 				},
// 				['喻园超市', ret.yuyuan],
// 				['韵苑超市', ret.yunyuan],
// 				['西边高层超市', ret.xibiangaoceng]
// 			]
		}],
		credits: {
			enabled:false
		},
		exporting: {
			enabled:false
		}
	});
	ecard.verticalMiddle(winHeight,"marketsDiv");
}
ecard.dinings=function(){
	$('#dinings').highcharts({
		chart: {
            type: 'column'
        },
        title: {
			text: '食堂的消费账单也有呢，<br/>你最爱的美味在哪里？<br/>是东园、百景，还是韵苑？<br/>我科三十多个食堂，都在等你任性刷~',
			style: {
				fontWeight:'bold',
				color: '#7B6159',
				fontFamily: 'DroidSans817c0905317c50'
			}
        },
		plotOptions: {
			series: {
				color: '#ff5722',
             }
		},
		xAxis: {
			type: 'category',
			labels: {
				rotation: -45,
				style: {
					fontSize: '11px',
					fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            title: {
                text: '消费金额（元）'
            },
        	labels: false
        },
        legend: {
            enabled: false
        },
        series: [{
            name: '消费',
            data: xyk.dinings,
//             [
//                 ['西一食堂', ret.xiyi],
//                 ['西二食堂', ret.xier],
//                 ['百景园', ret.baijingyuan],
//                 ['喻园', ret.yuyuan],
//                 ['其他', ret.others]
//             ],
			dataLabels: {
                enabled: true,
                color: '#000000',
                align: 'right',
                format: '{point.y:.1f}', // one decimal
                x:10,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }],
        credits: {
            enabled:false
        },
        exporting: {
            enabled:false
        }
    });
	ecard.verticalMiddle(winHeight,"diningsDiv");
}
ecard.verticalMiddle=function(winHeight,id){
	var div=document.getElementById(id);
	var divHeight=div.offsetHeight;
	var margin=(winHeight-divHeight)/2*0.7;
// 	div.style="margin-top:"+margin+"px;-webkit-transition: all 2s ease-out;"
	$(div).animate({marginTop:margin+"px"},2000);
}
</script>
</head>
<body style="margin:0px; font-family:DroidSans817c0905317c50">
<div class="section sec1" style="background:#9bddd4;">
	<div>
	<img src="/elecFee/images/ecard/end.png" style="display:none;width:90%"/>
	<img src="/elecFee/images/ecard/caihong.png" style="width:90%"/>
	<img src="/elecFee/images/ecard/zhangdan.png" style="width:50%;transform: translate(30%,-40%);"/>
	<img src="/elecFee/images/ecard/boy.png" style="position:absolute;bottom:60px;right:10px"/>
	<img src="/elecFee/images/ecard/text.png" style="position:absolute;bottom:20px;right:10%"/>
	</div>
</div>
<div class="section sec2" >
	<blockquote id="introText" class="yead_editor yead-selected" data-author="Wxeditor" style="font-size:14px;border:0px;padding-top:25px;margin:auto;white-space: normal;">
	    <section style="text-align:center">
	        <section style="-ms-transform:rotate(8deg);-moz-transform:rotate(5deg);-webkit-transform:rotate(5deg);-o-transform:rotate(5deg);transform:rotate(5deg);display:inline-block;box-shadow:#757575 2px 2px 10px">
	            <section class="textSection" style="border:1px solid #cec0c0">
	                <section style="width:16em;height:2em;position:relative;z-index:9;margin:auto;-ms-transform:rotate(-39deg);-moz-transform:rotate(-39deg);-webkit-transform:rotate(-39deg);-o-transform:rotate(-39deg);transform:rotate(-39deg);margin-left:-13em;display:inline-block">
	                    <img class="wx-img" data-wxsrc="/elecFee/images/ecard/ttttt.png" style="width:98px;height:28px;display:inline" src="/elecFee/images/ecard/ttttt.png" data-type="png" data-s="150,640"/>
	                </section>
	                <section style="width:300px;padding:0 1em 2em 2em;-ms-transform:rotate(-6deg);-moz-transform:rotate(-6deg);-webkit-transform:rotate(-6deg);-o-transform:rotate(-6deg);transform:rotate(-6deg);text-align:justify;margin:auto" data-style="font-size:14px;">
	                    <p style="font-size:14px;line-height:30px">
	                        <span style="font-size: 22px; " id="introSpan"></span>
	                    </p>
	                    <p style="text-align: left;">
	                        &nbsp;&nbsp;<img data-s="150,640" data-type="png" id="introPic" src="/elecFee/images/ecard/intro.png" style="width:80%"/>
	                    </p>
	                </section>
	            </section>
	        </section>
	    </section>
	</blockquote>
</div>
<div class="section sec3" >
	<div id="descDiv">
		<blockquote id="m915" class="yead_editor yead-selected" data-author="Wxeditor" style="font-size:14px;border:0px;padding:0px;margin:5px auto;white-space: normal;">
		    <section style="box-sizing:border-box">
		        <section style="margin-top:10px;margin-bottom:10px;text-align:center;box-sizing:border-box">
		            <section style="margin-bottom:-.9em;display:inline-block;vertical-align:top;box-sizing:border-box">
		                <section class="yead_bdtc yead_bdrc" style="filter:alpha(opacity=50);-moz-opacity:.5;-khtml-opacity:.5;opacity:.5;display:inline-block;vertical-align:bottom;border-right-width:.4em;border-right-style:solid;border-right-color:#f1363f;border-top-width:.4em;border-top-style:solid;border-top-color:#f1363f;box-sizing:border-box;max-width:5%;border-bottom-width:.4em!important;border-bottom-style:solid!important;border-bottom-color:transparent!important;border-left-width:.4em!important;border-left-style:solid!important;border-left-color:transparent!important"></section>
		                <section class="yead_bgc" style="padding:2px 5px;display:inline-block;vertical-align:top;font-size:22px;line-height:2em;border-radius:5px 5px 0 0;color:#fff;box-sizing:border-box;max-width:90%;background-color:#f1363f">
		                    <p style="margin:0;box-sizing:border-box">
		                        <span style=" font-size: 24px;"><strong style="box-sizing:border-box">每月消费情况</strong></span>
		                    </p>
		                </section>
		                <section class="yead_bdtc yead_bdlc" style="filter:alpha(opacity=50);-moz-opacity:.5;-khtml-opacity:.5;opacity:.5;display:inline-block;vertical-align:bottom;border-top-width:.4em;border-top-style:solid;border-top-color:#f1363f;border-left-width:.4em;border-left-style:solid;border-left-color:#f1363f;box-sizing:border-box;max-width:5%;border-right-width:.4em!important;border-right-style:solid!important;border-right-color:transparent!important;border-bottom-width:.4em!important;border-bottom-style:solid!important;border-bottom-color:transparent!important"></section>
		            </section>
		            <section style="height:.9em;border-top-width:3px;border-top-style:solid;border-top-color:#000;box-sizing:border-box"></section>
		        </section>
		    </section>
		</blockquote>
		<div id="desc" class="chart"></div>
	</div>
</div>
<div class="section sec4" >
	<blockquote id="stcsText" class="yead_editor yead-selected" data-author="Wxeditor" style="font-size:14px;border:0px;padding-top:25px;margin:auto;white-space: normal;">
	    <section style="text-align:center">
	        <section style="-ms-transform:rotate(8deg);-moz-transform:rotate(5deg);-webkit-transform:rotate(5deg);-o-transform:rotate(5deg);transform:rotate(5deg);display:inline-block;box-shadow:#757575 2px 2px 10px">
	            <section class="textSection" style="border:1px solid #cec0c0">
	                <section style="width:16em;height:2em;position:relative;z-index:9;margin:auto;-ms-transform:rotate(-39deg);-moz-transform:rotate(-39deg);-webkit-transform:rotate(-39deg);-o-transform:rotate(-39deg);transform:rotate(-39deg);margin-left:-13em;display:inline-block">
	                    <img class="wx-img" data-wxsrc="/elecFee/images/ecard/ttttt.png" style="width:98px;height:28px;display:inline" src="/elecFee/images/ecard/ttttt.png" data-type="png" data-s="150,640"/>
	                </section>
	                <section style="width:300px;padding:0 1em 2em 2em;-ms-transform:rotate(-6deg);-moz-transform:rotate(-6deg);-webkit-transform:rotate(-6deg);-o-transform:rotate(-6deg);transform:rotate(-6deg);text-align:justify;margin:auto" data-style="font-size:14px;">
	                    <p style="font-size:14px;line-height:30px">
	                        <span style="font-size: 22px; " id="stcsSpan"></span>
	                    </p>
	                    <p style="text-align: left;">
	                        <img data-s="150,640" data-type="png" id="stcsPic" src="/elecFee/images/ecard/stcs.png" style="width:100%"/>
	                    </p>
	                </section>
	            </section>
	        </section>
	    </section>
	</blockquote>
</div>
<div class="section sec5" >
	<div id="marketsDiv">
		<blockquote id="m915" class="yead_editor yead-selected" data-author="Wxeditor" style="font-size:14px;border:0px;padding:0px;margin:5px auto;white-space: normal;">
		    <section style="box-sizing:border-box">
		        <section style="margin-top:10px;margin-bottom:10px;text-align:center;box-sizing:border-box">
		            <section style="margin-bottom:-.9em;display:inline-block;vertical-align:top;box-sizing:border-box">
		                <section class="yead_bdtc yead_bdrc" style="filter:alpha(opacity=50);-moz-opacity:.5;-khtml-opacity:.5;opacity:.5;display:inline-block;vertical-align:bottom;border-right-width:.4em;border-right-style:solid;border-right-color:#f1363f;border-top-width:.4em;border-top-style:solid;border-top-color:#f1363f;box-sizing:border-box;max-width:5%;border-bottom-width:.4em!important;border-bottom-style:solid!important;border-bottom-color:transparent!important;border-left-width:.4em!important;border-left-style:solid!important;border-left-color:transparent!important"></section>
		                <section class="yead_bgc" style="padding:2px 5px;display:inline-block;vertical-align:top;font-size:22px;line-height:2em;border-radius:5px 5px 0 0;color:#fff;box-sizing:border-box;max-width:90%;background-color:#f1363f">
		                    <p style="margin:0;box-sizing:border-box">
		                        <span style=" font-size: 24px;"><strong style="box-sizing:border-box">超市贡献值</strong></span>
		                    </p>
		                </section>
		                <section class="yead_bdtc yead_bdlc" style="filter:alpha(opacity=50);-moz-opacity:.5;-khtml-opacity:.5;opacity:.5;display:inline-block;vertical-align:bottom;border-top-width:.4em;border-top-style:solid;border-top-color:#f1363f;border-left-width:.4em;border-left-style:solid;border-left-color:#f1363f;box-sizing:border-box;max-width:5%;border-right-width:.4em!important;border-right-style:solid!important;border-right-color:transparent!important;border-bottom-width:.4em!important;border-bottom-style:solid!important;border-bottom-color:transparent!important"></section>
		            </section>
		            <section style="height:.9em;border-top-width:3px;border-top-style:solid;border-top-color:#000;box-sizing:border-box"></section>
		        </section>
		    </section>
		</blockquote>
		<div id="markets" class="chart"></div>
	</div>
</div>
<div class="section sec6" >
	<div id="diningsDiv">
		<blockquote id="m915" class="yead_editor yead-selected" data-author="Wxeditor" style="font-size:14px;border:0px;padding:0px;margin:5px auto;white-space: normal;">
		    <section style="box-sizing:border-box">
		        <section style="margin-top:10px;margin-bottom:10px;text-align:center;box-sizing:border-box">
		            <section style="margin-bottom:-.9em;display:inline-block;vertical-align:top;box-sizing:border-box">
		                <section class="yead_bdtc yead_bdrc" style="filter:alpha(opacity=50);-moz-opacity:.5;-khtml-opacity:.5;opacity:.5;display:inline-block;vertical-align:bottom;border-right-width:.4em;border-right-style:solid;border-right-color:#f1363f;border-top-width:.4em;border-top-style:solid;border-top-color:#f1363f;box-sizing:border-box;max-width:5%;border-bottom-width:.4em!important;border-bottom-style:solid!important;border-bottom-color:transparent!important;border-left-width:.4em!important;border-left-style:solid!important;border-left-color:transparent!important"></section>
		                <section class="yead_bgc" style="padding:2px 5px;display:inline-block;vertical-align:top;font-size:22px;line-height:2em;border-radius:5px 5px 0 0;color:#fff;box-sizing:border-box;max-width:90%;background-color:#f1363f">
		                    <p style="margin:0;box-sizing:border-box">
		                        <span style=" font-size: 24px;"><strong style="box-sizing:border-box">各食堂消费 </strong></span>
		                    </p>
		                </section>
		                <section class="yead_bdtc yead_bdlc" style="filter:alpha(opacity=50);-moz-opacity:.5;-khtml-opacity:.5;opacity:.5;display:inline-block;vertical-align:bottom;border-top-width:.4em;border-top-style:solid;border-top-color:#f1363f;border-left-width:.4em;border-left-style:solid;border-left-color:#f1363f;box-sizing:border-box;max-width:5%;border-right-width:.4em!important;border-right-style:solid!important;border-right-color:transparent!important;border-bottom-width:.4em!important;border-bottom-style:solid!important;border-bottom-color:transparent!important"></section>
		            </section>
		            <section style="height:.9em;border-top-width:3px;border-top-style:solid;border-top-color:#000;box-sizing:border-box"></section>
		        </section>
		    </section>
		</blockquote>
		<div id="dinings" class="chart"></div>
	</div>
</div>
<div class="section sec7" >
	<blockquote id="otherText" class="yead_editor yead-selected" data-author="Wxeditor" style="font-size:14px;border:0px;padding-top:25px;margin:auto;white-space: normal;">
	    <section style="text-align:center">
	        <section style="-ms-transform:rotate(8deg);-moz-transform:rotate(5deg);-webkit-transform:rotate(5deg);-o-transform:rotate(5deg);transform:rotate(5deg);display:inline-block;box-shadow:#757575 2px 2px 10px">
	            <section class="textSection" style="border:1px solid #cec0c0">
	                <section style="width:16em;height:2em;position:relative;z-index:9;margin:auto;-ms-transform:rotate(-39deg);-moz-transform:rotate(-39deg);-webkit-transform:rotate(-39deg);-o-transform:rotate(-39deg);transform:rotate(-39deg);margin-left:-13em;display:inline-block">
	                    <img class="wx-img" data-wxsrc="/elecFee/images/ecard/ttttt.png" style="width:98px;height:28px;display:inline" src="/elecFee/images/ecard/ttttt.png" data-type="png" data-s="150,640"/>
	                </section>
	                <section style="width:300px;padding:0 1em 2em 2em;-ms-transform:rotate(-6deg);-moz-transform:rotate(-6deg);-webkit-transform:rotate(-6deg);-o-transform:rotate(-6deg);transform:rotate(-6deg);text-align:justify;margin:auto" data-style="font-size:14px;">
	                    <p style="font-size:14px;line-height:30px">
	                        <span style="font-size: 22px; " id="otherSpan"></span>
	                    </p>
	                    <p style="text-align: left;">
	                        <img data-s="150,640" data-type="png" id="otherPic" src="/elecFee/images/ecard/other.png" style="width:100%"/>
	                    </p>
	                </section>
	            </section>
	        </section>
	    </section>
	</blockquote>
</div>
<div class="section sec8" >
	<blockquote id="mostText" class="yead_editor yead-selected" data-author="Wxeditor" style="font-size:14px;border:0px;padding-top:25px;margin:auto;white-space: normal;">
	    <section style="text-align:center">
	        <section style="-ms-transform:rotate(8deg);-moz-transform:rotate(5deg);-webkit-transform:rotate(5deg);-o-transform:rotate(5deg);transform:rotate(5deg);display:inline-block;box-shadow:#757575 2px 2px 10px">
	            <section class="textSection" style="border:1px solid #cec0c0">
	                <section style="width:16em;height:2em;position:relative;z-index:9;margin:auto;-ms-transform:rotate(-39deg);-moz-transform:rotate(-39deg);-webkit-transform:rotate(-39deg);-o-transform:rotate(-39deg);transform:rotate(-39deg);margin-left:-13em;display:inline-block">
	                    <img class="wx-img" data-wxsrc="/elecFee/images/ecard/ttttt.png" style="width:98px;height:28px;display:inline" src="/elecFee/images/ecard/ttttt.png" data-type="png" data-s="150,640"/>
	                </section>
	                <section style="width:300px;padding:0 1em 2em 2em;-ms-transform:rotate(-6deg);-moz-transform:rotate(-6deg);-webkit-transform:rotate(-6deg);-o-transform:rotate(-6deg);transform:rotate(-6deg);text-align:justify;margin:auto" data-style="font-size:14px;">
	                    <p style="font-size:14px;line-height:30px">
	                        <span style="font-size: 22px; " id="mostSpan"></span>
	                    </p>
	                    <p style="text-align: left;">
	                        &nbsp;&nbsp;<img data-s="150,640" data-type="png" id="mostPic" src="/elecFee/images/ecard/most2.png" style="80%"/>
	                    </p>
	                </section>
	            </section>
	        </section>
	    </section>
	</blockquote>
</div>
<div class="section sec9" >
	<blockquote id="lostText" class="yead_editor yead-selected" data-author="Wxeditor" style="font-size:14px;border:0px;padding-top:25px;margin:auto;white-space: normal;">
	    <section style="text-align:center">
	        <section style="-ms-transform:rotate(8deg);-moz-transform:rotate(5deg);-webkit-transform:rotate(5deg);-o-transform:rotate(5deg);transform:rotate(5deg);display:inline-block;box-shadow:#757575 2px 2px 10px">
	            <section class="textSection" style="border:1px solid #cec0c0">
	                <section style="width:16em;height:2em;position:relative;z-index:9;margin:auto;-ms-transform:rotate(-39deg);-moz-transform:rotate(-39deg);-webkit-transform:rotate(-39deg);-o-transform:rotate(-39deg);transform:rotate(-39deg);margin-left:-13em;display:inline-block">
	                    <img class="wx-img" data-wxsrc="/elecFee/images/ecard/ttttt.png" style="width:98px;height:28px;display:inline" src="/elecFee/images/ecard/ttttt.png" data-type="png" data-s="150,640"/>
	                </section>
	                <section style="width:300px;padding:0 1em 2em 2em;-ms-transform:rotate(-6deg);-moz-transform:rotate(-6deg);-webkit-transform:rotate(-6deg);-o-transform:rotate(-6deg);transform:rotate(-6deg);text-align:justify;margin:auto" data-style="font-size:14px;">
	                    <p style="font-size:14px;line-height:30px">
	                        <span style="font-size: 22px; " id="lostSpan"></span>
	                    </p>
	                    <p style="text-align: left;">
	                       	<img data-s="150,640" data-type="png" id="lostPic" 
	                        src="/elecFee/images/ecard/lost.png" style="width:100%"/>
	                    </p>
	                </section>
	            </section>
	        </section>
	    </section>
	</blockquote>
</div>
<div class="section sec10" >
	<blockquote id="autoText" class="yead_editor yead-selected" data-author="Wxeditor" style="font-size:14px;border:0px;padding-top:25px;margin:auto;white-space: normal;">
	    <section style="text-align:center">
	        <section style="-ms-transform:rotate(8deg);-moz-transform:rotate(5deg);-webkit-transform:rotate(5deg);-o-transform:rotate(5deg);transform:rotate(5deg);display:inline-block;box-shadow:#757575 2px 2px 10px">
	            <section class="textSection" style="border:1px solid #cec0c0">
	                <section style="width:16em;height:2em;position:relative;z-index:9;margin:auto;-ms-transform:rotate(-39deg);-moz-transform:rotate(-39deg);-webkit-transform:rotate(-39deg);-o-transform:rotate(-39deg);transform:rotate(-39deg);margin-left:-13em;display:inline-block">
	                    <img class="wx-img" data-wxsrc="/elecFee/images/ecard/ttttt.png" style="width:98px;height:28px;display:inline" src="/elecFee/images/ecard/ttttt.png" data-type="png" data-s="150,640"/>
	                </section>
	                <section style="width:300px;padding:0 1em 2em 2em;-ms-transform:rotate(-6deg);-moz-transform:rotate(-6deg);-webkit-transform:rotate(-6deg);-o-transform:rotate(-6deg);transform:rotate(-6deg);text-align:justify;margin:auto" data-style="font-size:14px;">
	                    <p style="font-size:14px;line-height:30px">
	                        <span style="font-size: 22px; " id="autoSpan"></span>
	                    </p>
	                    <p style="text-align: left;">
	                        <img data-s="150,640" data-type="png" id="autoPic" src="/elecFee/images/ecard/auto.png" style="width:100%"/>
	                    </p>
	                </section>
	            </section>
	        </section>
	    </section>
	</blockquote>
</div>
<div class="section sec11" >
	<blockquote id="endText" class="yead_editor yead-selected" data-author="Wxeditor" style="font-size:14px;border:0px;padding-top:25px;margin:auto;white-space: normal;">
	    <section style="text-align:center">
	        <section style="-ms-transform:rotate(8deg);-moz-transform:rotate(5deg);-webkit-transform:rotate(5deg);-o-transform:rotate(5deg);transform:rotate(5deg);display:inline-block;box-shadow:#757575 2px 2px 10px">
	            <section class="textSection" style="border:1px solid #cec0c0">
	                <section style="width:16em;height:2em;position:relative;z-index:9;margin:auto;-ms-transform:rotate(-39deg);-moz-transform:rotate(-39deg);-webkit-transform:rotate(-39deg);-o-transform:rotate(-39deg);transform:rotate(-39deg);margin-left:-13em;display:inline-block">
	                    <img class="wx-img" data-wxsrc="/elecFee/images/ecard/ttttt.png" style="width:98px;height:28px;display:inline" src="/elecFee/images/ecard/ttttt.png" data-type="png" data-s="150,640"/>
	                </section>
	                <section style="width:300px;padding:0 1em 2em 2em;-ms-transform:rotate(-6deg);-moz-transform:rotate(-6deg);-webkit-transform:rotate(-6deg);-o-transform:rotate(-6deg);transform:rotate(-6deg);text-align:justify;margin:auto" data-style="font-size:14px;">
	                    <p style="line-height:32px;text-align:center;font-size: 22px; ">
	                        2016年,<br/>
	                       	 您或许已经记不清的每一笔收入和支出账单了。<br/>
	                       	 但是没有关系，因为我们会帮你记得！<br/>
							2017，校园卡，伴你走过每一天！
	                    </p>
	                    <p style="text-align: left;">
	                        <img data-s="150,640" data-type="png" id="endPic" src="/elecFee/images/ecard/end.jpg" style="width:90%"/>
	                    	<p style="text-align: right;padding-right: 15px;">
	                    	文案：熊倩艺 法学201402班</P>
	                    </p>
	                </section>
	            </section>
	        </section>
	    </section>
	</blockquote>
</div>

<div id="music" style="position:absolute;top:20px;right:20px;width:41px;height:41px;">
	<img id="yinbiao" src="/elecFee/images/ecard/yinbiao.png" style="-webkit-animation:gogogo 2s infinite linear"/>
	<audio id="musicaudio" class="audio" loop="" autoplay="autoplay">
		<source src="KOTOKO.mp3">
	</audio>
	<div id="control" style="background:url(/elecFee/images/ecard/music_c0fda01.gif);
		width:60px;height:60px;margin-top:-50px;right:-10px;position:absolute;">
	</div>
</div>
<script>
$.post("/elecFee/addCount.view",{videoName:"xykBill"});

var winHeight=document.body.clientHeight;
//数据
var xyk={
	desc:null,//个人消费描述
	dinings:null,//食堂
	markets:null,//超市
	tyb:null//体育部
};
var page=PageSlider.instance({
	startPage:1,		
	loop:true,
	translate3d:true,
	callback:function(page,direction){
		if(page==2){
			if(xyk.desc){
				ecard.intro();
			}else{
				$.post("desc.do",function(data){
					xyk.desc=data;
					ecard.intro();
				},"json");
			}
		}
		if(page==3){
			if(xyk.desc){
				ecard.desc();
			}else{
				$.post("desc.do",function(data){
					xyk.desc=data;
					ecard.desc();
				},"json");
			}
		}
		if(page==4){
			if(xyk.desc){
				ecard.stcs();
			}else{
				$.post("desc.do",function(data){
					xyk.desc=data;
					ecard.stcs();
				},"json");
			}
		}
		if(page==5){
			if(xyk.markets){
				ecard.markets();
			}else{
				$.post("markets.do",function(data){
					var marketsData=[];
					for(var i=0;i<data.length;i++){
						marketsData[i]=[data[i].MC,data[i].JE];
					}
					//第0个金额最大，突出
					marketsData[0]={
						name: marketsData[0][0],
						y: marketsData[0][1],
						sliced: true,
						selected: true
					};
					xyk.markets=marketsData;
					ecard.markets();
				},"json");
			}
		}
		if(page==6){
			if(xyk.dinings){
				ecard.dinings();
			}else{
				$.post("dinings.do",function(data){
					var diningData=[];
					for(var i=0;i<data.length;i++){
						diningData[i]=[data[i].MC,data[i].JE];
					}
					xyk.dinings=diningData;
					ecard.dinings();
				},"json");
			}
		}
		if(page==7){
			if(xyk.desc){
				ecard.other(direction);
			}else{
				$.post("desc.do",function(data){
					xyk.desc=data;
					ecard.other(direction);
				},"json");
			}
		}
		if(page==8){
			if(xyk.desc){
				ecard.most();
			}else{
				$.post("desc.do",function(data){
					xyk.desc=data;
					ecard.most();
				},"json");
			}
		}
		if(page==9){
			if(xyk.desc){
				ecard.lost(direction);
			}else{
				$.post("desc.do",function(data){
					xyk.desc=data;
					ecard.lost(direction);
				},"json");
			}
		}
		if(page==10){
			if(xyk.desc){
				ecard.auto();
			}else{
				$.post("desc.do",function(data){
					xyk.desc=data;
					ecard.auto();
				},"json");
			}
		}
	}
});
$("#control").click(function(){
	if(document.getElementById("musicaudio").paused){
		document.getElementById("musicaudio").play();
		$("#yinbiao").css("-webkit-animation","gogogo 2s infinite linear");
		$("#control").css("background","url(/elecFee/images/ecard/music_c0fda01.gif)");
	}else{
		document.getElementById("musicaudio").pause();
		$("#yinbiao").css("-webkit-animation","");
		$("#control").css("background","");
	}
});
</script>
</body>
</html>
