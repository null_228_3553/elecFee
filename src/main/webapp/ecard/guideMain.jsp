<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="" />
	<meta name="decription" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
	<title></title>		
<link href="/elecFee/css/icon.css" media="screen" rel="stylesheet" type="text/css" />
<link href="/elecFee/css/style1.css" media="screen" rel="stylesheet" type="text/css" />
<link href="/elecFee/css/color_02.css" media="screen" rel="stylesheet" type="text/css" />
<link href="/elecFee/css/sys.css" media="screen" rel="stylesheet" type="text/css" />
<style type="text/css">
.quancun {
	font-size:14px;color:#adadad;
}
.li{
	height:50px;color: #7a7a7a;font-size: 18px;
}
.icon_div{
	font-size: 40px !important;
    border-right: 2px solid #dedede;
    padding-right: 10px;
    margin-left: 15px;
    margin-right: 10px;
	float:left;
	display:inline;
	line-height:40px;
	padding-top:5px;
}
.text_div{
	display:inline;float:left;padding-top:5px;
}
</style>
<script src="/elecFee/js/jquery.js" type="text/javascript"></script>
<script src="/elecFee/js/resetAndroidFontsize.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
});
</script>
</head>
<body>
<jsp:include page="guideHeader.jsp" flush="true">
	<jsp:param name="title" value="校园卡充值指南" />
</jsp:include>
<div class="main_02">
	<div class="list_div" id="list_div">
	<ul class="demo_list" >
        <li id="sjxz" class="li" onclick="window.location.href='/elecFee/ecard/terminalPlace.jsp'">
			<div class="icon_div">
				<img src="/elecFee/images/zzzd.png" height="50px" width="50px"/>
			</div>
        	<div class="text_div">
	            <strong class="">自助终端安放点</strong>
	            <p class="quancun">self-service terminals</p>
        	</div>
        </li>
		<li id="zdcz" class="li" onclick="window.location.href='/elecFee/ecard/guideZdcz.jsp'">
			<div class="icon_div">
				<img src="/elecFee/images/zdcz.png" height="50px" />
			</div>
			<div class="text_div">
        		<strong>自动充值</strong>
        		<p class="quancun">Auto Recharge</p>
        	</div>
        </li>
        <li id="jsyh" class="li" onclick="window.location.href='/elecFee/ecard/guideJsyh.jsp'">
			<div class="icon_div">
				<img src="/elecFee/images/jsyh.png" height="50px" />
			</div>
        	<div class="text_div">
        		<Strong>建设银行手机银行</Strong>
        		<p class="quancun">圈存操作流程</p>
        	</div>
        </li>
        <li id="gsyh" class="li" onclick="window.location.href='/elecFee/ecard/guideGsyh.jsp'">
			<div class="icon_div">
				<img src="/elecFee/images/gsyh.png" height="50px" />
			</div>
			<div class="text_div">
        		<strong>工商银行手机银行</strong>
        		<p class="quancun">圈存操作流程</p>
        	</div>
        </li>
		<li id="zgyh" class="li" onclick="window.location.href='/elecFee/ecard/guideZgyh.jsp'">
			<div class="icon_div">
				<img src="/elecFee/images/zgyh.png" height="50px" />
			</div>
			<div class="text_div">
        		<strong>中国银行手机银行</strong>
        		<p class="quancun">圈存操作流程</p>
        	</div>
        </li>
    </ul>	
	</div>
</div>
<jsp:include page="../footer.jsp">
	<jsp:param name="source" value="校园卡服务中心" /> 
</jsp:include>
</body>
</html>