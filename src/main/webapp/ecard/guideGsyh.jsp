<%@ page language="java" contentType="text/html; charset=utf-8"%>
<html>
 <head></head>
 <body style="margin:5px;">
  &lt;%@ page language=&quot;java&quot; contentType=&quot;text/html; charset=utf-8&quot;%&gt;     
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
  <meta name="keywords" content="" /> 
  <meta name="decription" content="" /> 
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" /> 
  <title></title> 
  <link href="/elecFee/css/icon.css" media="screen" rel="stylesheet" type="text/css" /> 
  <link href="/elecFee/css/style1.css" media="screen" rel="stylesheet" type="text/css" /> 
  <link href="/elecFee/css/color_02.css" media="screen" rel="stylesheet" type="text/css" /> 
  <jsp:include page="guideHeader.jsp" flush="true"> 
   <jsp:param name="title" value="校园卡充值指南" /> 
  </jsp:include> 
  <p style="text-align: center; margin-top:60px"> <span style="FONT-FAMILY: 宋体, SimSun"><strong><span style="FONT-SIZE: 32px">工商银行手机银行圈存</span></strong></span> </p> 
  <p class="MsoListParagraph" style="margin-left: 24px; text-align: center;"> <span style="font-size: 18px;font-family: 宋体, SimSun"></span> </p> 
  <p style="text-align: center;"> <span style="font-size: 18px;font-family: 宋体, SimSun"></span> </p> 
  <p> <br /> </p> 
  <p> <span style="font-size: 18px;font-family: 宋体, SimSun">1.进入应用市场下载“工行手机银行”APP安装</span> </p> 
  <p> <span style="font-size: 18px;font-family: 宋体, SimSun">2.登陆手机银行，点击下方的“e缴费”</span> </p> 
  <p style="text-align: center"> <span style="font-size: 18px;font-family: 宋体, SimSun"><img width="100%" src="/elecFee/images/gsyh/gsyh1.jpg" style="border:1px solid;" /></span> </p> 
  <p> <br /> </p> 
  <p> <span style="font-size: 18px;font-family: 宋体, SimSun">3.点击“更多”</span> </p> 
  <p style="text-align: center"> <span style="font-size: 18px;font-family: 宋体, SimSun"><img width="100%" src="/elecFee/images/gsyh/gsyh2.jpg" style="border:1px solid;" /></span> </p> 
  <p> <br /> </p> 
  <p> <span style="font-size: 18px;font-family: 宋体, SimSun">4.选择“校园卡充值”</span> </p> 
  <p style="text-align: center"> <span style="font-size: 18px;font-family: 宋体, SimSun"><img width="100%" src="/elecFee/images/gsyh/gsyh3.jpg" style="border:1px solid;" /></span> </p> 
  <p> <br /> </p> 
  <p> <span style="font-size: 18px;font-family: 宋体, SimSun">5.选择“华中大校园卡充值”</span> </p> 
  <p style="text-align: center"> <span style="font-size: 18px;font-family: 宋体, SimSun"><img width="100%" src="/elecFee/images/gsyh/gsyh4.jpg" style="border:1px solid;" /></span> </p> 
  <p> <br /> </p> 
  <p> <span style="font-size: 18px;font-family: 宋体, SimSun">6.进入充值页面，输入绑定的银行卡号</span> </p> 
  <p style="text-align: center"> <span style="font-size: 18px;font-family: 宋体, SimSun"><img width="100%" src="/elecFee/images/gsyh/gsyh5.jpg" style="border:1px solid;" /></span> </p> 
  <p> <br /> </p> 
  <p> <span style="font-size: 18px;font-family: 宋体, SimSun">7.确认银行卡卡号和校园卡卡号后，输入充值金额，点击缴费</span> </p> 
  <p style="text-align: center"> <span style="font-size: 18px;font-family: 宋体, SimSun"><img width="100%" src="/elecFee/images/gsyh/gsyh6.jpg" style="border:1px solid;" /></span> </p> 
  <p> <br /> </p> 
  <p style="white-space: normal;text-indent: 36px"> <span style="font-size: 18px;font-family: 宋体"><span style="font-size: 18px; text-align: center; text-indent: 36px;"></span><span style="font-size: 18px; text-align: center;">11.</span>自助服务终端的“补助领取”功能或食堂消费</span><span style="font-size: 18px">POS</span><span style="font-size: 18px;font-family: 宋体">上刷卡，将充值资金领取到校园卡上才能消费。</span> </p> 
  <p style="white-space: normal;text-indent: 36px"> <span style="font-size: 18px;font-family: 宋体">自助服务终端操作流程：</span> </p> 
  <p style="white-space: normal;text-indent: 36px"> <span style="font-size: 18px;font-family: 宋体">首先在屏幕上点击“补助领取”：</span> </p> 
  <p style="white-space: normal;text-align: center"> <img width="100%" src="/elecFee/images/gsyh/gsyh13.jpg" style="border:1px solid;" /> </p> 
  <p style="white-space: normal;text-indent: 36px"> <span style="font-size: 18px;font-family: 宋体">按照提示要求，选择转入钱包，确认后完成</span><span style="font-size: 18px;font-family: 宋体">充值资金领取</span><span style="font-size: 18px;font-family: 宋体">：</span> </p> 
  <p style="white-space: normal;text-align: center"> <img width="100%" src="/elecFee/images/gsyh/gsyh14.jpg" style="border:1px solid;" /> </p> 
  <p style="white-space: normal"> <span style="font-size: 19px;font-family: 宋体, SimSun;color: #ff0000">原因说明：</span><span style="font-size: 19px;font-family: 宋体, SimSun;color: rgb(255,0,0)">由于使用</span><span style="font-size: 19px;font-family: 宋体, SimSun;color: rgb(255,0,0)">网银、手机银行充值无法实时读写卡片，资金将首先进入过渡账户。用户在<span style="font-size: 18px;text-indent: 36px">自助服务终端上使用“补助领取”功能或在消费</span><span style="font-size: 18px;text-indent: 36px">POS</span><span style="font-size: 18px;text-indent: 36px">上刷卡后才能将过渡账户金额写入卡钱包，改变卡余额。</span></span> </p> 
  <div class="button_02_outside button_color" style="margin-bottom:44px"> 
   <input type="button" onclick="window.history.go(-1);" value="返回" class="button_02" /> 
  </div> 
  <jsp:include page="../footer.jsp"> 
   <jsp:param name="source" value="校园卡服务中心" /> 
  </jsp:include>  
 </body>
</html>