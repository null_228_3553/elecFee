<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
 <head></head>
 <body style="margin:5px;">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
  <meta name="keywords" content=""> 
  <meta name="decription" content=""> 
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"> 
  <title></title> 
  <link href="/elecFee/css/icon.css" media="screen" rel="stylesheet" type="text/css"> 
  <link href="/elecFee/css/style1.css" media="screen" rel="stylesheet" type="text/css"> 
  <link href="/elecFee/css/color_02.css" media="screen" rel="stylesheet" type="text/css"> 
  <style type="text/css">
body{
	font-size:18px;
}
</style> 
  <jsp:include page="guideHeader.jsp" flush="true"> 
   <jsp:param name="title" value="校园卡充值指南" /> 
  </jsp:include> 
  <div id="js_article" class="rich_media" style="margin-top:54px"> 
   <div id="js_top_ad_area" class="top_banner"></div> 
   <div class="rich_media_inner"> 
    <div id="page-content"> 
     <div id="img-content" class="rich_media_area_primary"> 
      <h2 class="rich_media_title" id="activity-name"> 中行电子渠道校园卡充值让您足不出户</h2> 
      <div class="rich_media_content " id="js_content"> 
       <p> <img data-s="300,640" data-type="jpeg" data-src="http://mmbiz.qpic.cn/mmbiz/EicJ2AA10DKgWj2sJHYmsWEAGrLDZmnvnd8XCxibst9LibQ8m8bSw4bNia4SzgWwKNfCZB8qucrUBJs6jIGLPygXfA/0?wx_fmt=jpeg" data-ratio="0.6572265625" data-w="1024" src="/elecFee/images/zgyh/640(0)" data-fail="0" width="100%"><br> </p> 
       <fieldset style="max-width: 100%; min-width: 0px; border: 1px solid rgb(226, 226, 226); line-height: 1.6; font-family: inherit; font-size: 1em; box-shadow: rgb(226, 226, 226) 0px 16px 1px -10px; box-sizing: border-box !important; word-wrap: break-word !important;"> 
        <p style="padding: 20px; max-width: 100%; min-height: 1em; border-color: rgb(249, 110, 87); text-align: center; color: rgb(255, 255, 255); line-height: 1.4; font-family: inherit; font-size: 1.4em; font-weight: bold; box-shadow: rgb(221, 221, 221) 0px 3px 3px; box-sizing: border-box !important; word-wrap: break-word !important; background-color: rgb(235, 63, 0);"> <span style="max-width: 100%; line-height: 1.4; letter-spacing: 3px; font-family: 仿宋_GB2312; font-size: 19px; box-sizing: border-box !important; word-wrap: break-word !important;">我行中银易商便民缴费APP、手机银行已开通华中科技大学充值通道，诚邀该校师生体验！</span><br style="max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important;"> </p> 
        <section class="" style="margin-top: 10px; max-width: 100%; box-sizing: border-box; border-color: rgb(235, 63, 0) transparent rgb(235, 63, 0) rgb(249, 110, 87); height: 2em; color: rgb(255, 255, 255); line-height: 1; font-family: inherit; font-size: 1em; border-top-width: 1.1em; border-right-width: 1em; border-bottom-width: 1.1em; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; display: inline-block; word-wrap: break-word !important;"> 
         <section style="margin-top: -1em; padding: 0.5em 1em; max-width: 100%; height: 2em; font-family: inherit; white-space: nowrap; box-sizing: border-box !important; word-wrap: break-word !important;">
           一、中银易商便民缴费APP： 
         </section> 
        </section> 
        <section style="padding: 5px; max-width: 100%; font-family: inherit; box-sizing: border-box !important; word-wrap: break-word !important;">
          仅只需要持有我行银行卡客户，即可按照以下流程进行校园卡的充值。 
        </section> 
       </fieldset> 
       <p style="max-width: 100%; min-height: 1em; box-sizing: border-box !important; word-wrap: break-word !important;"> <br style="max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important;"> </p> 
       <fieldset class="" style="padding: 15px; max-width: 100%; min-width: 0px; border: 1px dotted rgb(235, 63, 0); line-height: 2em; font-family: 宋体; min-height: 1.5em; border-bottom-right-radius: 15px; border-bottom-left-radius: 10px; box-sizing: border-box !important; word-wrap: break-word !important;"> 
        <legend style="max-width: 100%; text-align: center; font-family: 微软雅黑; box-sizing: border-box !important; word-wrap: break-word !important;"> <p class="" style="padding-right: 20px; padding-bottom: 4px; padding-left: 20px; max-width: 100%; min-height: 1.5em; color: rgb(255, 255, 255); line-height: 2em; font-size: 14px; border-bottom-right-radius: 100%; border-bottom-left-radius: 100%; box-sizing: border-box !important; word-wrap: break-word !important; background-color: rgb(235, 63, 0);"> <strong style="max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important;">操作步骤</strong> </p> </legend> 
        <p style="max-width: 100%; min-height: 1.5em; line-height: 2em; box-sizing: border-box !important; word-wrap: break-word !important;"> <span class="" style="max-width: 100%; line-height: 2em; box-sizing: border-box !important; word-wrap: break-word !important;">1、扫描二维码下载“中银易商APP”。</span> </p> 
        <p style="max-width: 100%; min-height: 1.5em; line-height: 2em; box-sizing: border-box !important; word-wrap: break-word !important;"> <span class="" style="max-width: 100%; line-height: 2em; box-sizing: border-box !important; word-wrap: break-word !important;"><img data-ratio="1.0277777777777777" data-s="300,640" data-src="http://mmbiz.qpic.cn/mmbiz/GaoicewuK9Cp6Q1h2KwcibMVjzUoIuiaQoIUCGGDUgQVrfgGzy1ibReyD2lialibkv0E21zFq4T8yBKAyBzcnM3j3FLg/640?wx_fmt=png" data-type="png" data-w="180" width="100%" src="/elecFee/images/zgyh/640(1)" data-fail="0"><br style="max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important;"></span> </p> 
        <p style="max-width: 100%; min-height: 1em; box-sizing: border-box !important; word-wrap: break-word !important;">2、安装中银易商客户端，注册用户并绑定中国银行借记卡。</p> 
        <p style="max-width: 100%; min-height: 1em; box-sizing: border-box !important; word-wrap: break-word !important;">3、登录APP，选择“便民缴费”。</p> 
        <p style="max-width: 100%; min-height: 1.5em; line-height: 2em; box-sizing: border-box !important; word-wrap: break-word !important;"> <img data-ratio="1.7771883289124668" data-s="300,640" data-src="http://mmbiz.qpic.cn/mmbiz/GaoicewuK9Cp6Q1h2KwcibMVjzUoIuiaQoIicx9ARSibLzRkib3pTYwhGc2RlC8IlNrDxS56rLgYJM9huQtAIfZib4a2w/640?wx_fmt=jpeg" data-type="jpeg" data-w="377" width="100%" src="/elecFee/images/zgyh/640(2)"><br style="max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important;"> </p> 
        <p style="max-width: 100%; min-height: 1.5em; line-height: 2em; box-sizing: border-box !important; word-wrap: break-word !important;"> <span class="" style="max-width: 100%; line-height: 2em; box-sizing: border-box !important; word-wrap: break-word !important;">4、选择“校园卡”，按照提示输入充值信息，提交支付。</span><br style="max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important;"> </p> 
        <p style="max-width: 100%; min-height: 1.5em; line-height: 2em; box-sizing: border-box !important; word-wrap: break-word !important;"> <img data-ratio="1.7826086956521738" data-s="300,640" data-src="http://mmbiz.qpic.cn/mmbiz/GaoicewuK9Cp6Q1h2KwcibMVjzUoIuiaQoIianrCYjoIAkaQsic4eapOHibDCuXsygOZg0EXMuq9CGCK2vzXUqkE9n9w/640?wx_fmt=jpeg" data-type="jpeg" data-w="391" width="100%" src="/elecFee/images/zgyh/640(3)"><br> </p> 
       </fieldset> 
       <p style="line-height: 25.6px;"> <br> </p> 
       <fieldset style="font-family: inherit; font-size: 1em; line-height: 1.6; white-space: normal; max-width: 100%; min-width: 0px; border: 1px solid rgb(226, 226, 226); box-shadow: rgb(226, 226, 226) 0px 16px 1px -10px; box-sizing: border-box !important; word-wrap: break-word !important;"> 
        <section class="" style="margin-top: 10px; max-width: 100%; box-sizing: border-box; border-color: rgb(235, 63, 0) transparent rgb(235, 63, 0) rgb(249, 110, 87); height: 2em; color: rgb(255, 255, 255); line-height: 1; font-family: inherit; font-size: 1em; border-top-width: 1.1em; border-right-width: 1em; border-bottom-width: 1.1em; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; display: inline-block; word-wrap: break-word !important;"> 
         <section style="margin-top: -1em; padding: 0.5em 1em; max-width: 100%; height: 2em; font-family: inherit; white-space: nowrap; box-sizing: border-box !important; word-wrap: break-word !important;">
           二、手机银行： 
         </section> 
        </section> 
        <section style="padding: 5px; max-width: 100%; font-family: inherit; box-sizing: border-box !important; word-wrap: break-word !important;">
          &nbsp; &nbsp; &nbsp; 需本人持银行卡和身份证以及手机前往我行开通手机银行。 
        </section> 
       </fieldset> 
       <fieldset class="" style="padding: 15px; white-space: normal; max-width: 100%; min-width: 0px; border: 1px dotted rgb(235, 63, 0); line-height: 2em; font-family: 宋体; min-height: 1.5em; border-bottom-right-radius: 15px; border-bottom-left-radius: 10px; box-sizing: border-box !important; word-wrap: break-word !important;"> 
        <legend style="max-width: 100%; text-align: center; font-family: 微软雅黑; box-sizing: border-box !important; word-wrap: break-word !important;"> <p class="" style="padding-right: 20px; padding-bottom: 4px; padding-left: 20px; max-width: 100%; min-height: 1.5em; color: rgb(255, 255, 255); line-height: 2em; font-size: 14px; border-bottom-right-radius: 100%; border-bottom-left-radius: 100%; box-sizing: border-box !important; word-wrap: break-word !important; background-color: rgb(235, 63, 0);"> <strong style="max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important;">操作步骤</strong> </p> </legend> 
        <p style="max-width: 100%; min-height: 1.5em; line-height: 2em; box-sizing: border-box !important; word-wrap: break-word !important;"> <span class="" style="max-width: 100%; line-height: 2em; box-sizing: border-box !important; word-wrap: break-word !important;">1、进入手机银行；</span> </p> 
        <p> <img data-s="300,640" data-type="jpeg" data-src="http://mmbiz.qpic.cn/mmbiz_jpg/EicJ2AA10DKhXic8bZiasYwFzicKcg3Qzx0ewanSxvEVpibUO2RibJib2PefEzYCBaHC9ZdzhEvy0KkE8vYrxsxxJVnrg/0?wx_fmt=jpeg" data-ratio="1.7777777777777777" data-w="540" src="/elecFee/images/zgyh/640(11)" width="100%"><br> <span style="line-height: 2em;">2、选择民生缴费；</span> </p> 
        <p> <span style="line-height: 2em;"><img data-s="300,640" data-type="jpeg" data-src="http://mmbiz.qpic.cn/mmbiz_jpg/EicJ2AA10DKhXic8bZiasYwFzicKcg3Qzx0eHGglA7vezKynjnV9goW2xIGwfUniaoKnibLdkI2yFxibjVLjP2tXlfpvQ/0?wx_fmt=jpeg" data-ratio="1.7802197802197801" data-w="455" src="/elecFee/images/zgyh/640(10)" width="100%"><br>3、选择校园卡；</span> </p> 
        <p> <span style="line-height: 2em;"><img data-s="300,640" data-type="jpeg" data-src="http://mmbiz.qpic.cn/mmbiz_jpg/EicJ2AA10DKhXic8bZiasYwFzicKcg3Qzx0ePziaPGYH2Ob8QUCIiatFqooWyvLGQhzIf3HYJJkibTtCYiblcaIicrmicT7A/0?wx_fmt=jpeg" data-ratio="1.7802197802197801" data-w="455" src="/elecFee/images/zgyh/640(9)" width="100%"><br></span> </p> 
        <p style="max-width: 100%; min-height: 1.5em; line-height: 2em; box-sizing: border-box !important; word-wrap: break-word !important;"> <span class="" style="max-width: 100%; line-height: 2em; box-sizing: border-box !important; word-wrap: break-word !important;">4、商户名称选择：“华中科技大学”；</span> </p> 
        <p style="max-width: 100%; min-height: 1.5em; line-height: 2em; box-sizing: border-box !important; word-wrap: break-word !important;"> <span class="" style="max-width: 100%; line-height: 2em; box-sizing: border-box !important; word-wrap: break-word !important;"><img data-s="300,640" data-type="jpeg" data-src="http://mmbiz.qpic.cn/mmbiz_jpg/EicJ2AA10DKhXic8bZiasYwFzicKcg3Qzx0eymITdDBfVvsuhYAg1PV5GUzeR1YGPmPmqOL4dIv5SseWibnfXupicL7A/0?wx_fmt=jpeg" data-ratio="1.7777777777777777" data-w="540" src="/elecFee/images/zgyh/640(8)" width="100%"><br></span> </p> 
        <p style="max-width: 100%; min-height: 1.5em; line-height: 2em; box-sizing: border-box !important; word-wrap: break-word !important;"> <span class="" style="max-width: 100%; line-height: 2em; box-sizing: border-box !important; word-wrap: break-word !important;">5、输入学号或学工号以及身份证号；</span> </p> 
        <p style="max-width: 100%; min-height: 1.5em; line-height: 2em; box-sizing: border-box !important; word-wrap: break-word !important;"> <span class="" style="max-width: 100%; line-height: 2em; box-sizing: border-box !important; word-wrap: break-word !important;"><img data-s="300,640" data-type="jpeg" data-src="http://mmbiz.qpic.cn/mmbiz_jpg/EicJ2AA10DKhXic8bZiasYwFzicKcg3Qzx0ek8xd7NjKReGDm10U2BZuYY3Rd1NweGSRIggia5TQiavjfKj7hM4aRuyw/0?wx_fmt=jpeg" data-ratio="1.7802197802197801" data-w="455" src="/elecFee/images/zgyh/640(7)" width="100%"><br></span> </p> 
        <p style="max-width: 100%; min-height: 1.5em; line-height: 2em; box-sizing: border-box !important; word-wrap: break-word !important;"> <span class="" style="max-width: 100%; line-height: 2em; box-sizing: border-box !important; word-wrap: break-word !important;">6、输入您需要的缴费金额；</span> </p> 
        <p style="max-width: 100%; min-height: 1.5em; line-height: 2em; box-sizing: border-box !important; word-wrap: break-word !important;"> <span class="" style="max-width: 100%; line-height: 2em; box-sizing: border-box !important; word-wrap: break-word !important;"><img data-s="300,640" data-type="jpeg" data-src="http://mmbiz.qpic.cn/mmbiz_jpg/EicJ2AA10DKhXic8bZiasYwFzicKcg3Qzx0eN7YJm1zzU4b4RLKOdcJvj5ojBq0vCLnDlVicQmicRK9qPaG6s71SrATQ/0?wx_fmt=jpeg" data-ratio="1.7802197802197801" data-w="455" src="/elecFee/images/zgyh/640(6)" width="100%"><br></span> </p> 
        <p style="max-width: 100%; min-height: 1.5em; line-height: 2em; box-sizing: border-box !important; word-wrap: break-word !important;"> <span class="" style="max-width: 100%; line-height: 2em; box-sizing: border-box !important; word-wrap: break-word !important;">7、选择认证工具（小于2万元的金额都可仅选择手机交易码，不必担心没有携带动态口令而带来的麻烦）；</span> </p> 
        <p style="max-width: 100%; min-height: 1.5em; line-height: 2em; box-sizing: border-box !important; word-wrap: break-word !important;"> <span class="" style="max-width: 100%; line-height: 2em; box-sizing: border-box !important; word-wrap: break-word !important;"><img data-s="300,640" data-type="jpeg" data-src="http://mmbiz.qpic.cn/mmbiz_jpg/EicJ2AA10DKhXic8bZiasYwFzicKcg3Qzx0e9K2bEjrVFc6kGiavRTSMibyaBxicibSH0xXeTvZiavL5jnasOxOt9CibeF0Q/0?wx_fmt=jpeg" data-ratio="1.7802197802197801" data-w="455" src="/elecFee/images/zgyh/640(4)" width="100%"><br></span> </p> 
        <p style="max-width: 100%; min-height: 1.5em; line-height: 2em; box-sizing: border-box !important; word-wrap: break-word !important;"> <span class="" style="max-width: 100%; line-height: 2em; box-sizing: border-box !important; word-wrap: break-word !important;">7、输入收到的手机交易码完成缴费。</span> </p> 
        <p style="max-width: 100%; min-height: 1.5em; line-height: 2em; box-sizing: border-box !important; word-wrap: break-word !important;"> <span class="" style="max-width: 100%; line-height: 2em; box-sizing: border-box !important; word-wrap: break-word !important;"><img data-s="300,640" data-type="jpeg" data-src="http://mmbiz.qpic.cn/mmbiz_jpg/EicJ2AA10DKhXic8bZiasYwFzicKcg3Qzx0e4V6DCGg6NWEnDrgr2JUb8ib1G2FlODlhbAgiaXTAYeQUFFblibCRSibJzA/0?wx_fmt=jpeg" data-ratio="1.7802197802197801" data-w="455" src="/elecFee/images/zgyh/640(5)" width="100%"><br></span> </p> 
       </fieldset> 
       <fieldset style="line-height: 25.6px; white-space: normal; max-width: 100%; min-width: 0px; border-width: 0px; box-sizing: border-box !important; word-wrap: break-word !important;"> 
        <section style="padding: 0.3em; max-width: 100%; border: 1px solid rgb(226, 226, 226); line-height: 1em; font-size: 1em; box-shadow: rgb(226, 226, 226) 0px 1em 0.1em -0.8em; box-sizing: border-box !important; word-wrap: break-word !important; background-color: rgb(255, 255, 255);"> 
         <section class="" style="padding: 0.5em; max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important; background-color: rgb(235, 63, 0);"> 
          <p style="margin-left: 8px; max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important;"> <br> </p> 
          <p style="margin-left: 8px; max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important;"> <span style="font-size: 20px;">温馨提示</span>：如果您充值成功后，在使用校园卡时发现资金未圈成成功。这是因校园卡在消费机上芯片读取时间有误造成的。请您在附近的圈存机上选择“领取补助”即可立马到帐。 </p> 
         </section> 
        </section> 
       </fieldset> 
       <p> <br> </p> 
      </div> 
      <div class="rich_media_tool" id="js_toobar3"> 
       <div id="js_read_area3" class="media_tool_meta tips_global meta_primary" style="display: none;">
         阅读 
        <span id="readNum3"></span> 
       </div> 
       <span style="display: none;" class="media_tool_meta meta_primary tips_global meta_praise" id="like3"> <i class="icon_praise_gray"></i><span class="praise_num" id="likeNum3"></span> </span> 
       <a id="js_report_article3" style="display: none;" class="media_tool_meta tips_global meta_extra" href="javascript:void(0);">投诉</a> 
      </div> 
     </div> 
     <div class="rich_media_area_primary sougou" id="sg_tj" style="display: none"></div> 
     <div class="rich_media_area_extra"> 
      <div class="mpda_bottom_container" id="js_bottom_ad_area"></div> 
      <div id="js_iframetest" style="display: none;"></div> 
     </div> 
    </div> 
   </div> 
  </div> 
  <div class="button_02_outside button_color" style="margin-bottom:44px">
	<input type="button" onclick="window.history.go(-1);" value="返回" class="button_02">
  </div>
  <jsp:include page="../footer.jsp"> 
   <jsp:param name="source" value="校园卡服务中心" /> 
  </jsp:include>  
 </body>
</html>