<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="" />
	<meta name="decription" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<title>密码安全</title>
</head>
<script src="/elecFee/js/jquery.js" type="text/javascript"></script>
<script src="safe.js" type="text/javascript"></script>
<body style="background-color: #f8f8f8;font-family:microsoft yahei;">
<div class="video" >
	<video id="video" controls="controls" width="100%" 
		poster="/elecFee/images/safe/mimaanquan.jpg" 
		src="http://m.hust.edu.cn:8082/dcp/uploadfiles/video/2016/mimaanquan.mp4" 
		style="position:fixed;z-index: 10;top:0;left:0;"></video>
</div>
<div class="title" style="background-color: #fff;text-align:center;font-size: 20px; margin-top:5px;padding: 3px 0;">
<blockquote id="m915" class="yead_editor yead-selected" data-author="Wxeditor" style="font-size:14px;border:0px;padding:0px;margin:5px auto;white-space: normal;">
    <section style="box-sizing:border-box">
        <section style="margin-top:10px;margin-bottom:10px;text-align:center;box-sizing:border-box">
            <section style="margin-bottom:-.9em;display:inline-block;vertical-align:top;box-sizing:border-box">
                <section class="yead_bdtc yead_bdrc" style="filter:alpha(opacity=50);-moz-opacity:.5;-khtml-opacity:.5;opacity:.5;display:inline-block;vertical-align:bottom;border-right-width:.4em;border-right-style:solid;border-right-color:#f1363f;border-top-width:.4em;border-top-style:solid;border-top-color:#f1363f;box-sizing:border-box;max-width:5%;border-bottom-width:.4em!important;border-bottom-style:solid!important;border-bottom-color:transparent!important;border-left-width:.4em!important;border-left-style:solid!important;border-left-color:transparent!important"></section>
                <section class="yead_bgc" style="padding:2px 5px;display:inline-block;vertical-align:top;font-size:18px;line-height:2em;border-radius:5px 5px 0 0;color:#fff;box-sizing:border-box;max-width:90%;background-color:#756263">
                    <p style="margin:0;box-sizing:border-box">
                        <strong style="box-sizing:border-box">密码安全</strong>
                    </p>
                </section>
                <section class="yead_bdtc yead_bdlc" style="filter:alpha(opacity=50);-moz-opacity:.5;-khtml-opacity:.5;opacity:.5;display:inline-block;vertical-align:bottom;border-top-width:.4em;border-top-style:solid;border-top-color:#f1363f;border-left-width:.4em;border-left-style:solid;border-left-color:#f1363f;box-sizing:border-box;max-width:5%;border-right-width:.4em!important;border-right-style:solid!important;border-right-color:transparent!important;border-bottom-width:.4em!important;border-bottom-style:solid!important;border-bottom-color:transparent!important"></section>
            </section>
            <section style="height:.9em;border-top-width:3px;border-top-style:solid;border-top-color:#000;box-sizing:border-box"></section>
        </section>
    </section>
</blockquote>
</div>
<div class="description" style="color:#999999;font-size:18px;background-color: #fff;margin-top:5px 0;padding: 8px 0;">
<blockquote id="m946" class="yead_editor yead-selected" data-author="Wxeditor" style="font-size:14px;border:0px;padding:0px;margin:5px auto;white-space: normal;">
    <section style="position:relative;margin:0 auto">
        <section style="margin:.5em auto">
            <section style="display:inline-block;width:100%;vertical-align:top">
                <section style="text-align:center;position:static">
                    <section style="display:inline-block;vertical-align:top;width:40%">
                        <section style="position:static">
                            <section style="margin-top:.5em;margin-bottom:.5em;position:static">
                                <section style="border-top:1px dotted #dfdfdf"></section>
                            </section>
                        </section>
                    </section>
                    <section style="display:inline-block;vertical-align:top;width:15%">
                        <section style="position:static">
                            <section style="margin:5px 0 10px;line-height:.8;position:static">
                                <section style="width:0;display:inline-block;border-top-width:.8em;border-top-style:solid;border-top-color:#d99694;border-left-width:.7em!important;border-left-style:solid!important;border-left-color:transparent!important;border-right-width:.7em!important;border-right-style:solid!important;border-right-color:transparent!important"></section>
                            </section>
                        </section>
                    </section>
                    <section style="display:inline-block;vertical-align:top;width:40%">
                        <section style="position:static">
                            <section style="margin-top:.5em;margin-bottom:.5em;position:static">
                                <section style="border-top:1px dotted #dfdfdf"></section>
                            </section>
                        </section>
                    </section>
                </section>
                <section id="content" style="position:static;overflow-y:auto;">
                    <section style="text-align:justify;padding:0 10px">
                        <p style="font-family: Simsun; font-size: 18px; line-height: 30px; white-space: normal; widows: 1; text-indent: 2em;">
大量用户选择将一个密码重复使用，这意味着如果任何一个系统的密码被攻破，攻击者就会在其他系统尝试这个密码，那么其他资源也会被攻击者轻易获取。因此为了安全，强烈建议不在多个系统中使用同一套相同的密码。
                        </p>
                        <p style="font-family: Simsun; font-size: 18px; line-height: 30px; white-space: normal; widows: 1; text-indent: 2em;">
在使用学校的各类信息系统时，必须保证本人的密码具有较高强度；如果有的系统已经为你初始化了密码，请在登录后立即修改密码，并且保证新设的密码具有较高强度。
                        </p>
                        <p style="font-family: Simsun; font-size: 18px; line-height: 30px; white-space: normal; widows: 1; text-indent: 2em;">
学校的信息系统中包含了你的大量个人敏感信息，要保管好自己的密码，绝对不要把密码借给他人使用。学校统一身份认证系统（https://pass.hust.edu.cn）已集成了校内10余个重要信息系统，其密码非常重要，一定要妥善保管；工作人员在帮你解决问题时绝对不会向你索要密码；如果密码忘记，可使用手机发送验证码或邮箱发送密码重置链接进行重置。
                        </p>
                    </section>
                </section>
            </section>
            <section style="position:static">
                <section style="margin-right:0;margin-left:0;text-align:center;position:static">
                    <section style="display:inline-block;vertical-align:top;width:40%">
                        <section style="position:static">
                            <section style="margin:18px 0 8px;position:static">
                                <section style="border-top:1px dotted #dfdfdf"></section>
                            </section>
                        </section>
                    </section>
                    <section style="display:inline-block;vertical-align:top;width:15%">
                        <section style="margin-top:10px;margin-bottom:10px;line-height:.8;position:static">
                            <section style="width:0;display:inline-block;border-bottom:.8em solid #79ccd5;border-left:.7em solid transparent!important;border-right:.7em solid transparent!important"></section>
                        </section>
                    </section>
                    <section style="display:inline-block;vertical-align:top;width:40%">
                        <section style="position:static">
                            <section style="margin:18px 0 8px;position:static">
                                <section style="border-top:1px dotted #dfdfdf"></section>
                                <div id="viewCount" style="text-align: right;font-size: 14px;padding-top:10px;"></div>
                            </section>
                        </section>
                    </section>
                </section>
            </section>
        </section>
    </section>
</blockquote>
</div>
</body>
</html>