<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="" />
	<meta name="decription" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<title>安全视频</title>
</head>
<link rel="stylesheet" href="/elecFee/css/bootstrap.css"></link>
<link rel="stylesheet" href="/elecFee/css/style.css"></link>
<link rel="stylesheet" href="/elecFee/css/icon.css"></link>
<link rel="stylesheet" href="/elecFee/css/color_03.css"></link>
<style>
.icon_div {
    font-size: 40px !important;
    padding-right: 10px;
    margin-left: 15px;
    margin-right: 10px;
    display: inline;
    line-height: 40px;
    padding-top: 5px;
}
</style>

<body style="background-color: #f8f8f8;font-family: microsoft yahei;font-size:18px;">
<header class="header_box key_color">
	<a class="header_home pp_a016 isz_24 left key_color" href="http://m.hust.edu.cn/wechat/"></a>
	网络安全视频
	<a  class="header_about pp_a017 isz_26 right key_color"></a>
</header>
<div class="container-fluid" style="padding-top:60px;padding-bottom:25px;">
	<div class="col-sm-4" style="color:black">
		<ul class="list-group">
        <li class="list-group-item" onclick="window.location.href='/elecFee/safe/yidonganquan.jsp'">
        	<div class="icon_div">
				<img src="/elecFee/images/safe/移动安全.png" height="50px"/>
			</div>
			移动安全
        </li>
        <li class="list-group-item" onclick="window.location.href='/elecFee/safe/mimaanquan.jsp'">
			<div class="icon_div">
				<img src="/elecFee/images/safe/密码安全.png" height="50px"/>
			</div>
			密码安全
		</li>
        <li class="list-group-item" onclick="window.location.href='/elecFee/safe/dianziyoujiananquan.jsp'">
			<div class="icon_div">
				<img src="/elecFee/images/safe/电子邮件安全.png" height="50px"/>
			</div>
			邮件安全
		</li>
        <li class="list-group-item" onclick="window.location.href='/elecFee/safe/chailvanquan.jsp'">
			<div class="icon_div">
				<img src="/elecFee/images/safe/差旅安全.png" height="50px"/>
			</div>
			差旅安全
		</li>
        <li class="list-group-item" onclick="window.location.href='/elecFee/safe/wulianquan.jsp'">
			<div class="icon_div">
				<img src="/elecFee/images/safe/物理安全.png" height="50px"/>
			</div>
			物理安全
		</li>
        <li class="list-group-item" onclick="window.location.href='/elecFee/safe/shejiaoanquan.jsp'">
			<div class="icon_div">
				<img src="/elecFee/images/safe/社交安全.png" height="50px"/>
			</div>
			社交安全
		</li>
        <li class="list-group-item" onclick="window.location.href='/elecFee/safe/wangluodiaoyuanquan.jsp'">
			<div class="icon_div">
				<img src="/elecFee/images/safe/网络钓鱼安全.png" height="50px"/>
			</div>
			网络钓鱼
		</li>
        <li class="list-group-item" onclick="window.location.href='/elecFee/safe/eyiruanjianyishi.jsp'">
			<div class="icon_div">
				<img src="/elecFee/images/safe/恶意软件意识.png" height="50px"/>
			</div>
			恶意软件
		</li>
       	</ul>
	</div>
</div>
<div class="bottom_box_02">
	<div class="bottom_box_inside">
		<span style="width:233px;" id="bottom_p_02">
			智慧华中大&nbsp;&nbsp;数据提供：网络与计算中心
		</span>
	</div>
</div>
</body>
</html>