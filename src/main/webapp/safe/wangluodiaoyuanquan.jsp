<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="" />
	<meta name="decription" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<title>网络钓鱼安全</title>
</head>
<script src="/elecFee/js/jquery.js" type="text/javascript"></script>
<script src="safe.js" type="text/javascript"></script>
<body style="background-color: #f8f8f8;font-family:microsoft yahei;">
<div class="video" >
	<video id="video" controls="controls" width="100%" 
		src="http://m.hust.edu.cn:8082/dcp/uploadfiles/video/2016/wangluodiaoyu.mp4" 
		poster="/elecFee/images/safe/wangluodiaoyu.jpg" 
		style="position:fixed;z-index: 10;top:0;left:0;"></video>
</div>
<div class="title" style="background-color: #fff;text-align:center;font-size: 20px; margin-top:5px;padding: 3px 0;">
<blockquote id="m915" class="yead_editor yead-selected" data-author="Wxeditor" style="font-size:14px;border:0px;padding:0px;margin:5px auto;white-space: normal;">
    <section style="box-sizing:border-box">
        <section style="margin-top:10px;margin-bottom:10px;text-align:center;box-sizing:border-box">
            <section style="margin-bottom:-.9em;display:inline-block;vertical-align:top;box-sizing:border-box">
                <section class="yead_bdtc yead_bdrc" style="filter:alpha(opacity=50);-moz-opacity:.5;-khtml-opacity:.5;opacity:.5;display:inline-block;vertical-align:bottom;border-right-width:.4em;border-right-style:solid;border-right-color:#f1363f;border-top-width:.4em;border-top-style:solid;border-top-color:#f1363f;box-sizing:border-box;max-width:5%;border-bottom-width:.4em!important;border-bottom-style:solid!important;border-bottom-color:transparent!important;border-left-width:.4em!important;border-left-style:solid!important;border-left-color:transparent!important"></section>
                <section class="yead_bgc" style="padding:2px 5px;display:inline-block;vertical-align:top;font-size:18px;line-height:2em;border-radius:5px 5px 0 0;color:#fff;box-sizing:border-box;max-width:90%;background-color:#756263">
                    <p style="margin:0;box-sizing:border-box">
                        <strong style="box-sizing:border-box">网络钓鱼安全</strong>
                    </p>
                </section>
                <section class="yead_bdtc yead_bdlc" style="filter:alpha(opacity=50);-moz-opacity:.5;-khtml-opacity:.5;opacity:.5;display:inline-block;vertical-align:bottom;border-top-width:.4em;border-top-style:solid;border-top-color:#f1363f;border-left-width:.4em;border-left-style:solid;border-left-color:#f1363f;box-sizing:border-box;max-width:5%;border-right-width:.4em!important;border-right-style:solid!important;border-right-color:transparent!important;border-bottom-width:.4em!important;border-bottom-style:solid!important;border-bottom-color:transparent!important"></section>
            </section>
            <section style="height:.9em;border-top-width:3px;border-top-style:solid;border-top-color:#000;box-sizing:border-box"></section>
        </section>
    </section>
</blockquote>
</div>
<div class="description" style="color:#999999;font-size:18px;background-color: #fff;margin-top:5px 0;padding: 8px 0;">
<blockquote id="m946" class="yead_editor yead-selected" data-author="Wxeditor" style="font-size:14px;border:0px;padding:0px;margin:5px auto;white-space: normal;">
    <section style="position:relative;margin:0 auto">
        <section style="margin:.5em auto">
            <section style="display:inline-block;width:100%;vertical-align:top">
                <section style="text-align:center;position:static">
                    <section style="display:inline-block;vertical-align:top;width:40%">
                        <section style="position:static">
                            <section style="margin-top:.5em;margin-bottom:.5em;position:static">
                                <section style="border-top:1px dotted #dfdfdf"></section>
                            </section>
                        </section>
                    </section>
                    <section style="display:inline-block;vertical-align:top;width:15%">
                        <section style="position:static">
                            <section style="margin:5px 0 10px;line-height:.8;position:static">
                                <section style="width:0;display:inline-block;border-top-width:.8em;border-top-style:solid;border-top-color:#d99694;border-left-width:.7em!important;border-left-style:solid!important;border-left-color:transparent!important;border-right-width:.7em!important;border-right-style:solid!important;border-right-color:transparent!important"></section>
                            </section>
                        </section>
                    </section>
                    <section style="display:inline-block;vertical-align:top;width:40%">
                        <section style="position:static">
                            <section style="margin-top:.5em;margin-bottom:.5em;position:static">
                                <section style="border-top:1px dotted #dfdfdf"></section>
                            </section>
                        </section>
                    </section>
                </section>
                <section id="content" style="position:static;overflow-y:auto;">
                    <section style="text-align:justify;padding:0 10px">
                        <p style="font-family: Simsun; font-size: 18px; line-height: 30px; white-space: normal; widows: 1; text-indent: 2em;">
网络钓鱼”是通过发送声称来自于银行或其他知名机构的欺骗性垃圾邮件，意图引诱收信人给出敏感信息（如用户名、口令、帐号 、密码或信用卡详细信息等）的一种攻击方式。
                        </p>
                        <p style="font-family: Simsun; font-size: 18px; line-height: 30px; white-space: normal; widows: 1; text-indent: 2em;">
最典型的网络钓鱼攻击将收信人引诱到一个通过精心设计与目标组织的网站非常相似的钓鱼网站上，并获取收信人在此网站上输入的个人敏感信息，通常这个攻击过程不会让受害者警觉。
                        </p>
                        <p style="font-family: Simsun; font-size: 18px; line-height: 30px; white-space: normal; widows: 1; text-indent: 2em;">
如何防范网络钓鱼，小编为大家找到了几个解决方案：
                        </p>
                        <p style="font-family: Simsun; font-size: 18px; line-height: 30px; white-space: normal; widows: 1; text-indent: 2em;">
1、为电脑安装杀毒软件，定期扫描系统、查杀病毒；及时更新病毒库、更新系统补丁；
                        </p>
                        <p style="font-family: Simsun; font-size: 18px; line-height: 30px; white-space: normal; widows: 1; text-indent: 2em;">
2、下载软件时尽量到官方网站或大型软件下载网站，在安装或打开来历不明的软件或文件前先杀毒；
                        </p>
                        <p style="font-family: Simsun; font-size: 18px; line-height: 30px; white-space: normal; widows: 1; text-indent: 2em;">
3、不随意打开不明网页链接，尤其是不良网站的链接，陌生人通过QQ 给自己传链接时，尽量不要打开；
                        </p>
                        <p style="font-family: Simsun; font-size: 18px; line-height: 30px; white-space: normal; widows: 1; text-indent: 2em;">
4、使用网络通信工具时不随意接收陌生人的文件，若接收可取消“隐藏已知文件类型扩展名”功能来查看文件类型；
                        </p>
                        <p style="font-family: Simsun; font-size: 18px; line-height: 30px; white-space: normal; widows: 1; text-indent: 2em;">
5、对公共磁盘空间加强权限管理，定期查杀病毒。
                         </p>
                    </section>
                </section>
            </section>
            <section style="position:static">
                <section style="margin-right:0;margin-left:0;text-align:center;position:static">
                    <section style="display:inline-block;vertical-align:top;width:40%">
                        <section style="position:static">
                            <section style="margin:18px 0 8px;position:static">
                                <section style="border-top:1px dotted #dfdfdf"></section>
                            </section>
                        </section>
                    </section>
                    <section style="display:inline-block;vertical-align:top;width:15%">
                        <section style="margin-top:10px;margin-bottom:10px;line-height:.8;position:static">
                            <section style="width:0;display:inline-block;border-bottom:.8em solid #79ccd5;border-left:.7em solid transparent!important;border-right:.7em solid transparent!important"></section>
                        </section>
                    </section>
                    <section style="display:inline-block;vertical-align:top;width:40%">
                        <section style="position:static">
                            <section style="margin:18px 0 8px;position:static">
                                <section style="border-top:1px dotted #dfdfdf"></section>
                                <div id="viewCount" style="text-align: right;font-size: 14px;padding-top:10px;"></div>
                            </section>
                        </section>
                    </section>
                </section>
            </section>
        </section>
    </section>
</blockquote>
</div>
</body>
</html>