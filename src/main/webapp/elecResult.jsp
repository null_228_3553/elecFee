<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="" />
	<meta name="decription" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
	<title></title>		
	<link href="css/style.css" media="screen" rel="stylesheet" type="text/css" />
	<link href="css/color_01.css" media="screen" rel="stylesheet" type="text/css" />
	<link href="css/dfcx.css" media="screen" rel="stylesheet" type="text/css" />
	<link href="css/icon.css" media="screen" rel="stylesheet" type="text/css" />
<script src="/elecFee/js/jquery.js" type="text/javascript"></script>
<script src="/elecFee/js/resetAndroidFontsize.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#leftTag").click(function(){
		$(this).attr("class","tab_01_select left_radius");
		$("#rightTag").attr("class","right_radius");
		$("#leftList").show();
		$("#leftHeader").show();
		$("#rightList").hide();
		$("#rightHeader").hide();
	});
	$("#rightTag").click(function(){
		$(this).attr("class","tab_01_select right_radius");
		$("#leftTag").attr("class","left_radius");
		$("#leftList").hide();
		$("#leftHeader").hide();
		$("#rightList").show();
		$("#rightHeader").show();
	});
	$("#setDormitory").click(function(){
		window.location.href="setDormitoryPre.do";
	});
	$("#setThreshold").click(function(){
		window.location.href="setThresholdPre.do";
	})
});
</script>
</head>
<body>
<jsp:include page="header.jsp" flush="true">
	<jsp:param name="title" value="电费查询" /> 
</jsp:include>
<div class="main_02">
<div class="dfcx_title_bar" style="padding-left:6px;width:calc(100% - 6px);">
<%-- 	<span class="dfcx_title_02 black_444"><c:out value="${dormitory.programId}"/></span> --%>
	<span class="dfcx_title_02 black_444"><c:out value="${dormitory.txtyq}"/></span>
<%-- 	<span class="dfcx_title_02 black_444"><c:out value="${dormitory.txtld}"/></span>	 --%>
	<span class="dfcx_title_02 black_444"><c:out value="${dormitory.txtroom}"/></span>
	<span class="dfcx_set key_color" id="setThreshold">设置提醒</span>		
	<span class="dfcx_set key_color" id="setDormitory">设置宿舍</span>		
</div>
<div class="dfcx_box_02 margin_top_10">
	<p class="dfcx_bar_02">
		<span class="dfcx_name_01">您的剩余电量为：</span>
		<font class="dfcx_name_02"><c:out value="${remainElec}"/></font>
	</p>
	<p class="dfcx_bar_03">
		最后抄表时间：<c:out value="${lastCheck}"/>
	</p>
</div>
<div class="tab_01" style="margin:15px 12px 0;">
    <a class="tab_01_select left_radius" id="leftTag">近七天抄表明细</a>
    <a class="right_radius" id="rightTag">近一个月购电明细</a>
</div>
 <p class="dfcx_row_header" id="leftHeader">
	<span class="left">抄表数据</span>
	<span class="right">抄表时间</span>
</p>
<p class="dfcx_row_header" id="rightHeader" style="display:none">
	<span class="left">充值电量</span>
	<span class="left" style="margin-left:30px;">实收电费</span>
	<span class="right">购电时间</span>
</p>
<div class="dfcx_list" id="leftList" >
	<c:forEach items="${detailList}" var="xx">
		<div class="dfcx_row">
			<p class="dfcx_electric"><c:out value="${xx.data}"/></p>
			<p class="dfcx_time"><c:out value="${xx.time}"/></p>
		</div>
	</c:forEach>
</div>
<div class="dfcx_list" id="rightList" style="display:none">
	<c:forEach items="${paymentList}" var="xx">
	<div class="dfcx_row">
		<p class="dfcx_electric"><c:out value="${xx.elecQty}"/></p>
		<p class="dfcx_money"><c:out value="${xx.fee}"/></p>
		<p class="dfcx_time"><c:out value="${xx.time}"/></p>
	</div>
	</c:forEach>
</div>
</div>
<jsp:include page="footer.jsp">
	<jsp:param name="source" value="电费查询" /> 
</jsp:include>

</body>
</html>