<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="" />
	<meta name="decription" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<title>500服务器内部错误</title>
<link rel="stylesheet" type="text/css" href="/elecFee/css/error.css">
</head>
<body>
	<div id="wrapper">
		<a class="logo" href="/"></a>
		<div id="main">
			<header id="header">
				<h1>
					<span class="icon">!</span>500<span class="sub">SERVER ERROR</span>
				</h1>
			</header>
			<div id="content">
				<h2>抱歉,服务器开小差了~~~</h2>
				<p>
					当您看到这个页面,表示服务器开小差了,这个错误是由服务器引起的,请您尝试刷新或继续浏览其他内容！
				</p>
				<div class="utilities">
					<a class="button right" href="javascript:;" onClick="window.location.reload()">刷新</a>
					<a class="button right" href="http://m.hust.edu.cn/wechat/">返回首页</a>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>
</html>