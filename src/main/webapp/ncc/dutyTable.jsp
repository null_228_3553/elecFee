<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="" />
	<meta name="decription" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
	<title></title>		
	<link href="/elecFee/css/style.css" media="screen" rel="stylesheet" type="text/css" />
	<link href="/elecFee/css/color_01.css" media="screen" rel="stylesheet" type="text/css" />
	<link href="/elecFee/css/sys.css" media="screen" rel="stylesheet" type="text/css" />
	<link href="/elecFee/css/kcdz.css" media="screen" rel="stylesheet" type="text/css" />
	<link href="/elecFee/css/icon.css" media="screen" rel="stylesheet" type="text/css" />
<script src="/elecFee/js/jquery.js" type="text/javascript"></script>
<script src="/elecFee/js/resetAndroidFontsize.js" type="text/javascript"></script>
<script type="text/javascript">
</script>
</head>
<body>
<header class="header_box key_color">
	<a class="header_home pp_a016 isz_24 left key_color" href="http://m.hust.edu.cn/wechat/"></a>
	网络中心值班表
	<a href="elecHelp.jsp" class="header_about pp_a017 isz_26 right key_color"></a>
</header>
<div class="main_02">
<div class="kc_list" style="display:block">
	<c:forEach items="${resList}" var="xx">
	<div class="xxcx_row" style="text-align:center;">
		<p class="key_color">${xx[0].dutytime}</p>
	</div>
	<div class="xxcx_row">
		<table style="width: 100%;">
		<c:forEach items="${xx}" var="yy">
		<tr>
			<td class="left_box" style="width:50px;"><c:out value="${yy.dutytype}"/>：</td>
			<td class="right_box"><c:out value="${yy.name}"/>（<c:out value="${yy.dept}"/>&nbsp;<c:out value="${yy.mobile}"/>）</td>
		</tr>
		</c:forEach>
		</table>
	</div>
	</c:forEach>
</div>
</div>
<div class="bottom_box_02">
	<div class="bottom_box_inside">
		<span style="width:233px;" id="bottom_p_02">
			智慧华中大&nbsp;&nbsp;数据提供：网络与计算中心
		</span>
	</div>
</div>

</body>
</html>