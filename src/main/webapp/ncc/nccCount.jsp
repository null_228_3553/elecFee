<%@ page language="java" contentType="text/html; charset=utf-8"%>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="renderer" content="webkit|ie-comp|ie-stand">
   	<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
	<title>网络中心每周运行统计</title>
<link href="../css/icon.css"  rel="stylesheet" type="text/css" />
<link href="../css/style1.css"  rel="stylesheet" type="text/css" />
<link href="../css/color_02.css"  rel="stylesheet" type="text/css" />
<link href="../css/bootstrap-3.3.7-dist/css/bootstrap.css"  rel="stylesheet" type="text/css" />
<style type="text/css">
body{
	font-size:18px;
}
.panel-header{
    margin: 10px 20px 0px;
}
.label-left{
	font-size:16px;
	font-family: 'Helvetica Neue', Helvetica, sans-serif;
    float: left;
    width: 50%;
    text-align:right;
    padding: 3px 10px 3px 0px;
    font-weight: normal;
}
.text{
	width:50%;
	font-size:20px;
}
.list-group-item{
	padding:5px;
}
.panel{
	margin-bottom:8px;
}
.panel .list-group{
	margin-left: 0px;
    margin-bottom: 10px;
}
.panel-primary > .panel-heading{
	background-color:#33b1b7;
}
.row{
	float:none;
}
</style>
</head>
<body>
<header class="header_box key_color">
	<a class="header_home pp_a016 isz_24 left key_color" href="http://m.hust.edu.cn/wechat/"></a>
	网络中心每周运行统计
	<a class="header_about pp_a017 isz_26 right key_color"></a>
</header>
	<div class="container-fluid " id="divBody" style="padding: 5px 5px 28px;">
<div class="panel panel-primary" style="margin-top: 55px;">
	<div class="panel-heading" style="background-color:#2a5f8c;">
		<h3 class="panel-title">统计日</h3>
	</div>
	<div class="panel-body" style="background-color: #f7f7f7;">
		日期：{{count.count_date}}
	</div>
</div>
<div class="panel panel-primary" style="margin-left: 0px;">
	<div class="panel-heading">
		<h3 class="panel-title">邮件系统</h3>
	</div>
	<div class="row list-group" style="display:none">
  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">邮件活跃总用户:</label>
			<span class="text">{{count.yj_users}}</span>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">平均每日收发量:</label>
			<span class="text">{{count.yj_mails}}</span>
		</div>
	</div>
</div>
<div class="panel panel-primary" style="margin-left: 0px;">
	<div class="panel-heading">
		<h3 class="panel-title">网站群平台</h3>
	</div>
	<div class="row list-group" style="display:none">
		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">网站群申请数:</label>
			<span class="text">{{count.wzq_apply}}</span>
		</div>
  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">网站群上线数:</label>
			<span class="text">{{count.wzq_prod}}</span>
		</div>
  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">网站群完成数:</label>
			<span class="text">{{count.wzq_done}}</span>
		</div>
	</div>
</div>
<div class="panel panel-primary" style="margin-left: 0px;">
	<div class="panel-heading">
		<h3 class="panel-title">正版化系统</h3>
	</div>
	<div class="row list-group" style="display:none">
  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">正版化office:</label>
			<span class="text">{{count.zbh_office}}</span>
		</div>
  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">正版化win10:</label>
			<span class="text">{{count.zbh_win10}}</span>
		</div>
  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">正版化win7:</label>
			<span class="text">{{count.zbh_win7}}</span>
		</div>
  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">正版化visio:</label>
			<span class="text">{{count.zbh_visio}}</span>
		</div>
  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">正版化project:</label>
			<span class="text">{{count.zbh_project}}</span>
		</div>
  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">正版化matlab:</label>
			<span class="text">{{count.zbh_matlab}}</span>
		</div>
	</div>
</div>
<div class="panel panel-primary" style="margin-left: 0px;">
	<div class="panel-heading">
		<h3 class="panel-title">校园卡系统</h3>
	</div>
	<div class="row list-group" style="display:none">
  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">教工卡总账户数:</label>
			<span class="text">{{count.xyk_jgk}}</span>
		</div>
  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">学生卡总账户数:</label>
			<span class="text">{{count.xyk_xsk}}</span>
		</div>
  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">普通卡总账户数:</label>
			<span class="text">{{count.xyk_ptk}}</span>
		</div>
  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">消费卡总账户数:</label>
			<span class="text">{{count.xyk_xfk}}</span>
		</div>
  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">消费笔数:</label>
			<span class="text">{{count.xyk_xf_bs}}</span>
		</div>
  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">圈存笔数:</label>
			<span class="text">{{count.xyk_qc_bs}}</span>
		</div>
  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">现金笔数:</label>
			<span class="text">{{count.xyk_xj_bs}}</span>
		</div>
  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">自动转账笔数:</label>
			<span class="text">{{count.xyk_zdzz_bs}}</span>
		</div>
  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">微校园笔数:</label>
			<span class="text">{{count.xyk_wxy_bs}}</span>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">扫码付笔数:</label>
			<span class="text">{{count.xyk_smf}}</span>
		</div>
	</div>
</div>
<div class="panel panel-primary" style="margin-left: 0px;">
	<div class="panel-heading">
		<h3 class="panel-title">网上办事大厅</h3>
	</div>
	<div class="row list-group" style="display:none">
  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">日平均流程数:</label>
			<span class="text">{{count.bsdt_avg}}</span>
		</div>
  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">办结数:</label>
			<span class="text">{{count.bsdt_done}}</span>
		</div>
  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">上线流程:</label>
			<span class="text">{{count.bsdt_prod}}</span>
		</div>
 		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">在开发中:{{count.bsdt_dev}}</label>
			<span class="text">{{count.bsdt_dev}}</span>
		</div>
	</div>
</div>
<div class="panel panel-primary" style="margin-left: 0px;">
	<div class="panel-heading">
		<h3 class="panel-title">统一通讯平台</h3>
	</div>
	<div class="row list-group" style="display:none">
		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">短信日平均:</label>
			<span class="text">{{count.dx_avg}}</span>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">会议总数:</label>
			<span class="text">{{count.hy_total}}</span>
		</div>
	</div>
</div>
	</div>
	<div class="bottom_box_02">
	<div class="bottom_box_inside">
		<span style="width:233px;" id="bottom_p_02">
			智慧华中大&nbsp;&nbsp;数据提供：网络与计算中心
		</span>
	</div>
</div>
	
<script src="../js/vue.js"></script>
<script src="../js/jquery.js"></script>
<script src="../js/layer.js"></script>
<script src="../js/sync.js"></script>
<script src="https://res.wx.qq.com/open/js/jweixin-1.2.0.js" type="text/javascript"></script>
<script src="../js/resetAndroidFontsize.js" type="text/javascript"></script>
<script type="text/javascript">
ElecFee.wxShare({
	title: '信息部每周统计',
	desc: '信息部各系统每周使用情况数据统计',
	link: window.location.href,
	imgUrl: 'https://m.hust.edu.cn/elecFee/images/hzd.jpg'
});
var db=new Vue({
	el:"#divBody",
	mounted:function(){
		$(".panel-heading").click(function(){
			$(this).next().toggle(500);
		});
		this.wxCount();
	},
	methods:{
		wxCount:function(){
			var that=this;
			var date=getQueryString("date");
			var loadLayer=layer.load(1,{shade: [0.5,'#fff']});
			$.post("nccCount.do",{date:date},function(data){
				layer.close(loadLayer);
				that.count=data;
			},"json");
		},		
	},
	data:{
		count:{},//显示list
	}
});
function getQueryString(name) { 
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i"); 
	var r = window.location.search.substr(1).match(reg); 
	if (r != null) 
		return unescape(r[2]); 
	return null; 
}
</script> 
	</body>
</html>
