<%@ page language="java" contentType="text/html; charset=utf-8"%>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="renderer" content="webkit|ie-comp|ie-stand">
   	<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
	<title>网络中心每周运行统计</title>
<link href="../css/icon.css"  rel="stylesheet" type="text/css" />
<link href="../css/style1.css"  rel="stylesheet" type="text/css" />
<link href="../css/color_02.css"  rel="stylesheet" type="text/css" />
<link href="../css/bootstrap-3.3.7-dist/css/bootstrap.css"  rel="stylesheet" type="text/css" />
<style type="text/css">
body{
	font-size:18px;
}
.panel-header{
    margin: 10px 20px 0px;
}
.label-left{
	font-size:16px;
	font-family: 'Helvetica Neue', Helvetica, sans-serif;
    float: left;
    width: 50%;
    text-align:right;
    padding: 3px 10px 3px 0px;
    font-weight: normal;
}
.text{
	width:50%;
	font-size:20px;
}
.list-group-item{
	padding:5px;
}
.panel{
	margin-bottom:8px;
}
.panel .list-group{
	margin-left: 0px;
    margin-bottom: 10px;
}
.panel-primary > .panel-heading{
	background-color:#33b1b7;
}
.row{
	float:none;
}
</style>
</head>
<body>
<header class="header_box key_color">
	<a class="header_home pp_a016 isz_24 left key_color" href="http://m.hust.edu.cn/wechat/"></a>
	网络中心每周运行统计
	<a class="header_about pp_a017 isz_26 right key_color"></a>
</header>
	<div class="container-fluid " id="divBody" style="padding: 5px 5px 28px;">
<div class="panel panel-primary" style="margin-top: 55px;">
	<div class="panel-heading" style="background-color:#2a5f8c;">
		<h3 class="panel-title">统计日</h3>
	</div>
	<div class="panel-body" style="background-color: #f7f7f7;">
		将于<span style="color:red">{{nextMonday}}19点30分</span>发给于头，请尽快填写
		
	</div>
</div>
<div class="panel panel-primary" style="margin-left: 0px;">
	<div class="panel-heading">
		<h3 class="panel-title">李凯</h3>
	</div>
	<div class="row list-group" style="display:none">
		  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">邮件活跃总用户:</label>
			<input type="number" class="text" v-model="count.yj_users">
		</div>
		  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">平均每日收发量:</label>
			<input type="number" class="text" v-model="count.yj_mails">
		</div>
		  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">网站群申请数:</label>
			<input type="number" class="text" v-model="count.wzq_apply">
		</div>
		  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">网站群上线数:</label>
			<input type="number" class="text" v-model="count.wzq_prod">
		</div>
		  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">网站群完成数:</label>
			<input type="number" class="text" v-model="count.wzq_done">
		</div>
		  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">正版化office:</label>
			<input type="number" class="text" v-model="count.zbh_office">
		</div>
		  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">正版化win10:</label>
			<input type="number" class="text" v-model="count.zbh_win10">
		</div>
		  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">正版化win7:</label>
			<input type="number" class="text" v-model="count.zbh_win7">
		</div>
  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">正版化visio:</label>
			<input type="number" class="text" v-model="count.zbh_visio">
		</div>
  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">正版化project:</label>
			<input type="number" class="text" v-model="count.zbh_project">
		</div>
  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">正版化matlab:</label>
			<input type="number" class="text" v-model="count.zbh_matlab">
		</div>
	</div>
</div>
<div class="panel panel-primary" style="margin-left: 0px;">
	<div class="panel-heading">
		<h3 class="panel-title">刘晓兰</h3>
	</div>
	<div class="row list-group" style="display:none">
  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">教工卡总账户数:</label>
			<input type="number" class="text" v-model="count.xyk_jgk">
		</div>
  		<!-- <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">教工卡新增:</label>
			<input type="number" class="text" v-model="count.xyk_jgk_inc">
		</div> -->
  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">学生卡总账户数:</label>
			<input type="number" class="text" v-model="count.xyk_xsk">
		</div>
  		<!-- <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">学生卡新增:</label>
			<input type="number" class="text" v-model="count.xyk_xsk_inc">
		</div> -->
  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">普通卡总账户数:</label>
			<input type="number" class="text" v-model="count.xyk_ptk">
		</div>
  		<!-- <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">普通卡新增:</label>
			<input type="number" class="text" v-model="count.xyk_ptk_inc">
		</div> -->
  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">消费卡总账户数:</label>
			<input type="number" class="text" v-model="count.xyk_xfk">
		</div>
  		<!-- <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">消费卡新增:</label>
			<input type="number" class="text" v-model="count.xyk_xfk_inc">
		</div> -->
  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">消费笔数:</label>
			<input type="number" class="text" v-model="count.xyk_xf_bs">
		</div>
  		<!-- <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">消费笔数新增:</label>
			<input type="number" class="text" v-model="count.xyk_xf_bs_inc">
		</div> -->
  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">圈存笔数:</label>
			<input type="number" class="text" v-model="count.xyk_qc_bs">
		</div>
  		<!-- <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">圈存笔数新增:</label>
			<input type="number" class="text" v-model="count.xyk_qc_bs_inc">
		</div>
  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">圈存金额:</label>
			<input type="number" class="text" v-model="count.xyk_qc_je">
		</div>
  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">圈存金额新增:</label>
			<input type="number" class="text" v-model="count.xyk_qc_je_inc">
		</div> -->
  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">现金笔数:</label>
			<input type="number" class="text" v-model="count.xyk_xj_bs">
		</div>
  		<!-- <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">现金笔数新增:</label>
			<input type="number" class="text" v-model="count.xyk_xj_bs_inc">
		</div>
  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">现金金额:</label>
			<input type="number" class="text" v-model="count.xyk_xj_je">
		</div>
  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">现金金额新增:</label>
			<input type="number" class="text" v-model="count.xyk_xj_je_inc">
		</div> -->
  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">自动转账笔数:</label>
			<input type="number" class="text" v-model="count.xyk_zdzz_bs">
		</div>
  		<!-- <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">自动转账笔数新增:</label>
			<input type="number" class="text" v-model="count.xyk_zdzz_bs_inc">
		</div>
  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">自动转账金额:</label>
			<input type="number" class="text" v-model="count.xyk_zdzz_je">
		</div>
  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">自动转账金额新增:</label>
			<input type="number" class="text" v-model="count.xyk_zdzz_je_inc">
		</div> -->
  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">微校园笔数:</label>
			<input type="number" class="text" v-model="count.xyk_wxy_bs">
		</div>
  		<!-- <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">微校园笔数新增:</label>
			<input type="number" class="text" v-model="count.xyk_wxy_bs_inc">
		</div>
  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">微校园金额:</label>
			<input type="number" class="text" v-model="count.xyk_wxy_je">
		</div>
		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">微校园金额新增:</label>
			<input type="number" class="text" v-model="count.xyk_wxy_je_inc">
		</div> -->
		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">扫码付笔数:</label>
			<input type="number" class="text" v-model="count.xyk_smf">
		</div>
	</div>
</div>
<div class="panel panel-primary" style="margin-left: 0px;">
	<div class="panel-heading">
		<h3 class="panel-title">熊鹰</h3>
	</div>
	<div class="row list-group" style="display:none">
  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">日平均流程数:</label>
			<input type="number" class="text" v-model="count.bsdt_avg">
		</div>
  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">办结数:</label>
			<input type="number" class="text" v-model="count.bsdt_done">
		</div>
  		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">上线流程:</label>
			<input type="number" class="text" v-model="count.bsdt_prod">
		</div>
 		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">在开发中:</label>
			<input type="number" class="text" v-model="count.bsdt_dev">
		</div>
	</div>
</div>
<div class="panel panel-primary" style="margin-left: 0px;">
	<div class="panel-heading">
		<h3 class="panel-title">郑君临</h3>
	</div>
	<div class="row list-group" style="display:none">
		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">短信日平均:</label>
			<input type="number" class="text" v-model="count.dx_avg">
		</div>
		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 list-group-item">
			<label class="label-left">会议总数:</label>
			<input type="number" class="text" v-model="count.hy_total">
		</div>
	</div>
</div>
<div class="panel panel-primary" v-on:click="nccCount" style="width:50%;margin-left:25%">
	<div style="background-color:#2a5f8c;padding: 10px 15px;color: white;text-align:center">
		<h3 class="panel-title">提交</h3>
	</div>
</div>
	</div>
	<div class="bottom_box_02">
	<div class="bottom_box_inside">
		<span style="width:233px;" id="bottom_p_02">
			智慧华中大&nbsp;&nbsp;数据提供：网络与计算中心
		</span>
	</div>
</div>
	
<script src="../js/vue.js"></script>
<script src="../js/jquery.js"></script>
<script src="../js/layer.js"></script>
<script src="../js/sync.js"></script>
<script src="https://res.wx.qq.com/open/js/jweixin-1.2.0.js" type="text/javascript"></script>
<script src="../js/resetAndroidFontsize.js" type="text/javascript"></script>
<script type="text/javascript">

var db=new Vue({
	el:"#divBody",
	mounted:function(){
		var date=new Date();
		for(var i=0;i<=7;i++){
			if(date.getDay()==1){
				this.nextMonday=date.toLocaleDateString()+"  星期一";
				break;
			}
			date.setDate(date.getDate()+1);
		}
		$(".panel-heading").click(function(){
			$(".list-group").slideUp();
			if($(this).next().is(":hidden"))
				$(this).next().slideDown();
// 			$(this).next().toggle(500);
		});
		var that=this;
		var loadLayer=layer.load(1,{shade: [0.5,'#fff']});
		$.post("nccCount.do",that.count,function(data){
			layer.close(loadLayer);
			if(data!=null){
				that.count=data;
				//复制数据到that.count
				$.extend(true,that.initCount,that.count);
			}
		},"json");
	},
	methods:{
		nccCount:function(){
			var that=this;
			var loadLayer=layer.load(1,{shade: [0.5,'#fff']});
			var param;
			//未修改的置为null，只传修改过的值
			for(i in that.count){
				if(that.count[i]==that.initCount[i]){
					that.count[i]=null;
				}
			}
			$.post("insertNccCount.do",that.count,function(data){
				layer.close(loadLayer);
				layer.alert("提交成功",{
					btn:["确定"],					
				},function(){
					window.location.reload();
				});
			});
		},	
	},
	data:{
		nextMonday:"",
		initCount:{},//初始对象
		count:{}//显示对象
	}
});
function getQueryString(name) { 
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i"); 
	var r = window.location.search.substr(1).match(reg); 
	if (r != null) 
		return unescape(r[2]); 
	return null; 
}
</script> 
	</body>
</html>
