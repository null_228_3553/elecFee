<%@ page language="java" contentType="text/html; charset=utf-8"%>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="renderer" content="webkit|ie-comp|ie-stand">
   	<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />
<title></title>		
<link href="../css/icon.css" media="screen" rel="stylesheet" type="text/css" />
<link href="../css/style.css" media="screen" rel="stylesheet" type="text/css" />
<link href="../css/color_red.css" media="screen" rel="stylesheet" type="text/css" />
<link href="../css/library.css" media="screen" rel="stylesheet" type="text/css" />
<style type="text/css">
label{
	font-size:16px
}
</style>
</head>
<body>
<header class="header_box key_color">
	<a class="header_home pp_a016 isz_24 left key_color"></a>
	图书馆借阅记录
	<a class="header_about pp_a017 isz_26 right key_color"></a>
</header>
<div id="divBody" class="main_02" style="padding:42px 0 49px 0;">
	<div id="aaaDiv" style="display:none">
		<div style="margin:0px;padding:0px;font-size:22px;">请输入登录密码</div> 
	</div>
	<div class="dfcx_box_02 margin_top_10">
		<p class="dfcx_bar_03">
		姓　　名：{{username}}
		</p>
		<p class="dfcx_bar_03">
		目前借书：{{onList.length}}本
		</p>
		<p class="dfcx_bar_03">
		开启提醒：
		</p>
	</div>
	<div class="tab_01" style="margin:15px 12px 0;">
    	<a class="tab_01_select left_radius" v-on:click="leftClick" id="leftTag">当前借出图书({{onList.length}}本)</a>
    	<a class="right_radius" v-on:click="rightClick" id="rightTag">历史借阅记录({{dueList.length}}本)</a>
	</div>
	<div class="dfcx_list" id="leftList" >
		<div v-for="row in onList" class="dfcx_row">
			<p class="dfcx_title">
			{{row.title}} / {{row.author}}
			</p>
			条形码：{{row.barcode}}<br/>
			索书号：{{row.callnumber}}<br/>
			状态：到期 {{row.duedate}}
		</div>
	</div>
	<div class="dfcx_list" id="rightList" style="display:none">
		<div v-for="row in dueList" class="dfcx_row">
			<p class="dfcx_title">
			{{row.title}}
			</p>
			作者：{{row.author}}<br/>
			借出时间：{{row.duedate}}<br/>
			详细信息：{{row.details}}
		</div>
	</div>
	<div class="bottom_box_02">
		<div class="bottom_box_inside">
			<span style="width:233px;" id="bottom_p_02">
				智慧华中大&nbsp;&nbsp;数据提供：大学生就业信息网
			</span>
		</div>
	</div>
</div>
<script type="text/javascript" src="../js/vue.js"></script>
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/layer.js"></script>
<script type="text/javascript" src="../js/mySync.js"></script>
<script type="text/javascript">
var db=new Vue({
	el:"#divBody",
	mounted:function(){
		var that=this;
		/* post2SRV("libRecords.do",function(data){
			if(data.retCode=="0000"){
				data=data.body;
				that.username=data.user.name;
				that.onList=data.items;
			}
		},"json"); */
		/* var remind=layer.confirm("1123213123213", {
			title : "<span style='font-size:16px;'>温馨提示</span>",
			icon : 6,
			offset : "200px",
			area : "95%",
			btn : [ "<span style='font-size:14px;'>确定</span>", "<span style='font-size:14px;'>不再提醒</span>" ]
		}, function() {
			layer.close(remind);
		}, function() {
		}); */
		
	},
	methods:{
		tabChange:function(event){
		},
		leftClick:function(event){
			$("#leftTag").attr("class","tab_01_select left_radius");
			$("#rightTag").attr("class","right_radius");
			$("#leftList").show();
			$("#rightList").hide();
		},
		rightClick:function(){
			this.submit();
		},
		submit:function(password){
			var that=this;
			post2SRV("hisRecords.do",{password:password},function(data){
				if(data.retCode=="0000"){
					that.dueList=data.body;
					
					$("#rightTag").attr("class","tab_01_select right_radius");
					$("#leftTag").attr("class","left_radius");
					$("#leftList").hide();
					$("#rightList").show();
				}else if(data.errMsg=="password.error"){
					layer.msg("密码错误");
					
				}else if(data.errMsg=="not.register"){
					layer.msg("未注册");
				}else{
					layer.msg(data.errMsg);
				}
			},"json");
		}
	},
	data:{
		username:"",
		onList:[],
		dueList:[]
	}
});
</script>
</body>
</html>