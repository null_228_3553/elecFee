<%@ page language="java" contentType="text/html; charset=utf-8"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="" />
	<meta name="decription" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
	<title>借书查询</title>		
<link href="/elecFee/css/icon.css"  rel="stylesheet" type="text/css" />
<link href="/elecFee/css/style1.css"  rel="stylesheet" type="text/css" />
<link href="/elecFee/css/color_02.css"  rel="stylesheet" type="text/css" />
<link href="/elecFee/css/bootstrap-3.3.7-dist/css/bootstrap.css"  rel="stylesheet" type="text/css" />
<link href="/elecFee/css/switch.css"  rel="stylesheet" type="text/css" />
<link href="/elecFee/css/library_login.css" media="screen" rel="stylesheet" type="text/css" />
<style type="text/css">
body{
	font-size:16px;
	background-color: #efeff4;
}
label{
	font-weight:normal;
}
.span4{
	width:80px;
	font-weight:normal;
}
.layerLi{
    position: relative;
}
.time{
	font-size:13px;
	color:gray;
	word-wrap: break-word;
	line-height: 17px;
}
.pass{
    width: 100%;
    margin-top: 4px;
    padding: 10px 5px 10px 32px;
    border: 1px solid rgb(178, 178, 178);
    height: 30px;
    border-radius: 3px;
}
.divTitle:after{
	content: ' ';
	display: block;
	width: 100%;
	height: 2px;
	margin-top: 10px;
	background: -moz-linear-gradient(left, rgba(147,184,189,0) 0%, rgba(147,184,189,0.8) 20%, rgba(147,184,189,1) 53%, rgba(147,184,189,0.8) 79%, rgba(147,184,189,0) 100%);
	background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(147,184,189,0)), color-stop(20%,rgba(147,184,189,0.8)), color-stop(53%,rgba(147,184,189,1)), color-stop(79%,rgba(147,184,189,0.8)), color-stop(100%,rgba(147,184,189,0)));
	background: -webkit-linear-gradient(left, rgba(147,184,189,0) 0%,rgba(147,184,189,0.8) 20%,rgba(147,184,189,1) 53%,rgba(147,184,189,0.8) 79%,rgba(147,184,189,0) 100%);
	background: -o-linear-gradient(left, rgba(147,184,189,0) 0%,rgba(147,184,189,0.8) 20%,rgba(147,184,189,1) 53%,rgba(147,184,189,0.8) 79%,rgba(147,184,189,0) 100%);
	background: -ms-linear-gradient(left, rgba(147,184,189,0) 0%,rgba(147,184,189,0.8) 20%,rgba(147,184,189,1) 53%,rgba(147,184,189,0.8) 79%,rgba(147,184,189,0) 100%);
	background: linear-gradient(left, rgba(147,184,189,0) 0%,rgba(147,184,189,0.8) 20%,rgba(147,184,189,1) 53%,rgba(147,184,189,0.8) 79%,rgba(147,184,189,0) 100%);
}
.divTitle2{
	margin:0px;
	padding:0px;
	font-size:22px;
	text-align:center;
	color: rgb(6, 106, 117);
	font-weight: bold;
	text-align: center;
}
.divTitle{
	margin:0px;
	padding:0px;
	font-size:22px;
	text-align:center;
	color: rgb(6, 106, 117);
	font-weight: bold;
	text-align: center;
}
.initPass{
	text-decoration: underline;
    font-weight: 200;
    vertical-align: sub;
    padding-right: 10px;
}
</style>
</head>
<body>
<header class="header_box key_color">
	<a class="header_home pp_a016 isz_24 left key_color" href="http://m.hust.edu.cn/wechat/"></a>
	借书查询
	<a class="header_about pp_a017 isz_26 right key_color"></a>
</header>
<div class="container-fluid"  id="divBody" style="padding-left:1px;padding-right:1px;">
	<ul class="nav nav-tabs" style="border-bottom: none;margin-top: 55px;">
		<li role="presentation" v-bind:class="active1">
			<a id="tab1" href="javascript:;" v-on:click="tabChange1">当前借书</a>
		</li>
		<li role="presentation" v-bind:class="active2">
			<a id="tab2" href="javascript:;" v-on:click="tabChange2">历史借阅</a>
		</li>
		<li style="float:right;padding: 10px;">
			<span class="glyphicon glyphicon-cog" aria-hidden="true" 
				style="font-size: 20px;margin-right: 10px;color: #337ab7;"
				v-on:click="setting"></span>
		</li>
	</ul>
	<ul class="list-group" id="leftGroup" style="margin-bottom:0px;margin-bottom:40px;">
		<li v-for="row in onList" class="list-group-item">
	   		<div style="margin-bottom:10px;" >
	   			{{row.title}}
	   		</div>
			<div class="time">作者：{{row.author}}</div>
			<div class="time">条形码：{{row.barcode}}</div>
			<div class="time">索书号：{{row.callnumber}}</div>
			<div class="time">状态：到期 {{row.duedate}}</div>
		</li>
		<div class="panel-footer" style="text-align: center">共{{onList.length}}条</div>
	</ul>
	<ul class="list-group" id="rightGroup" style="margin-bottom:0px;margin-bottom:40px;display:none">
		<li v-for="row in dueList" class="list-group-item">
	   		<div style="margin-bottom:10px;" >
	   			{{row.title}}
	   		</div>
			<div class="time">作者：{{row.author}}</div>
			<div class="time">借出时间：{{row.duedate}}</div>
			<div class="time">详细信息：{{row.details}}</div>
		</li>
		<div class="panel-footer" style="text-align: center">共{{dueList.length}}条</div>
	</ul>
	<div id="settingDiv" style="display:none">
		<div class="alert alert-warning" role="alert" style="margin-bottom:0px;text-align:center;">
			打开推送开关，系统将会提前三天发送消息提醒你还书
		</div>
		<ul class="mui-table-view" style="padding-top:10px;padding-left:50px;">
			<li class="layerLi">
				<label class="span4" style="margin-bottom: 20px;">
					还书提醒:
				</label>
				<div class="switch" style="display:inline-block;vertical-align: middle;">
					<span class="tg-list-item">
    					<input class="tgl tgl-skewed" id="mainChk" v-model="remind" type="checkbox">
    					<label class="tgl-btn" data-tg-off="OFF" data-tg-on="ON" for="mainChk"></label>
  					</span>
				</div>
			</li>
		</ul>
	</div>
	<div id="passDiv" style="display:none;position:relative;top: 0px;
    		width: 100%;padding: 18px 6% 10px 6%;background: rgb(247, 247, 247);border: 1px solid rgba(147, 184, 189,0.8);">
		<h1 class="divTitle2">请输入登录密码</h1> 
		<h1 class="divTitle" style="font-size:17px;color:black;font-weight:100;
				font-style: italic;padding-top: 3px;">
    		首次使用请点击初始化密码
    	</h1> 
		<p style="margin-top: 5px;color: rgb(64, 92, 96);"> 
			<label for="password">
				密码(6位以上数字，勿重复)： 
			</label>
			<input class="pass" v-model="password" type="password" placeholder="eg. 123456" /> 
		</p>
		<p class="keeplogin" style="margin-bottom:0px"> 
			<input  type="checkbox" checked id="loginkeeping" style="zoom:150%" /> 
			<label for="loginkeeping" style="font-size:14px;vertical-align: super;">
				保持登录
			</label>
		</p>
		<p class="login button"> 
			<label class="initPass" v-on:click="showRegister">初始化密码</label>
			<button v-on:click="login" class="btn btn-primary" style="width:100px">
				登录
			</button>
		</p>
	</div>
	<div id="registerDiv" style="display:none;position:relative;top: 0px;
    		width: 100%;padding: 18px 6% 10px 6%;background: rgb(247, 247, 247);border: 1px solid rgba(147, 184, 189,0.8);">
		<h1 class="divTitle">请设置登录密码</h1> 
		<p style="margin-top: 5px;color: rgb(64, 92, 96);"> 
			<label for="password">
				密码(6位以上数字，勿重复)： 
			</label>
			<input class="pass" v-model="password" type="password" placeholder="eg. 123456" /> 
		</p>
		<p style="margin-top: 5px;color: rgb(64, 92, 96);"> 
			<label for="password">
				再次输入： 
			</label>
			<input class="pass" v-model="rePassword" type="password" placeholder="eg. 123456" /> 
		</p>
		<p class="keeplogin" style="margin-bottom:0px"> 
			<input  type="checkbox" checked id="loginkeeping" style="zoom:150%" /> 
			<label for="loginkeeping" style="font-size:14px;vertical-align: super;">
				保持登录
			</label>
		</p>
		<p class="login button"> 
			<button v-on:click="register" class="btn btn-primary" style="width:100px">
				登录
			</button>
		</p>
	</div>
</div>

<div class="bottom_box_02">
	<div class="bottom_box_inside">
		<span style="width:233px;" id="bottom_p_02">
			智慧华中大&nbsp;&nbsp;数据提供：华中科技大学图书馆
		</span>
	</div>
</div>

<script src="/elecFee/js/vue.js" type="text/javascript"></script>
<script src="/elecFee/js/jquery.js" type="text/javascript"></script>
<script src="/elecFee/js/layer.js" type="text/javascript"></script>
<script src="/elecFee/js/mySync.js" type="text/javascript"></script>
<script src="https://res.wx.qq.com/open/js/jweixin-1.2.0.js" type="text/javascript"></script>
<script src="/elecFee/js/resetAndroidFontsize.js" type="text/javascript"></script>
<script type="text/javascript">
ElecFee.wxShare({
	title: '借书查询',
	desc: '借书查询-华中科技大学图书馆公共查询系统',
	link: window.location.href,
	imgUrl: 'https://m.hust.edu.cn/elecFee/images/jscx.png'
});

var loginLayer,registerLayer;
var db=new Vue({
	el:"#divBody",
	mounted:function(){
		var that=this;
		post2SRV("libRecords.do",function(data){
			if(data.retCode=="0000"){
				data=data.body;
				that.onList=data.items;
			}
		},"json");
	},
	methods:{
		show2:function(){
			this.active1="";
			this.active2="active";
			$("#leftGroup").hide();
			$("#rightGroup").show();
		},	
		tabChange1:function(){
			this.active1="active";
			this.active2="";
			$("#leftGroup").show();
			$("#rightGroup").hide();
		},
		tabChange2:function(){
			if(this.dueList.length==0){
				var that=this;
				post2SRV("hisRecords.do",function(data){
					if(data.retCode=="0000"){
						that.dueList=data.body;
						that.show2();
					}else if(data.errMsg=="password.error"){
						layer.msg("密码错误");
						that.showLogin();
					}else if(data.errMsg=="not.register"){
						layer.msg("未注册");
						that.showRegister();
					}else{
						layer.msg(data.errMsg);
					}
				},"json");
			}else{
				this.show2();
			}
		},
		showLogin:function(){
			loginLayer=layer.open({
				title:false,
				closeBtn:0,
				cancel:function(){
					layer.close(loginLayer);
				},
				type:1,
				content:$("#passDiv"),
				shadeClose:true,
				area:"95%",
				offset: "15%",
				zIndex:2
			});
		},
		login:function(){
			var that=this;
			post2SRV("hisRecords.do",{password:this.password},function(data){
				if(data.retCode=="0000"){
					layer.close(loginLayer);
					that.dueList=data.body;
					that.show2();
				}else if(data.errMsg=="password.error"){
					layer.msg("密码错误");
				}else if(data.errMsg=="not.register"){
					layer.msg("未注册");
				}else{
					layer.msg(data.errMsg);
				}
			},"json");
		},
		showRegister:function(){
			if(loginLayer!=null){
				layer.close(loginLayer);
			}
			registerLayer=layer.open({
				title:false,
				closeBtn:0,
				cancel:function(){
					layer.close(registerLayer);
				},
				type:1,
				content:$("#registerDiv"),
				shadeClose:true,
				area:"95%",
				offset: "15%",
				zIndex:2
			});
		},
		register:function(){
			var that=this;
			post2SRV("register.do",{password:this.password,rePassword:this.rePassword},function(data){
				if(data.retCode=="0000"){
					layer.close(registerLayer);
					that.dueList=data.body;
					that.show2();
				}else{
					layer.msg(data.errMsg);
				}
			},"json");
		},
		setting:function(){
			var setLayer=layer.open({
				title:"<div style='font-size:16px'>是否开启提醒</div>",
				btn: ["确定"],
				yes:function(){
					var param={
						remind:db.remind
					}
					$.post("addLibraryRemind.do",param,function(data){
						layer.msg('设置成功');
						layer.close(setLayer);
					});
				},
				cancel:function(){
					db.butErrMsg="";
					layer.close(setLayer);
				},
				type:1,
				content:$("#settingDiv"),
				shadeClose:true,
				area:"95%",
				offset: "15%",
				zIndex:2
			});
			var loadLayer=layer.load(1,{shade: [0.5,'#fff']});
			$.post("libraryRemind.do",function(data){
				layer.close(loadLayer);
				db.remind=data=="1"?true:false;
			},"json");
		}
	},
	data:{
		active1:"active",
		active2:"",
		onList:[],
		dueList:[],
		remind:false,
		password:"",
		rePassword:""
	}
});
</script>
</body>
</html>