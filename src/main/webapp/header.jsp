<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %> 
<header class="header_box key_color">
	<a class="header_home pp_a016 isz_24 left key_color" href="http://m.hust.edu.cn/wechat/"></a>
	<c:out value="${param.title}"/>
	<a href="elecHelp.jsp" class="header_about pp_a017 isz_26 right key_color"></a>
</header>