<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="" />
	<meta name="decription" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
	<title></title>		
	<link href="/elecFee/css/style.css" media="screen" rel="stylesheet" type="text/css" />
	<link href="/elecFee/css/color_01.css" media="screen" rel="stylesheet" type="text/css" />
	<link href="/elecFee/css/dfcx.css" media="screen" rel="stylesheet" type="text/css" />
	<link href="/elecFee/css/icon.css" media="screen" rel="stylesheet" type="text/css" />
<script src="/elecFee/js/jquery.js" type="text/javascript"></script>
<script src="/elecFee/js/resetAndroidFontsize.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
	var warnSwitch="${dormitory.warnSwitch}";
	if(warnSwitch=="on"){
		on();
	}else{
		off();
	}
	
	
	$("#OK").click(function(){
		if(!/^[0-9]{1,3}$/.test($("#threshold").val())){
			$("#errSpan").html("阈值号只允许输入一到三位数字");
			return;
		}
		$("#form1").submit();
	});
});
function changeState(obj){
	if(obj.getAttribute("class")=="onoff-button on"){
		off();
		return;
	}
	if(obj.getAttribute("class")=="onoff-button off"){
		on();
		return;
	}
}
function off(){
	$("#subscribedBtn").attr("class","onoff-button off")
	$("#warnSwitch").val("off");
	$("#thresholdLi").hide();
}
function on(){
	$("#subscribedBtn").attr("class","onoff-button on")
	$("#warnSwitch").val("on");
	$("#thresholdLi").show();
}
</script>
</head>
<body>
<jsp:include page="header.jsp" flush="true">
	<jsp:param name="title" value="设置电量提醒" /> 
</jsp:include>
<div class="main_02">
<div class="main_list">
	<form id="form1" method="post" action="setThreshold.do">
	<div class="dfcx_title_bar">
		<span class="dfcx_title_01 black_444">
		<c:if test="${dormitory ne null}">
			<span class="dfcx_title_02 black_444">当前宿舍：</span>
			<span class="dfcx_title_02 black_444"><c:out value="${dormitory.programId}"/></span>
			<span class="dfcx_title_02 black_444"><c:out value="${dormitory.txtyq}"/></span>
			<span class="dfcx_title_02 black_444"><c:out value="${dormitory.txtld}"/></span>	
			<span class="dfcx_title_02 black_444"><c:out value="${dormitory.txtroom}"/></span>
		</c:if>
		</span>			
	</div>
	<ul class="dfcx_box">
		<li class="dfcx_bar">
			<span class="dfcx_area">是否开启订阅</span>
			<span class="dfcx_area_01" style="float:right;margin-top:10px;">
				<div id="subscribedBtn" class="onoff-button on" onclick="changeState(this)">1</div>
				<input type="hidden" id="warnSwitch" name="warnSwitch" value="on"/>
			</span>
		</li>
		<li class="dfcx_bar" id="thresholdLi">
			<span class="dfcx_area">电量阈值提醒</span>
			<span class="dfcx_area_01" style="float:right">
				<input value="${dormitory.threshold }" id="threshold" name="threshold" 
				type="text" class="dfcx_text_01" style="display:inline;text-align:right;width:150px;color:black" />
				度
			</span>
		</li>
	</ul>
	<span style="color:red;font-size:18px;padding-left:20px;" id="errSpan"><c:out value="${error}"/></span>
	<div class="button_02_outside button_color margin_top_20">
		<input type="button" id="OK" value="设置" class="button_02"/>
	</div>
	</form>
</div>
</div>
<jsp:include page="footer.jsp">
	<jsp:param name="source" value="电费查询" /> 
</jsp:include>
</body>
</html>