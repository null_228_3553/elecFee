function post2SRV(callUrl,formData,callback,dataType){
	showLoading();
	if(typeof dataType=='undefined'){
		dataType=callback;
		callback=formData;
		formData=null;
	}
	$.ajax({
		url:callUrl,
		data:formData,    
		type:'post',
		async:true,
		dataType:dataType,    
		success:function(data){
			closeLoading();
			callback(data);
		},
		error : function() {
			closeLoading();
			$("#pwdSpan").html("网络忙，请稍后再试！");
			$("#pwdDiv").dialog("open");    
		} 
    });
}
function showLoading(){
	var h = $(document).height();
	$("#overlay").css({"height": h });	
	$("#overlay").css({'display':'block','opacity':'0.8'});
	$("#showbox").css({'margin-top':'200px','opacity':'1','display':'block'});
}
function closeLoading(){
	$("#showbox").css({'margin-top':'250px','opacity':'0','display':'none'});
	$("#overlay").css({'display':'none','opacity':'0'});
}
function hideHead(){
	/*if($("#warn1").html()==undefined && $("#warn2").html()==undefined){
		$(window).scrollTop(112);
	}else{
		$(window).scrollTop(134);
	}*/
	$(".dfcx_box_02").hide();
}
function getCookie(name){
	var arr, reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
	if (arr = document.cookie.match(reg)) 
		return unescape(arr[2]);
	else 
		return null;
}
function encode64(input){
	var keyStr = "ABCDEFGHIJKLMNOP" +"QRSTUVWXYZabcdef" +"ghijklmnopqrstuv" +"wxyz0123456789+/" + "=";
	var output = "";
	var chr1, chr2, chr3 = "";
	var enc1, enc2, enc3, enc4 = "";
	var i = 0;
	do{
		chr1 = input.charCodeAt(i++);
		chr2 = input.charCodeAt(i++);
		chr3 = input.charCodeAt(i++);
		enc1 = chr1 >> 2;
		enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
		enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
		enc4 = chr3 & 63;
		if (isNaN(chr2)){
			enc3 = enc4 = 64;
		}else if (isNaN(chr3)){
			enc4 = 64;
		}
		output = output 
		 	+keyStr.charAt(enc1) 
		 	+keyStr.charAt(enc2) 
		 	+keyStr.charAt(enc3) 
		 	+keyStr.charAt(enc4);
		chr1 = chr2 = chr3 = "";
		enc1 = enc2 = enc3 = enc4 = "";
	}while(i < input.length);
	return output;
}
