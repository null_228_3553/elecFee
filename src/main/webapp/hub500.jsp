<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="" />
	<meta name="decription" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<title>500服务器内部错误</title>
<link rel="stylesheet" type="text/css" href="/elecFee/css/error.css">
</head>
<body>
	<div id="wrapper">
		<a class="logo" href="/"></a>
		<div id="main">
			<header id="header">
				<h1>
					<span class="icon">!</span><span class="sub" style="font-size:30px;">选课系统暂停使用</span>
				</h1>
			</header>
			<div id="content">
				<h2>抱歉,选课系统暂停使用~~~</h2>
				<p style="text-indent:2em;color:#333333">
					因技术原因，微信端选课功能暂停使用，请使用PC端访问hub系统(http://hub.hust.edu.cn)进行选课！如有疑问，请致电027-87540900
				</p>
				<div class="utilities">
					<a class="button right" href="http://m.hust.edu.cn/wechat/app.jsp">返回首页</a>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>
</html>