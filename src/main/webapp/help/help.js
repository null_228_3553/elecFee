$(document).ready(function(){
	//根据数字找到该问答所有的<p>
	var divStr="";
	var id=getQueryString("q");
	var pStart=0;
	var pEnd=0;
	var pArr=$("p");
	//找到pstart 标题以数字加、开头
	for(var i=0;i<pArr.length;i++){
		if(pArr[i].innerText.startsWith(id+"、")){
			pStart=i;
			break;
		}
	}
	//找到pend 标题以数字加、开头
	for(var i=pStart+1;i<pArr.length;i++){
		//1位数字加、
		var firSec=pArr[i].innerText.substring(0,2);
		if(/[0-9]、/.test(firSec)){
			pEnd=i;
			break;
		}
		//2位数字加、
		firSec=pArr[i].innerText.substring(0,3);
		if(/\d{2}、/.test(firSec)){
			pEnd=i;
			break;
		}
	}
	//找不到则为最后一个
	if(pEnd==0){
		pEnd=pArr.length;
	}
	for(var i=pStart;i<pEnd;i++){
		divStr+=pArr[i].outerHTML;
		//隐藏原节点
		$(pArr[i]).hide();
	}
	//创造新div，显示border
	var div = document.createElement("div");
	div.innerHTML=divStr;
	
	$(pArr[i-1]).after(div);
	
// 	var winHeight=window.screen.height;
// 	var divHeight=div.offsetHeight;
	div.style.border="1px solid red";
	var yAxis=div.getBoundingClientRect().top;
	$("body").animate({  
	    scrollTop: yAxis-150
	}, 200);  
});
function getQueryString(name) { 
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i"); 
	var r = window.location.search.substr(1).match(reg); 
	if (r != null) 
		return unescape(r[2]); 
	return null; 
}