<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="" />
	<meta name="decription" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
	<title></title>		
	<link href="css/style.css" media="screen" rel="stylesheet" type="text/css" />
	<link href="css/color_01.css" media="screen" rel="stylesheet" type="text/css" />
	<link href="css/dfcx.css" media="screen" rel="stylesheet" type="text/css" />
	<link href="css/icon.css" media="screen" rel="stylesheet" type="text/css" />
</head>
<body>
<jsp:include page="header.jsp" flush="true">
	<jsp:param name="title" value="帮助" /> 
</jsp:include>
<div class="main_02">
	<p class="help_title_01">电费系统-帮助</p>
	<p class="help_title_02">上班时间：</p>
	<p class="helep_content_01">每日17点至22点（节假日另见充值点通知）</p>
	<p class="help_title_02">各充值点：</p>
	<p class="helep_content_01">紫菘二栋充值点： 每周一、二、四、五、日</p>
	<p class="helep_content_01">西八舍充值点：    每周一、二、三、五、六 </p>
	<p class="helep_content_01">东八舍充值点：    每周一、三、四、五、六</p>
	<p class="helep_content_01">韵苑五栋充值点： 每周一、三、四、五、六</p>
	<p class="helep_content_01">韵苑21栋充值点：每周一、二、四、五、日</p>
</div>
</body>
</html>