<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="" />
	<meta name="decription" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
	<title></title>		
	<link href="/elecFee/css/style.css" media="screen" rel="stylesheet" type="text/css" />
	<link href="/elecFee/css/color_01.css" media="screen" rel="stylesheet" type="text/css" />
	<link href="/elecFee/css/dfcx.css" media="screen" rel="stylesheet" type="text/css" />
	<link href="/elecFee/css/icon.css" media="screen" rel="stylesheet" type="text/css" />
<script src="/elecFee/js/jquery.js" type="text/javascript"></script>
<script src="/elecFee/js/resetAndroidFontsize.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#programId").change(function(){
		var param={"programId":$("#programId").val()};
		$.post("queryTxtyq.do",param,function(data){
			$("#txtyq").show();
			//字符串转dom对象 false表示不执行data中的js脚本
			var s=$.parseHTML(data,false);
			$("#txtyq").html(s);
		},"text");
	});
	
	$("#txtyq").change(function(){
		var param={"txtyq":$("#txtyq").val(),
				"programId":$("#programId").val()};
		$.post("queryTxtld.do",param,function(data){
			$("#txtld").show();
			//字符串转dom对象 false表示不执行data中的js脚本
			var s=$.parseHTML(data,false);
			$("#txtld").html(s);
		},"text");
	});
	
	$("#OK").click(function(){
		if($("#programId").val()=="-1"){
			$("#errSpan").html("请选择楼栋区域");
			return;
		}
		if($("#txtyq").val()=="-1"){
			$("#errSpan").html("请选择楼号");
			return;
		}
		if($("#txtld").val()=="-1"){
			$("#errSpan").html("请选择楼层号");
			return;
		}
		if($("#Txtroom").val()==""){
			$("#errSpan").html("请输入房间号");
			return;
		}
		if(!/^[0-9A-Za-z]{3,4}$/.test($("#Txtroom").val())){
			$("#errSpan").html("房间号只允许输入三到四位数字");
			return;
		}
		$("#form1").submit();
	});
});
function clickSelect(obj){
	$("#programId").click();
}
</script>
</head>
<body>
<jsp:include page="header.jsp" flush="true">
	<jsp:param name="title" value="设置我的宿舍" /> 
</jsp:include>
<div class="main_02">
<div class="main_list">
	<form id="form1" method="post" action="saveDormitory.do">
	<div class="dfcx_title_bar">
		<span class="dfcx_title_01 black_444">
		<c:if test="${dormitory eq null}">
			请设置您当前的宿舍
		</c:if>
		<c:if test="${dormitory ne null}">
			<span class="dfcx_title_02 black_444">当前宿舍：</span>
			<span class="dfcx_title_02 black_444"><c:out value="${dormitory.programId}"/></span>
			<span class="dfcx_title_02 black_444"><c:out value="${dormitory.txtyq}"/></span>
			<span class="dfcx_title_02 black_444"><c:out value="${dormitory.txtld}"/></span>	
			<span class="dfcx_title_02 black_444"><c:out value="${dormitory.txtroom}"/></span>
		</c:if>
		</span>			
	</div>
	<ul class="dfcx_box">
		<li class="dfcx_bar">
			<span class="dfcx_area">区域</span>
			<span class="dfcx_area_01">
				<select name="programId" id="programId" class="select_style">
				${area}
				</select>
			</span>
			<i class="pp_b005 isz_18 lightgray item_03_d_arrow" onclick="clickSelect(this)"></i>
		</li>
		<li class="dfcx_bar">
			<span class="dfcx_area">楼号</span>
			<span class="dfcx_area_01">
				<select name="txtyq" id="txtyq" style="display:none"  class="select_style">
					<option selected="selected" value="-1">-请选择-</option>
				</select>
			</span>
			<i class="pp_b005 isz_18 lightgray item_03_d_arrow" ></i>
		</li>
		<li class="dfcx_bar">
			<span class="dfcx_area">楼层</span>
			<span class="dfcx_area_01">
				<select name="txtld" id="txtld" style="display:none"  class="select_style">
					<option selected="selected" value="-1">-请选择-</option>
				</select>
			</span>
			<i class="pp_b005 isz_18 lightgray item_03_d_arrow"></i>
		</li>
		<li class="dfcx_bar">
			<span class="dfcx_area">房间号</span>
			<span class="dfcx_area_01">
				<input id="Txtroom" name="Txtroom" type="text" class="dfcx_text_01" style="color:black" />
			</span>
		</li>
	</ul>
	<span style="color:red;font-size:18px;padding-left:20px;" id="errSpan"><c:out value="${error}"/></span>
	<div class="button_02_outside button_color margin_top_20">
		<input type="button" id="OK" value="设为我的宿舍" class="button_02"/>
	</div>
	</form>
</div>
</div>
<jsp:include page="footer.jsp">
	<jsp:param name="source" value="电费查询" /> 
</jsp:include>
</body>
</html>