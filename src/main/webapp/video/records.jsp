<%@ page language="java" contentType="text/html; charset=utf-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="" />
	<meta name="decription" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<title>往期视频回顾</title>
</head>
<link rel="stylesheet" href="/elecFee/css/bootstrap.css"></link>
<link rel="stylesheet" href="/elecFee/css/style.css"></link>
<link rel="stylesheet" href="/elecFee/css/icon.css"></link>
<link rel="stylesheet" href="/elecFee/css/color_03.css"></link>
<style>
.icon_div {
    font-size: 40px !important;
    display: inline;
    line-height: 40px;
    padding-top: 5px;
}
</style>

<body style="background-color: #f8f8f8;font-family: microsoft yahei;font-size:18px;">
<header class="header_box key_color">
	<a class="header_home pp_a016 isz_24 left key_color" href="http://m.hust.edu.cn/wechat/"></a>
	往期直播回顾
	<a  class="header_about pp_a017 isz_26 right key_color"></a>
</header>
<div class="container-fluid" id="divBody" style="padding:60px 0px 25px 0px;">
	<div class="col-sm-12" style="color:black">
		<ul class="list-group" >
        <li v-for="row in list" class="list-group-item" v-on:click="goRecord(row.id)" style="padding:15px 5px 15px 15px">
        	<table>
        	<tr>
        	<td>
	        	<div class="icon_div">
					<img :src="row.imgSrc"height="50px"/>
				</div>
			</td>
			<td>
				<div style="padding:0px 5px;font-size:16px;">{{row.videoName}}</div>
			</td>
			<td>
				<div class="icon_div">
					<a :href="row.pptSrc">
						<img src="/elecFee/images/video/ppt.jpg" height="30px"/>
					</a>
				</div>
			</td>
			</table>
        </li>
       	</ul>
	</div>
</div>
<form id="form1" method="post">
	<input type="hidden" name="nickname" id="nickname">
</form>
<div class="bottom_box_02">
	<div class="bottom_box_inside">
		<span style="width:233px;" id="bottom_p_02">
			智慧华中大&nbsp;&nbsp;数据提供：网络与计算中心
		</span>
	</div>
</div>
<script src="../js/vue.js"></script>
<script src="../js/jquery.js"></script>
<script type="text/javascript">
var obj=[{
	//id:"39058103",
	id:"67338459",
	imgSrc:"/elecFee/images/video/matlab.jpg",
	videoName:"2017Matlab高校技术巡讲：华中科技大学专场",
	pptSrc:"/elecFee/video/ppts/matlab_main.pdf"
},{
	//id:"92757717",
	id:"28186872",
	imgSrc:"/elecFee/images/video/matlab.jpg",
	videoName:"2017Matlab高校技术巡讲：华中科技大学同济校区专场",
	pptSrc:"/elecFee/video/ppts/matlab_medic.pdf"
}];
var db=new Vue({
	el:"#divBody",
	mounted:function(){
		var that=this;
		$.post("/elecFee/getName.do",function(data){
			that.nickname=data.name;
		},"json");
	},
	methods:{
		goRecord:function(id){
			var url='http://hust.gensee.com/training/site/v/'+id;
			var form=$("#form1");
			form.attr("action",url);
			$("#nickname").val(db.nickname);
			form.submit();
		}
	},
	data:{
		nickname:"",
		list:obj
	}
});
</script> 
</body>
</html>