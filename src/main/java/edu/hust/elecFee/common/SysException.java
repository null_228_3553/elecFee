package edu.hust.elecFee.common;

public class SysException extends RuntimeException{

	public SysException(String message) {
		super(message);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
