package edu.hust.elecFee.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

@Component("httpUtil")
public class HttpUtil {
	private Log log = LogFactory.getLog(getClass());
	private static String url="http://202.114.18.218/main.aspx";
	
	public String httpPost(Map<String,String> map){
		String viewState=(String) map.get(Dict.viewState);
		String eventValidation=(String) map.get(Dict.eventValidation);
		if(viewState==null||"".equals(viewState) 
				|| eventValidation==null||"".equals(eventValidation)){
			String resStr=httpPost("");
			viewState=getViewState(resStr);
			eventValidation=getEventValidation(resStr);
			map.put(Dict.viewState, viewState);
			map.put(Dict.eventValidation, eventValidation);
		}
		String params=Util.getUrlParam(map);
		return httpPost(params);
	}
	
	public String httpPost(String params){
		OutputStreamWriter outputStreamWriter = null;
        BufferedReader reader = null;
        StringBuffer resultBuffer = new StringBuffer();
        String tempLine = null;
        try {
	        URL localURL = new URL(url);
	        HttpURLConnection httpURLConnection = (HttpURLConnection)localURL.openConnection();
	        httpURLConnection.setConnectTimeout(30*1000);
	        httpURLConnection.setReadTimeout(30*1000);
	        httpURLConnection.setDoOutput(true);
	        httpURLConnection.setRequestMethod("POST");
	        httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
	        httpURLConnection.setRequestProperty("Content-Length", String.valueOf(params.length()));
        
            outputStreamWriter = new OutputStreamWriter(httpURLConnection.getOutputStream(),"UTF-8");
            
            outputStreamWriter.write(params.toString());
            outputStreamWriter.flush();
            
            if (httpURLConnection.getResponseCode() >= 300) {
                throw new Exception("HTTP Request is not success, Response code is " + httpURLConnection.getResponseCode());
            }
            reader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream(),"UTF-8"));
            
            while ((tempLine = reader.readLine()) != null) {
                resultBuffer.append(tempLine);
            }
        } catch (IOException e) {
        	log.error(e);
		} catch (Exception e) {
        	log.error(e);
		} finally {
			try{
	            if (outputStreamWriter != null) {
	                outputStreamWriter.close();
	            }
	            if (reader != null) {
	                reader.close();
	            }
            }catch(IOException e){
            	log.error(e);
            }
        }
        return resultBuffer.toString();
	}
	
	public String getViewState(String res) {
		String viewState="";
		int vIndex=res.indexOf("<input type=\"hidden\" name=\"__VIEWSTATE\" id=\"__VIEWSTATE\"");
		if(vIndex!=-1){
			int startIndex=res.indexOf("value", vIndex);
			int endIndex=res.indexOf("/>", startIndex);
			try {
				viewState=URLEncoder.encode(res.substring(startIndex+7,endIndex-2), "UTF-8");
			} catch (UnsupportedEncodingException e) {
	        	log.error(e);
			}
		}
		return viewState;
	}
		
	public String getEventValidation(String res) {
		String eventValidation="";
		int eIndex=res.indexOf("<input type=\"hidden\" name=\"__EVENTVALIDATION\" id=\"__EVENTVALIDATION\"");
		if(eIndex!=-1){
			int startIndex=res.indexOf("value", eIndex);
			int endIndex=res.indexOf("/>", startIndex);
			try {
				eventValidation=URLEncoder.encode(res.substring(startIndex+7,endIndex-2),"UTF-8");
			} catch (UnsupportedEncodingException e) {
	        	log.error(e);
			}
		}
		return eventValidation;
	}
}
