package edu.hust.elecFee.batch;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import edu.hust.elecFee.bean.MyBatch;
import edu.hust.elecFee.common.Util;
import edu.hust.elecFee.service.RouterNative;
import edu.hust.elecFee.service.XywService;

@Service
public class NccBatch {
	protected Log log=LogFactory.getLog(getClass());

	@Autowired
	private JdbcTemplate jdbcTpltCore;
	@Autowired
	private JdbcTemplate jdbcTpltSam;
	@Autowired
	private XywService xywService;
	@Autowired
	private RouterNative routerNative;

	private SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
	private SimpleDateFormat sdf2=new SimpleDateFormat("yyyy年MM月dd日EEEE");

	//暂停通知，由呼叫中心通知
	//	@MyBatch(cron = "0 0 18 * * ?")
	public void dutyRemind(){
		String content="尊敬的{0}老师：\n微校园提醒您，明天（{1}）轮到您值班，请于8点半之前到达网络中心，辛苦了！\n\n"
				+ "点击查看全部值班表";
		String remind="明天呼叫中心是{0}值班！\n\n点击查看全部值班表";
		String title="网络中心值班提醒";
		String callback="https://m.hust.edu.cn/elecFee/ncc/dutyTable.view";

		String qrySql="select * from nccremind where dutytime=to_date(?,'yyyy-mm-dd')";
		String updateSql="update nccremind set remind=?,remindtime=? where userid=? and dutytime=to_date(?,'yyyy-mm-dd')";
		//查询明天的值班情况
		Date tomoDate=new Date(System.currentTimeMillis()+1000 * 60 * 60 * 24);
		String tomoDateStr=sdf.format(tomoDate);
		List<Map<String,Object>> list=jdbcTpltCore.queryForList(qrySql,new Object[]{
				tomoDateStr
		});
		String allNames="";
		for(Map<String,Object> map:list){
			String userId=(String)map.get("userid");
			if(userId==null)
				continue;
			String name=(String)map.get("name");
			String desc=MessageFormat.format(content,name,sdf2.format(tomoDate));
			//发给值班人员和我
			routerNative.sendRemind(new String[]{
					userId,
					//					"2016210052"
			},title, desc, callback);
			//更新状态
			jdbcTpltCore.update(updateSql,new Object[]{
					"1",new Date(),userId,tomoDateStr
			});
			allNames+=name+"，";
		}
		//发给领导 只发一次
		String desc2=MessageFormat.format(remind, allNames.substring(0, allNames.length()-1));
		routerNative.sendRemind(new String[]{
				"2003010155","2000010078","1998010241","1996010051",
				//"2016210052"
		},title, desc2, callback);//余头王主任吴主任李书记我

		System.out.println(list);
		log.error("发送值班提醒："+new Date());
	}

	//每周一 8点10点12点
	@MyBatch(cron = "0 0 8,10,12 ? * MON")
	public void nccCountPreRemind1() {
		nccCountPreRemind();
	}
	private void nccCountPreRemind(){
		//发消息
		String title="请填写信息部各系统数据统计";
		String desc="将于周一晚上七点半发送给于头，请尽快填写！";
		String callback="https://m.hust.edu.cn/elecFee/ncc/nccCountPre.jsp";
		routerNative.sendRemind(new String[]{
				"2016210052","2016210176","2016210087","2016210155","2016210198","2016210049","2018210079"
		}, title,desc,callback);//我刘毛熊郑李杨
		log.error("发送信息部统计提醒："+new Date());
	}
	
	//每周一 19点30
	@MyBatch(cron = "0 30 19 ? * MON")
	public void nccCountRemind() {
		String dateStr=sdf.format(Util.nextMonday(new Date()));

		//发消息
		String title="上周的信息部各系统数据统计";
		String desc="截止到"+dateStr+"，上周的信息部各系统数据统计，各位领导敬请查收！";
		String callback="https://m.hust.edu.cn/elecFee/ncc/nccCount.jsp?date="+dateStr;

		routerNative.sendRemind(new String[]{
				"2003010155","2000010078","1998010241","1996010051","2003010730","1996010099",
				"2016210052","2016210176","2016210087","2016210155","2016210198","2016210049","2018210079","2007210183"
		}, title,desc,callback);//余头王主任吴主任李书记郑老师龙老师,我刘毛熊郑李杨张
		log.error("发送信息部统计提醒："+new Date());
	}

	@MyBatch(cron = "0 10 8 * * ?")
	public void wxCountRemind() throws ParseException{

		//先查询数据库，没数据再找数据
		String yesterday=sdf.format(new Date(System.currentTimeMillis()-3600*24*1000));
		String qrySql="select * from dcp_wechat.wx_count where count_date=?";

		Map<String, Object> wxMap=null;
		try{
			wxMap=jdbcTpltCore.queryForMap(qrySql,yesterday);
		}catch(EmptyResultDataAccessException e){}

		if(wxMap==null){
			//总关注人数
			JSONObject joFollows=routerNative.totalFollows();
			JSONArray jaFollows=joFollows.getJSONArray("userlist");
			int wxTotal=jaFollows.size();

			//分类关注人数，本科生，研究生，教职工，博士生，交换生，留学生，专科生
			String totalSql="select count(*) from dcp_wechat.wx_uim_tab where status='1'";
			int dbTotal=jdbcTpltCore.queryForObject(totalSql, Integer.class);
			double percent=(double)wxTotal/dbTotal;
			//列转行sql
			String countSql=
					"select * from( "
							+" select substr(userid_noencrypt,0,1) as fir,count(*) as count "
							+" from dcp_wechat.wx_uim_tab where status='1' "
							+" and substr(userid_noencrypt,0,1) in('9','1','0','2','V','I','X','D','M','U') "
							+" group by substr(userid_noencrypt,0,1) "
							+" )pivot (sum(count) for fir in (0,1,2,9,'D' D,'I' I,'M' M,'U' U,'V' V,'X' X))";
			Map<String, Object> countMap=jdbcTpltCore.queryForMap(countSql);
			int bks=((BigDecimal)countMap.get("U")).intValue();
			int yjs=((BigDecimal)countMap.get("M")).intValue();
			int bss=((BigDecimal)countMap.get("D")).intValue();
			int jzg=((BigDecimal)countMap.get("0")).intValue()
					+((BigDecimal)countMap.get("1")).intValue()
					+((BigDecimal)countMap.get("2")).intValue()
					+((BigDecimal)countMap.get("9")).intValue();
			int jhs=((BigDecimal)countMap.get("X")).intValue();
			int lxs=((BigDecimal)countMap.get("I")).intValue();
			int zks=((BigDecimal)countMap.get("V")).intValue();
			//数据库数据和微信接口数据有误差，按比例修改
			bks=(int) (bks*percent);
			yjs=(int) (yjs*percent);
			bss=(int) (bss*percent);
			jzg=(int) (jzg*percent);
			jhs=(int) (jhs*percent);
			lxs=(int) (lxs*percent);
			zks=(int) (zks*percent);

			//昨日增长量
			String increaseSql="select count(*) from dcp_wechat.wx_uim_tab where to_char(opt_date,'yyyy-MM-dd')=?";
			int increase=jdbcTpltCore.queryForObject(increaseSql, new Object[]{yesterday} ,Integer.class);

			/**
			 * 以下是校园网数据
			 */
			//当天最多在线
			Date yes=sdf.parse(yesterday);
			Date tod=new Date(yes.getTime()+3600*24*1000);
			int maxOnline=jdbcTpltSam.queryForObject("select max(NUM) from ONLINE_NUM where RECORD_TIME between ? and ?",
					new Object[]{yes,tod},Integer.class);
			//总开户数
			int totalUsers=xywService.queryTotalUsers();
			//昨天开户数
			int yestodayUsers=xywService.queryYesterdayUsers();


			//插入数据库
			String insertSql="insert into dcp_wechat.wx_count(COUNT_DATE,TOTAL,INCREASE,BKS,YJS,BSS,JZG,JHS,LXS,ZKS,XYW_TOTAL,XYW_INCREASE,XYW_DAY_MOST)"
					+ " values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
			jdbcTpltCore.update(insertSql,new Object[]{
					yesterday,wxTotal,increase,bks,yjs,bss,jzg,jhs,lxs,zks,totalUsers,yestodayUsers,maxOnline
			}); 
		}

		//发消息
		String title=yesterday+"的微校园和校园网使用统计";
		String desc=yesterday+"的微校园和校园网的使用统计，各位领导敬请查收！";
		String callback="https://m.hust.edu.cn/elecFee/ncc/wxCount.html?date="+yesterday;

		routerNative.sendRemind(new String[]{
				"2003010155","2000010078","1998010241","1996010051","2003010730","1996010099",
				"1999010178","2007210183","2016210052"
		}, title,desc,callback);//余头王主任吴主任李书记郑老师龙老师柳老师张洁卉我
		log.error("发送微校园统计提醒："+new Date());

	}
}
