package edu.hust.elecFee.batch;

import java.util.Date;
import java.util.Iterator;
import java.util.Map.Entry;

import org.springframework.stereotype.Service;

import edu.hust.elecFee.bean.MyBatch;
import edu.hust.elecFee.bean.Qr;

@Service
public class QrMapBatch {
	
	//每小时的第五分钟执行,清理qrMap中的uuid
	@MyBatch(cron = "0 5 0/1 * * ?")
	public void qrMapClean(){
		Iterator<Entry<String,Qr>> it=Qr.qrMap.entrySet().iterator();
 		while(it.hasNext()){
			Entry<String,Qr> entry=it.next();
			if(entry.getValue().getState().equals(Qr.used)){
				Qr.qrMap.remove(entry.getKey());
			}else{
				Date qrDate=entry.getValue().getGeneTime();
				Date now=new Date();
				if((now.getTime()-qrDate.getTime())>1000*60*60){
					Qr.qrMap.remove(entry.getKey());
				}
			}
		}
 		//打印剩下的qr
 		it=Qr.qrMap.entrySet().iterator();
 		while(it.hasNext()){
 			Entry<String,Qr> entry=it.next();
 			System.out.println(entry.getValue());
 		}
	}
}
