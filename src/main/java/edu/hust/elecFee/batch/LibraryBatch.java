package edu.hust.elecFee.batch;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import edu.hust.elecFee.bean.MyBatch;
import edu.hust.elecFee.service.LibraryService;
import edu.hust.elecFee.service.RouterNative;

@Service
public class LibraryBatch {
	protected Log log=LogFactory.getLog(getClass());
	
	@Autowired
	private JdbcTemplate jdbcTpltCore;
	@Autowired
	private LibraryService libraryService;
	@Autowired
	private RouterNative routerNative;
	
	private String title="图书馆还书通知";
	private String content=""
			+ "{0}：您好，您借阅的图书即将到期，请尽快归还或者办理续借以免产生罚款。\n\n";
	private String detail="借阅书籍：{0}\n"
			+ "到期时间：{1}\n\n";

	//每天10点执行
	@MyBatch(cron = "0 0 10 * * ?")
	public void brithRemind(){
		String sql="select userId from dcp_wechat.library_user where remind='1'";
		List<String> list=jdbcTpltCore.queryForList(sql,String.class);
		/*for(String userId:list){
			JSONObject jo=libraryService.libRecords(userId);
			JSONArray ja=jo.getJSONArray("items");
			if(ja==null){
				continue;
			}
			for(int i=0;i<ja.size();i++){
				JSONObject item=ja.getJSONObject(i);
				String dueDate=item.getString("duedate");
				
				
				routerNative.sendRemind(userId,title,content,null);
//				routerNative.sendBirthRemind("2016210052", content);
				System.out.println("已向"+userId+"发送到期提醒");
				log.error("已向"+userId+"发送到期提醒");
			}
		}*/
	}
}
