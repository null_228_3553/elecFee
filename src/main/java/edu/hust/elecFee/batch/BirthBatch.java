package edu.hust.elecFee.batch;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import edu.hust.elecFee.bean.MyBatch;
import edu.hust.elecFee.service.RouterNative;

@Service
public class BirthBatch {
	protected Log log=LogFactory.getLog(getClass());
	
	@Autowired
	private JdbcTemplate jdbcTpltCore;
	@Autowired
	private RouterNative routerNative;
	
	private String content1=""
			+ "尊敬的{0}老师：您好\n\n"
			+ "今天是您的生日，华中大微校园恭祝您生日快乐，万事如意，身体健康，心想事成！\n\n"
			+ "愿这特殊的日子里，您的每时每刻都充满欢乐！";
	private String content2=""
			+ "亲爱的{0}同学：您好\n\n"
			+ "今天是您的生日，华中大微校园恭祝您生日快乐，万事如意，身体健康，心想事成！\n\n"
			+ "愿这特殊的日子里，您的每时每刻都充满欢乐！";
	private String content3=""
			+ "Dear {0}：\n\n"
			+ "Today is your birthday, Micro Campus wish you Happy Birthday!\n\n"
			+ "Wishing you a wonderful day and a year filled with happiness！";

	//每天9点执行
	@MyBatch(cron = "0 0 9 * * ?")
	public void brithRemind(){
		String sql="select t.name,t.userid_noencrypt,u.user_name_pinyin "
				+ "from dcp_wechat.wx_uim_tab t,uicm_user u,uicm_user_account a " 
				+ "where t.userid_noencrypt=a.id_number and a.user_id=u.user_id "
				+ "and to_char(u.birthday,'MMDD')=?";
		SimpleDateFormat sdf=new SimpleDateFormat("MMdd");
		String mmdd=sdf.format(new Date());
		List<Map<String,Object>> list=jdbcTpltCore.queryForList(sql,mmdd);
		for(Map<String,Object> map:list){
			String name=(String)map.get("name");
			String userId=(String)map.get("userid_noencrypt");
			String pinyin=(String)map.get("user_name_pinyin");
			String content="";
			char first=userId.charAt(0);
			if(first=='I'){//留学生
				content=MessageFormat.format(content3, pinyin);
			}else if(first>='0'&&first<='9'){//老师
				content=MessageFormat.format(content1, name);
			}else{//学生
				content=MessageFormat.format(content2, name);
			}
			String title="微校园祝您生日快乐";
			routerNative.sendRemind(userId,title,content,null);
//			routerNative.sendBirthRemind("2016210052", content);
			System.out.println("已向"+userId+"发送生日祝福");
			log.error("已向"+userId+"发送生日祝福");
		}
	}
}
