package edu.hust.elecFee.batch;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import edu.hust.elecFee.bean.MyBatch;
import edu.hust.elecFee.service.Router;

@Service
public class JobBatch {
	protected Log log=LogFactory.getLog(getClass());
	
	@Autowired
	private JdbcTemplate jdbcTpltJob;
	@Autowired
	private JdbcTemplate jdbcTpltCore;
	@Autowired
	private Router router;
	
	private SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
	private String title="今天的校园宣讲会";
	private String remindLink="https://m.hust.edu.cn/elecFee/job/job.jsp";

	//每天8点半
	@MyBatch(cron = "0 30 8 * * ?")
	public void jobRemind(){
		String jobSql="select ctype,title,zpdd,kssj,jssj from tb_zph where substr(dbms_lob.substr(KSSJ),0,10)=?";
		Date tody=new Date(System.currentTimeMillis());
		String todyStr=sdf.format(tody);
		//查询今天的宣讲会
		List<Map<String,Object>> jobList=jdbcTpltJob.queryForList(jobSql,new Object[]{
				todyStr
		});
		List<Map<String,Object>> mainJobList=new ArrayList<Map<String,Object>>();
		List<Map<String,Object>> mediJobList=new ArrayList<Map<String,Object>>();
		for(int i=0;i<jobList.size();i++){
			Map<String,Object> job=jobList.get(i);
			if("0".equals(job.get("CTYPE"))){
				mainJobList.add(job);
			}else{
				mediJobList.add(job);
			}
		}
		
		String userSql="select * from jobRemind where jobmain='1' or jobmedi='1'";
		//查询推送用户列表
		List<Map<String,Object>> userList=jdbcTpltCore.queryForList(userSql);
		String mainUser="";
		String mediUser="";
		for(int i=0;i<userList.size();i++){
			Map<String,Object> user=userList.get(i);
			if("1".equals(user.get("JOBMAIN"))){
				mainUser+=(String)user.get("USERID")+",";
			}
			if("1".equals(user.get("JOBMEDI"))){
				mediUser+=(String)user.get("USERID")+",";
			}
		}
		//去掉最后逗号
		if(mainUser.length()>0)
			mainUser=mainUser.substring(0, mainUser.length()-1);
		if(mediUser.length()>0)
			mediUser=mediUser.substring(0, mediUser.length()-1);
		
		String mainContent="<div style='font-size:14px;padding:5px;line-height:25px;'>"
				+ "<div style='color:grey'>您好，今天有如下招聘会将在主校区召开：</div>";;
		String mainDesc="您好，今天有如下招聘会将在主校区召开：\n\n";
		String mediContent="<div style='font-size:14px;padding:5px;line-height:25px;'>"
				+ "<div style='color:grey'>您好，今天有如下招聘会将在同济校区召开：</div>";;
		String mediDesc="您好，今天有如下招聘会将在同济校区召开：\n\n";
		for(Map<String,Object> job:mainJobList){
			mainContent+="<div style='margin-top:10px;'>"
					+ "<span style='font-size:16px;'>"+job.get("TITLE")+"</span></br>"
					+ "<span style='color:grey'>时间："+job.get("KSSJ")+"</span></br>"
					+ "<div style='color:grey;border-bottom:solid 1px #eee'>地点："+job.get("ZPDD")+"</div>"
					+ "</div>";
			mainDesc+=job.get("TITLE")+"\n"
					+ "时间："+job.get("KSSJ")+"\n"
					+ "地点："+job.get("ZPDD")+"\n\n";
		}
		for(Map<String,Object> job:mediJobList){
			mediContent+="<div style='margin-top:10px;'>"
					+ "<span style='font-size:16px;'>"+job.get("TITLE")+"</span></br>"
					+ "<span style='color:grey'>时间："+job.get("KSSJ")+"</span></br>"
					+ "<div style='color:grey;border-bottom:solid 1px #eee'>地点："+job.get("ZPDD")+"</div>"
					+ "</div>";
			mediDesc+=job.get("TITLE")+"\n"
					+ "时间："+job.get("KSSJ")+"\n"
					+ "地点："+job.get("ZPDD")+"\n\n";
		}
		mainContent+="<div style='margin-top:10px;color:#7F7E7E;font-size:14px'>消息来自华中大微校园‘校园宣讲会’应用</div>"
				+ "</div>";
		mediContent+="<div style='margin-top:10px;color:#7F7E7E;font-size:14px'>消息来自华中大微校园‘校园宣讲会’应用</div>"
				+ "</div>";
		
		try {
			if(mainJobList.size()>0 && mainUser.length()>0){
				router.sendWxMessage(mainUser, title, mainDesc, URLEncoder.encode(mainContent,"utf-8"),remindLink);
			}
			if(mediJobList.size()>0 && mediUser.length()>0){
				router.sendWxMessage(mediUser, title, mediDesc, URLEncoder.encode(mediContent,"utf-8"),remindLink);
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		log.error("发送就业宣讲会提醒："+new Date());
	}
}
