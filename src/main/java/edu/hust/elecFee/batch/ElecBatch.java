package edu.hust.elecFee.batch;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import edu.hust.elecFee.bean.MyBatch;
import edu.hust.elecFee.business.ElecBusiness;
import edu.hust.elecFee.service.RouterNative;

@Service
public class ElecBatch {
	protected Log log=LogFactory.getLog(getClass());
	
	@Autowired
	private JdbcTemplate jdbcTpltCore;
	@Autowired
	private ElecBusiness elecBusiness;
	@Autowired
	private RouterNative routerNative;
	
	private SimpleDateFormat sdf=new SimpleDateFormat("yy年MM月dd日HH时mm分");
	private int step=1000;
	public static String content=""
			+ "宿舍号： {0} {1} 室 \n"
			+ "检查时间：{2} \n"
			+ "当前电量：剩余{3}度 \n"
			+ "请尽快充值";

	//每天2点查询
	@MyBatch(cron = "0 0 2 * * ?")
	public void qryLow(){
		//存所有宿舍电费的map，避免重复查询接口
		Map<String,BigDecimal> elecMap=new HashMap<String,BigDecimal>();
		
		log.error("跑批开始时间："+new Date());
		String qrySql="select * from(select a.*,ROWNUM RN from mydormitory a where dormitorystate='N')"
				+ "where RN>=? and RN<?";
		String insertSql="insert into lowElecQueue(id,userId,remainElec,programid,txtyq,txtld,txtroom,mentioned,checkTime) values (lowElecQueueSeq.nextval,?,?,?,?,?,?,?,?)";
		int i=0;
		//每次去1000条循环处理
		while(true){
			List<Map<String,Object>> list=jdbcTpltCore.queryForList(qrySql,
					new Object[]{i*step,(i+1)*step});
			if(list==null||list.size()==0)
				break;
			for(Map<String,Object> map:list){
				String programid=(String)map.get("programid");//小区
				String txtyq=(String)map.get("txtyq");//楼栋
				String txtld=(String)map.get("txtld");//楼层
				String txtroom=(String)map.get("txtroom");//房间号
				String userId=(String)map.get("userId");
				String warnSwitch=(String)map.get("warnSwitch");
				BigDecimal threshold=(BigDecimal)map.get("threshold");
				if(warnSwitch.equals("on")){
					try{
						BigDecimal remainElec=null;
						if(elecMap.containsKey(txtyq+txtroom)){
							remainElec=elecMap.get(txtyq+txtroom);
						}else{
							Map<String,Object> resMap=elecBusiness.queryDetail(null, null, programid, txtyq, txtld, txtroom);
							if(resMap==null||resMap.get("remainElec")==null)
								continue;
							remainElec=new BigDecimal((String)resMap.get("remainElec"));
							elecMap.put(txtyq+txtroom, remainElec);
						}
						if(remainElec.compareTo(threshold)==-1){
							jdbcTpltCore.update(insertSql,new Object[]{
								userId,remainElec,programid,txtyq,txtld,txtroom,"0",new Timestamp(System.currentTimeMillis())
							});
						}
					}catch(Exception e){
						log.error(e);
						continue;
					}
				}
			}
			i++;
		}
		log.error("跑批结束时间："+new Date());
	}
	
	//8点发送微信提醒
	@MyBatch(cron = "0 0 8 * * ?")
	public void elecRemind(){
		String title="宿舍电量不足提醒";
		String callback="https://m.hust.edu.cn/elecFee/";
		log.error("电费不足提醒推送开始时间："+new Date());
		String qryListSql="select * from lowElecQueue where mentioned='0'";
		String updateSql="update lowElecQueue set mentioned=? where id=?";
		List<Map<String,Object>> list=jdbcTpltCore.queryForList(qryListSql);
		for(Map<String,Object> map:list){
			BigDecimal id=(BigDecimal)map.get("id");
			String txtyq=(String)map.get("txtyq");//楼栋
			String txtroom=(String)map.get("txtroom");//房间号
			String userId=(String)map.get("userId");//
			BigDecimal remainElec=(BigDecimal)map.get("remainElec");
			Timestamp checkTime=(Timestamp)map.get("checkTime");
			String timeStr=sdf.format(new Date(checkTime.getTime()));
			
			//发微信提醒
			String message=MessageFormat.format(content,txtyq,txtroom,timeStr,remainElec);
			routerNative.sendRemind(userId,title,message,callback);
//			routerNative.sendElecRemind("2016210052",message);
			log.error(message);
			//更新状态
			jdbcTpltCore.update(updateSql,new Object[]{"1",id});
		}
		log.error("电费不足提醒推送结束时间："+new Date());
	}
}
