package edu.hust.elecFee.bean;

public class NccCount {
	private String count_date;
	private Double yj_users;
	private Double yj_mails;
	private Double xyk_jgk;
	private Double xyk_jgk_inc;
	private Double xyk_xsk;
	private Double xyk_xsk_inc;
	private Double xyk_ptk;
	private Double xyk_ptk_inc;
	private Double xyk_xfk;
	private Double xyk_xfk_inc;
	private Double xyk_xf_bs;
	private Double xyk_xf_bs_inc;
	private Double xyk_xf_je;
	private Double xyk_xf_je_inc;
	private Double xyk_qc_bs;
	private Double xyk_qc_bs_inc;
	private Double xyk_qc_je;
	private Double xyk_qc_je_inc;
	private Double xyk_xj_bs;
	private Double xyk_xj_bs_inc;
	private Double xyk_xj_je;
	private Double xyk_xj_je_inc;
	private Double xyk_zdzz_bs;
	private Double xyk_zdzz_bs_inc;
	private Double xyk_zdzz_je;
	private Double xyk_zdzz_je_inc;
	private Double xyk_wxy_bs;
	private Double xyk_wxy_bs_inc;
	private Double xyk_wxy_je;
	private Double xyk_smf;
	private Double xyk_wxy_je_inc;
	private Double bsdt_avg;
	private Double bsdt_done;
	private Double bsdt_prod;
	private Double bsdt_dev;
	private Double dx_avg;
	private Double hy_total;
	private Double wzq_apply;
	private Double wzq_prod;
	private Double wzq_done;
	private Double zbh_office;
	private Double zbh_win10;
	private Double zbh_win7;
	private Double zbh_visio;
	private Double zbh_project;
	private Double zbh_matlab;
	public Double getXyk_smf() {
		return xyk_smf;
	}
	public void setXyk_smf(Double xyk_smf) {
		this.xyk_smf = xyk_smf;
	}
	public Double getZbh_matlab() {
		return zbh_matlab;
	}
	public void setZbh_matlab(Double zbh_matlab) {
		this.zbh_matlab = zbh_matlab;
	}
	public String getCount_date() {
		return count_date;
	}
	public void setCount_date(String count_date) {
		this.count_date = count_date;
	}
	public Double getYj_users() {
		return yj_users;
	}
	public void setYj_users(Double yj_users) {
		this.yj_users = yj_users;
	}
	public Double getYj_mails() {
		return yj_mails;
	}
	public void setYj_mails(Double yj_mails) {
		this.yj_mails = yj_mails;
	}
	public Double getXyk_jgk() {
		return xyk_jgk;
	}
	public void setXyk_jgk(Double xyk_jgk) {
		this.xyk_jgk = xyk_jgk;
	}
	public Double getXyk_jgk_inc() {
		return xyk_jgk_inc;
	}
	public void setXyk_jgk_inc(Double xyk_jgk_inc) {
		this.xyk_jgk_inc = xyk_jgk_inc;
	}
	public Double getXyk_xsk() {
		return xyk_xsk;
	}
	public void setXyk_xsk(Double xyk_xsk) {
		this.xyk_xsk = xyk_xsk;
	}
	public Double getXyk_xsk_inc() {
		return xyk_xsk_inc;
	}
	public void setXyk_xsk_inc(Double xyk_xsk_inc) {
		this.xyk_xsk_inc = xyk_xsk_inc;
	}
	public Double getXyk_ptk() {
		return xyk_ptk;
	}
	public void setXyk_ptk(Double xyk_ptk) {
		this.xyk_ptk = xyk_ptk;
	}
	public Double getXyk_ptk_inc() {
		return xyk_ptk_inc;
	}
	public void setXyk_ptk_inc(Double xyk_ptk_inc) {
		this.xyk_ptk_inc = xyk_ptk_inc;
	}
	public Double getXyk_xfk() {
		return xyk_xfk;
	}
	public void setXyk_xfk(Double xyk_xfk) {
		this.xyk_xfk = xyk_xfk;
	}
	public Double getXyk_xfk_inc() {
		return xyk_xfk_inc;
	}
	public void setXyk_xfk_inc(Double xyk_xfk_inc) {
		this.xyk_xfk_inc = xyk_xfk_inc;
	}
	public Double getXyk_xf_bs() {
		return xyk_xf_bs;
	}
	public void setXyk_xf_bs(Double xyk_xf_bs) {
		this.xyk_xf_bs = xyk_xf_bs;
	}
	public Double getXyk_xf_bs_inc() {
		return xyk_xf_bs_inc;
	}
	public void setXyk_xf_bs_inc(Double xyk_xf_bs_inc) {
		this.xyk_xf_bs_inc = xyk_xf_bs_inc;
	}
	public Double getXyk_xf_je() {
		return xyk_xf_je;
	}
	public void setXyk_xf_je(Double xyk_xf_je) {
		this.xyk_xf_je = xyk_xf_je;
	}
	public Double getXyk_xf_je_inc() {
		return xyk_xf_je_inc;
	}
	public void setXyk_xf_je_inc(Double xyk_xf_je_inc) {
		this.xyk_xf_je_inc = xyk_xf_je_inc;
	}
	public Double getXyk_qc_bs() {
		return xyk_qc_bs;
	}
	public void setXyk_qc_bs(Double xyk_qc_bs) {
		this.xyk_qc_bs = xyk_qc_bs;
	}
	public Double getXyk_qc_bs_inc() {
		return xyk_qc_bs_inc;
	}
	public void setXyk_qc_bs_inc(Double xyk_qc_bs_inc) {
		this.xyk_qc_bs_inc = xyk_qc_bs_inc;
	}
	public Double getXyk_qc_je() {
		return xyk_qc_je;
	}
	public void setXyk_qc_je(Double xyk_qc_je) {
		this.xyk_qc_je = xyk_qc_je;
	}
	public Double getXyk_qc_je_inc() {
		return xyk_qc_je_inc;
	}
	public void setXyk_qc_je_inc(Double xyk_qc_je_inc) {
		this.xyk_qc_je_inc = xyk_qc_je_inc;
	}
	public Double getXyk_xj_bs() {
		return xyk_xj_bs;
	}
	public void setXyk_xj_bs(Double xyk_xj_bs) {
		this.xyk_xj_bs = xyk_xj_bs;
	}
	public Double getXyk_xj_bs_inc() {
		return xyk_xj_bs_inc;
	}
	public void setXyk_xj_bs_inc(Double xyk_xj_bs_inc) {
		this.xyk_xj_bs_inc = xyk_xj_bs_inc;
	}
	public Double getXyk_xj_je() {
		return xyk_xj_je;
	}
	public void setXyk_xj_je(Double xyk_xj_je) {
		this.xyk_xj_je = xyk_xj_je;
	}
	public Double getXyk_xj_je_inc() {
		return xyk_xj_je_inc;
	}
	public void setXyk_xj_je_inc(Double xyk_xj_je_inc) {
		this.xyk_xj_je_inc = xyk_xj_je_inc;
	}
	public Double getXyk_zdzz_bs() {
		return xyk_zdzz_bs;
	}
	public void setXyk_zdzz_bs(Double xyk_zdzz_bs) {
		this.xyk_zdzz_bs = xyk_zdzz_bs;
	}
	public Double getXyk_zdzz_bs_inc() {
		return xyk_zdzz_bs_inc;
	}
	public void setXyk_zdzz_bs_inc(Double xyk_zdzz_bs_inc) {
		this.xyk_zdzz_bs_inc = xyk_zdzz_bs_inc;
	}
	public Double getXyk_zdzz_je() {
		return xyk_zdzz_je;
	}
	public void setXyk_zdzz_je(Double xyk_zdzz_je) {
		this.xyk_zdzz_je = xyk_zdzz_je;
	}
	public Double getXyk_zdzz_je_inc() {
		return xyk_zdzz_je_inc;
	}
	public void setXyk_zdzz_je_inc(Double xyk_zdzz_je_inc) {
		this.xyk_zdzz_je_inc = xyk_zdzz_je_inc;
	}
	public Double getXyk_wxy_bs() {
		return xyk_wxy_bs;
	}
	public void setXyk_wxy_bs(Double xyk_wxy_bs) {
		this.xyk_wxy_bs = xyk_wxy_bs;
	}
	public Double getXyk_wxy_bs_inc() {
		return xyk_wxy_bs_inc;
	}
	public void setXyk_wxy_bs_inc(Double xyk_wxy_bs_inc) {
		this.xyk_wxy_bs_inc = xyk_wxy_bs_inc;
	}
	public Double getXyk_wxy_je() {
		return xyk_wxy_je;
	}
	public void setXyk_wxy_je(Double xyk_wxy_je) {
		this.xyk_wxy_je = xyk_wxy_je;
	}
	public Double getXyk_wxy_je_inc() {
		return xyk_wxy_je_inc;
	}
	public void setXyk_wxy_je_inc(Double xyk_wxy_je_inc) {
		this.xyk_wxy_je_inc = xyk_wxy_je_inc;
	}
	public Double getBsdt_avg() {
		return bsdt_avg;
	}
	public void setBsdt_avg(Double bsdt_avg) {
		this.bsdt_avg = bsdt_avg;
	}
	public Double getBsdt_done() {
		return bsdt_done;
	}
	public void setBsdt_done(Double bsdt_done) {
		this.bsdt_done = bsdt_done;
	}
	public Double getBsdt_prod() {
		return bsdt_prod;
	}
	public void setBsdt_prod(Double bsdt_prod) {
		this.bsdt_prod = bsdt_prod;
	}
	public Double getBsdt_dev() {
		return bsdt_dev;
	}
	public void setBsdt_dev(Double bsdt_dev) {
		this.bsdt_dev = bsdt_dev;
	}
	public Double getDx_avg() {
		return dx_avg;
	}
	public void setDx_avg(Double dx_avg) {
		this.dx_avg = dx_avg;
	}
	public Double getHy_total() {
		return hy_total;
	}
	public void setHy_total(Double hy_total) {
		this.hy_total = hy_total;
	}
	public Double getWzq_apply() {
		return wzq_apply;
	}
	public void setWzq_apply(Double wzq_apply) {
		this.wzq_apply = wzq_apply;
	}
	public Double getWzq_prod() {
		return wzq_prod;
	}
	public void setWzq_prod(Double wzq_prod) {
		this.wzq_prod = wzq_prod;
	}
	public Double getWzq_done() {
		return wzq_done;
	}
	public void setWzq_done(Double wzq_done) {
		this.wzq_done = wzq_done;
	}
	public Double getZbh_office() {
		return zbh_office;
	}
	public void setZbh_office(Double zbh_office) {
		this.zbh_office = zbh_office;
	}
	public Double getZbh_win10() {
		return zbh_win10;
	}
	public void setZbh_win10(Double zbh_win10) {
		this.zbh_win10 = zbh_win10;
	}
	public Double getZbh_win7() {
		return zbh_win7;
	}
	public void setZbh_win7(Double zbh_win7) {
		this.zbh_win7 = zbh_win7;
	}
	public Double getZbh_visio() {
		return zbh_visio;
	}
	public void setZbh_visio(Double zbh_visio) {
		this.zbh_visio = zbh_visio;
	}
	public Double getZbh_project() {
		return zbh_project;
	}
	public void setZbh_project(Double zbh_project) {
		this.zbh_project = zbh_project;
	}
	
}
