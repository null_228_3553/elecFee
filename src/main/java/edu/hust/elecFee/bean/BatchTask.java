package edu.hust.elecFee.bean;

import java.lang.reflect.Method;
import java.util.Date;

import com.alibaba.fastjson.annotation.JSONField;

public class BatchTask {
	public static int stop=0;
	public static int run=1;
	
	private int id;	//0开始
	private String group;//组
	private String name;
	private Object obj;//batch的类实例
	private Method met;//batch的方法
	private String cron="";	//cron表达式
	private int executeTimes=0;	//执行过的次数
	@JSONField(format="yyyy-MM-dd HH:mm:ss")
	private Date lastExeTime;	//最后一次开始
	@JSONField(format="yyyy-MM-dd HH:mm:ss")
	private Date lastfinishTime;	//最后一次结束时间
	private int state=BatchTask.run;	//1启动 0禁用
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Object getObj() {
		return obj;
	}
	public void setObj(Object obj) {
		this.obj = obj;
	}
	public Method getMet() {
		return met;
	}
	public void setMet(Method met) {
		this.met = met;
	}
	public String getCron() {
		return cron;
	}
	public void setCron(String cron) {
		this.cron = cron;
	}
	public int getExecuteTimes() {
		return executeTimes;
	}
	public void setExecuteTimes(int executeTimes) {
		this.executeTimes = executeTimes;
	}
	public Date getLastExeTime() {
		return lastExeTime;
	}
	public void setLastExeTime(Date lastExeTime) {
		this.lastExeTime = lastExeTime;
	}
	public Date getLastfinishTime() {
		return lastfinishTime;
	}
	public void setLastfinishTime(Date lastfinishTime) {
		this.lastfinishTime = lastfinishTime;
	}
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	
}
