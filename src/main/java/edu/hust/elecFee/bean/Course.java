package edu.hust.elecFee.bean;
public class Course{
	private String day;
	private String courseName;
	private String className;
	private String courseTime;
	private String roomName;
	private String remark;
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getCourseTime() {
		return courseTime;
	}
	public void setCourseTime(String courseTime) {
		this.courseTime = courseTime;
	}
	public String getRoomName() {
		return roomName;
	}
	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}
	@Override
	public String toString() {
		return "Course [day=" + day + ", courseName=" + courseName + ", className=" + className + ", courseTime="
				+ courseTime + ", roomName=" + roomName + ", remark=" + remark + "]";
	}
}
