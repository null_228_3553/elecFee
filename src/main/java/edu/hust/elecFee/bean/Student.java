package edu.hust.elecFee.bean;

import java.math.BigDecimal;

public class Student {
	private String stdno;//账号
	private String carno;//学号
	private String name;//姓名
	private String passwd;//密码
	private BigDecimal leftfee;//剩余金额
	
	public String getStdno() {
		return stdno;
	}
	public void setStdno(String stdno) {
		this.stdno = stdno;
	}
	public String getCarno() {
		return carno;
	}
	public void setCarno(String carno) {
		this.carno = carno;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPasswd() {
		return passwd;
	}
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
	public BigDecimal getLeftfee() {
		return leftfee;
	}
	public void setLeftfee(BigDecimal leftfee) {
		this.leftfee = leftfee;
	}
	
}
