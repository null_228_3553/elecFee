package edu.hust.elecFee.bean;

import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Qr {
	
	public static String unuse="1";
	public static String used="2";
	public static String scaned="3";
	public static Map<String,Qr> qrMap=new ConcurrentHashMap<String,Qr>();
	
	public Qr(){}
	public Qr(String uuid, Date geneTime, String state) {
		this.uuid = uuid;
		this.geneTime = geneTime;
		this.state = state;
	}
	private String uuid ;
	private Date geneTime;
	private String state;//unuse  used   scaned
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public Date getGeneTime() {
		return geneTime;
	}
	public void setGeneTime(Date geneTime) {
		this.geneTime = geneTime;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	@Override
	public String toString() {
		String a="uuid="+uuid+"---geneTime="+geneTime+"---state="+state;
		return a;
	}
	
}
