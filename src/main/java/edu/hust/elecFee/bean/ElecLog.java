package edu.hust.elecFee.bean;

import java.sql.Date;

public class ElecLog {
	
    private int id;
    private String userId;
    private String trans;
    private String ip;
    private String dormStr;
    private String retMsg;
    private boolean success;
    private String errMsg;
    private Date qryDate;
    private long processTime;
    
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getTrans() {
		return trans;
	}
	public void setTrans(String trans) {
		this.trans = trans;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getDormStr() {
		return dormStr;
	}
	public void setDormStr(String dormStr) {
		this.dormStr = dormStr;
	}
	public String getRetMsg() {
		return retMsg;
	}
	public void setRetMsg(String retMsg) {
		this.retMsg = retMsg;
	}
	public boolean getSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getErrMsg() {
		return errMsg;
	}
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
	public Date getQryDate() {
		return qryDate;
	}
	public void setQryDate(Date qryDate) {
		this.qryDate = qryDate;
	}
	public long getProcessTime() {
		return processTime;
	}
	public void setProcessTime(long processTime) {
		this.processTime = processTime;
	}
    
}
