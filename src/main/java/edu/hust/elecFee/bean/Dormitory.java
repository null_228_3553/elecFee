package edu.hust.elecFee.bean;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class Dormitory {
	
	private String programId ;
	private String txtyq;
	private String txtld;
	private String txtroom;
	private String dormitoryState;
	private String warnSwitch;//默认on
	private BigDecimal threshold;//默认10度
	private Timestamp createTime;
	
	public String getProgramId() {
		return programId;
	}
	public void setProgramId(String programId) {
		this.programId = programId;
	}
	public String getTxtyq() {
		return txtyq;
	}
	public void setTxtyq(String txtyq) {
		this.txtyq = txtyq;
	}
	public String getTxtld() {
		return txtld;
	}
	public void setTxtld(String txtld) {
		this.txtld = txtld;
	}
	public String getTxtroom() {
		return txtroom;
	}
	public void setTxtroom(String txtroom) {
		this.txtroom = txtroom;
	}
	public String getDormitoryState() {
		return dormitoryState;
	}
	public void setDormitoryState(String dormitoryState) {
		this.dormitoryState = dormitoryState;
	}
	public Timestamp getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}
	public String getWarnSwitch() {
		return warnSwitch;
	}
	public void setWarnSwitch(String warnSwitch) {
		this.warnSwitch = warnSwitch;
	}
	public BigDecimal getThreshold() {
		return threshold;
	}
	public void setThreshold(BigDecimal threshold) {
		this.threshold = threshold;
	}
	
}
