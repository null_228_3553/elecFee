package edu.hust.elecFee.bean;

import java.lang.reflect.Method;
import java.util.Date;
import java.util.Map;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class JobProxy implements Job{
	
	public void execute(JobExecutionContext context) throws JobExecutionException {
		Map<?,?> dataMap=context.getTrigger().getJobDataMap();
		BatchTask task=(BatchTask)dataMap.get("task");
		Method m=task.getMet();
		Object o=task.getObj();
		Object[] args=(Object[])dataMap.get("args");
		try {
			task.setLastExeTime(new Date());
			if(args==null){
				m.invoke(o);
			}else{
				m.invoke(o,args);
			}
			task.setLastfinishTime(new Date());
			task.setExecuteTimes(task.getExecuteTimes()+1);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

}
