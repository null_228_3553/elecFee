package edu.hust.elecFee.bean;

import java.lang.annotation.ElementType;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)  
@Target(ElementType.METHOD)//注解作用于方法上
public @interface MyBatch {
	 String cron();
}
