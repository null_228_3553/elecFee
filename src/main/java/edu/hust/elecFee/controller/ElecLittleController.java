package edu.hust.elecFee.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;


@Controller
public class ElecLittleController {
	private static String url="http://202.114.18.218/main.aspx";
	
	//查询小区
	@RequestMapping(value = "/ltXq")
	@ResponseBody
	public String ltXq(HttpServletRequest request,HttpServletResponse response){
		
		JSONObject jo=new JSONObject();
		try {
			Response res=Jsoup.connect(url).execute();
			Document doc=Jsoup.parse(res.body());
			Element select=doc.getElementById("programId");
			Elements option=select.getElementsByTag("option");
			JSONArray ja=new JSONArray();
			for(Element e:option){
				String value=e.attr("value");
				JSONObject joTemp=new JSONObject();
				joTemp.put("value", value);
				ja.add(joTemp);
			}
			jo.put("xq", ja);
			return success(jo);
		} catch (IOException e) {
			return excep();
		}
		
	}
	
	//查询楼栋
	@RequestMapping(value = "/ltLd")
	@ResponseBody
	public String ltLd(HttpServletRequest request,HttpServletResponse response){
//		HttpSession session=request.getSession();
		Map<String,String> map=new HashMap<String,String>();
		map.put("eventTarget","programId");
//		map.put("__VIEWSTATE",(String)session.getAttribute("viewState"));
//		map.put("__EVENTVALIDATION",(String)session.getAttribute("eventValidation"));
//		map.put("programId", request.getParameter("programId"));
		map.put("__VIEWSTATE","/wEPDwULLTEyNjgyMDA1OTgPZBYCAgMPZBYIAgEPEA8WBh4NRGF0YVRleHRGaWVsZAUM5qW85qCL5Yy65Z+fHg5EYXRhVmFsdWVGaWVsZAUM5qW85qCL5Yy65Z+fHgtfIURhdGFCb3VuZGdkEBUHBuS4nOWMugznlZnlrabnlJ/mpbwG6KW/5Yy6DOmfteiLkeS6jOacnwzpn7Xoi5HkuIDmnJ8G57Sr6I+YCy3or7fpgInmi6ktFQcG5Lic5Yy6DOeVmeWtpueUn+alvAbopb/ljLoM6Z+16IuR5LqM5pyfDOmfteiLkeS4gOacnwbntKvoj5gCLTEUKwMHZ2dnZ2dnZxYBAgZkAgUPEGRkFgBkAhcPPCsADQBkAhkPPCsADQBkGAMFHl9fQ29udHJvbHNSZXF1aXJlUG9zdEJhY2tLZXlfXxYCBQxJbWFnZUJ1dHRvbjEFDEltYWdlQnV0dG9uMgUJR3JpZFZpZXcxD2dkBQlHcmlkVmlldzIPZ2RacAGIrDSD3OQVZZAXoJDr23zPkQ==");
		map.put("__EVENTVALIDATION","/wEWDQKisvDsAwLc1sToBgL+zqXMDgK50MfoBgKhi6GaBQLdnbOlBgLtuMzrDQLrwqHzBQL61dqrBgLSwpnTCALSwtXkAgLs0fbZDALs0Yq1BbDmWenjYrcs+Wbi4fSUso76axua");
		map.put("programId", "紫菘");
		
		JSONObject jo=new JSONObject();
		try {
			Response res=Jsoup.connect(url).data(map).execute();
			Document doc=Jsoup.parse(res.body());
			Element select=doc.getElementById("txtyq");
			Elements option=select.getElementsByTag("option");
			JSONArray ja=new JSONArray();
			for(Element e:option){
				String value=e.attr("value");
				JSONObject joTemp=new JSONObject();
				joTemp.put("value", value);
				ja.add(joTemp);
			}
			jo.put("ld", ja);
			return success(jo);
		} catch (IOException e) {
			return excep();
		}
		
	}

	//查询楼层
	@RequestMapping(value = "/ltLc")
	@ResponseBody
	public String ltLc(HttpServletRequest request,HttpServletResponse response){
//		HttpSession session=request.getSession();
		Map<String,String> map=new HashMap<String,String>();
		map.put("eventTarget","txtyq");
//		map.put("__VIEWSTATE",(String)session.getAttribute("viewState"));
//		map.put("__EVENTVALIDATION",(String)session.getAttribute("eventValidation"));
//		map.put("txtyq", request.getParameter("txtyq"));
		map.put("__VIEWSTATE","/wEPDwULLTEyNjgyMDA1OTgPZBYCAgMPZBYIAgEPEA8WBh4NRGF0YVRleHRGaWVsZAUM5qW85qCL5Yy65Z+fHg5EYXRhVmFsdWVGaWVsZAUM5qW85qCL5Yy65Z+fHgtfIURhdGFCb3VuZGdkEBUHBuS4nOWMugznlZnlrabnlJ/mpbwG6KW/5Yy6DOmfteiLkeS6jOacnwzpn7Xoi5HkuIDmnJ8G57Sr6I+YCy3or7fpgInmi6ktFQcG5Lic5Yy6DOeVmeWtpueUn+alvAbopb/ljLoM6Z+16IuR5LqM5pyfDOmfteiLkeS4gOacnwbntKvoj5gCLTEUKwMHZ2dnZ2dnZxYBAgVkAgUPEA8WBh8ABQbmpbzlj7cfAQUG5qW85Y+3HwJnZBAVDwvntKvoj5gxMOagiwvntKvoj5gxMeagiwvntKvoj5gxMuagiwvntKvoj5gxM+agiwvntKvoj5gxNOagiwrntKvoj5gx5qCLCue0q+iPmDLmoIsK57Sr6I+YM+agiwrntKvoj5g05qCLCue0q+iPmDXmoIsK57Sr6I+YNuagiwrntKvoj5g35qCLCue0q+iPmDjmoIsK57Sr6I+YOeagiwst6K+36YCJ5oupLRUPC+e0q+iPmDEw5qCLC+e0q+iPmDEx5qCLC+e0q+iPmDEy5qCLC+e0q+iPmDEz5qCLC+e0q+iPmDE05qCLCue0q+iPmDHmoIsK57Sr6I+YMuagiwrntKvoj5gz5qCLCue0q+iPmDTmoIsK57Sr6I+YNeagiwrntKvoj5g25qCLCue0q+iPmDfmoIsK57Sr6I+YOOagiwrntKvoj5g55qCLAi0xFCsDD2dnZ2dnZ2dnZ2dnZ2dnZxYBAg5kAhcPPCsADQBkAhkPPCsADQBkGAMFHl9fQ29udHJvbHNSZXF1aXJlUG9zdEJhY2tLZXlfXxYCBQxJbWFnZUJ1dHRvbjEFDEltYWdlQnV0dG9uMgUJR3JpZFZpZXcxD2dkBQlHcmlkVmlldzIPZ2Rr3t2Usgyvi1QMghl/kW0PM6iiDw==");
		map.put("__EVENTVALIDATION","/wEWHAKesLbOBgLc1sToBgL+zqXMDgK50MfoBgKhi6GaBQLdnbOlBgLtuMzrDQLrwqHzBQLQ67mUCwLQ643/AgLQ65HaCQLQ6+WlAQLQ68mACAKtxvnpAQLC3ZvECwLn9L2/DgKcgt4pArGZ8IQKAtawkv8MAsvPtOoGAtDx9agOAvWIlgMClJSw2ggC+tXaqwYC0sKZ0wgC0sLV5AIC7NH22QwC7NGKtQU8Wj4OA5guYnex7N0uaD/8eHCrQg==");
		map.put("txtyq", "紫菘11栋");
		
		JSONObject jo=new JSONObject();
		try {
			Response res=Jsoup.connect(url).data(map).execute();
			Document doc=Jsoup.parse(res.body());
			Element select=doc.getElementById("txtld");
			Elements option=select.getElementsByTag("option");
			JSONArray ja=new JSONArray();
			for(Element e:option){
				String value=e.attr("value");
				JSONObject joTemp=new JSONObject();
				joTemp.put("value", value);
				ja.add(joTemp);
			}
			jo.put("lc", ja);
			return success(jo);
		} catch (IOException e) {
			return excep();
		}
	}
	
	//查询电费
	@RequestMapping(value = "/ltDf")
	@ResponseBody
	public String ltDf(HttpServletRequest request,HttpServletResponse response){
//		HttpSession session=request.getSession();
		Map<String,String> map=new HashMap<String,String>();
//		map.put("__VIEWSTATE",(String)session.getAttribute("viewState"));
//		map.put("__EVENTVALIDATION",(String)session.getAttribute("eventValidation"));
//		map.put("programId", request.getParameter("programId"));
//		map.put("txtyq",request.getParameter("txtyq"));
//		map.put("txtld",request.getParameter("txtld"));
//		map.put("txtroom",request.getParameter("txtroom"));
		map.put("__VIEWSTATE","/wEPDwULLTEyNjgyMDA1OTgPZBYCAgMPZBYKAgEPEA8WBh4NRGF0YVRleHRGaWVsZAUM5qW85qCL5Yy65Z+fHg5EYXRhVmFsdWVGaWVsZAUM5qW85qCL5Yy65Z+fHgtfIURhdGFCb3VuZGdkEBUHBuS4nOWMugznlZnlrabnlJ/mpbwG6KW/5Yy6DOmfteiLkeS6jOacnwzpn7Xoi5HkuIDmnJ8G57Sr6I+YCy3or7fpgInmi6ktFQcG5Lic5Yy6DOeVmeWtpueUn+alvAbopb/ljLoM6Z+16IuR5LqM5pyfDOmfteiLkeS4gOacnwbntKvoj5gCLTEUKwMHZ2dnZ2dnZxYBAgVkAgUPEA8WBh8ABQbmpbzlj7cfAQUG5qW85Y+3HwJnZBAVDwvntKvoj5gxMOagiwvntKvoj5gxMeagiwvntKvoj5gxMuagiwvntKvoj5gxM+agiwvntKvoj5gxNOagiwrntKvoj5gx5qCLCue0q+iPmDLmoIsK57Sr6I+YM+agiwrntKvoj5g05qCLCue0q+iPmDXmoIsK57Sr6I+YNuagiwrntKvoj5g35qCLCue0q+iPmDjmoIsK57Sr6I+YOeagiwst6K+36YCJ5oupLRUPC+e0q+iPmDEw5qCLC+e0q+iPmDEx5qCLC+e0q+iPmDEy5qCLC+e0q+iPmDEz5qCLC+e0q+iPmDE05qCLCue0q+iPmDHmoIsK57Sr6I+YMuagiwrntKvoj5gz5qCLCue0q+iPmDTmoIsK57Sr6I+YNeagiwrntKvoj5g25qCLCue0q+iPmDfmoIsK57Sr6I+YOOagiwrntKvoj5g55qCLAi0xFCsDD2dnZ2dnZ2dnZ2dnZ2dnZxYBAgFkAgkPEA8WBh8ABQnmpbzlsYLlj7cfAQUJ5qW85bGC5Y+3HwJnZBAVBwQx5bGCBDLlsYIEM+WxggQ05bGCBDXlsYIENuWxggst6K+36YCJ5oupLRUHBDHlsYIEMuWxggQz5bGCBDTlsYIENeWxggQ25bGCAi0xFCsDB2dnZ2dnZ2dkZAIXDzwrAA0AZAIZDzwrAA0AZBgDBR5fX0NvbnRyb2xzUmVxdWlyZVBvc3RCYWNrS2V5X18WAgUMSW1hZ2VCdXR0b24xBQxJbWFnZUJ1dHRvbjIFCUdyaWRWaWV3MQ9nZAUJR3JpZFZpZXcyD2dkZJGBhUCK64cDlO7Tj/w2U4n7qig=");
		map.put("__EVENTVALIDATION","/wEWIwLdoOmiAgLc1sToBgL+zqXMDgK50MfoBgKhi6GaBQLdnbOlBgLtuMzrDQLrwqHzBQLQ67mUCwLQ643/AgLQ65HaCQLQ6+WlAQLQ68mACAKtxvnpAQLC3ZvECwLn9L2/DgKcgt4pArGZ8IQKAtawkv8MAsvPtOoGAtDx9agOAvWIlgMClJSw2ggCg5T44w4CgpT44w4CgZT44w4CgJT44w4Ch5T44w4ChpT44w4Cj5S8ngIC+tXaqwYC0sKZ0wgC0sLV5AIC7NH22QwC7NGKtQWKrZEtZE4fychcI2JkLiIGY4+U9w==");
		map.put("programId", "紫菘");
		map.put("txtyq", "紫菘11栋");
		map.put("txtld", "2层");
		map.put("Txtroom", "202");
		map.put("ImageButton1.x", "9");
		map.put("ImageButton1.y", "8");

		JSONObject jo=new JSONObject();
		try {
			Response res=Jsoup.connect(url).data(map).execute();
			Document doc=Jsoup.parse(res.body());
			String reMainElec=doc.getElementById("TextBox3").attr("value");
			String lastCheck=doc.getElementById("TextBox2").attr("value");
			jo.put("reMainElec", reMainElec);
			jo.put("lastCheck", lastCheck);
			Element sevenTable=doc.getElementById("GridView2");
			if(sevenTable!=null){
				JSONArray ja=new JSONArray();
				Elements trs=sevenTable.getElementsByTag("tr");
				for(int i=1;i<trs.size();i++){//第一行为标题
					Elements tds=trs.get(i).getElementsByTag("td");
					JSONObject joTemp=new JSONObject();
					joTemp.put("data", tds.get(0).html());
					joTemp.put("time", tds.get(1).html());
					ja.add(joTemp);
				}
				jo.put("sevenTable", ja);
			}
			Element monthTable=doc.getElementById("GridView1");
			if(monthTable!=null){
				JSONArray ja=new JSONArray();
				Elements trs=monthTable.getElementsByTag("tr");
				for(int i=1;i<trs.size();i++){//第一行为标题
					Elements tds=trs.get(i).getElementsByTag("td");
					JSONObject joTemp=new JSONObject();
					joTemp.put("recharge", tds.get(0).html());
					joTemp.put("fee", tds.get(1).html());
					joTemp.put("time", tds.get(2).html());
					ja.add(joTemp);
				}
				jo.put("monthTable", ja);
			}
			return success(jo);
		} catch (IOException e) {
			return excep();
		}
	}
	
	private String success(JSONObject jo) {
		jo.put("returnCode", "000000");
		return jo.toString();
	}
	
	private String excep() {
		JSONObject jo=new JSONObject();
		jo.put("returnCode", "error");
		jo.put("returnMsg", "网络异常，请稍后再试");
		return jo.toString();
	}

}
