package edu.hust.elecFee.controller;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import edu.hust.elecFee.bean.Dormitory;
import edu.hust.elecFee.business.DormitoryBusiness;
import edu.hust.elecFee.business.ElecBusiness;
import edu.hust.elecFee.common.Dict;

@Controller
public class DormitoryController {

	@Autowired
	private DormitoryBusiness dormitoryBusiness;
	@Autowired
	private ElecBusiness elecBusiness;
	
	@RequestMapping(value = "/startAction")
	public ModelAndView startAction(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
		ModelAndView mav=new ModelAndView();
		String userId=(String)request.getSession().getAttribute("userId");
		Dormitory dormitory=dormitoryBusiness.checkDormitory(userId);
		if(dormitory==null){
			String area=elecBusiness.queryAllProgramId();
			return new ModelAndView("setDormitory","area",area);
		}else{
			HttpSession session=request.getSession();
			String viewState=(String)session.getAttribute(Dict.viewState);
			String eventValidation=(String)session.getAttribute(Dict.eventValidation);
			Map<String, Object> resMap=elecBusiness.queryDetail(viewState,eventValidation,
					dormitory.getProgramId(),dormitory.getTxtyq(),dormitory.getTxtld(),dormitory.getTxtroom());

			mav.addObject("dormitory", dormitory);
			//不存在该房间信息
			if(resMap==null){
				String area=elecBusiness.queryAllProgramId();
				mav.addObject("area",area);
				mav.addObject("error", "没有该宿舍的电费信息，请核对后重新设置宿舍");
				mav.setViewName("setDormitory");
			}else{
				mav.addAllObjects(resMap);
				mav.setViewName("elecResult");
			}
			return mav;
		}
	}
	
	@RequestMapping(value = "/saveDormitory")
	public ModelAndView saveDormitory(HttpServletRequest request,HttpServletResponse response){
		ModelAndView mav=new ModelAndView();

		String userId=(String)request.getSession().getAttribute("userId");
		String programId=request.getParameter("programId");
		String txtyq=request.getParameter("txtyq");
		String txtld=request.getParameter("txtld");
		String txtroom=request.getParameter("Txtroom");
		Dormitory dormitory=dormitoryBusiness.saveDormitory(userId,programId,txtyq,txtld,txtroom);
		
		HttpSession session=request.getSession();
		String viewState=(String)session.getAttribute(Dict.viewState);
		String eventValidation=(String)session.getAttribute(Dict.eventValidation);
		Map<String, Object> resMap=elecBusiness.queryDetail(viewState, eventValidation, 
				programId, txtyq, txtld, txtroom);

		mav.addObject("dormitory", dormitory);
		//不存在该房间信息
		if(resMap==null){
			String area=elecBusiness.queryAllProgramId();
			mav.addObject("area",area);
			mav.addObject("error", "没有该宿舍的电费信息，请核对后重新设置宿舍");
			mav.setViewName("setDormitory");
		}else{
			mav.addAllObjects(resMap);
			mav.setViewName("elecResult");
		}
		return mav;
	}
	
	@RequestMapping(value = "/setDormitoryPre")
	public ModelAndView setDormitoryPre(HttpServletRequest request,HttpServletResponse response){
		String userId=(String)request.getSession().getAttribute("userId");
		ModelAndView mav=new ModelAndView();
		String area=elecBusiness.queryAllProgramId();
		mav.addObject("area",area);
		
		Dormitory dormitory=dormitoryBusiness.checkDormitory(userId);
		if(dormitory!=null){
			mav.addObject("dormitory",dormitory);
		}
		
		mav.setViewName("setDormitory");
		return mav;
	}
	
	@RequestMapping(value = "/setThresholdPre")
	public ModelAndView setThresholdPre(HttpServletRequest request,HttpServletResponse response){
		String userId=(String)request.getSession().getAttribute("userId");
		ModelAndView mav=new ModelAndView();
		
		Dormitory dormitory=dormitoryBusiness.checkDormitory(userId);
		if(dormitory!=null){
			mav.addObject("dormitory",dormitory);
		}
		
		mav.setViewName("setThreshold");
		return mav;
	}
	
	@RequestMapping(value = "/setThreshold")
	public ModelAndView setThreshold(HttpServletRequest request,HttpServletResponse response){
		HttpSession session=request.getSession();
		String userId=(String)session.getAttribute("userId");
		String warnSwitch=(String)request.getParameter("warnSwitch");
		String threshold=(String)request.getParameter("threshold");
		
		ModelAndView mav=new ModelAndView();
		
		Dormitory dormitory=dormitoryBusiness.saveSwitch(warnSwitch, threshold, userId);
		String viewState=(String)session.getAttribute(Dict.viewState);
		String eventValidation=(String)session.getAttribute(Dict.eventValidation);
		Map<String, Object> resMap=elecBusiness.queryDetail(viewState,eventValidation,
				dormitory.getProgramId(),dormitory.getTxtyq(),dormitory.getTxtld(),dormitory.getTxtroom());

		mav.addObject("dormitory", dormitory);
		//不存在该房间信息
		if(resMap==null){
			String area=elecBusiness.queryAllProgramId();
			mav.addObject("area",area);
			mav.addObject("error", "没有该宿舍的电费信息，请核对后重新设置宿舍");
			mav.setViewName("setDormitory");
		}else{
			mav.addAllObjects(resMap);
			mav.setViewName("elecResult");
		}
		
		return mav;
	}
	
}
