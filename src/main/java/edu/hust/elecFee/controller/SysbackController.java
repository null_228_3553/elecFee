package edu.hust.elecFee.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSONObject;

import edu.hust.elecFee.bean.Course;


@Controller
@RequestMapping("/sys")
public class SysbackController {
	
	@Autowired
	private JdbcTemplate jdbcTpltCore;
	
	@RequestMapping(value = "/xtmsDelete")
	@ResponseBody
	public String xtmsDelete(Integer xtmsId){
		String sql="delete from dcp_wechat.sys_xtms where xtms_id=?";
		jdbcTpltCore.update(sql,new Object[]{
				xtmsId
		});
		return new JSONObject().toString();
	}
	
	@RequestMapping(value = "/xtmsUpdate")
	@ResponseBody
	public String xtmsUpdate(Integer xtmsId,String room,String title,String content){
		String sql="update dcp_wechat.sys_xtms set "
				+ "title=?,content=? where xtms_id=?";
		jdbcTpltCore.update(sql,new Object[]{
				title,content,xtmsId
		});
		return new JSONObject().toString();
	}
	
	@RequestMapping(value = "/xtmsAdd")
	@ResponseBody
	public String xtmsAdd(String room,String title,String content){
		String sql="insert into dcp_wechat.sys_xtms "
				+ "select max(xtms_id)+1,?,?,? from dcp_wechat.sys_xtms";
		jdbcTpltCore.update(sql,new Object[]{
				room,title,content
		});
		return new JSONObject().toString();
	}
	
	@RequestMapping(value = "/yjmsUpdate")
	@ResponseBody
	public String yjmsUpdate(String room,String computer,String useTime,String cpu,String memory,
			String chip,String disk,String monitor){
		String sql="update dcp_wechat.sys_yjms set "
				+ "computer=?,use_time=?,cpu=?,memory=?,chip=?,disk=?,monitor=?"
				+ " where room=?";
		jdbcTpltCore.update(sql,new Object[]{
				computer,useTime,cpu,memory,chip,disk,monitor,room
		});
		return new JSONObject().toString();
	}
	
	@RequestMapping(value = "/jxapDelete")
	@ResponseBody
	public String jxapDelete(String jxapId){
		jdbcTpltCore.update("delete from dcp_wechat.sys_jxap_detail where jxap_id=?",new Object[]{
				jxapId
		});
		jdbcTpltCore.update("delete from dcp_wechat.sys_jxap where jxap_id=?",new Object[]{
				jxapId
		});
		return new JSONObject().toString();
	}
	
	@RequestMapping(value = "/jxapAdd")
	@ResponseBody
	public String jxapAdd(HttpServletRequest request,@RequestParam("file") MultipartFile file) throws IOException{
		List<Course> list=new ArrayList<Course>();

		XWPFDocument xdoc = null;
		try{
			xdoc = new XWPFDocument(file.getInputStream());
			String weekName=xdoc.getParagraphs().get(0).getText();
	        List<XWPFTable> tables = xdoc.getTables();  
	        XWPFTable table=tables.get(0);
	        List<XWPFTableRow> rows=table.getRows();
	        String day = null;//星期几
	        //第一行抬头略过
	        for(int i=1;i<rows.size();i++){
	        	XWPFTableRow row=rows.get(i);
	        	List<XWPFTableCell> cells=row.getTableCells();
	        	
	        	//星期几为空使用上一个
	        	if(!"".equals(cells.get(0).getText())){
	        		day=cells.get(0).getText();
	        	}
	        	
	        	//空行不算
	        	if("".equals(cells.get(1).getText().trim())){
	        		continue;
	        	}
	        	if(!"".equals(getText(cells.get(3)))){
	            	Course course=new Course();
	            	course.setDay(day);
	            	course.setCourseName(getText(cells.get(1)));
	            	course.setClassName(getText(cells.get(2)));
	        		course.setRoomName(getText(cells.get(3)));
	        		course.setRemark(getText(cells.get(6)));
	        		course.setCourseTime("1To4");
	        		list.add(course);
	        	}
	        	if(!"".equals(getText(cells.get(4)))){
	            	Course course=new Course();
	            	course.setDay(day);
	            	course.setCourseName(getText(cells.get(1)));
	            	course.setClassName(getText(cells.get(2)));
	        		course.setRoomName(getText(cells.get(4)));
	        		course.setRemark(getText(cells.get(6)));
	        		course.setCourseTime("5To8");
	        		list.add(course);
	        	}
	        	if(!"".equals(getText(cells.get(5)))){
	            	Course course=new Course();
	            	course.setDay(day);
	            	course.setCourseName(getText(cells.get(1)));
	            	course.setClassName(getText(cells.get(2)));
	        		course.setRoomName(getText(cells.get(5)));
	        		course.setRemark(getText(cells.get(6)));
	        		course.setCourseTime("9To12");
	        		list.add(course);
	        	}
	        }
	        //插入数据库
			String fileName=file.getOriginalFilename();
			fileName=fileName.replaceAll(".docx", "");
			int jxapId=jxapSave(weekName,fileName);
			jxapSaveDetail(list,jxapId);
		}catch(Exception e){
			e.printStackTrace();
			JSONObject jo=new JSONObject();
	        jo.put("retcode", "error");
	        jo.put("errMsg", e.getMessage());
			return jo.toString();
		}finally{
			xdoc.close();
		}
        
        //返回数据
        JSONObject jo=new JSONObject();
        jo.put("retcode", "0000");
		return jo.toString();
	}
	
	private int jxapSave(String weekName, String fileName) {
		int jxapId;
		BigDecimal b=jdbcTpltCore.queryForObject("select max(jxap_id)+1 from dcp_wechat.sys_jxap", BigDecimal.class);
		if(b==null){
			jxapId=1;
		}else{
			jxapId=b.intValue();
		}
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		jdbcTpltCore.update("insert into dcp_wechat.sys_jxap values(?,?,?,?)",new Object[]{
				jxapId,weekName,sdf.format(new Date()),fileName
		});
		return jxapId;
	}

	private void jxapSaveDetail(List<Course> list, int jxapId) {
		String sql="insert into dcp_wechat.sys_jxap_detail "
				+ "select max(jxap_detail_id)+1,?,?,?,?,?,?,? from dcp_wechat.sys_jxap_detail";
		for(Course course:list){
			jdbcTpltCore.update(sql, new Object[]{
				jxapId,course.getDay(),course.getCourseName(),course.getClassName(),
				course.getCourseTime(),course.getRoomName(),course.getRemark()
			});
		}
	}

	private String getText(XWPFTableCell cell){
		String text=cell.getText();
		//去除ascii为12288的奇葩空格
		text=text.replace((char)12288+"", "")
				.replace((char)160+"", "").trim();
		//去除回车符
		if(text.indexOf("\n")!=-1){
			text=text.replace("\n", " ");
		}
		return text;
	}
}
