package edu.hust.elecFee.controller;

import java.io.File;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baidu.ueditor.ActionEnter;


@Controller
@RequestMapping("/ueditor")
public class UeditorController {
	
	@RequestMapping(value = "/ueditor")
	@ResponseBody
	private String ueditor(HttpServletRequest request){
		
		String rootPath=request.getServletContext().getRealPath("/ueditor/");
		rootPath=rootPath+File.separator;
		ActionEnter ae=new ActionEnter(request,rootPath);
		String out=ae.exec();
		
		return out;
	}
	
}
