package edu.hust.elecFee.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;


@Service("batchAop")
public class BatchAop {
	protected Log log=LogFactory.getLog(getClass());
	
	public Object control(ProceedingJoinPoint pjp) throws Throwable{
		Object retObj=null;

		ServletRequestAttributes s=(ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
		HttpServletRequest request=s.getRequest();
		String userId=(String)request.getSession().getAttribute("userId");
		if(userId==null || (!userId.equals("2016210052")&& !userId.equals("2018210079")) ){
			JSONObject jo=new JSONObject();
			jo.put("retCode", "error");
			jo.put("retMsg", "非管理员账号"+userId);
			retObj=jo.toString();
		}else{
			MethodSignature signature =  (MethodSignature) pjp.getSignature(); 
			
			Object[] args=pjp.getArgs();
			
			Class<?> c=signature.getReturnType();
			
			if(c.getName().equals("java.lang.String") 
					|| c.getName().equals("void")){//返回类型String
				JSONObject jo=null;
				try{
					retObj=pjp.proceed(args);
					jo=JSON.parseObject((String)retObj);
					if(jo==null){
						jo=new JSONObject();
					}
					jo.put("retCode", "success");
				}catch(Exception e){
					log.error(e);
					jo=new JSONObject();
					jo.put("retCode", "error");
					jo.put("retMsg", e.getMessage());
				}
				retObj=jo.toString();//异步提交需返回 根据success字段判断交易是否成功
			}else{//返回类型 org.springframework.web.servlet.ModelAndView
				retObj=pjp.proceed(args);
			}
		}
		
		return retObj;
	}
	
}
