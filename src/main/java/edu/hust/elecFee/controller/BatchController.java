package edu.hust.elecFee.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import edu.hust.elecFee.bean.BatchTask;
import edu.hust.elecFee.service.BatchManager;

@Controller
@RequestMapping("/batch")
public class BatchController{

	@Autowired
	private BatchManager batchManager;
	
//	@RequestMapping(value={"/**/*.jpg","/**/*.png","/**/*.gif"})
	/*public void jpg(HttpServletRequest request,HttpServletResponse response) throws IOException{
		String contextPath = request.getContextPath();
		String requestURI = request.getRequestURI();		
		String fileName = requestURI.substring(contextPath.length()+"/batch/".length());
		String filePath = resourcePath+fileName;
		
		byte[] bytes = readByteArrayFromResource(filePath);
		if (bytes == null) {
			response.sendError(404);
			return;
		}
		response.getOutputStream().write(bytes);
	}*/
//	@RequestMapping(value={"/**/*.css","/**/*.js","/*.html"})
	/*public void text(HttpServletRequest request,HttpServletResponse response) throws IOException{
		String contextPath = request.getContextPath();
		String requestURI = request.getRequestURI();		
		String fileName = requestURI.substring(contextPath.length()+"/batch/".length());
		String filePath = resourcePath+fileName;
		
		String text = readFromResource(filePath);
		if(text==null){
			response.sendError(404);
			return;
		}
		if (filePath.endsWith(".html")) {
			response.setContentType("text/html;charset=utf-8");
		}else if (filePath.endsWith(".css")) {
			response.setContentType("text/css;charset=utf-8");
		} else if (filePath.endsWith(".js")) {
			response.setContentType("text/javascript;charset=utf-8");
		}
		response.getWriter().write(text);
	}*/
	
	@RequestMapping("/batchList.do")
	@ResponseBody
	public String batchList(HttpServletRequest request){
		List<BatchTask> list=batchManager.getTaskList();
		JSONObject jo=new JSONObject();
		jo.put("list", JSON.toJSON(list));
		return succJson(jo);
	}
	
	@RequestMapping("/pause.do")
	@ResponseBody
	public String pause(HttpServletRequest request){
		int id=Integer.valueOf(request.getParameter("id"));
		try {
			batchManager.pauseTask(id);
		} catch (Exception e) {
			return errJson(e.getMessage());
		}
		return succJson();
	}
	
	@RequestMapping("/resume.do")
	@ResponseBody
	public String resume(HttpServletRequest request){
		int id=Integer.valueOf(request.getParameter("id"));
		try {
			batchManager.resumeTask(id);
		} catch (Exception e) {
			return errJson(e.getMessage());
		}
		return succJson();
	}
	
	@RequestMapping("/runOnce.do")
	@ResponseBody
	public String runOnce(HttpServletRequest request){
		int id=Integer.valueOf(request.getParameter("id"));
		try {
			batchManager.runOnce(id);
		} catch (Exception e) {
			return errJson(e.getMessage());
		}
		return succJson();
	}
	
	@RequestMapping("/update.do")
	@ResponseBody
	public String update(HttpServletRequest request){
		int id=Integer.valueOf(request.getParameter("id"));
		String cron=request.getParameter("id");
		try {
			batchManager.modifyTask(id, cron);
		} catch (Exception e) {
			return errJson(e.getMessage());
		}
		return succJson();
	}
	
	private String errJson(String msg){
		JSONObject jo=new JSONObject();
		jo.put("retCode", "error");
		jo.put("retMsg", msg);
		return jo.toString();
	}
	private String succJson(){
		JSONObject jo=new JSONObject();
		jo.put("retCode", "success");
		return jo.toString();
	}
	
	private String succJson(JSONObject jo){
		jo.put("retCode", "success");
		return jo.toString();
	}
	
	/*public final static int DEFAULT_BUFFER_SIZE = 1024 * 4;
	private String resourcePath="resource/html/";
	
	private String readFromResource(String resource) throws IOException {
		InputStream in = null;
		try {
			in = Thread.currentThread().getContextClassLoader().getResourceAsStream(resource);
			if (in == null) {
				return null;
			}
			InputStreamReader reader = new InputStreamReader(in, "UTF-8");
			StringWriter writer = new StringWriter();
			char[] buffer = new char[DEFAULT_BUFFER_SIZE];
			int n = 0;
			while (-1 != (n = reader.read(buffer))) {
				writer.write(buffer, 0, n);
			}
			return writer.toString();
		} finally {
			if(in!=null)
				in.close();
		}
	}
	private byte[] readByteArrayFromResource(String resource) throws IOException {
		InputStream in = null;
		try {
			in = Thread.currentThread().getContextClassLoader().getResourceAsStream(resource);
			if (in == null) {
				return null;
			}
			ByteArrayOutputStream output = new ByteArrayOutputStream();
			copy(in, output);
			return output.toByteArray();
		} finally {
			in.close();
		}
	}
	private long copy(InputStream input, OutputStream output) throws IOException {
		final int EOF = -1;
		byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
		long count = 0;
		int n = 0;
		while (EOF != (n = input.read(buffer))) {
			output.write(buffer, 0, n);
			count += n;
		}
		return count;
	}*/
}
