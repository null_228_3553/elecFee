package edu.hust.elecFee.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Connection;
import org.jsoup.Connection.Method;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
public class VoteController {
	protected Log log=LogFactory.getLog(getClass());

	static String url="http://vote.office.hust.edu.cn/system/resource/survey/solesurveyvote.jsp?rd=0";
	
	@RequestMapping(method ={RequestMethod.POST},value = "/vote")
	@ResponseBody
	public String vote(HttpServletRequest request,HttpServletResponse response){
		Connection connection = Jsoup.connect(url);
        connection.header("Cache-Control", "max-age=0");
        connection.header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
        connection.header("Accept-Encoding", "gzip, deflate");
        connection.header("Accept-Language", "zh-CN,zh;q=0.8");
        connection.header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36");
        connection.header("Content-Type", "application/x-www-form-urlencoded");
        connection.header("Referer", "http://vote.office.hust.edu.cn/");
        connection.header("Origin", "http://vote.office.hust.edu.cn/");
        connection.header("Upgrade-Insecure-Requests", "1");
        connection.header("Connection", "keep-alive");
        connection.header("Host", "vote.office.hust.edu.cn");
		try {
			Response res = connection.data("survey_sole_152532", "survey_ok",
			    		"viewid", "152532",
			    		"treeid", "1001",
			    		"owner", "1321060550",
			    		"mode", "10",
			    		"survey_id_152532", "1154",
			    		"imageField.x", "44",
			    		"imageField.y", "11",
			    		"sole:1193", "1445",//校园卡二期
			    		"sole:1193", "1447",//微校园
			    		"sole:1193", "1469"//统一身份认证
			    		)
					.method(Method.POST).execute();
			String body=res.body();
			if(body.indexOf("投票成功")!=-1){
				return "投票成功，感谢您的帮助";
			}
			if(body.indexOf("IP")!=-1){
				return "当前IP已经不能再投票";
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "error";
	}
	
	@RequestMapping(method ={RequestMethod.POST},value = "/vote2")
	@ResponseBody
	public String vote2(HttpServletRequest request,HttpServletResponse response){
		String str1=voteZhxy();
		String str2=voteJcss();
		if(str1.indexOf("投票成功，感谢您的参与")!=-1 && str2.indexOf("投票成功，感谢您的参与")!=-1){
			return "投票成功，感谢您的参与";
		}else if(str1.indexOf("当前IP已经不能再投票")!=-1||str2.indexOf("当前IP已经不能再投票")!=-1){
			return "当前IP已经不能再投票";
		}else{
			return "error";
		}
		
	}
	private String voteZhxy(){//智慧校园
		Connection connection = Jsoup.connect("http://diaocha.www.edu.cn/diaocha/vote.php?topic_id=1112&item_id=36947");
        connection.header("Cache-Control", "max-age=0");
        connection.header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
        connection.header("Accept-Encoding", "gzip, deflate");
        connection.header("Accept-Language", "zh-CN,zh;q=0.8");
        connection.header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36");
        connection.header("Content-Length", "173");
        connection.header("Content-Type", "application/x-www-form-urlencoded");
        connection.header("Referer", "http://www.edu.cn/html/info/2016cxpx/index.shtml?topic_id=1112&from=timeline&isappinstalled=0");
        connection.header("Origin", "http://www.edu.cn/");
        connection.header("Upgrade-Insecure-Requests", "1");
        connection.header("Connection", "keep-alive");
        connection.header("Host", "diaocha.www.edu.cn");
        
        Map<String,String> cookies=new HashMap<String,String>();
        cookies.put("Hm_lvt_b841bd73360946ada23db9fa82c9c6ae", "1483510124");
        cookies.put("Hm_lpvt_b841bd73360946ada23db9fa82c9c6ae", "1483510493");
		try {
			Response res = connection.data(
					"alert", "投票成功，感谢您的参与！",
			    	"back", "http://www.edu.cn/html/info/2016cxpx/index2.shtml?=topic_id=1112#curmao")
					.cookies(cookies)
					.method(Method.POST).execute();
			String body=res.body();
			
			if(body.indexOf("gb2312")!=-1){
				return "投票成功，感谢您的参与";
			}
			if(body.indexOf("javascript")!=-1){
				return "当前IP已经不能再投票";
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "error";
	}
	private String voteJcss(){//基础设施
		Connection connection = Jsoup.connect("http://diaocha.www.edu.cn/diaocha/vote.php?topic_id=1115&item_id=36990");

		connection.header("Host","diaocha.www.edu.cn");
		connection.header("Connection","keep-alive");
		connection.header("Content-Length","173");
		connection.header("Cache-Control","max-age=0");
		connection.header("Accept","text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
		connection.header("Origin","http://www.edu.cn");
		connection.header("Upgrade-Insecure-Requests","1");
		connection.header("User-Agent","Mozilla/5.0 (iPhone; CPU iPhone OS 8_0 like Mac OS X) AppleWebKit/600.1.3 (KHTML, like Gecko) Version/8.0 Mobile/12A4345d Safari/600.1.4");
		connection.header("Content-Type","application/x-www-form-urlencoded");
		connection.header("Referer","http://www.edu.cn/html/info/2016cxpx/index.shtml?topic_id=1112");
		connection.header("Accept-Encoding","gzip, deflate");
		connection.header("Accept-Language","zh-CN,zh;q=0.8");
		connection.header("Cookie","Hm_lvt_b841bd73360946ada23db9fa82c9c6ae=1483510124,1483518781; Hm_lpvt_b841bd73360946ada23db9fa82c9c6ae=1483518781");

        Map<String,String> cookies=new HashMap<String,String>();
        cookies.put("Hm_lvt_b841bd73360946ada23db9fa82c9c6ae", "1483510124,1483518781");
        cookies.put("Hm_lpvt_b841bd73360946ada23db9fa82c9c6ae", "1483518781");
        
        try {
			Response res = connection.data(
					"alert", "投票成功，感谢您的参与！",
			    	"back", "http://www.edu.cn/html/info/2016cxpx/index2.shtml?=topic_id=1112#curmao")
					.cookies(cookies)
					.method(Method.POST).execute();
			String body=res.body();
			
			if(body.indexOf("gb2312")!=-1){
				return "投票成功，感谢您的参与";
			}
			if(body.indexOf("javascript")!=-1){
				return "当前IP已经不能再投票";
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "error";
	}
	
	public static void main(String[] args) throws UnsupportedEncodingException{
		new VoteController().vote2(null, null);
		/*String a="alert=%CD%B6%C6%B1%B3%C9%B9%A6%A3%AC%B8%D0%D0%BB%C4%FA%B5%C4%B2%CE%D3%EB%A3%A1&back=http%3A%2F%2Fwww.edu.cn%2Fhtml%2Finfo%2F2016cxpx%2Findex.shtml%3Ftopic_id%3D1112%23curmao";
		            alert=%CD%B6%C6%B1%B3%C9%B9%A6%A3%AC%B8%D0%D0%BB%C4%FA%B5%C4%B2%CE%D3%EB%A3%A1&back=http%3A%2F%2Fwww.edu.cn%2Fhtml%2Finfo%2F2016cxpx%2Findex.shtml%3Ftopic_id%3D1112%23curmao
		String b=URLDecoder.decode(a,"gbk");
		System.out.println(b);*/
	}
}
