package edu.hust.elecFee.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import edu.hust.elecFee.bean.Book;
import edu.hust.elecFee.service.LibraryService;

@Controller
@RequestMapping("/library")
public class LibraryController {

	@Autowired
	private LibraryService libraryService;
	
	@RequestMapping("/libRecords")
	@ResponseBody
	public String libRecords(HttpSession session){
		String userId=(String)session.getAttribute("userId");
		JSONObject jo=libraryService.libRecords(userId);
		return addHeader(jo).toJSONString();
	}
	
	@RequestMapping("/register")
	@ResponseBody
	public String register(HttpSession session,String password,String rePassword){
		String userId=(String)session.getAttribute("userId");
		List<Book> list=null;
		try{
			list=libraryService.register(userId, password, rePassword);
		}catch(Exception e){
			if(e instanceof RuntimeException){
				RuntimeException e1=(RuntimeException)e;
				JSONObject errJo=new JSONObject();
				errJo.put("retCode", "error");
				errJo.put("errMsg", e1.getMessage());
				return errJo.toJSONString();
			}else{
				JSONObject errJo=new JSONObject();
				errJo.put("retCode", "error");
				errJo.put("errMsg", "系统忙，请稍后再试");
				return errJo.toJSONString();
			}
		}
		return addHeader(list).toJSONString();
	}
	
	@RequestMapping("/hisRecords")
	@ResponseBody
	public String hisRecords(HttpSession session,String password){
		String userId=(String)session.getAttribute("userId");
		List<Book> list=null;
		try{
			list=libraryService.hisRecords(userId, password);
		}catch(Exception e){
			if(e instanceof RuntimeException){
				RuntimeException e1=(RuntimeException)e;
				JSONObject errJo=new JSONObject();
				errJo.put("retCode", "error");
				errJo.put("errMsg", e1.getMessage());
				return errJo.toJSONString();
			}else{
				JSONObject errJo=new JSONObject();
				errJo.put("retCode", "error");
				errJo.put("errMsg", "系统忙，请稍后再试");
				return errJo.toJSONString();
			}
		}
		return addHeader(list).toJSONString();
	}
	
	@RequestMapping(value = "/libraryRemind")
	@ResponseBody
	public String libraryRemind(HttpSession session){
		String userId=(String)session.getAttribute("userId");
		String remind=libraryService.qryLibraryRemind(userId);
		return JSON.toJSONString(remind);
	}
	
	@RequestMapping(value = "/addLibraryRemind")
	@ResponseBody
	public String addLibraryRemind(HttpSession session,String remind){
		String userId=(String)session.getAttribute("userId");
		remind="true".equals(remind)?"1":"0";
		libraryService.addLibraryRemind(userId,remind);
		return null;
	}
	
	private JSONObject addHeader(Object body){
		JSONObject jo=new JSONObject();
		jo.put("retCode", "0000");
		jo.put("body", JSON.toJSON(body));
		return jo;
	}
}
