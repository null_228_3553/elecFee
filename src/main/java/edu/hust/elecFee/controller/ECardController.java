package edu.hust.elecFee.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import edu.hust.elecFee.service.EcardService;


@Controller
@RequestMapping("/ecard")
public class ECardController<SimpleDateFormatter> {
	protected Log log=LogFactory.getLog(getClass());
//	private DecimalFormat df=new DecimalFormat("#.##"); 
	@Autowired
	private EcardService ecardService;
	
	@RequestMapping(method ={RequestMethod.POST},value = "/xyk2017")
	@ResponseBody
	public String xyk2017(HttpSession session){
		String userId=(String)session.getAttribute("userId");
		//校园卡2017数据
		Map<String,Object> retMap=ecardService.qryXyk2017(userId);
		return JSON.toJSONString(retMap);
	}
	
	@RequestMapping(method ={RequestMethod.POST},value = "/desc")
	@ResponseBody
	public String desc(HttpServletRequest request,HttpServletResponse response){
		JSONObject jo=new JSONObject();
		String userId=(String)request.getSession().getAttribute("userId");
		//个人消费
		Map<String,Object> retMap=ecardService.qryTotal(userId);
		//均值
//		Map<String,Double> avgMap=ecardService.qryAvg();
		
		if(retMap!=null){
			//均值
//			jo.addProperty("avg", avgMap.get("avg"));
//			jo.addProperty("top90", avgMap.get("top90"));
			//个人消费
			jo.put("CZZJE", toDouble(retMap.get("CZZJE")));//总充值
			jo.put("CZZXF", toDouble(retMap.get("CZZXF")));//总消费
			jo.put("WF", toDouble(retMap.get("WF")));//网费
			jo.put("XC", toDouble(retMap.get("XC")));//校车
			jo.put("ST", toDouble(retMap.get("ST")));//食堂
			jo.put("CS", toDouble(retMap.get("CS")));//超市
			jo.put("SHJ", toDouble(retMap.get("SHJ")));//售货机
			jo.put("TSFK", toDouble(retMap.get("TSFK")));//图书罚款
			jo.put("JAN", toDouble(retMap.get("JAN")));//1月
			jo.put("FEB", toDouble(retMap.get("FEB")));//2月
			jo.put("MAR", toDouble(retMap.get("MAR")));//3月
			jo.put("APR", toDouble(retMap.get("APR")));//4月
			jo.put("MAY", toDouble(retMap.get("MAY")));//5月
			jo.put("JUN", toDouble(retMap.get("JUN")));//6月
			jo.put("JULY",toDouble(retMap.get("JULY")));//7月
			jo.put("AUG", toDouble(retMap.get("AUG")));//8月
			jo.put("SEP", toDouble(retMap.get("SEP")));//9月
			jo.put("OCT", toDouble(retMap.get("OCT")));//10月
			jo.put("NOV", toDouble(retMap.get("NOV")));//11月
			jo.put("DEC", toDouble(retMap.get("DEC")));//12月
			jo.put("MAX", toDouble(retMap.get("MAX")));//最大一次消费
			jo.put("MAX_ADRESS", (String)retMap.get("MAX_ADRESS"));//最大一次地点
			jo.put("MAX_TIME", toDate(retMap.get("MAX_TIME")));//最大一次时间
			jo.put("HKCS", Integer.valueOf((String)retMap.get("HKCS")));//换卡次数
			jo.put("QC", (String)retMap.get("QC"));//是否开通圈存
		}
		return jo.toString();
	}
	
	@RequestMapping(method ={RequestMethod.POST},value = "/markets")
	@ResponseBody
	public String markets(HttpServletRequest request,HttpServletResponse response){
		String userId=(String)request.getSession().getAttribute("userId");
		
		List<Map<String,Object>> retList=ecardService.qryMarkets(userId);
		return JSON.toJSONString(retList);
	}
	
	@RequestMapping(method ={RequestMethod.POST},value = "/dinings")
	@ResponseBody
	public String dinings(HttpServletRequest request,HttpServletResponse response){
		String userId=(String)request.getSession().getAttribute("userId");
		
		List<Map<String,Object>> retList=ecardService.qryDinings(userId);
		return JSON.toJSONString(retList);
	}
	
	@RequestMapping(method ={RequestMethod.POST},value = "/sport")
	@ResponseBody
	public String sport(HttpServletRequest request,HttpServletResponse response){
		String userId=(String)request.getSession().getAttribute("userId");
		
		Double sportTime=ecardService.qrySport(userId);
		JSONObject jo=new JSONObject();
		jo.put("YDSC", sportTime);
		return jo.toString();
	}

	private String toDate(Object obj){
		SimpleDateFormat sdf1=new SimpleDateFormat("yyyy/MM/dd HH:mm");
		SimpleDateFormat sdf2=new SimpleDateFormat("yyyy年MM月dd日HH时mm分");
		try {
			Date date=sdf1.parse((String)obj);
			return sdf2.format(date);
		} catch (ParseException e) {
			e.printStackTrace();
			return "";
		}
	}
	private Double toDouble(Object obj){
		try{
			Double d=Double.valueOf((String)obj);
			return d;
		}catch(Exception e){
			return 0.0;
		}
	}
}
