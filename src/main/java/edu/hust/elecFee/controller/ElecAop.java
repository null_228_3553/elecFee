package edu.hust.elecFee.controller;

import java.sql.Date;
import java.sql.Timestamp;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import edu.hust.elecFee.bean.Dormitory;
import edu.hust.elecFee.bean.ElecLog;
import edu.hust.elecFee.service.LogService;


@Service("elecAop")
public class ElecAop {
	protected Log log=LogFactory.getLog(getClass());
	
	@Autowired
	private LogService logService;
	
	public Object control(ProceedingJoinPoint pjp) throws Throwable{
		MethodSignature signature =  (MethodSignature) pjp.getSignature(); 
		Class<?> c=signature.getReturnType();
		Object[] args=pjp.getArgs();

		//返回类型ModelAndView才记日志
		if(c.getName().equals("org.springframework.web.servlet.ModelAndView")){
			ElecLog elecLog=new ElecLog();
			ModelAndView mav=null;
			HttpServletRequest request = null;
			for(int i=0;i<args.length;i++){
				if(args[i] instanceof HttpServletRequest)
					request=(HttpServletRequest) args[i];
			}
			elecLog.setUserId((String)request.getSession().getAttribute("userId"));
			
			String transCode="startAction.do";
			if(request.getRequestURI().lastIndexOf(".")!=-1)
				transCode=request.getRequestURI().substring(request.getContextPath().length() + 1, request.getRequestURI().lastIndexOf("."));
			log.info("begin trans:"+transCode);
	 		elecLog.setTrans(transCode);
			log.info("paramter:"+request.getParameterMap());

			Timestamp startTime=new Timestamp(System.currentTimeMillis());
			log.info("ip:"+request.getRemoteAddr()+"  "+startTime);
			elecLog.setIp(request.getRemoteAddr());
			elecLog.setQryDate(new Date(startTime.getTime()));
			try{
				mav=(ModelAndView) pjp.proceed(args);
				if(mav.getModel().get("remainElec")!=null){
					String remainElec=(String) mav.getModel().get("remainElec");
					elecLog.setRetMsg(remainElec);
				}
				if(mav.getModel().get("dormitory")!=null){
					Dormitory dormitory=(Dormitory)mav.getModel().get("dormitory");
					String dormStr=dormitory.getProgramId()+"|"+dormitory.getTxtyq()+"|"
							+dormitory.getTxtld()+"|"+dormitory.getTxtroom();
					elecLog.setDormStr(dormStr);
				}
				if(mav.getModel().get("error")!=null){
					String errMsg=(String) mav.getModel().get("error");
					elecLog.setSuccess(false);
					elecLog.setErrMsg(errMsg);
				}else{
					elecLog.setSuccess(true);
				}
			}catch(Exception e){
				log.error(e);
				elecLog.setSuccess(false);
				elecLog.setErrMsg(e.toString());
			}finally{
				log.info("返回"+mav);
				log.info("请求耗时"+(System.currentTimeMillis()-startTime.getTime())+"毫秒");
				elecLog.setProcessTime(System.currentTimeMillis()-startTime.getTime());
				logService.saveLog(elecLog);
			}
			return mav;
		}else{
			return pjp.proceed(args);
		}
	}
}
