package edu.hust.elecFee.controller;

import java.sql.Date;
import java.sql.Timestamp;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import edu.hust.elecFee.bean.ElecLog;
import edu.hust.elecFee.common.SysException;
import edu.hust.elecFee.service.LogService;


@Service("sysAop")
public class SysAop {
	protected Log log=LogFactory.getLog(getClass());
	
	@Autowired
	private LogService logService;

	public Object control(ProceedingJoinPoint pjp) throws Throwable{
		ElecLog elecLog=new ElecLog();
		MethodSignature signature =  (MethodSignature) pjp.getSignature(); 
		Object retObj=null;
		
		Object[] args=pjp.getArgs();
		HttpServletRequest request = null;
		for(int i=0;i<args.length;i++){
			if(args[i] instanceof HttpServletRequest)
				request=(HttpServletRequest) args[i];
		}
		elecLog.setUserId((String)request.getSession().getAttribute("userId"));

		String transCode=request.getRequestURI().substring(request.getContextPath().length() + 1, request.getRequestURI().lastIndexOf("."));
 		log.info("begin trans:"+transCode);
 		elecLog.setTrans(transCode);
 		log.info("paramter:"+request.getParameterMap());

		Timestamp startTime=new Timestamp(System.currentTimeMillis());
		log.info("ip:"+request.getRemoteAddr()+"  "+startTime);
		elecLog.setIp(request.getRemoteAddr());
		elecLog.setQryDate(new Date(startTime.getTime()));
		
		Class<?> c=signature.getReturnType();
		try{
			if(c.getName().equals("java.lang.String")){//返回类型String
				JSONObject jo=null;
				try{
					retObj=pjp.proceed(args);
					jo=JSON.parseObject((String)retObj);
					jo.put("success", true);
					elecLog.setSuccess(true);
					if(jo.get(LogService.retMsg)!=null)
						elecLog.setRetMsg((String)jo.get("retMsg"));
				}catch(Exception e){
					log.error(e);
					log.error(e.getStackTrace());
					jo=new JSONObject();
					jo.put("success", false);
					if(e instanceof SysException)
						jo.put("errMsg", e.getMessage());
					else
						jo.put("errMsg", "网络忙，请稍后再试");
					elecLog.setSuccess(false);
					elecLog.setErrMsg((String)jo.get("errMsg"));
				}
				retObj=jo.toString();//异步提交需返回 根据success字段判断交易是否成功
			}else{//返回类型 org.springframework.web.servlet.ModelAndView
				ModelAndView mav=null;
				try{
					mav=(ModelAndView) pjp.proceed(args);
					elecLog.setSuccess(true);
					if(mav.getModel().get(LogService.retMsg)!=null)
						elecLog.setRetMsg((String)mav.getModel().get(LogService.retMsg));
					retObj=mav;
				}catch(Exception e){
					log.error(e);
					elecLog.setSuccess(false);
					if(e instanceof SysException)
						elecLog.setErrMsg(e.getMessage());
					else
						elecLog.setErrMsg(e.toString());
					log.error(e);
					throw e;//页面跳转交易出错直接抛出  页面显示“网络忙，请稍后再试” 查看sync.js
				}
			}
		}finally{
			log.info("返回"+retObj);
			log.info("请求耗时"+(System.currentTimeMillis()-startTime.getTime())+"毫秒");
			elecLog.setProcessTime(System.currentTimeMillis()-startTime.getTime());
//			System.out.println(elecLog.getRetMsg()+"   "+elecLog.getSuccess()+"   "+elecLog.getErrMsg());
			logService.saveLog(elecLog);
		}
		
		return retObj;
	}
}
