package edu.hust.elecFee.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import edu.hust.elecFee.business.ElecBusiness;
import edu.hust.elecFee.common.Dict;


@Controller
public class ElecController {
	@Autowired
	private ElecBusiness elecBusiness;
	
	//查询楼号
	@RequestMapping(method =RequestMethod.POST, value = "/queryTxtyq")
	@ResponseBody
	public String queryTxtyq(HttpServletRequest request,HttpServletResponse response){
		HttpSession session=request.getSession();
		String eventTarget="programId";
		String viewState=(String)session.getAttribute(Dict.viewState);
		String eventValidation=(String)session.getAttribute(Dict.eventValidation);
		String programId=request.getParameter(Dict.programId);
		String output=elecBusiness.queryTxtyq(eventTarget, viewState, eventValidation, programId);
	
		//系统参数塞入session
		session.setAttribute(Dict.viewState, elecBusiness.getNewViewState());
		session.setAttribute(Dict.eventValidation, elecBusiness.getNewEventValidation());
		
		return output;
	}
	
	//查询楼层号
	@RequestMapping(method =RequestMethod.POST, value = "/queryTxtld")
	@ResponseBody
	public String queryTxtld(HttpServletRequest request,HttpServletResponse response){
		HttpSession session=request.getSession();
		String eventTarget="txtyq";
		String viewState=(String)session.getAttribute(Dict.viewState);
		String eventValidation=(String)session.getAttribute(Dict.eventValidation);
		String programId=request.getParameter(Dict.programId);
		String txtyq=request.getParameter(Dict.txtyq);
		String output=elecBusiness.queryTxtld(eventTarget,viewState,eventValidation,programId,txtyq);

		//系统参数塞入session
		session.setAttribute(Dict.viewState, elecBusiness.getNewViewState());
		session.setAttribute(Dict.eventValidation, elecBusiness.getNewEventValidation());
		
		return output;
	}
	
	//查询楼层号
	@RequestMapping(method ={RequestMethod.POST,RequestMethod.GET},value = "/queryDetail")
	@ResponseBody
	public ModelAndView queryDetail(HttpServletRequest request,HttpServletResponse response){
		ModelAndView mav=new ModelAndView();

		HttpSession session=request.getSession();
		String viewState=(String)session.getAttribute(Dict.viewState);
		String eventValidation=(String)session.getAttribute(Dict.eventValidation);
		String programId=request.getParameter(Dict.programId);
		String txtyq=request.getParameter(Dict.txtyq);
		String txtld=request.getParameter(Dict.txtld);
		String txtroom=request.getParameter(Dict.txtroom);
		
		Map<String,Object> resMap=elecBusiness.queryDetail(viewState, eventValidation, programId, txtyq, txtld, txtroom);
		//系统参数塞入session
		session.setAttribute(Dict.viewState, elecBusiness.getNewViewState());
		session.setAttribute(Dict.eventValidation, elecBusiness.getNewEventValidation());

		//不存在该房间信息
		if(resMap==null){
			mav.addObject("error", "没有该宿舍的电费信息，请核对后重新设置宿舍");
			mav.setViewName("setDormitory");
		}else{
			mav.addAllObjects(resMap);
			mav.setViewName("elecResult");
		}
		return mav;
	}
}
