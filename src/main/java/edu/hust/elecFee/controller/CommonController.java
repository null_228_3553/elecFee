package edu.hust.elecFee.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;

import edu.hust.elecFee.common.Util;
import edu.hust.elecFee.service.RouterNative;
import edu.hust.elecFee.service.SysService;


@Controller
public class CommonController {
	protected Log log=LogFactory.getLog(getClass());
	@Autowired
	private JdbcTemplate jdbcTpltCore;
	@Autowired
	private RouterNative routerNative;
	@Autowired
	private SysService sysService;
	
	@RequestMapping(value = "/getName")
	@ResponseBody
	public String getName(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException{
		String userId=(String)request.getSession().getAttribute("userId");
		String name=sysService.qryName(userId);
		JSONObject jo=new JSONObject();
		jo.put("name",name);
		return jo.toJSONString();
	}
	
	@RequestMapping(method ={RequestMethod.GET},value = "/telecast")
	public String telecast(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException{
		String userId=(String)request.getSession().getAttribute("userId");
		String userName=URLEncoder.encode(sysService.qryName(userId),"UTF-8");
		Response res=Jsoup.connect("http://hust.gensee.com/training/site/s/66056911?nickname="+userName).execute();
		return res.body();
//		request.getRequestDispatcher("http://hust.gensee.com/training/site/s/66056911?nickname="+userName).forward(request, response);
//		response.sendRedirect("http://hust.gensee.com/training/site/s/66056911?nickname="+userName);
	}
	
	@RequestMapping(method ={RequestMethod.POST},value = "/addCount")
	@ResponseBody
	public String addCount(HttpServletRequest request,HttpServletResponse response){
		String videoName=request.getParameter("videoName");
		String callbackFunName = request.getParameter("callbackFunName");
		//阅读量+1
		String sql="update videoCount set viewCount=viewCount+1 where videoName=?";
		jdbcTpltCore.update(sql, new Object[]{videoName});
		//查询阅读量
		sql="select viewCount from videoCount where videoName=?";
		int viewCount=jdbcTpltCore.queryForObject(sql, new Object[]{videoName},java.lang.Integer.class);
		return callbackFunName+"("+viewCount+")";
//		return String.valueOf(viewCount);
	}
	
	@RequestMapping(value = "/sign")
	@ResponseBody
	public String sign(HttpServletRequest request,HttpServletResponse response){
		long now = System.currentTimeMillis()/1000;//接口要求秒级
		String nonceStr=UUID.randomUUID().toString();
		String url=request.getParameter("url");
		
		JSONObject signJo = new JSONObject();
		signJo.put("appId", RouterNative.corpId);
		signJo.put("nonceStr", nonceStr);
		signJo.put("timestamp", now);

		//获取jsapi_ticket
		String jsapiTicket = routerNative.getJsapiTicket();
		try {
			// 注意这里参数名必须全部小写，且必须有序
			String str = "jsapi_ticket=" + jsapiTicket 
					+ "&noncestr=" + nonceStr
					+ "&timestamp=" + now 
					+ "&url=" + url;
			MessageDigest crypt = MessageDigest.getInstance("SHA-1");
			crypt.update(str.getBytes("UTF-8"));
			String signature = Util.byteToHex(crypt.digest());
			signJo.put("signature", signature);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return signJo.toString();
	}
}
