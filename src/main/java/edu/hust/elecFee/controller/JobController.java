package edu.hust.elecFee.controller;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;

import edu.hust.elecFee.service.JobService;


@Controller
@RequestMapping("/job")
public class JobController {
	protected Log log=LogFactory.getLog(getClass());
	@Autowired
	private JobService jobService;
	
	@RequestMapping(value = "/jobList")
	@ResponseBody
	public String jobList(HttpServletRequest request,HttpServletResponse response){
		int cType=0;
		if(request.getParameter("cType")!=null){
			cType=Integer.valueOf((String)request.getParameter("cType"));
		}
		String time=(String)request.getParameter("time");
		if(null==time||"".equals(time)||"选择时间".equals(time))
			time="今天起所有";
		String startDate=(String)request.getParameter("startDate");
		String endDate=(String)request.getParameter("endDate");
		String keyWord=(String)request.getParameter("keyWord");
		int page=0;
		if(request.getParameter("page")!=null){
			page=Integer.valueOf((String)request.getParameter("page"));
		}
		
		List<Map<String, Object>> list = null;
		try {
			list = jobService.qryList(cType,time,startDate,endDate,keyWord, page);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return JSON.toJSONString(list);
	}

	@RequestMapping(value = "/jobDetail")
	@ResponseBody
	public String jobDetail(HttpServletRequest request,HttpServletResponse response){
		String id=(String)request.getParameter("id");
		Map<String,Object> detMap=jobService.qryDetail(id);
		
		return JSON.toJSONString(detMap);
	}
	
	@RequestMapping(value = "/jobRemind")
	@ResponseBody
	public String jobRemind(HttpServletRequest request,HttpServletResponse response){
		String userId=(String)request.getSession().getAttribute("userId");
		
		Map<String,Object> detMap=jobService.qryJobRemind(userId);
		
		return JSON.toJSONString(detMap);
	}
	
	@RequestMapping(value = "/addJobRemind")
	@ResponseBody
	public String addJobRemind(HttpServletRequest request,HttpServletResponse response){
		String userId=(String)request.getSession().getAttribute("userId");
		String mainSub=(String)request.getParameter("mainSub");
		String mediSub=(String)request.getParameter("mediSub");
		
		mainSub="true".equals(mainSub)?"1":"0";
		mediSub="true".equals(mediSub)?"1":"0";
		
		jobService.addJobRemind(userId,mainSub,mediSub);
		
		return null;
	}
}
