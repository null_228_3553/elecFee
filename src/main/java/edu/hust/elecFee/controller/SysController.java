package edu.hust.elecFee.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;

import edu.hust.elecFee.bean.Student;
import edu.hust.elecFee.common.SysException;
import edu.hust.elecFee.service.LogService;
import edu.hust.elecFee.service.Router;
import edu.hust.elecFee.service.SblylService;
import edu.hust.elecFee.service.SysService;


@Controller
@RequestMapping("/sys")
public class SysController {
	protected Log log=LogFactory.getLog(getClass());
	@Autowired
	private SysService sysService;
	@Autowired
	private SblylService sblylService;
	@Autowired
	private Router router;
	@Autowired
	private JdbcTemplate jdbcTpltCore;
	
	@RequestMapping(method ={RequestMethod.POST,RequestMethod.GET},value = "/sysMain")
	public ModelAndView sysMain(HttpServletRequest request,HttpServletResponse response){
		ModelAndView mav=new ModelAndView("sys/sysMain");
		HttpSession session=request.getSession();
		setSession(session);
		
		mav.addObject("name", session.getAttribute("name"));
		mav.addObject("jxStu", session.getAttribute("jxStu"));
		mav.addObject("zfStu", session.getAttribute("zfStu"));
		mav.addObject(LogService.retMsg,session.getAttribute("name"));
		return mav;
	}
	
	@RequestMapping(method ={RequestMethod.POST},value = "/qryJxPwd")
	@ResponseBody
	public String qryJxPwd(HttpServletRequest request,HttpServletResponse response){
		JSONObject jo=new JSONObject();
		HttpSession session=request.getSession();
		setSession(session);

		Student jxStu=(Student) session.getAttribute("jxStu");
		jo.put("passwd", jxStu.getPasswd());
		jo.put(LogService.retMsg, jxStu.getPasswd());
		
		return jo.toString();
	}
	
	@RequestMapping(method ={RequestMethod.POST},value = "/qryZfPwd")
	@ResponseBody
	public String qryZfPwd(HttpServletRequest request,HttpServletResponse response){
		JSONObject jo=new JSONObject();
		
		HttpSession session=request.getSession();
		String userId=(String) session.getAttribute("userId");
		String passwd=request.getParameter("passwd");
		boolean result=router.chekPasswd(userId,passwd);
		
		if(result==false){
			log.error("密码错误");
			throw new SysException("密码错误");
		}else{
			setSession(session);
			Student zfStu=(Student) session.getAttribute("zfStu");
			if(zfStu==null){
				log.error("系统中没有您的自费账号的密码<br/>请联系管理员");
				throw new SysException("系统中没有您的自费账号的密码<br/>请联系管理员");
			}else{
				jo.put("passwd", zfStu.getPasswd());
				jo.put(LogService.retMsg, zfStu.getPasswd());
			}
		}
		return jo.toString();
	}
	
	@RequestMapping(method ={RequestMethod.POST},value = "/qryZfFee")
	@ResponseBody
	public String qryZfFee(HttpServletRequest request,HttpServletResponse response){
		JSONObject jo=new JSONObject();
		HttpSession session=request.getSession();
		setSession(session);

		Student zfStu=(Student) session.getAttribute("zfStu");
		if(zfStu==null){
			log.error("系统中没有您的自费账号的密码<br/>请联系管理员");
			throw new SysException("系统中没有您的自费账号的密码<br/>请联系管理员");
		}else{
			jo.put("leftFee", zfStu.getLeftfee().toString());
			jo.put(LogService.retMsg, zfStu.getLeftfee().toString());
		}
		return jo.toString();
	}
	@RequestMapping(method ={RequestMethod.POST,RequestMethod.GET},value = "/sblyl")
	public ModelAndView sblyl(HttpServletRequest request,HttpServletResponse response){
		ModelAndView mav=new ModelAndView("sys/sblyl");
		/*Document doc=router.httpGetGB2312("http://m.ncs.hust.edu.cn/search/openlab_status.php");
		Element zDiv=doc.getElementsByClass("barss").get(0);
		Element dDiv=doc.getElementsByClass("barss").get(1);
		dDiv.html(dDiv.html().replace("201", "201/202"));
		mav.addObject("zDiv", zDiv.html());
		mav.addObject("dDiv", dDiv.html());*/
		
		Map<String, Map<String, Object>> map=sblylService.labOccupiedRate();
		mav.addAllObjects(map);
		return mav;
	}

	@RequestMapping(method ={RequestMethod.POST,RequestMethod.GET},value = "/jxap")
	public ModelAndView jxap(HttpServletRequest request,HttpServletResponse response){
		ModelAndView mav=new ModelAndView("sys/jxap");
		/*List<Map<String,String>> retList=jxapService.jxap();
		mav.addObject("list", retList);*/
		
		List<Map<String,Object>> retList2=jdbcTpltCore.queryForList("select * from("
				+ "select a.*,rownum as rn from dcp_wechat.sys_jxap a order by jxap_id desc"
				+ ")where rn<=6");
		mav.addObject("list2", retList2);
		return mav;
	}
	
	@RequestMapping(method ={RequestMethod.POST,RequestMethod.GET},value = "/jxapDetail")
	@ResponseBody
	public String jxapDetail(HttpServletRequest request,HttpServletResponse response){
		/*String dirPath=request.getServletContext().getRealPath("/sys/")+"/";
		String href = request.getParameter("href");
		
		JSONObject jo=sysService.jxapDetail(href,dirPath);
		return jo.toString();*/
		String jxapId = request.getParameter("href");
		List<Map<String,Object>> retList=jdbcTpltCore.queryForList(
			"select * from dcp_wechat.sys_jxap a,dcp_wechat.sys_jxap_detail b where a.jxap_id=b.jxap_id and a.jxap_id=?"
			,new Object[]{jxapId});
		
		JSONObject jo=new JSONObject();
		jo.put("title", retList.get(0).get("WEEK_NAME"));
		jo.put("list", retList);
		return jo.toString();
	}
	
	@RequestMapping(method ={RequestMethod.POST,RequestMethod.GET},value = "/yjms")
	@ResponseBody
	public String yjms(HttpServletRequest request){
		JSONObject jo=new JSONObject();
		List<Map<String, Object>> list=jdbcTpltCore.queryForList("select * from dcp_wechat.sys_yjms");
		for(Map<String, Object> map:list){
			if(map.get("ROOM").equals("z208")){
				jo.put("z208", map);
			}
			if(map.get("ROOM").equals("d201")){
				jo.put("d201", map);
			}
			if(map.get("ROOM").equals("d202")){
				jo.put("d202", map);
			}
			if(map.get("ROOM").equals("d206")){
				jo.put("d206", map);
			}
			if(map.get("ROOM").equals("d301")){
				jo.put("d301", map);
			}
		}
		return jo.toString();
	}
	
	@RequestMapping(method ={RequestMethod.POST,RequestMethod.GET},value = "/xtms")
	@ResponseBody
	public String xtms(HttpServletRequest request){
		JSONObject jo=new JSONObject();
		List<Map<String, Object>> list=jdbcTpltCore.queryForList("select * from dcp_wechat.sys_xtms "
				+ "order by xtms_id");
		List<Map<String, Object>> z208List=new ArrayList<>();
		List<Map<String, Object>> d201List=new ArrayList<>();
		List<Map<String, Object>> d202List=new ArrayList<>();
		List<Map<String, Object>> d206List=new ArrayList<>();
		List<Map<String, Object>> d301List=new ArrayList<>();
		for(Map<String, Object> map:list){
			if(map.get("ROOM").equals("z208")){
				z208List.add(map);
			}
			if(map.get("ROOM").equals("d201")){
				d201List.add(map);
			}
			if(map.get("ROOM").equals("d202")){
				d202List.add(map);
			}
			if(map.get("ROOM").equals("d206")){
				d206List.add(map);
			}
			if(map.get("ROOM").equals("d301")){
				d301List.add(map);
			}
		}
		jo.put("z208", z208List);
		jo.put("d201", d201List);
		jo.put("d202", d202List);
		jo.put("d206", d206List);
		jo.put("d301", d301List);
		return jo.toString();
	}
	
	@RequestMapping(method ={RequestMethod.POST,RequestMethod.GET},value = "/sjxz")
	public ModelAndView sjxz(HttpServletRequest request,HttpServletResponse response){
		return new ModelAndView("sys/sjxz");
	}
	
	private void setSession(HttpSession session){
		Long sTime=(Long) session.getAttribute("sTime");
		//5分钟查一次
		if(sTime==null || (System.currentTimeMillis()-sTime)>5*60*1000){
			setSessionAttr(session);
		//时间范围内session过期
		}else if(session.getAttribute("name")==null){
			setSessionAttr(session);
		}
	}
	private void setSessionAttr(HttpSession session){
		String userId=(String) session.getAttribute("userId");
		//账号查一次放session里
		List<Student> stuList=sysService.queryAccount(userId);
		for(Student stu:stuList){
			if(stu.getStdno().substring(0,1).equals("1"))
				session.setAttribute("jxStu", stu);
			if(stu.getStdno().substring(0,1).equals("2"))
				session.setAttribute("zfStu", stu);
		}
		if(session.getAttribute("name")==null){//发接口获取姓名
			String name=sysService.qryName(userId);
			session.setAttribute("name", name);
		}
		session.setAttribute("sTime", System.currentTimeMillis());
	}
}
