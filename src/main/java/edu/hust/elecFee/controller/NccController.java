package edu.hust.elecFee.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.neusoft.util.CasFilterConfigManager;

import edu.hust.elecFee.bean.NccCount;
import edu.hust.elecFee.bean.Qr;
import edu.hust.elecFee.common.Util;
import edu.hust.elecFee.service.NccService;
import edu.hust.elecFee.service.RouterNative;


@Controller
@RequestMapping("/ncc")
public class NccController {
	protected Log log=LogFactory.getLog(getClass());
	
	private List<Map<String,Object>> buildingList;
	private long lastBuildingTime=0l;
	@Autowired
	private RouterNative routerNative;
	@Autowired
	private NccService nccService;
	
	@RequestMapping(value = "/dutyTable")
	public ModelAndView dutyTable(HttpServletRequest request,HttpServletResponse response){
		
		List<List<Map<String,Object>>> resList=nccService.dutyList();
		
		ModelAndView mav=new ModelAndView("ncc/dutyTable");
		mav.addObject("resList", resList);
		return mav;
	}
	
	@RequestMapping(value = "/netManager")
	@ResponseBody
	private String netManager(){
		long now=System.currentTimeMillis();
		//2小时刷新
		if(buildingList==null || (now-lastBuildingTime)>1000*3600*2 ){
			buildingList=new ArrayList<>();
			lastBuildingTime=now;
			buildingList=nccService.buildingList();
		}
		//当进行toJSONString的时候，默认如果重用对象的话，会使用引用的方式进行引用对象
		//fastjson把对象转化成json避免$ref ,
		String out=JSON.toJSONString(buildingList,SerializerFeature.DisableCircularReferenceDetect);
		return out;
	}
	
	@RequestMapping(value = "/wxCount")
	@ResponseBody
	private String wxCount(String date){
		Map<String, Object> wxMap=nccService.qryWxCount(date);
		return JSONObject.toJSONString(wxMap);
	}
	private SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
	
	@RequestMapping(value = "/insertNccCount")
	@ResponseBody
	private String insertNccCount(NccCount nccCount){
		String dateStr=nccCount.getCount_date();
		if(dateStr==null || "".equals(dateStr)){
			dateStr=sdf.format(Util.nextMonday(new Date()));
			nccCount.setCount_date(dateStr);
		}
		NccCount nccCountDb=nccService.selectNccCount(dateStr);
		if(nccCountDb==null){
			nccService.insertNccCount(nccCount);
		}else{
			nccService.updateNccCount(nccCount);
		}
		return null;
	}
	@RequestMapping(value = "/nccCount")
	@ResponseBody
	private String nccCount(String date){
		if(date==null || "".equals(date)){
			date=sdf.format(Util.nextMonday(new Date()));
		}
		NccCount nccCount=nccService.selectNccCount(date);
		return JSONObject.toJSONString(nccCount);
	}
	
	@RequestMapping(value = "/qrLogin")
	private ModelAndView qrLogin(HttpSession session,String token){
		String url="https://pass.hust.edu.cn/cas/getUserByToken?"
				+ "token="+token;
		//http get请求
		String userId=routerNative.sendGet(url);
		session.setAttribute("userId", userId);
		
		return new ModelAndView("WEB-INF/jsp/index");
	}
	
	@RequestMapping(value = "/uuid")
	@ResponseBody
	private String uuid(String callback){
		String uuid=UUID.randomUUID().toString().replaceAll("-", "");
		Date now=new Date();
		JSONObject jo=new JSONObject();
		jo.put("uuid", uuid);
		Qr.qrMap.put(uuid, new Qr(uuid,now,Qr.unuse));
		
		String ret=jo.toJSONString();
		//提供cas项目的jsonp跨域请求
		return retCallback(callback, ret);
	}
	
	@RequestMapping(value = "/checkQRCodeScan")
	@ResponseBody
	private String checkQRCodeScan(String uuid,String callback){
		JSONObject jo=new JSONObject();
		Qr qr=Qr.qrMap.get(uuid);
		if(qr==null){
			jo.put("retCode", "error");
			jo.put("retMsg", "二维码无效");
		}else if(qr.getState().equals("used")){
			jo.put("retCode", "error");
			jo.put("retMsg", "二维码已使用");
		//被扫描后置成已使用状态  前端获取该状态后刷新二维码
		}else if(qr.getState().equals(Qr.scaned)){
			jo.put("retCode", Qr.used);
		}
		
		String ret=jo.toJSONString();
		//提供cas项目的jsonp跨域请求
		return retCallback(callback, ret);
	}
	
	@RequestMapping(value = "/qryCert")
	@ResponseBody
	private String qryCert(HttpServletRequest request,
			String appId,String callback){
		
		if(appId==null||"".equals(appId)){
			JSONObject errJo=new JSONObject();
			errJo.put("errCode", "error");
			errJo.put("errMsg", "appId不存在");
			return retCallback(callback, errJo.toJSONString());
		}
		
		Map<String,Object> map=nccService.qryCert(request.getServletContext().getRealPath("/"),
				appId);
		if(map==null){
			JSONObject errJo=new JSONObject();
			errJo.put("errCode", "error");
			errJo.put("errMsg", "appId不存在");
			return retCallback(callback, errJo.toJSONString());
		}
		
		String ret=JSON.toJSONString(map);
		//提供cas项目的jsonp跨域请求
		return retCallback(callback, ret);
	}
	
	@RequestMapping(value = "/qryMobileCert")
	@ResponseBody
	private String qryMobileCert(HttpServletRequest request,String appId,String uuid,String callback){
		//访问二维码的来源页
		String origin=request.getHeader("origin");
		String serverName=CasFilterConfigManager.getItemValue("serverName");

		Qr qr=Qr.qrMap.get(uuid);
		
		JSONObject errJo=new JSONObject();
		if(StringUtils.isEmpty(appId)){
			errJo.put("errCode", "error");
			errJo.put("errMsg", "appId不存在");
			return retCallback(callback, errJo.toJSONString());
		}
		if(!serverName.equals(origin)){
			errJo.put("errCode", "error");
			errJo.put("errMsg", "必须在智慧华中大的页面访问该二维码");
//			return retCallback(callback, errJo.toJSONString());
		}
		if(qr==null){
			errJo.put("errCode", "error");
			errJo.put("errMsg", "二维码无效");
			return retCallback(callback, errJo.toJSONString());
		}
		if(!qr.getState().equals(Qr.unuse)){
			errJo.put("errCode", "error");
			errJo.put("errMsg", "二维码已过期");
			return retCallback(callback, errJo.toJSONString());
		}
		
		Map<String,Object> map=nccService.qryCert(request.getServletContext().getRealPath("/"),
				appId);
		//访问后将二维码设置成已扫描状态，
		qr.setState(Qr.scaned);
		if(map==null){
			errJo.put("errCode", "error");
			errJo.put("errMsg", "appId无效");
			return retCallback(callback, errJo.toJSONString());
		}
		
		String ret=JSON.toJSONString(map);
		//提供cas项目的jsonp跨域请求
		return retCallback(callback, ret);
	}
	
	//提供cas项目的jsonp跨域请求
	public String retCallback(String callback,String ret){
		if(callback!=null){
			return callback+"("+ret+")";
		}else{
			return ret;
		}
	}
}
