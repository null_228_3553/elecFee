package edu.hust.elecFee.service;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;

import edu.hust.elecFee.bean.Student;
import edu.hust.elecFee.common.Util;


@Service("sysService")
public class SysService{
	protected Log log=LogFactory.getLog(getClass());
	@Autowired
	private JdbcTemplate jdbcTpltSys;
	@Autowired
	private Router router;
	Map<String,JSONObject> jxapMap=new HashMap<String,JSONObject>();
	Map<String,String> sblylMap=new HashMap<String,String>();
	
	public List<Student> queryAccount(String userId) {
		long startTime=System.currentTimeMillis();
		List<Student> stuList=new ArrayList<Student>();
		String sql="select stdno,carno,name,passwd,leftfee from student where carno=?";
		List<Map<String,Object>> resList=jdbcTpltSys.queryForList(sql, new Object[]{userId});
		for(Map<String,Object> resMap:resList){
			Student stu=new Student();
			stu=(Student) Util.map2Bean(resMap, Student.class);
			stuList.add(stu);
		}
		long period=System.currentTimeMillis()-startTime;
		log.info("queryAccount查询耗时："+period);
		return stuList;
	}
	
	public Student queryZfAccount(String userId) {
		String sql="select stdno,carno,name,passwd,leftfee from student where carno=? "
				+ "and left(stdno ,1)='2'";
		try{
			long startTime=System.currentTimeMillis();
			Map<String,Object> resMap=jdbcTpltSys.queryForMap(sql, new Object[]{userId});
			Student stu=(Student) Util.map2Bean(resMap, Student.class);
			long period=System.currentTimeMillis()-startTime;
			log.info("queryZfAccount查询耗时："+period);
			return stu;
		}catch(EmptyResultDataAccessException e){
			log.info(e);
			return null;
		}
	}
	
	public String qryName(String userId) {
		long startTime=System.currentTimeMillis();
		String name="";
		try{ 
			JSONObject jObj=router.qryUser(userId);
			name=(String)jObj.get("USER_NAME");
		}catch(Exception e){
			name="未知";
		}
		long period=System.currentTimeMillis()-startTime;
		log.info("接口查询耗时："+period);
		return name;
	}
	private List<Map<String,String>> getTdText(Elements tds,String day,int startIndex){
		List<Map<String,String>> list=new ArrayList<Map<String,String>>();
		//有一个ascii码为12288的中文空格
		String roomName=tds.get(startIndex+2).text().trim().replace((char)12288+"", "")
				.replace((char)160+"", "");
		if(!roomName.equals("")){
			Map<String,String> map=new HashMap<String,String>();
			map.put("courseTime", "1To4");
			map.put("day", day);
			map.put("roomName", roomName);
			map.put("courseName", tds.get(startIndex).text());
			map.put("className", tds.get(startIndex+1).text());
			map.put("remark", tds.get(startIndex+5).text());
			list.add(map);
		}
		roomName=tds.get(startIndex+3).text().trim().replace((char)12288+"", "")
				.replace((char)160+"", "");
		if(!roomName.equals("")){
			Map<String,String> map=new HashMap<String,String>();
			map.put("courseTime", "5To8");
			map.put("day", day);
			map.put("roomName", roomName);
			map.put("courseName", tds.get(startIndex).text());
			map.put("className", tds.get(startIndex+1).text());
			map.put("remark", tds.get(startIndex+5).text());
			list.add(map);
		}
		roomName=tds.get(startIndex+4).text().trim().replace((char)12288+"", "")
				.replace((char)160+"", "");
		if(!roomName.equals("")){
			Map<String,String> map=new HashMap<String,String>();
			map.put("courseTime", "9To12");
			map.put("day", day);
			map.put("roomName", roomName);
			map.put("courseName", tds.get(startIndex).text());
			map.put("className", tds.get(startIndex+1).text());
			map.put("remark", tds.get(startIndex+5).text());
			list.add(map);
		}
		return list;
	}
	public JSONObject jxapDetail(String href, String dirPath) {
		JSONObject jo=jxapMap.get(href);
		if(jo==null){
			jo=new JSONObject();
			List<Map<String,String>> list=new ArrayList<Map<String,String>>();
			Document doc2 = router.httpGet(href, null);
			String title = "";
			Elements ps=doc2.getElementsByClass("Content").first().getElementsByTag("p");
			for(Element p:ps){
				//空行不算
				if(p.html().equals("&nbsp;")||
						p.text().trim().equals("")){
					continue;
				}else{
					title=p.text();
					break;
				}
			}
			
			Element tbody=doc2.getElementsByTag("table").first().getElementsByTag("tbody").first();
			Elements trs=tbody.getElementsByTag("tr");
			String day="";
			int tdsLength=0;
			for(int i=0;i<trs.size();i++){
				Element tr=trs.get(i);
				Elements tds=tr.getElementsByTag("td");
				//第一行来算表格列数
				if(i==0){
					tdsLength=tds.size();
				}
				//带标题的行
				if(tds.size()==tdsLength){
					day=tds.get(0).text();
					List<Map<String,String>> subList=getTdText(tds,day,1);
					list.addAll(subList);
				//不带标题的行，列数-1
				}else{
					List<Map<String,String>> subList=getTdText(tds,day,0);
					list.addAll(subList);
				}
			}
			jo.put("title", title);
			jo.put("list", list);
			jxapMap.put(href, jo);
		}
		return jo;
	}

}
