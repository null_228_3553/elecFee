package edu.hust.elecFee.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("jxapService")
public class JxapService {
	
	protected Long lastLoadTime;
	protected Long period;
	private ReentrantReadWriteLock rwl;
	private List<Map<String,String>> jxapList;
	@Autowired
	private Router router;
	
	public JxapService() {
		rwl=new ReentrantReadWriteLock();
		period=180*60*1000l;//180分钟
	}
	
	public List<Map<String,String>> jxap() {
		if(jxapList==null || (System.currentTimeMillis()-lastLoadTime)>period){
			lastLoadTime=System.currentTimeMillis();
			try{
				//更新数据 写锁
				rwl.writeLock().lock();
				updateCache();
			}finally{
				rwl.writeLock().unlock();
			}
		}
		try{
			//读取数据 读锁
			rwl.readLock().lock();
			return jxapList;
		}finally{
			rwl.readLock().unlock();
		}
	}
	
	public void updateCache() {
		Document doc=router.httpGet("http://tbcc.hust.edu.cn/sjsykb/index.htm", null);
		Elements trs = doc.getElementsByTag("table").get(0)
				.getElementsByTag("tr");
		jxapList=new ArrayList<Map<String,String>>();
		for(int i=1;i<trs.size();i++){
			Element tr=trs.get(i);
			Element a=tr.getElementsByTag("a").get(0);
			String href="http://tbcc.hust.edu.cn"+a.attr("href");
			String text=a.text();
			String time=tr.getElementsByTag("td").get(1).text();
			Map<String,String> map=new HashMap<String,String>();
			map.put("href", href);
			map.put("text", text);
			map.put("time", time);
			jxapList.add(map);
		}
	}


}
