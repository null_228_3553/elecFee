package edu.hust.elecFee.service;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service("ecardService")
public class EcardService {
	@Autowired
	private JdbcTemplate jdbcTpltCore;
	private Map<String,Double> avgMap;
	private Map<String,String> stMap;
	
	String[] stArr=new String[]{""};
	
	public Map<String, Object> qryXyk2017(String userId) {
		String sql="select * from sjfx_xyk_all_2017 where xgh=?";
		try{
			Map<String,Object> retMap=jdbcTpltCore.queryForMap(sql, userId);
			Map<String,String> stMap=getStMap();
			List<Map<String,Object>> retList=new ArrayList<Map<String,Object>>();
			//取出食堂信息加入list
			for(Entry<String,String> stEntry:stMap.entrySet()){
				Double money=Double.valueOf((String)retMap.get(stEntry.getKey()));
				if(money>0){
					Map<String,Object> map=new HashMap<String,Object>();
					map.put("JE", money);
					map.put("MC", stEntry.getValue());
					retList.add(map);
				}
			}
			Collections.sort(retList,new Comparator<Map<String,Object>>(){
				public int compare(Map<String, Object> o1, Map<String, Object> o2) {
					if(((Double)o1.get("JE"))>((Double)o2.get("JE")))
						return -1;
					if(((Double)o1.get("JE"))<((Double)o2.get("JE")))
						return 1;
					return 0;
				}
			});
			//将5个之后的合并为其他
			if(retList.size()>5){
				Double otherTotal=0.0;
				for(int i=5;i<retList.size();i++){
					otherTotal=otherTotal+(Double)retList.get(i).get("JE");
					retList.remove(i);
					i--;
				}
				Map<String,Object> otherMap=new HashMap<String,Object>();
				otherMap.put("MC", "其他");
				otherMap.put("JE", otherTotal);
				retList.add(otherMap);
			}
			retMap.put("stList", retList);
			return retMap;
		}catch(EmptyResultDataAccessException e){
			return new HashMap<String,Object>();
		}
	}
	
	private Map<String,String> getStMap(){
		if(stMap==null){
			stMap=new HashMap<String,String>();
			stMap.put("DXY","丁香园");
			stMap.put("DYIST","东一食堂");
			stMap.put("DSST","东三食堂");
			stMap.put("DYST","东园食堂");
			stMap.put("DJGST","东教工食堂");
			stMap.put("TQY","同沁园");
			stMap.put("YYCT","喻园餐厅");
			stMap.put("XYST","学一食堂");
			stMap.put("XEST","学二食堂");
			stMap.put("JDZXCT","接待中心餐厅");
			stMap.put("XLY","杏林园");
			stMap.put("FLW","枫林湾");
			stMap.put("JQY","济沁园");
			stMap.put("QZY","清真园");
			stMap.put("BPW","百品屋");
			stMap.put("BHY","百惠园");
			stMap.put("BJY","百景园");
			stMap.put("ZJY","紫荆园");
			stMap.put("XIYST","西一食堂");
			stMap.put("XIEST","西二食堂");
			stMap.put("JXL","集贤楼");
			stMap.put("JJY","集锦园");
			stMap.put("YYST","韵苑食堂");
		}
		return stMap;
	}
	
	public Map<String,Double> qryAvg(){
		if(avgMap==null){
			avgMap=new HashMap<String,Double>();
			String sql90="select max(czzxf) from "
					+ "(select to_number(czzxf) czzxf from sjfx_xyk_all_2016 order by to_number(czzxf)) "
					+ "where rownum<(select 0.9*count(*) from sjfx_xyk_all_2016)";
			String avgSql="select avg(czzxf) from sjfx_xyk_all_2016";
			Double top90=jdbcTpltCore.queryForObject(sql90, Double.class);
			Double avg=jdbcTpltCore.queryForObject(avgSql, Double.class);
			DecimalFormat df=new DecimalFormat("#.##");
			avgMap.put("avg", Double.valueOf(df.format(avg)));
			avgMap.put("top90", Double.valueOf(df.format(top90)));
		}
		return avgMap;
	}
	
	public Map<String,Object> qryTotal(String userId){
		String sql="select * from sjfx_xyk_all_2016 where xgh=?";
		try{
			Map<String,Object> retMap=jdbcTpltCore.queryForMap(sql, userId);
			return retMap;
		}catch(EmptyResultDataAccessException e){
			return null;
		}

	}
	
	public List<Map<String,Object>> qryMarkets(String userId){
		String sql="select MC,sum(JE) JE from  sjfx_xyk_cs where xgh=? group by MC order by JE desc";
		List<Map<String,Object>> retList=jdbcTpltCore.queryForList(sql, userId);
		//将5个之后的合并为其他
		if(retList.size()>5){
			BigDecimal otherTotal=new BigDecimal(0);
			for(int i=5;i<retList.size();i++){
				otherTotal=otherTotal.add((BigDecimal)retList.get(i).get("JE"));
				retList.remove(i);
				i--;
			}
			Map<String,Object> otherMap=new HashMap<String,Object>();
			otherMap.put("MC", "其他");
			otherMap.put("JE", otherTotal);
			retList.add(otherMap);
		}
		return retList;
	}
	
	public List<Map<String,Object>> qryDinings(String userId){
		String sql="select MC,sum(JE) JE from  sjfx_xyk_st where xgh=? group by MC order by JE desc";
		List<Map<String,Object>> retList=jdbcTpltCore.queryForList(sql, userId);
		//将5个之后的合并为其他
		if(retList.size()>5){
			BigDecimal otherTotal=new BigDecimal(0);
			for(int i=5;i<retList.size();i++){
				otherTotal=otherTotal.add((BigDecimal)retList.get(i).get("JE"));
				retList.remove(i);
				i--;
			}
			Map<String,Object> otherMap=new HashMap<String,Object>();
			otherMap.put("MC", "其他");
			otherMap.put("JE", otherTotal);
			retList.add(otherMap);
		}
		return retList;
	}
	
	public Double qrySport(String userId){
		try{
			String ydsc=jdbcTpltCore.queryForObject("select YDSC from sjfx_tyb", String.class);
			return Double.valueOf(ydsc);
		}catch(EmptyResultDataAccessException e){
			return 0.0;
		}
	}

}
