package edu.hust.elecFee.service;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Service;

@Service
public class HttpClientService {

	public HttpClient getSslClient(){
		SSLContext sslcontext = createIgnoreVerifySSL();  

		// 设置协议http和https对应的处理socket链接工厂的对象  
		Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()  
				.register("http", PlainConnectionSocketFactory.INSTANCE)  
				.register("https", new SSLConnectionSocketFactory(sslcontext))  
				.build();
		PoolingHttpClientConnectionManager connManager = new PoolingHttpClientConnectionManager(socketFactoryRegistry);  
		
		CookieStore cookieStore=new BasicCookieStore();
		CloseableHttpClient httpClient = HttpClients.custom()
				.setConnectionManager(connManager)
				.setDefaultCookieStore(cookieStore)
				.build(); 
		return httpClient;
	}
	
	public HttpClient getDefaultClient(){
		return HttpClients.custom().build();
	}
	
	public String sendGet(HttpClient client,String url){
		HttpResponse response=null;
		HttpGet get = new HttpGet(url);
		String out=null;
    	try {
			response = client.execute(get);
			out=EntityUtils.toString(response.getEntity());
		} catch (IOException e) {
			e.printStackTrace();
    	}finally{
    		get.releaseConnection();
    	}
    	return out;
	}
	
    public String sendPost(HttpClient client,String url, String param){
    	HttpResponse response=null;
		HttpPost post = new HttpPost(url);
		String out=null;
    	try {
    		post.setEntity(new StringEntity(param,"UTF-8"));
    		response = client.execute(post);
    		out=EntityUtils.toString(response.getEntity());
		} catch (IOException e) {
			e.printStackTrace();
    	}finally{
    		post.releaseConnection();
    	}
		return out;
    }
    
    public HttpResponse sendPostForResponse(HttpClient client,HttpPost post, Map<String,String> paraMap){
		HttpResponse response=null;
		try {
			if(paraMap!=null){
				List<NameValuePair> paramList = new ArrayList<NameValuePair>();
				Iterator<Entry<String, String>> it=paraMap.entrySet().iterator();
				while(it.hasNext()){
					Entry<String,String> entry=it.next();
					paramList.add(new BasicNameValuePair(entry.getKey(),entry.getValue()));
				}
				post.setEntity(new UrlEncodedFormEntity(paramList, "UTF-8"));
			}
			response = client.execute(post);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return response;
	}

	private SSLContext createIgnoreVerifySSL(){  
		SSLContext sc = null;
		try {
			sc = SSLContext.getInstance("SSLv3");
			X509TrustManager trustManager = new X509TrustManager() {  
				@Override  
				public void checkClientTrusted(  
						java.security.cert.X509Certificate[] paramArrayOfX509Certificate,  
						String paramString) throws CertificateException {  
				}  
				@Override  
				public void checkServerTrusted(  
						java.security.cert.X509Certificate[] paramArrayOfX509Certificate,  
						String paramString) throws CertificateException {  
				}  
				@Override  
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {  
					return null;  
				}  
			};  
			sc.init(null, new TrustManager[] { trustManager }, null);
		} catch (NoSuchAlgorithmException | KeyManagementException e) {
			e.printStackTrace();
		}  
		return sc;  
	}  

}
