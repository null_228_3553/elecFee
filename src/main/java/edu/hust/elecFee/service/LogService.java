package edu.hust.elecFee.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import edu.hust.elecFee.bean.ElecLog;

@Service("logService")
public class LogService {
	/**
	 * 日志中显示的返回字段
	 */
	public static final String retMsg="retMsg";	
	@Autowired
	private JdbcTemplate jdbcTpltCore;
	
	public void saveLog(ElecLog elecLog) {
		String sql="insert into elecLog values(elecSeq.NEXTVAL,?,?,?,?,?,?,?,?,?)";
		jdbcTpltCore.update(sql, new Object[]{
				elecLog.getUserId(),elecLog.getTrans(),elecLog.getIp(),
				elecLog.getDormStr(),elecLog.getRetMsg(),elecLog.getSuccess(),
				elecLog.getErrMsg(),elecLog.getQryDate(),elecLog.getProcessTime()
		});
	}
}
