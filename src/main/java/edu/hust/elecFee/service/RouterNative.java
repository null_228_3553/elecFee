package edu.hust.elecFee.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.text.MessageFormat;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import edu.hust.elecFee.common.Util;

@Service("routerNative")
public class RouterNative {
	private Log log = LogFactory.getLog(getClass());
	public static String corpId="wx40c9cd4c54d13656";
	public static String secret="30Y6KbybuDbAi187uQJXrO75MRB3Akdb6V3UVZ3xHT0";
//	public static String secret="78jTtJPjO6D9l60aZewgczM4LXLbjXPa7cA_arBgRfVTrdR9JphTDanA8uBIHUZx";
	private String accessToken;
	private String jsapiTicket;
	private long accessTokenTime;
	private long jsapiTicketTime;
	
	public void sendRemind(String userId, String title, String desc){
		sendRemind(userId,title,desc,null);
	}
	public void sendRemind(String userId, String title, String desc, String callback){
		log.error("发送微信提醒开始：发送给"+userId);
		send(Util.getMD5(userId),title,desc,callback);
	}
	
	public void sendRemind(String[] users, String title, String desc){
		sendRemind(users,title,desc,null);
	}
	public void sendRemind(String[] users, String title, String desc, String callback){
		String toUser="";
		for(int i=0;i<users.length;i++){
			toUser+=Util.getMD5(users[i])+"|";
		}
		toUser=toUser.substring(0, toUser.length()-1);
		log.error("发送微信提醒开始：发送给"+users);
		send(toUser,title,desc,callback);
	}
	
	private void send(String toUser,String title,String desc,String callback){
		String url=MessageFormat.format("https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token={0}", 
				getAccessToken());
		JSONObject jo=new JSONObject();
		jo.put("touser", toUser);
		jo.put("msgtype", "news");//图文
		jo.put("agentid", 28);//消息中心
		JSONObject news=new JSONObject();
		JSONArray articles=new JSONArray();
		JSONObject article=new JSONObject();
		article.put("title", title);
		article.put("description", desc);
		if(callback!=null)
			article.put("url", callback);
    	articles.add(article);
    	news.put("articles", articles);
		jo.put("news", news);

		String res=sendPost(url,jo.toString());
		log.error(res);
	}
	
	//每7200秒获取一次accessToken
	public String getAccessToken(){
		if(accessTokenTime==0l||accessToken==null||
				(System.currentTimeMillis()-accessTokenTime)>7200*1000){
			String url=MessageFormat.format("https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid={0}&corpsecret={1}", 
					corpId,secret);
			String retStr=sendGet(url);
			JSONObject jo=JSON.parseObject(retStr);
			accessToken=(String)jo.get("access_token");
			accessTokenTime=System.currentTimeMillis();
		}
		return accessToken;
	}
	public String getJsapiTicket(){
		if(jsapiTicketTime==0l||jsapiTicket==null||
				(System.currentTimeMillis()-jsapiTicketTime)>7200*1000){
			String url="https://qyapi.weixin.qq.com/cgi-bin/get_jsapi_ticket?access_token="
					+ getAccessToken() ;
			String retStr=sendGet(url);
			JSONObject jo=JSON.parseObject(retStr);
			jsapiTicket=(String)jo.get("ticket");
			jsapiTicketTime=System.currentTimeMillis();
		}
		return jsapiTicket;
	}
	
	public String sendGet(String urlStr) {
        String result = "";
        BufferedReader in = null;
        try {
            URL realUrl = new URL(urlStr);
            // 打开和URL之间的连接
            URLConnection connection = realUrl.openConnection();
            // 设置通用的请求属性
            connection.setRequestProperty("accept", "application/json, text/javascript, */*; q=0.01");
            connection.setRequestProperty("connection", "Keep-Alive");
            connection.setRequestProperty("user-agent",
                    "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36");
            // 建立实际的连接
            connection.connect();
            
            // 定义 BufferedReader输入流来读取URL的响应
            in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {
            System.out.println("发送GET请求出现异常！" + e);
            e.printStackTrace();
        }
        // 使用finally块来关闭输入流
        finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return result;
    }

    public String sendPost(String url, String param) {
        PrintWriter out = null;
        BufferedReader in = null;
        String result = "";
        try {
            URL realUrl = new URL(url);
            // 打开和URL之间的连接
            URLConnection conn = realUrl.openConnection();
            // 设置通用的请求属性
            conn.setRequestProperty("accept", "application/json, text/javascript, */*; q=0.01");
            conn.setRequestProperty("connection", "Keep-Alive");
            conn.setRequestProperty("user-agent",
                    "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36");
            // 发送POST请求必须设置如下两行
            conn.setDoOutput(true);
            conn.setDoInput(true);
            // 获取URLConnection对象对应的输出流
            out = new PrintWriter(conn.getOutputStream());
            // 发送请求参数
            out.print(param);
            // flush输出流的缓冲
            out.flush();
            // 定义BufferedReader输入流来读取URL的响应
            in = new BufferedReader(
                    new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {
            System.out.println("发送 POST 请求出现异常！"+e);
            e.printStackTrace();
        }
        //使用finally块来关闭输出流、输入流
        finally{
            try{
                if(out!=null){
                    out.close();
                }
                if(in!=null){
                    in.close();
                }
            }
            catch(IOException ex){
                ex.printStackTrace();
            }
        }
        return result;
    }
    //获取人数
    public JSONObject totalFollows() {
    	String url="https://qyapi.weixin.qq.com/cgi-bin/user/simplelist?access_token="+getAccessToken()+"&department_id=1&fetch_child=1";
    	String out=sendGet(url);
    	JSONObject jo=(JSONObject)JSONObject.parse(out);
		return jo;
    }

}
