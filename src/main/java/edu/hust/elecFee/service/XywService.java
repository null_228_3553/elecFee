package edu.hust.elecFee.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.annotation.PostConstruct;

import org.apache.cxf.Bus;
import org.apache.cxf.BusFactory;
import org.apache.cxf.bus.CXFBusFactory;
import org.apache.cxf.frontend.ClientProxyFactoryBean;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import com.ruijie.spl.api.QueryUserParams;
import com.ruijie.spl.api.QueryUserResult;
import com.ruijie.spl.api.SamService;

@Service("xywService")
public class XywService {
	private SamService samService;
	private SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
	@PostConstruct
	public void init() throws IOException{
		URL u=ResourceUtils.getURL("classpath:sam.properties");
    	InputStream stream=u.openStream();
    	Properties pro = new Properties();
    	InputStreamReader isr = null;
		try {
			isr = new InputStreamReader(stream,"UTF-8");
	    	pro.load(isr);
		} finally{
			isr.close();
		}
		String username = pro.getProperty("username");
		String password = pro.getProperty("password");
		String address = pro.getProperty("address");
		String wsdllocation = pro.getProperty("wsdllocation");
		
		CXFBusFactory busFactory = new CXFBusFactory();
        Bus bus = busFactory.createBus();
        BusFactory.setThreadDefaultBus(bus);
        ClientProxyFactoryBean factory = new ClientProxyFactoryBean();
        factory.setBus(bus);
        factory.setUsername(username);
        factory.setPassword(password);
        factory.setServiceClass(SamService.class);
        factory.setAddress(address);
        factory.setWsdlLocation(wsdllocation);
        
		samService = (SamService)factory.create();
	}
	
	public int queryTotalUsers(){
		QueryUserParams params = new QueryUserParams();
		QueryUserResult userResult = samService.queryUser(params);
		int total=userResult.getTotal();
		return total;
	}
	
	public int queryYesterdayUsers(){
		QueryUserParams params = new QueryUserParams();
		Date today = null;
		try {
			today = sdf.parse(sdf.format(new Date()));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Date yestoday=new Date(today.getTime()-3600*24*1000);
		params.setFromCreateTime(yestoday);
		params.setToCreateTime(today);
		QueryUserResult userResult = samService.queryUser(params);
		int total=userResult.getTotal();
		return total;
	}

}
