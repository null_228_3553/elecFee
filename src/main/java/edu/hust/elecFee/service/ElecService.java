package edu.hust.elecFee.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service("elecService")
public class ElecService {
	@Autowired
	private JdbcTemplate jdbcTpltCore;
	
	private List<String> areaList;
	public List<String> queryArea() {
		if(areaList==null){
			areaList=jdbcTpltCore.queryForList("select programId from area", String.class);
		}
		return areaList;
	}
	public List<String> queryBuilding(String programId){
		List<String> buildingList=jdbcTpltCore.queryForList("select txtyq from building where programId=?", 
				new Object[]{programId}, String.class);  
		return buildingList;
	}
	public List<String> queryFloor(String txtyq){
		List<String> floorList=jdbcTpltCore.queryForList("select txtld from floor where txtyq=?", 
				new Object[]{txtyq}, String.class);  
		return floorList;
	}

}
