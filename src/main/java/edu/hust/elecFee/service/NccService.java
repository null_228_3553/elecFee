package edu.hust.elecFee.service;


import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import edu.hust.elecFee.bean.NccCount;
import edu.hust.elecFee.common.Util;


@Service("nccService")
public class NccService{
	
	@Autowired
	private JdbcTemplate jdbcTpltCore;
	@Autowired
	private JdbcTemplate jdbcTpltEhall;

	private SimpleDateFormat sdf1=new SimpleDateFormat("yyyy-MM-dd");
	private SimpleDateFormat sdf2=new SimpleDateFormat("yyyy年MM月dd日 EEEE",Locale.SIMPLIFIED_CHINESE);

	public Map<String,Object> qryWxCount(String date){
		//若date为空，默认昨天
		if(date==null||"".equals(date))
			date=sdf1.format(new Date(System.currentTimeMillis()-3600*24*1000));
		String qrySql="select * from dcp_wechat.wx_count where count_date=?";
		Map<String, Object> wxMap=jdbcTpltCore.queryForMap(qrySql,date);
		
		//格式化date 新增星期
		try {
			wxMap.put("COUNT_DATE", sdf2.format(sdf1.parse(date)));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return wxMap;
	}
	
	public List<List<Map<String,Object>>> dutyList(){
		Date lastDay=new Date(System.currentTimeMillis()-1000*60*60*24);
		String sql="select a.dutytime,a.name,a.dutytype,b.mobile,b.dept from nccremind a,nccemployee b where a.name=b.name and dutytime>=to_date(?,'yyyy-mm-dd') order by a.dutytime,a.dutytype";
		List<Map<String,Object>> list=jdbcTpltCore.queryForList(sql, new Object[]{sdf1.format(lastDay)});
		List<List<Map<String,Object>>> resList=new ArrayList<List<Map<String,Object>>>();
		for(int i=0;i<list.size();i++){
			List<Map<String,Object>> tempList=new ArrayList<Map<String,Object>>();
			Map<String,Object> map=list.get(i);
			Date dutyTime=(Date)map.get("dutytime");
			tempList.add(formatMap(map));

			//相同日期的加入一个List
			while(true){
				i++;
				if(i<list.size()&&list.get(i).get("dutytime").equals(dutyTime)){
					tempList.add(formatMap(list.get(i)));
				}else{
					i--;
					resList.add(tempList);
					break;
				}
			}
		}
		return resList;
	}
	private Map<String,Object> formatMap(Map<String,Object> map){
		//日期为今天的改名
		Date dutyTime=(Date) map.get("dutytime");
		String dutyTimeStr=sdf1.format(dutyTime);
		if(sdf1.format(new Date()).equals(dutyTimeStr)){
			map.put("dutytime", "<span style='color:red'>---今天 "+sdf2.format(dutyTime)+"---</span>");
		}else{
			map.put("dutytime", sdf2.format(dutyTime));
		}
		if(map.get("dutytype").equals("repair")){
			map.put("dutytype", "维修");
		}else{
			map.put("dutytype", "值班");
		}
		return map;
	}
	
	public List<Map<String, Object>> buildingList() {
		List<Map<String,Object>> buildingList=new ArrayList<>();
		String sql="select a.user_name,a.mobile,b.id_number,a.user_sex,c.userlevel,c.room,c.responsible "
				+"from uicm_user a,uicm_user_account b,netmanager c "
				+"where a.user_id=b.user_id and b.id_number=c.userid ";
		List<Map<String,Object>> userList=jdbcTpltCore.queryForList(sql);
		for(Map<String,Object> map:userList){
			map.put("USER_SEX", "1".equals(map.get("USER_SEX"))?"男":"女");
			String[] buildingArr=((String)map.get("RESPONSIBLE")).split("、");
			for(int j=0;j<buildingArr.length;j++){
				Map<String,Object> buildingMap=new HashMap<>();
				buildingMap.put("buildingName",buildingArr[j]);
				buildingMap.put("user", map);
				buildingList.add(buildingMap);
			}
		}
		//排序
		Collections.sort(buildingList,new Comparator<Map<String,Object>>() {
    		Pattern p = Pattern.compile("[0-9]");//找出数字

            public int compare(Map<String,Object> o1, Map<String,Object> o2) {
            	String s1=(String) o1.get("buildingName");
            	s1=s1.replaceAll("\\(.*\\)", "");//去除括号中内容
            	String s2=(String) o2.get("buildingName");
            	s2=s2.replaceAll("\\(.*\\)", "");//去除括号中内容
            	
            	String num1="";//s1中的数字
        		Matcher m1 = p.matcher(s1);
        		while(m1.find()){
        			num1+=m1.group();
        		}
        		String str1 = m1.replaceAll("");//s1中字符

            	String num2="";//s2中的数字
        		Matcher m2 = p.matcher(s2);
        		while(m2.find()){
        			num2+=m2.group();
        		}
        		String str2 = m2.replaceAll("");//s2中字符

        		//楼栋名不同直接比
            	if(!str1.equals(str2)){
            		return  str1.compareTo(str2);
            	//楼栋名相同比数字
            	}else{
            		if(Integer.valueOf(num1)>Integer.valueOf(num2)){
            			return 1;
            		}else if(Integer.valueOf(num1)<Integer.valueOf(num2)){
            			return -1;
            		}else{
            			return 0;
            		}
            	}
            }
        });
		return buildingList;
	}
	public Map<String,Object> qryCert(String dirPath, String appId) {
		Map<String,Object> resMap=null;
		String insertSql="insert into jrxt(appid,dwmc,lxrxm,lxrrybh,lxrdh,lxryx,xtmc,xtzygn,zdym,ipdz,yhlx,rzfs,xtkfyy,wlhj,jrkssj,certNo)"
				+ "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		String coreSql="select * from jrxt where appId=?";
		String certNoSql="select max(certNO) from jrxt";
		String ehallSql="select * from lcdba.wxb_tysfrzsq where uuid=?";
		//查one数据库
		try{
			resMap=jdbcTpltCore.queryForMap(coreSql, new Object[]{appId});
		}catch(EmptyResultDataAccessException e){}
		
		//没有则查ehall数据库
		if(resMap==null){
			try{
				resMap=jdbcTpltEhall.queryForMap(ehallSql, new Object[]{appId});
				int certNo;
				try{
					certNo=jdbcTpltCore.queryForObject(certNoSql, Integer.class)+1;
				}catch(EmptyResultDataAccessException|NullPointerException e){
					certNo=10001;
				}
				//插入one数据库
				jdbcTpltCore.update(insertSql, new Object[]{
					resMap.get("UUID"),resMap.get("DWMC"),resMap.get("LXRXM"),
					resMap.get("LXRRYBH"),resMap.get("LXRDH"),resMap.get("LXRYX"),
					resMap.get("XTMC"),resMap.get("XTZYGN"),resMap.get("ZDYM"),
					resMap.get("IPDZ"),resMap.get("YHLX"),resMap.get("RZFS"),
					resMap.get("XTKFYY"),resMap.get("WLHJ"),resMap.get("JRKSSJ"),
					certNo
				});
				resMap.put("certNo", certNo);
			}catch(EmptyResultDataAccessException e){}
		}
		//生成证书
		File file=new File(dirPath+"/ncc/cert/"+appId+".jpg");
		if(!file.exists()){
			String xtmc=(String)resMap.get("XTMC");
			String dwmc=(String)resMap.get("DWMC");
			String jrkssj=(String)resMap.get("JRKSSJ");
			imgCert(dirPath, appId, xtmc, dwmc, jrkssj);
		}

		return resMap;
	}
	public void imgCert(String dirPath, String appId,String xtmc, String dwmc, String jrsqsj){
		String text1="      兹授权"+xtmc+"（负责单位："+dwmc+"）接入华中科技大学统一身份认证系统（pass.hust.edu.cn）。";
		String text2="      接入时间："+jrsqsj+"。";
//		String text3="      在授权期间，必须遵守《统一身份认证系统相关协议》。";
		String srcImageFile=dirPath+"/ncc/cert/blank.jpg";
		String destImageFile=dirPath+"/ncc/cert/"+appId+".jpg";
		Color color=new Color(64,64,64);
		Font font=new Font("微软雅黑", Font.PLAIN, 27);
		try{
			File img = new File(srcImageFile);
		    Image src = ImageIO.read(img);
		    int width = src.getWidth(null);
		    int height = src.getHeight(null);
		    BufferedImage image = new BufferedImage(width, height,BufferedImage.TYPE_INT_RGB);
		    Graphics2D g = image.createGraphics();
		    g.drawImage(src, 0, 0, width, height, null);
		    g.setColor(color);
		    g.setFont(font);
		    g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP));
		    // 在指定坐标绘制水印文字
    		g.drawString(text1.substring(0,38), 140, height/2-20);
    		g.drawString(text1.substring(38,text1.length()), 140, height/2+20);
    		g.drawString(text2, 140, height/2+60);
//    		g.drawString(text3, 140, height/2+100);
		   
		    g.dispose();
		    ImageIO.write((BufferedImage) image, "JPEG", new File(destImageFile));// 输出到文件流
		} catch (Exception e) {
		    e.printStackTrace();
		}
	}
	
	public NccCount selectNccCount(String dateStr){
		try{
			Map<String,Object> resMap=jdbcTpltCore.queryForMap("select * from dcp_wechat.ncc_count where count_date=?", new Object[]{dateStr});
			NccCount nccCount=new NccCount();
			nccCount=(NccCount) Util.map2Bean(resMap, NccCount.class);
			return nccCount;
		}catch(EmptyResultDataAccessException e){
			return null;
		}
	}

	public void insertNccCount(NccCount nccCount){
		try {
			String sql="insert into dcp_wechat.ncc_count (";
			Field[] fields=NccCount.class.getDeclaredFields();
			for(int i=0;i<fields.length;i++){
				String methodName="get"
						+fields[i].getName().substring(0,1).toUpperCase()
						+fields[i].getName().substring(1);
				Method method=NccCount.class.getMethod(methodName);
				Object res=method.invoke(nccCount);
				if( res!=null){
					sql+=fields[i].getName()+",";
				}
			}
			if(sql.endsWith(",")){
				sql=sql.substring(0, sql.length()-1);
			}
			sql+=") values (";
			for(int i=0;i<fields.length;i++){
				String methodName="get"
						+fields[i].getName().substring(0,1).toUpperCase()
						+fields[i].getName().substring(1);
				Method method=NccCount.class.getMethod(methodName);
				Object res=method.invoke(nccCount);
				if( res!=null){
					if(res instanceof String
							||res instanceof Date){
						sql+="\'"+res+"\',";
					}else{
						sql+=res+",";
					}
				}
			}
			if(sql.endsWith(",")){
				sql=sql.substring(0, sql.length()-1);
			}
			sql+=")";
			jdbcTpltCore.update(sql);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void updateNccCount(NccCount nccCount) {
		try {
			String sql="update dcp_wechat.ncc_count set ";
			Field[] fields=NccCount.class.getDeclaredFields();
			for(int i=0;i<fields.length;i++){
				String methodName="get"
						+fields[i].getName().substring(0,1).toUpperCase()
						+fields[i].getName().substring(1);
				Method method=NccCount.class.getMethod(methodName);
				Object res=method.invoke(nccCount);
				if( res!=null){
					if(res instanceof String
							||res instanceof Date){
						sql+=" "+fields[i].getName()+"=\'"+res+"\',";
					}else{
						sql+=" "+fields[i].getName()+"="+res+",";
					}
				}
			}
			if(sql.endsWith(",")){
				sql=sql.substring(0, sql.length()-1);
			}
			sql+=" where count_date=\'"+nccCount.getCount_date()+"\'";
			jdbcTpltCore.update(sql);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
