package edu.hust.elecFee.service;


import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;


@Service("jobService")
public class JobService{
	private static int pageCount=20;
	@Autowired
	private JdbcTemplate jdbcTpltJob;
	@Autowired
	private JdbcTemplate jdbcTpltCore;
	protected SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");

	/**
	 * 
	 * @param cType 0主校区  1同济
	 * @param string 
	 * @param startDate 
	 * @param time 今天起所有  近一周  近一月  所有  自定义
	 * @param keyWord 
	 * @return
	 * @throws ParseException 
	 */
	public List<Map<String, Object>> qryList(int cType, String time, String startDateStr, String endDateStr, String keyWord, int page) throws ParseException{
		List<Object> args=new ArrayList<Object>();
		args.add(cType);
		String sql="select content_id,title,dbms_lob.substr(kssj) kssj,dbms_lob.substr(zpdd) zpdd,top_level from tb_zph "
				+ "where ctype=? ";
		if(time.equals("今天起所有")||time.equals("近一周") || time.equals("近一月")){
			//今天起始时间
			Calendar calendar=Calendar.getInstance();
			String dateStr=sdf.format(calendar.getTime());
			Date today=new Date(sdf.parse(dateStr).getTime());
			//今天起
			sql+="and to_date(dbms_lob.substr(kssj),'YYYY-MM-DD HH24:MI')>? ";
			args.add(today);
			//近一周
			if(time.equals("近一周")){
				calendar.add(Calendar.WEEK_OF_YEAR, 1);
				Date oneWeek=new Date(calendar.getTime().getTime());
				sql+="and to_date(dbms_lob.substr(kssj),'YYYY-MM-DD HH24:MI')<? ";
				args.add(oneWeek);
			//近一月
			}else if(time.equals("近一月")){
				calendar.add(Calendar.MONTH, 1);
				Date oneMonth=new Date(calendar.getTime().getTime());
				sql+="and to_date(dbms_lob.substr(kssj),'YYYY-MM-DD HH24:MI')<? ";
				args.add(oneMonth);
			}
		}else if(time.equals("自定义")){
			Date startDate=new Date(sdf.parse(startDateStr).getTime());
			Date endDate=new Date(sdf.parse(endDateStr).getTime());
			sql+="and to_date(dbms_lob.substr(kssj),'YYYY-MM-DD HH24:MI')>? "
				+ "and to_date(dbms_lob.substr(kssj),'YYYY-MM-DD HH24:MI')<? ";
			args.add(startDate);
			args.add(endDate);
		}
		
		if(keyWord!=null &&!"".equals(keyWord)){
			sql+="and (title like ? or dbms_lob.substr(zpdd) like ?) ";
			args.add("%"+keyWord+"%");
			args.add("%"+keyWord+"%");
		}
		//排序 分页
		sql="select * from(select a.*,rownum rn from ("+sql+" order by kssj)a )where rn>? and rn<=?";
		args.add(pageCount*page);
		args.add(pageCount*(page+1));
		
		/*String sql="select content_id,title,dbms_lob.substr(kssj) kssj,dbms_lob.substr(zpdd) zpdd,top_level from tb_zph "
				+ "where ctype=? and to_date(dbms_lob.substr(kssj),'YYYY-MM-DD HH24:MI')>? "
				+ "order by dbms_lob.substr(kssj)";*/
		List<Map<String, Object>> list=jdbcTpltJob.queryForList(sql,args.toArray());
		return list;
	}

	public Map<String, Object> qryDetail(String id) {
		String sql="select *from tb_zph where content_id=? ";
		Map<String,Object> detMap=jdbcTpltJob.queryForMap(sql, new Object[]{id});
		
		//调整table宽度
		if(!"".equals(detMap.get("TXT")) && detMap.get("TXT")!=null){
			Document doc=Jsoup.parse((String)detMap.get("TXT"));
			Elements tables=doc.getElementsByTag("table");
			
			for(int i=0;i<tables.size();i++){
				Element table=tables.get(i);
				table.removeAttr("width");
//				table.attr("style",table.attr("style")+"border:1px solid #eee;");
				Elements trs=table.getElementsByTag("tr");
				//重新调整width  px改% 修改第0行每一列的宽度
				if(trs.size()>0){
					Double total=0.0;
					Element trFirst=trs.get(0);
					Elements tds=trFirst.getElementsByTag("td");
					for(int k=0;k<tds.size();k++){
						Element td=tds.get(k);
						td.removeAttr("nowrap");
						if(td.attr("width")!=null){
							total+=Double.valueOf(td.attr("width"));
						}
					}
					for(int k=0;k<tds.size();k++){
						Element td=tds.get(k);
						if(td.attr("width")!=null){
							Double width=Double.valueOf(td.attr("width"))*100/total;
							td.attr("width",width+"%");
						}
					}
				}
				//第一行开始 删除width 和nowrap
				for(int j=1;j<trs.size();j++){
					Elements tds=trs.get(j).getElementsByTag("td");
					for(int k=0;k<tds.size();k++){
						Element td=tds.get(k);
						td.removeAttr("nowrap");
						td.removeAttr("width");
					}
				}
				//去除table中p的text-indent
				Elements ps=table.getElementsByTag("p");
				for(int m=0;m<ps.size();m++){
					Element p=ps.get(m);
					String style=p.attr("style");
					//去除style中的text-indent
					style=style.replaceAll("text-indent.*;", "");
					p.attr("style",style);
				}
			}
			
			detMap.put("TXT", doc.toString());
		}
		
		String attachSql="select * from tb_zphattach where content_id=?";
		List<Map<String,Object>> attachList=jdbcTpltJob.queryForList(attachSql, new Object[]{id});
		detMap.put("attach", attachList);
		
		//更新阅读量
		String updateViewCountSql="update hustjob.jc_content_count set views=views+1 where content_id=?";
		jdbcTpltJob.update(updateViewCountSql, new Object[]{id});
		//查询阅读量
		String countSql="select views from hustjob.jc_content_count where content_id=?";
		int viewCount=jdbcTpltJob.queryForObject(countSql, new Object[]{id},Integer.class);
		detMap.put("viewCount", viewCount);
		
		return detMap;
	}

	public Map<String, Object> qryJobRemind(String userId) {
		String sql="select *from jobRemind where userId=? ";
		try{
			Map<String,Object> retMap=jdbcTpltCore.queryForMap(sql, new Object[]{userId});
			return retMap;
		}catch(EmptyResultDataAccessException e){
			return new HashMap<String,Object>();
		}
	}

	public void addJobRemind(String userId, String jobMain, String jobMedi) {
		String sql="MERGE INTO jobRemind t1 "
		    +"USING(SELECT ? userid,? jobMain,? jobMedi FROM dual) t2 "
		    +"ON(t1.userid=t2.userid) "
		    +"WHEN MATCHED THEN "
		    +"update set t1.jobMain=t2.jobMain,t1.jobMedi=t2.jobMedi where t1.userid=t2.userid "
		    +"WHEN NOT MATCHED THEN "
		    +"insert(userid,jobMain,jobMedi) VALUES(t2.userid,t2.jobMain,t2.jobMedi)";
		jdbcTpltCore.update(sql,new Object[]{userId,jobMain,jobMedi});
		return;
	}
}
