package edu.hust.elecFee.service;


import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import edu.hust.elecFee.bean.Book;

@Service("libraryService")
public class LibraryService{

	protected Log log=LogFactory.getLog(getClass());
	private String loginUrl="https://ftp.lib.hust.edu.cn/patroninfo*chx";
	private String qryUrl="http://202.114.9.29/api/opac/getrecord?cardno={0}";

	@Autowired
	private JdbcTemplate jdbcTpltCore;
	@Autowired
	private HttpClientService httpClientService;
	
	public JSONObject libRecords(String userId){
		String qryUrl=MessageFormat.format(this.qryUrl, userId);
		HttpClient client=httpClientService.getDefaultClient();
		String out=httpClientService.sendGet(client, qryUrl);
		if(out==null)
			throw new RuntimeException("接口出错");
		JSONObject jo=JSON.parseObject(out);
		return jo;
	}
	
	public List<Book> register(String userId,String pin1,String pin2){
		//1、访问登录页获取cookie
		HttpClient sslClient=httpClientService.getSslClient();
		httpClientService.sendGet(sslClient, loginUrl);
		HttpPost post=new HttpPost(loginUrl);
		try{
			//构造form
			Map<String,String> paraMap=new HashMap<String,String>();
			paraMap.put("code",userId);
			paraMap.put("pin1",pin1);
			paraMap.put("pin2",pin2);
			paraMap.put("submit.x","40");
			paraMap.put("submit.y","18");
			paraMap.put("submit","submit");
			//模拟登录
			HttpResponse response = httpClientService
					.sendPostForResponse(sslClient, post, paraMap);
			//登陆成功，跳转到详情页
			if(response.getStatusLine().getStatusCode()==302) {
				//更新密码
				updatePassword(userId,pin1);
				//详情页URL
				String item_url = "https://ftp.lib.hust.edu.cn"+response.getFirstHeader("Location").getValue();
				item_url=item_url.replace("items", "readinghistory");//历史页
				//访问详情页，解析html
				String html = httpClientService.sendGet(sslClient, item_url);
				List<Book> list=parseHtml(html);
				return list;
			}else if(response.getStatusLine().getStatusCode()==200) {
				String html=EntityUtils.toString(response.getEntity());
				Document doc=Jsoup.parse(html);
				Element e=doc.getElementsByClass("patron_errmsg").get(0);
				log.error(e);
				throw new RuntimeException(e.text());
			}
		}catch(Exception e){
			throw new RuntimeException(e.getMessage());
		}finally{
			post.releaseConnection();
		}
		return null;
	}

	public List<Book> hisRecords(String userId,String password){
		//1、访问登录页获取cookie
		HttpClient sslClient=httpClientService.getSslClient();
		httpClientService.sendGet(sslClient, loginUrl);

		//前端传来password，则更新
		boolean update=true;
		//前端未传password，从数据库取
		if(password==null){
			update=false;//从数据库取，不更新password
			password=qryPassword(userId);
		}
		//模拟登陆，获取数据
		if(password!=null){
			HttpPost post=new HttpPost(loginUrl);
			try{
				//构造form
				Map<String,String> paraMap=new HashMap<String,String>();
				paraMap.put("code",userId);
				paraMap.put("pin",password);
				paraMap.put("submit.x","40");
				paraMap.put("submit.y","18");
				paraMap.put("submit","submit");
				//模拟登录
				HttpResponse response = httpClientService
						.sendPostForResponse(sslClient, post, paraMap);
				//登陆成功，跳转到详情页
				if(response.getStatusLine().getStatusCode()==302) {
					//更新密码
					if(update==true){
						updatePassword(userId,password);
					}
					//详情页URL
					String item_url = "https://ftp.lib.hust.edu.cn"+response.getFirstHeader("Location").getValue();
					item_url=item_url.replace("items", "readinghistory");//历史页
					//访问详情页，解析html
					String html = httpClientService.sendGet(sslClient, item_url);
					List<Book> list=parseHtml(html);
					return list;
				//登陆失败，密码错误或者未注册
				}else if(response.getStatusLine().getStatusCode()==200) {
					String html=EntityUtils.toString(response.getEntity());
					Document doc=Jsoup.parse(html);
					Element e=doc.getElementsByClass("patron_errmsg").get(0);
					//用户密码输入错误，再次输入
					if(e.text().equals("抱歉, 您提交的信息无效, 请重试")) {
						throw new RuntimeException("password.error");
					//该用户未初始化密码，跳转到注册界面
					}else if(e.equals("PIN")) {
						throw new RuntimeException("not.register");
					}
				}
			}catch(Exception e){
				throw new RuntimeException(e.getMessage());
			}finally{
				post.releaseConnection();
			}
		}
		//数据库也没存密码，让用户跳转到密码输入页
		throw new RuntimeException("password.error");
	}

	public String qryPassword(String userId){
		String sql="select password from dcp_wechat.library_user where userId=?";
		try{
			String password=jdbcTpltCore.queryForObject(sql, new Object[]{userId}, String.class);
			return password;
		}catch(EmptyResultDataAccessException e){
			return null;	
		}
	}
	
	private void updatePassword(String userId, String password) {
		String sql="select remind from dcp_wechat.library_user where userId=? ";
		try{
			jdbcTpltCore.queryForMap(sql, new Object[]{userId});
			//存在则更新
			String updateSql="update dcp_wechat.library_user set password=? where userid=?";
			jdbcTpltCore.update(updateSql,new Object[]{password,userId});		
		}catch(EmptyResultDataAccessException e){
			//不存在则插入
			String insertSql="insert into dcp_wechat.library_user(userid,password)"
					+ "values(?,?)";
			jdbcTpltCore.update(insertSql,new Object[]{userId,password});
		}
	}

	private List<Book> parseHtml(String html) {
		List<Book> list=new ArrayList<Book>();
		Document doc=Jsoup.parse(html);
		Elements es=doc.getElementsByClass("patFuncEntry");
		for(Element e:es){
			Book book=new Book();
			book.setTitle(e.getElementsByClass("patFuncTitleMain").text());
			book.setAuthor(e.getElementsByClass("patFuncAuthor").text());
			book.setDuedate(e.getElementsByClass("patFuncDate").text());
			book.setDetails(e.getElementsByClass("patFuncDetails").text());
			list.add(book);
		}
		return list;
	}
	
	public String qryLibraryRemind(String userId) {
		String sql="select remind from dcp_wechat.library_user where userId=? ";
		String remind=null;
		try{
			remind=jdbcTpltCore.queryForObject(sql, new Object[]{userId},String.class);
		}catch(EmptyResultDataAccessException e){
			remind="0";
		}
		return remind;
	}
	
	public void addLibraryRemind(String userId, String remind) {
		String sql="select remind from dcp_wechat.library_user where userId=? ";
		try{
			jdbcTpltCore.queryForMap(sql, new Object[]{userId});
			//存在则更新
			String updateSql="update dcp_wechat.library_user set remind=? where userid=?";
			jdbcTpltCore.update(updateSql,new Object[]{remind,userId});
		}catch(EmptyResultDataAccessException e){
			//不存在则插入
			String insertSql="insert into dcp_wechat.library_user(userid,remind)"
					+ "values(?,?)";
			jdbcTpltCore.update(insertSql,new Object[]{userId,remind});
		}
	}
}
