package edu.hust.elecFee.service;

import java.io.IOException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.Connection.Method;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import edu.hust.elecFee.common.SysException;
import edu.hust.elecFee.common.Util;

@Service("router")
public class Router {
	private Log log = LogFactory.getLog(getClass());
	//接口地址
	private static final String url="http://one.hust.edu.cn/dcp/ifs";
//	private static final String url="http://te.m.hust.edu.cn:8081/dcp/ifs";
	//接口超时时间30秒
	private static int timeout=30*1000;
	//秘钥
	private static String privateKey="abcdefghijklmnopqrs12345";
	//系统名
	private static String sysId="elecFee";
	
	public void sendWxMessage(String userId,String title,String desc,String content,String remindLink){
		String param=MessageFormat.format(""
				+ "sysid={0}&module=remind&function=sendWxMessage&receiveUser={1}&title={2}"
				+ "&description={3}&content={4}&remindLink={5}",
				sysId,userId,title,desc,content,remindLink);
		String encodeParam=Util.desEncode(param,privateKey);
		JSONObject res=httpPostJson(url,sysId,encodeParam);
		log.error(res);
	}
	
	public JSONObject qryUser(String userId){
		String function="getUserInfo";
		String param=MessageFormat.format(""
				+ "sysid={0}&module=profile&function={1}&user_id={2}"
				, sysId ,function, userId);
		String encodeParam=Util.desEncode(param,privateKey);

		JSONObject responseMessage=httpPostJson(url,sysId,encodeParam);
		JSONObject responseMap=(JSONObject)responseMessage.get("map");
		
		JSONArray ja=(JSONArray)responseMap.get("list");
		if(ja.size()>0){
			JSONObject jObj=(JSONObject)ja.get(0);
			return jObj;
		}else{
			log.error("用户不存在");
			throw new SysException("用户不存在");
		}
	}
	
	public boolean chekPasswd(String userId, String passwd) {
		String function="getUserByAccountAndPassword";
		String param=MessageFormat.format(""
				+ "sysid={0}&module=profile&function={1}&passWord={2}&idNumber={3}"
				, sysId ,function, passwd, userId);
		String encodeParam=Util.desEncode(param,privateKey);
		
		JSONObject responseMap=httpPostJson(url,sysId,encodeParam);
		JSONObject responseBody=((JSONArray)responseMap.get("list")).getJSONObject(0);
		if(responseBody.get("error")==null){
			return true;
		}else{
			return false;
		}
	}
	
	public JSONObject httpPostJson(String url, String sysid, String param){
		Map<String,String> paraMap=new HashMap<String,String>();
		paraMap.put("sysid", sysId);
		paraMap.put("param", param);
		return httpPostJson(url,paraMap);
	}	
	public JSONObject httpPostJson(String url, Map<String,String> map){
		Response res=null;
		log.info("发送http post请求到"+url);
		long startTime=System.currentTimeMillis();
		try {
			res=Jsoup.connect(url).data(map)
					.timeout(timeout).ignoreContentType(true)
					.method(Method.POST).execute();
		} catch (IOException e) {
			log.error(e);
			throw new SysException("网络忙，请稍后再试");
		}
		log.info("请求耗时"+(System.currentTimeMillis()-startTime)+"毫秒");
		JSONObject response=JSON.parseObject(res.body());
		JSONObject responseMap=(JSONObject) response.get("message");
		return responseMap;
	}
	public Document httpGetGB2312(String url){
		Document doc = null;
		try {
			doc = Jsoup.parse(new URL(url).openStream(), "GB2312", url);
		} catch (IOException e) {
			log.error(e);
			throw new SysException("网络忙，请稍后再试");
		}
		return doc;
	}
	
	public Document httpGet(String url, Map<String,String> map){
		Response res=null;
		log.info("发送http get请求到"+url);
		long startTime=System.currentTimeMillis();
		try {
			if(map==null){
				res=Jsoup.connect(url).timeout(timeout)
						.method(Method.GET).execute();
			}else{
				res=Jsoup.connect(url).data(map)
						.timeout(timeout)
						.method(Method.GET).execute();
			}
		} catch (IOException e) {
			log.error(e);
			throw new SysException("网络忙，请稍后再试");
		}
		log.info("请求耗时"+(System.currentTimeMillis()-startTime)+"毫秒");
		return Jsoup.parse(res.body());
	}

}
