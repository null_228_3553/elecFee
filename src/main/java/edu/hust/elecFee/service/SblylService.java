package edu.hust.elecFee.service;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service("sblylService")
public class SblylService {
	
	@Autowired
	private JdbcTemplate jdbcTpltSys;
	protected Long lastLoadTime;
	protected Long period;
	private ReentrantReadWriteLock rwl;
	private List<Map<String,Object>> labhouseList;
	private DecimalFormat df;

	public SblylService() {
		rwl=new ReentrantReadWriteLock();
		period=180*60*1000l;//180分钟
		df=new DecimalFormat("0.##");
	}
	
	private int getCount(List<Map<String, Object>> countList, String bh) {
		//有则直接return
		for(Map<String, Object> map:countList){
			if(map.get("bh").equals(bh)){
				return (Integer)map.get("count");
			}
		}
		//没有返回0
		return 0;
	}
	
	public Map<String,Map<String,Object>> labOccupiedRate(){
		Map<String,Map<String,Object>> retMap=new HashMap<String,Map<String,Object>>();
		
		String countSql="select count(*) count,substring(mchno,0,8) bh from std_chk group by substring(mchno,0,8)";
		List<Map<String,Object>> countList=jdbcTpltSys.queryForList(countSql);

		List<Map<String,Object>> labhouseList=getLabhouseList();
		for(Map<String,Object> labhouseMap:labhouseList){
			if(labhouseMap.get("bh").equals("KFSY401")){
				int count=getCount(countList,"KFSY401");
				BigDecimal total=(BigDecimal)labhouseMap.get("r_num");
				labhouseMap.put("count", count);
				labhouseMap.put("rate", df.format(100*(double)count/total.intValue()));
				retMap.put("z401", labhouseMap);
			}
			if(labhouseMap.get("bh").equals("KFSY403")){
				int count=getCount(countList,"KFSY403");
				BigDecimal total=(BigDecimal)labhouseMap.get("r_num");
				labhouseMap.put("count", count);
				labhouseMap.put("rate", df.format(100*(double)count/total.intValue()));
				retMap.put("z403", labhouseMap);
			}
			if(labhouseMap.get("bh").equals("N060201")){
				int count=getCount(countList,"N060201");
				BigDecimal total=(BigDecimal)labhouseMap.get("r_num");
				labhouseMap.put("count", count);
				labhouseMap.put("rate", df.format(100*(double)count/total.intValue()));
				retMap.put("z208", labhouseMap);
			}
			if(labhouseMap.get("bh").equals("DXQ0002")){
				int count=getCount(countList,"DXQ0002");
				BigDecimal total=(BigDecimal)labhouseMap.get("r_num");
				labhouseMap.put("count", count);
				labhouseMap.put("rate", df.format(100*(double)count/total.intValue()));
				retMap.put("d201", labhouseMap);
			}
			if(labhouseMap.get("bh").equals("DXQ0001")){
				int count=getCount(countList,"DXQ0001");
				BigDecimal total=(BigDecimal)labhouseMap.get("r_num");
				labhouseMap.put("count", count);
				labhouseMap.put("rate", df.format(100*(double)count/total.intValue()));
				retMap.put("d301", labhouseMap);
			}
		}
		return retMap;
	}
	
	public List<Map<String,Object>> getLabhouseList() {
		if(labhouseList==null || (System.currentTimeMillis()-lastLoadTime)>period){
			lastLoadTime=System.currentTimeMillis();
			try{
				//更新数据 写锁
				rwl.writeLock().lock();
				updateCache();
			}finally{
				rwl.writeLock().unlock();
			}
		}
		try{
			//读取数据 读锁
			rwl.readLock().lock();
			return labhouseList;
		}finally{
			rwl.readLock().unlock();
		}
	}
	
	public void updateCache() {
		String labhouseSql="select bh,mc,r_num from labhouse";
		this.labhouseList=jdbcTpltSys.queryForList(labhouseSql);
	}


}
