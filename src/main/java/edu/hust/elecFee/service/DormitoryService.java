package edu.hust.elecFee.service;

import java.sql.Timestamp;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import edu.hust.elecFee.bean.Dormitory;
import edu.hust.elecFee.common.Util;

@Service("dormitoryService")
public class DormitoryService {
	private Log log = LogFactory.getLog(getClass());
	@Autowired
	private JdbcTemplate jdbcTpltCore;
	
	public Dormitory checkDomitory(String userId){
		String querySql="select * from myDormitory where dormitoryState='N' and userId=?";
		try{
			Map<String,Object> resMap=jdbcTpltCore.queryForMap(querySql, new Object[]{userId});
			Dormitory domitory=new Dormitory();
			domitory=(Dormitory) Util.map2Bean(resMap, Dormitory.class);
			return domitory;
		}catch(EmptyResultDataAccessException e){
			log.info(e);
			return null;
		}
	}

	public void updateState(String userId,String state){
		jdbcTpltCore.update("update mydormitory set dormitoryState=? where userId=?", 
				new Object[]{state,userId});
	}
	
	public Dormitory saveDormitory(String userId, String programId, String txtyq,
			String txtld, String txtroom) {
		jdbcTpltCore.update("insert into mydormitory(userId,programId,txtyq,txtld,txtroom,dormitoryState,createTime) values(?,?,?,?,?,'N',?)", 
				new Object[]{userId,programId,txtyq,txtld,txtroom,new Timestamp(System.currentTimeMillis())});
		
		Dormitory dormitory=new Dormitory();
		dormitory.setDormitoryState("N");
		dormitory.setProgramId(programId);
		dormitory.setTxtld(txtld);
		dormitory.setTxtroom(txtroom);
		dormitory.setTxtyq(txtyq);
		return dormitory;
	}

	public void saveSwitch(String warnSwitch, String threshold, String userId) {
		jdbcTpltCore.update("update mydormitory set warnSwitch=?,threshold=? where userId=? and dormitoryState='N'", 
				new Object[]{warnSwitch,threshold,userId});
	}

}
