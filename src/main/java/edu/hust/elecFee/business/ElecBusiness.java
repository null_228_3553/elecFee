package edu.hust.elecFee.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.hust.elecFee.common.Dict;
import edu.hust.elecFee.common.HttpUtil;

@Service
public class ElecBusiness {

	private String newEventValidation;
	private String newViewState;
	
	@Autowired
	private HttpUtil httpUtil;
	
	private String areaString;
	public String queryAllProgramId(){
		if(areaString!=null){
			return areaString;
		}
		
		String res=httpUtil.httpPost("");
		int index=res.indexOf("<select name=\"programId\" id=\"programId\">");
		if(index!=-1){
			int startIndex=res.indexOf("<option",index);
			int endIndex=res.indexOf("</select>", startIndex);
			areaString=res.substring(startIndex,endIndex);
		}
		return areaString;
	}
	
	public String queryTxtyq(String eventTarget,String viewState,String eventValidation,String programId){
		Map<String,String> paramMap=new HashMap<String,String>();
		paramMap.put(Dict.eventTarget, eventTarget);
		paramMap.put(Dict.viewState, viewState);
		paramMap.put(Dict.eventValidation, eventValidation);
		paramMap.put(Dict.programId, programId);
		
		String res=httpUtil.httpPost(paramMap);
		newEventValidation=httpUtil.getEventValidation(res);
		newViewState=httpUtil.getViewState(res);
		
		String output="";
		int index=res.indexOf("<select name=\"txtyq\" id=\"txtyq\">");
		if(index!=-1){
			int startIndex=res.indexOf("<option",index);
			int endIndex=res.indexOf("</select>", startIndex);
			output=res.substring(startIndex,endIndex);
		}
		return output;
	}
	public String queryTxtld(String eventTarget,String viewState,String eventValidation,String programId,String txtyq){
		Map<String,String> paramMap=new HashMap<String,String>();
		paramMap.put(Dict.eventTarget, eventTarget);
		paramMap.put(Dict.viewState, viewState);
		paramMap.put(Dict.eventValidation, eventValidation);
		paramMap.put(Dict.programId, programId);
		paramMap.put(Dict.txtyq, txtyq);
		
		String res=httpUtil.httpPost(paramMap);
		newEventValidation=httpUtil.getEventValidation(res);
		newViewState=httpUtil.getViewState(res);
		
		//返回值
		String output="";
		int index=res.indexOf("<select name=\"txtld\" id=\"txtld\">");
		if(index!=-1){
			int startIndex=res.indexOf("<option",index);
			int endIndex=res.indexOf("</select>", startIndex);
			output=res.substring(startIndex,endIndex);
		}
		return output;
	}
	public Map<String,Object> queryDetail(String viewState,String eventValidation,String programId,String txtyq,String txtld,String txtroom){
		//为了viewState和newEventValidation两个值
		queryTxtyq("programId", viewState, eventValidation, programId);
		queryTxtld("txtyq", newViewState, newEventValidation, programId, txtyq);

		Map<String,String> paramMap=new HashMap<String,String>();
		paramMap.put(Dict.viewState, newViewState);
		paramMap.put(Dict.eventValidation, newEventValidation);
		paramMap.put(Dict.programId, programId);
		paramMap.put(Dict.txtyq, txtyq);
		paramMap.put(Dict.txtld, txtld);
		paramMap.put(Dict.txtroom, txtroom);
		paramMap.put("ImageButton1.x", "9");
		paramMap.put("ImageButton1.y", "8");

		String res=httpUtil.httpPost(paramMap);
		if(res.indexOf("alert('不存在该仪表信息！');")!=-1){
			return null;
		}
		newEventValidation=httpUtil.getEventValidation(res);
		newViewState=httpUtil.getViewState(res);
		
		Map<String,Object> resMap=new HashMap<String,Object>();
		int index=res.indexOf("id=\"GridView1\"");
		if(index!=-1){
			int startIndex=res.indexOf("<tr>",index);
			int endIndex=res.indexOf("</table>", startIndex);

			List<Map<String,String>> paymentList=new ArrayList<Map<String,String>>();
			String trs=res.substring(startIndex,endIndex);
			int tdIndex=trs.indexOf("<td>");
			while(tdIndex!=-1){
				Map<String,String> map=new HashMap<String,String>();
				//一行tr的第一个td
				int tdIndexEnd=trs.indexOf("</td>",tdIndex);
				String elecQty=trs.substring(tdIndex+4,tdIndexEnd);
				map.put("elecQty", elecQty);
				//一行tr的第二个td
				tdIndex=trs.indexOf("<td>",tdIndexEnd);
				tdIndexEnd=trs.indexOf("</td>",tdIndex);
				String fee=trs.substring(tdIndex+4,tdIndexEnd);
				map.put("fee", fee);
				//一行tr的第三个td
				tdIndex=trs.indexOf("<td>",tdIndexEnd);
				tdIndexEnd=trs.indexOf("</td>",tdIndex);
				String time=trs.substring(tdIndex+4,tdIndexEnd);
				map.put("time", time);
				
				paymentList.add(map);
				//下一行
				tdIndex=trs.indexOf("<td>",tdIndexEnd);
			}		
			resMap.put("paymentList", paymentList);
		}
		index=res.indexOf("id=\"GridView2\"");
		if(index!=-1){
			int startIndex=res.indexOf("<tr>",index);
			int endIndex=res.indexOf("</table>", startIndex);
			
			List<Map<String,String>> detailList=new ArrayList<Map<String,String>>();
			String trs=res.substring(startIndex,endIndex);
			int tdIndex=trs.indexOf("<td>");
			while(tdIndex!=-1){
				Map<String,String> map=new HashMap<String,String>();
				//一行tr的第一个td
				int tdIndexEnd=trs.indexOf("</td>",tdIndex);
				String data=trs.substring(tdIndex+4,tdIndexEnd);
				map.put("data", data);
				//一行tr的第二个td
				tdIndex=trs.indexOf("<td>",tdIndexEnd);
				tdIndexEnd=trs.indexOf("</td>",tdIndex);
				String time=trs.substring(tdIndex+4,tdIndexEnd);
				map.put("time", time);
				
				detailList.add(map);
				//下一行
				tdIndex=trs.indexOf("<td>",tdIndexEnd);
			}
			resMap.put("detailList", detailList);
		}
		index=res.indexOf("name=\"TextBox2\"");
		if(index!=-1){
			int startIndex=res.indexOf("value=",index);
			int endIndex=res.indexOf("\" ", startIndex);
			resMap.put("lastCheck", res.substring(startIndex+7,endIndex));
		}
		index=res.indexOf("name=\"TextBox3\"");
		if(index!=-1){
			int startIndex=res.indexOf("value=",index);
			int endIndex=res.indexOf("\" ", startIndex);
			resMap.put("remainElec", res.substring(startIndex+7,endIndex));
		}
		return resMap;
	}
	

	public String getNewEventValidation() {
		return newEventValidation;
	}

	public String getNewViewState() {
		return newViewState;
	}

}
