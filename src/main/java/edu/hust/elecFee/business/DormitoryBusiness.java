package edu.hust.elecFee.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import edu.hust.elecFee.bean.Dormitory;
import edu.hust.elecFee.service.DormitoryService;

@Service
public class DormitoryBusiness {

	@Autowired
	private DormitoryService dormitoryService;
	@Autowired
	private TransactionTemplate transTemplateCore;
	
	public Dormitory checkDormitory(String userId) {
		
		Dormitory dormitory=dormitoryService.checkDomitory(userId);
		return dormitory;
	}
	
	public Dormitory saveSwitch(String warnSwitch,String threshold,String userId) {
		dormitoryService.saveSwitch(warnSwitch,threshold,userId);
		Dormitory dormitory=dormitoryService.checkDomitory(userId);
		return dormitory;
	}
	
	public Dormitory saveDormitory(final String userId, final String programId,
			final String txtyq, final String txtld, final String txtroom) {
		return transTemplateCore.execute(new TransactionCallback<Dormitory>() {
			public Dormitory doInTransaction(TransactionStatus transactionstatus) {
				dormitoryService.updateState(userId, "C");
				Dormitory dormitory=dormitoryService.saveDormitory(userId, programId, txtyq, txtld, txtroom);
				return dormitory;
			}
		});
	}
	
	
}
