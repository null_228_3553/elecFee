package com.ruijie.spl.api;

public class CompensatePeriodBillResult extends SamApiBaseResult {
	private UserInfo userInfo;
	public CompensatePeriodBillResult(){
		
	}
	public CompensatePeriodBillResult(int errorCode,String errorMessage){
		this.errorCode=errorCode;
		this.errorMessage=errorMessage;
		this.userInfo=null;
	}
	
	public CompensatePeriodBillResult(int errorCode,String errorMessage,UserInfo userInfo){
		this.errorCode=errorCode;
		this.errorMessage=errorMessage;
		this.userInfo=userInfo;
	}
	public UserInfo getUserInfo() {
		return userInfo;
	}
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

}
