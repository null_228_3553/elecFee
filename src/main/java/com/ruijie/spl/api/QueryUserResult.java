package com.ruijie.spl.api;

public class QueryUserResult extends SamApiBaseResult{
	private UserInfo [] data;
	private int total;
	
	public QueryUserResult(){
		
	}
	
	public QueryUserResult(int errorCode,String errorMessage){
		this.errorCode=errorCode;
		this.errorMessage=errorMessage;
	}
	
	public QueryUserResult(int errorCode,String errorMessage, int total,UserInfo[] data){
		this.errorCode=errorCode;
		this.errorMessage=errorMessage;
		this.total=total;
		this.data=data;
	}
	public UserInfo[] getData() {
		return data;
	}
	public void setData(UserInfo[] data) {
		this.data = data;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
}
