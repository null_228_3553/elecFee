package com.ruijie.spl.api;

public class FindUserBindInfoByUserIdResultV2 extends SamApiBaseResult{
private SpiUserBindInfoV2[] infos;

public FindUserBindInfoByUserIdResultV2(){
	
}

public FindUserBindInfoByUserIdResultV2(int errorCode,String errorMessage){
	this.errorCode=errorCode;
	this.errorMessage=errorMessage;
	this.setInfos(null);
}

public FindUserBindInfoByUserIdResultV2(int errorCode,String errorMessage,SpiUserBindInfoV2[] infos){
	this.errorCode=errorCode;
	this.errorMessage=errorMessage;
	this.setInfos(infos);
}

public SpiUserBindInfoV2[] getInfos() {
	return infos;
}

public void setInfos(SpiUserBindInfoV2[] infos) {
	this.infos = infos;
}
}
