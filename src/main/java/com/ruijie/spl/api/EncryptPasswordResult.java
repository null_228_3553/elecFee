package com.ruijie.spl.api;

public class EncryptPasswordResult extends SamApiBaseResult{
	private String encryptPassword;

	public EncryptPasswordResult(){
		
	}
	public EncryptPasswordResult(int errorCode,String errorMessage){
		this.errorCode=errorCode;
		this.errorMessage=errorMessage;
	}
	
	public EncryptPasswordResult(int errorCode,String errorMessage,String encryptPassword){
		this.encryptPassword=encryptPassword;
		this.errorCode=errorCode;
		this.errorMessage=errorMessage;
	}
	
	public String getEncryptPassword() {
		return encryptPassword;
	}
	public void setEncryptPassword(String encryptPassword) {
		this.encryptPassword = encryptPassword;
	}

}
