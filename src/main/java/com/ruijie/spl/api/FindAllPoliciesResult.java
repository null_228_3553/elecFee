package com.ruijie.spl.api;

public class FindAllPoliciesResult extends SamApiBaseResult{
	private String[] policyNames;
	public FindAllPoliciesResult(){
		
	}
	
	public FindAllPoliciesResult(int errorCode,String errorMessage){
		this.errorCode=errorCode;
		this.errorMessage=errorMessage;
		this.policyNames=null;
	}
	
	public FindAllPoliciesResult(int errorCode,String errorMessage,String[] policyNames){
		this.errorCode=errorCode;
		this.errorMessage=errorMessage;
		this.policyNames=policyNames;
	}
	public String[] getPolicyNames() {
		return policyNames;
	}
	public void setPolicyNames(String[] policyNames) {
		this.policyNames = policyNames;
	}
}
