package com.ruijie.spl.api;

public class FindAllUserTemplatePackagesResult extends SamApiBaseResult {
	private UserTemplatePackageInfo[] templatePackages;

	public FindAllUserTemplatePackagesResult() {

	}

	public FindAllUserTemplatePackagesResult(int errorCode, String errorMessage) {
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.templatePackages =null;
	}

	public FindAllUserTemplatePackagesResult(int errorCode, String errorMessage, UserTemplatePackageInfo[] templatePackages) {
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.templatePackages = templatePackages;
	}

	public UserTemplatePackageInfo[] getTemplatePackages() {
		return templatePackages;
	}

	public void setTemplatePackages(UserTemplatePackageInfo[] templatePackages) {
		this.templatePackages = templatePackages;
	}

}
