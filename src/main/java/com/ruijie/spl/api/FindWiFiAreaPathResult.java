package com.ruijie.spl.api;


public class FindWiFiAreaPathResult extends SamApiBaseResult{
	private String[] wifiAreaName;
	private String[] wifiAreaPath;
	
	public FindWiFiAreaPathResult(){}
	
	public  FindWiFiAreaPathResult(int errorCode, String errorMessage){
		super(errorCode,errorMessage);
	}
	
	public  FindWiFiAreaPathResult(int errorCode, String errorMessage,String[] wifiAreaName,String[] wifiAreaPath){
		super(errorCode,errorMessage);
		this.wifiAreaName = wifiAreaName;
		this.wifiAreaPath = wifiAreaPath;
	}

	public String[] getWifiAreaName() {
		return wifiAreaName;
	}

	public void setWifiAreaName(String[] wifiAreaName) {
		this.wifiAreaName = wifiAreaName;
	}

	public String[] getWifiAreaPath() {
		return wifiAreaPath;
	}

	public void setWifiAreaPath(String[] wifiAreaPath) {
		this.wifiAreaPath = wifiAreaPath;
	}
}