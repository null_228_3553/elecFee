package com.ruijie.spl.api;


public class FindOnlineUserIdListResult  extends SamApiBaseResult{
	
	private String[] userId ;
	
	private int total;
	
	public FindOnlineUserIdListResult(){
		
	}
	
	public FindOnlineUserIdListResult(int errorCode,String errorMessage){
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		
	}
	
	public FindOnlineUserIdListResult(int errorCode,String errorMessage,String[]  userId,int total){
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.userId = userId;
		this.total = total;
		
	}

	public String[] getUserId() {
		return userId;
	}

	public void setUserId(String[]  userId) {
		this.userId = userId;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

}