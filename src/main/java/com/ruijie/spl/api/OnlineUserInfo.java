package com.ruijie.spl.api;

import java.util.Date;

public class OnlineUserInfo {
	private String userId;//用户名
	private String userName;//用户姓名
	private String userIpv4;
	private String usergroupId;
	private String userTemplateId;
	private String packageName;
	private String userMac;
	private Short userVlan;
	private String gateway;
	private String dns;
	private String netMask;
	private String userIpv6;
	private String ipv6Gateway;
	private String userIpv6LocalLink;
	private Short ipv6Num;
	private String nasIp;
	private String nasIpv6;
	private Long nasPort;
	private String nasCommunity;
	private String nasType;
	private String nasTypeNum;
	private String nasName;
	private String nasLocation;
	private String wpNasIp;
    private Long   wpNasPort;
    private String serviceId;
    private String serviceSuffix;//接入控制
    private String suVersion; // 客户端信息
    private String policyId;
    private String accountId;
    private Date onlineTime; // 上线时间或创建时间
    private String apMac;      //AP的MAC地址
	private String areaName;   //地区名称
	private String ssid;       //无线SSID
	private Short accessType;  //接入类型
	private String timesegmentId;//接入时段名
	
	/**
	 * 用户运营商串号
	 */
	private String operatorsUserId; // 用户运营商串号
	
	/**
	 * 用户运营商名称
	 */
	private String operatorsName; // 用户运营商名称
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserIpv4() {
		return userIpv4;
	}
	public void setUserIpv4(String userIpv4) {
		this.userIpv4 = userIpv4;
	}
	public String getUsergroupId() {
		return usergroupId;
	}
	public void setUsergroupId(String usergroupId) {
		this.usergroupId = usergroupId;
	}
	public String getUserTemplateId() {
		return userTemplateId;
	}
	public void setUserTemplateId(String userTemplateId) {
		this.userTemplateId = userTemplateId;
	}
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public String getUserMac() {
		return userMac;
	}
	public void setUserMac(String userMac) {
		this.userMac = userMac;
	}
	public Short getUserVlan() {
		return userVlan;
	}
	public void setUserVlan(Short userVlan) {
		this.userVlan = userVlan;
	}
	public String getGateway() {
		return gateway;
	}
	public void setGateway(String gateway) {
		this.gateway = gateway;
	}
	public String getDns() {
		return dns;
	}
	public void setDns(String dns) {
		this.dns = dns;
	}
	public String getNetMask() {
		return netMask;
	}
	public void setNetMask(String netMask) {
		this.netMask = netMask;
	}
	public String getUserIpv6() {
		return userIpv6;
	}
	public void setUserIpv6(String userIpv6) {
		this.userIpv6 = userIpv6;
	}
	public String getIpv6Gateway() {
		return ipv6Gateway;
	}
	public void setIpv6Gateway(String ipv6Gateway) {
		this.ipv6Gateway = ipv6Gateway;
	}
	public String getUserIpv6LocalLink() {
		return userIpv6LocalLink;
	}
	public void setUserIpv6LocalLink(String userIpv6LocalLink) {
		this.userIpv6LocalLink = userIpv6LocalLink;
	}
	public Short getIpv6Num() {
		return ipv6Num;
	}
	public void setIpv6Num(Short ipv6Num) {
		this.ipv6Num = ipv6Num;
	}
	public String getNasIp() {
		return nasIp;
	}
	public void setNasIp(String nasIp) {
		this.nasIp = nasIp;
	}
	public String getNasIpv6() {
		return nasIpv6;
	}
	public void setNasIpv6(String nasIpv6) {
		this.nasIpv6 = nasIpv6;
	}
	public Long getNasPort() {
		return nasPort;
	}
	public void setNasPort(Long nasPort) {
		this.nasPort = nasPort;
	}
	public String getNasCommunity() {
		return nasCommunity;
	}
	public void setNasCommunity(String nasCommunity) {
		this.nasCommunity = nasCommunity;
	}
	public String getNasType() {
		return nasType;
	}
	public void setNasType(String nasType) {
		this.nasType = nasType;
	}
	public String getNasTypeNum() {
		return nasTypeNum;
	}
	public void setNasTypeNum(String nasTypeNum) {
		this.nasTypeNum = nasTypeNum;
	}
	public String getNasName() {
		return nasName;
	}
	public void setNasName(String nasName) {
		this.nasName = nasName;
	}
	public String getNasLocation() {
		return nasLocation;
	}
	public void setNasLocation(String nasLocation) {
		this.nasLocation = nasLocation;
	}
	public String getWpNasIp() {
		return wpNasIp;
	}
	public void setWpNasIp(String wpNasIp) {
		this.wpNasIp = wpNasIp;
	}
	public Long getWpNasPort() {
		return wpNasPort;
	}
	public void setWpNasPort(Long wpNasPort) {
		this.wpNasPort = wpNasPort;
	}
	public String getServiceId() {
		return serviceId;
	}
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	public String getServiceSuffix() {
		return serviceSuffix;
	}
	public void setServiceSuffix(String serviceSuffix) {
		this.serviceSuffix = serviceSuffix;
	}
	public String getSuVersion() {
		return suVersion;
	}
	public void setSuVersion(String suVersion) {
		this.suVersion = suVersion;
	}
	public String getPolicyId() {
		return policyId;
	}
	public void setPolicyId(String policyId) {
		this.policyId = policyId;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public Date getOnlineTime() {
		return onlineTime;
	}
	public void setOnlineTime(Date onlineTime) {
		this.onlineTime = onlineTime;
	}
	public String getApMac() {
		return apMac;
	}
	public void setApMac(String apMac) {
		this.apMac = apMac;
	}
	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	public String getSsid() {
		return ssid;
	}
	public void setSsid(String ssid) {
		this.ssid = ssid;
	}
	public Short getAccessType() {
		return accessType;
	}
	public void setAccessType(Short accessType) {
		this.accessType = accessType;
	}
	public String getTimesegmentId() {
		return timesegmentId;
	}
	public void setTimesegmentId(String timesegmentId) {
		this.timesegmentId = timesegmentId;
	}
	public String getOperatorsUserId() {
		return operatorsUserId;
	}
	public void setOperatorsUserId(String operatorsUserId) {
		this.operatorsUserId = operatorsUserId;
	}
	public String getOperatorsName() {
		return operatorsName;
	}
	public void setOperatorsName(String operatorsName) {
		this.operatorsName = operatorsName;
	}
}
