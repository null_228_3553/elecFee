package com.ruijie.spl.api;

public class QueryInhibitParam extends SamApiBaseParams {
	private String nasIpv6;
	private String userIpv4;
	private String userIpv4Ext;
	private String userIpv6;
	private String userIpv6Ext;
	private String userMac;
	private String nasIpv4;
	private String apMac;  //AP MAC
	private String ssid;  //SSID
	private String wpNasIp;//WEB接入设备Ip
	private Long wpNasPort;//web接入设备端口
	private Long nasPort;
	private Short isValid;
	private String notice;  //提示信息
	private int limit;
    private int offSet;
	
	public String getNasIpv6() {
		return nasIpv6;
	}
	public void setNasIpv6(String nasIpv6) {
		this.nasIpv6 = nasIpv6;
	}
	public String getUserIpv4() {
		return userIpv4;
	}
	public void setUserIpv4(String userIpv4) {
		this.userIpv4 = userIpv4;
	}
	public String getUserIpv4Ext() {
		return userIpv4Ext;
	}
	public void setUserIpv4Ext(String userIpv4Ext) {
		this.userIpv4Ext = userIpv4Ext;
	}
	public String getUserIpv6() {
		return userIpv6;
	}
	public void setUserIpv6(String userIpv6) {
		this.userIpv6 = userIpv6;
	}
	public String getUserIpv6Ext() {
		return userIpv6Ext;
	}
	public void setUserIpv6Ext(String userIpv6Ext) {
		this.userIpv6Ext = userIpv6Ext;
	}
	public String getUserMac() {
		return userMac;
	}
	public void setUserMac(String userMac) {
		this.userMac = userMac;
	}
	public String getNasIpv4() {
		return nasIpv4;
	}
	public void setNasIpv4(String nasIpv4) {
		this.nasIpv4 = nasIpv4;
	}
	public String getApMac() {
		return apMac;
	}
	public void setApMac(String apMac) {
		this.apMac = apMac;
	}
	public String getSsid() {
		return ssid;
	}
	public void setSsid(String ssid) {
		this.ssid = ssid;
	}
	public String getWpNasIp() {
		return wpNasIp;
	}
	public void setWpNasIp(String wpNasIp) {
		this.wpNasIp = wpNasIp;
	}
	public Long getWpNasPort() {
		return wpNasPort;
	}
	public void setWpNasPort(Long wpNasPort) {
		this.wpNasPort = wpNasPort;
	}
	public Long getNasPort() {
		return nasPort;
	}
	public void setNasPort(Long nasPort) {
		this.nasPort = nasPort;
	}
	public Short getIsValid() {
		return isValid;
	}
	public void setIsValid(Short isValid) {
		this.isValid = isValid;
	}
	public String getNotice() {
		return notice;
	}
	public void setNotice(String notice) {
		this.notice = notice;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public int getOffSet() {
		return offSet;
	}
	public void setOffSet(int offSet) {
		this.offSet = offSet;
	}
}
