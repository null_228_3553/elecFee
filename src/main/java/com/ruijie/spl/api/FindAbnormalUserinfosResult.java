package com.ruijie.spl.api;

public class FindAbnormalUserinfosResult extends SamApiBaseResult{
private String[] userinfoIds;
public FindAbnormalUserinfosResult(){
	
}
public FindAbnormalUserinfosResult(int errorCode,String errorMessage){
	this.errorCode=errorCode;
	this.errorMessage=errorMessage;
	this.userinfoIds=null;
}

public FindAbnormalUserinfosResult(int errorCode,String errorMessage,String[] userinfoIds){
	this.errorCode=errorCode;
	this.errorMessage=errorMessage;
	this.userinfoIds=userinfoIds;
}

public String[] getUserinfoIds() {
	return userinfoIds;
}

public void setUserinfoIds(String[] userinfoIds) {
	this.userinfoIds = userinfoIds;
}
}
