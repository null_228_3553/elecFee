package com.ruijie.spl.api;


public class BindOperatorForSelfResult extends SamApiBaseResult{
     private int bindLeftTimes;     
     public BindOperatorForSelfResult(){    	 
     }
     
     public  BindOperatorForSelfResult(int errorCode, String errorMessage){
 		super(errorCode,errorMessage);

     }
     
     
     public  BindOperatorForSelfResult(int errorCode, String errorMessage,int bindTimes){
    		super(errorCode,errorMessage);
    		this.bindLeftTimes = bindTimes;    	 
     }

	public int getBindTimes() {
		return bindLeftTimes;
	}

	public void setBindTimes(int bindTimes) {
		this.bindLeftTimes = bindTimes;
	}
     
     
}