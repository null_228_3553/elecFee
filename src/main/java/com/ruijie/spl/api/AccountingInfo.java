package com.ruijie.spl.api;

import java.math.BigDecimal;

public class AccountingInfo {
	private String accountId;
	private boolean canOverdraft;
	private BigDecimal overdraftFee;
	private BigDecimal overdraftFeeLeft;
	private Short accountState;
	private BigDecimal accountFee;
	private BigDecimal preAccountFee;

	public Short getAccountState() {
		return accountState;
	}

	public void setAccountState(Short accountState) {
		this.accountState = accountState;
	}

	public BigDecimal getAccountFee() {
		return accountFee;
	}

	public void setAccountFee(BigDecimal accountFee) {
		this.accountFee = accountFee;
	}

	public BigDecimal getPreAccountFee() {
		return preAccountFee;
	}

	public void setPreAccountFee(BigDecimal preAccountFee) {
		this.preAccountFee = preAccountFee;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public boolean isCanOverdraft() {
		return canOverdraft;
	}

	public void setCanOverdraft(boolean canOverdraft) {
		this.canOverdraft = canOverdraft;
	}

	public BigDecimal getOverdraftFee() {
		return overdraftFee;
	}

	public void setOverdraftFee(BigDecimal overdraftFee) {
		this.overdraftFee = overdraftFee;
	}

	public BigDecimal getOverdraftFeeLeft() {
		return overdraftFeeLeft;
	}

	public void setOverdraftFeeLeft(BigDecimal overdraftFeeLeft) {
		this.overdraftFeeLeft = overdraftFeeLeft;
	}

}
