package com.ruijie.spl.api;

import java.math.BigDecimal;
import java.util.Date;


public class UserInfo {
	private String userId;
//	private String  password;
	private String  userName;
	private String  userGroupName;
	private String  atName;
	private String  packageName;
	private int sex;
	private int freeAuthen;
	private int certType;
	private String  certNo;
	private String  edu;
	private String  address;
	private String postCode;
	private String  tel;
	private String  mobile;
	private String  email;
	private String  userIp;
//	private String  userMac;
//	private String  nasIp;
//	private long nasPort;
	private String  authorIp;
	private String  authorIpV6;//add
	private String  filterId;
	private String  loginInfo;
	private String  userPrivilege;
	private String vlanName;//add
	private String   userVlan;
	private String  baclName;
	private String  selfServPrem;
	private Date autologicDestroyTime;
	private int stateFlag;
	private Date periodLimitTime; //周期开始时间
	private Date nextBillingTime;//周期结束时间
	private String policyId;//add
	private Date suspendTime;//add
	private Date latestSelfSuspendTime;//add
	private String accountId;//add
	private int accountState;//add
	private BigDecimal accountFee;//add
	private BigDecimal preAccountFee;//add
	private BigDecimal overDraftFee;//add
	private BigDecimal overDraftFeeLeft;//add
	private boolean canOverDraft;//add
	private String suVersion;//add
	private int onlineState;//add
	private String field1;
	private String field2;
	private String field3;
	private String field4;
	private String field5;
	private String field6;
	private String field7;
	private String field8;
	private String field9;
	private String field10;
	private String field11;
	private String field12;
	private String field13;
	private String field14;
	private String field15;
	private String field16;
	private String field17;
	private String field18;
	private String field19;
	private String field20;
	private String reserved0;
	private String reserved1;
	private String reserved2;
	private String reserved3;
	private String reserved4;
	private String reserved5;
	private String reserved6;
	private String reserved7;
	private String reserved8;
	private String reserved9;
	// ww add fields
	private Date latelyLogoutTime;
	private Date expireTime;
	private Date createTime; //开户时间
	private Date periodStartTime;
	/**
	 * 组播选项。null表示不开启组播限制功能，其他值见 @see com.ruijie.spl.common.collection.Ipv6MulticastOptionCollection
	 */
	private Short ipv6MulticastOption;

	/**
	 * 用户可访问的IPv6组播地址。可以是多个，使用豆号分隔
	 */
	private String ipv6MulticastAddress;
	
	/**
	 * 组播选项。null表示不开启组播限制功能，其他值见 @see com.ruijie.spl.common.collection.Ipv4MulticastOptionCollection
	 */
	private Short ipv4MulticastOption;

	/**
	 * 用户可访问的IPv4组播地址。可以是多个，使用豆号分隔
	 */
	private String ipv4MulticastAddress;
	
	/**
	 * 用户运营商串号
	 */
	private String operatorsUserId; // 用户运营商串号
	
	/**
	 * 用户运营商名称
	 */
	private String operatorsName; // 用户运营商名称
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserGroupName() {
		return userGroupName;
	}
	public void setUserGroupName(String userGroupName) {
		this.userGroupName = userGroupName;
	}
	public String getAtName() {
		return atName;
	}
	public void setAtName(String atName) {
		this.atName = atName;
	}
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public int getSex() {
		return sex;
	}
	public void setSex(int sex) {
		this.sex = sex;
	}
	public int getFreeAuthen() {
		return freeAuthen;
	}
	public void setFreeAuthen(int freeAuthen) {
		this.freeAuthen = freeAuthen;
	}
	public int getCertType() {
		return certType;
	}
	public void setCertType(int certType) {
		this.certType = certType;
	}
	public String getCertNo() {
		return certNo;
	}
	public void setCertNo(String certNo) {
		this.certNo = certNo;
	}
	public String getEdu() {
		return edu;
	}
	public void setEdu(String edu) {
		this.edu = edu;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPostCode() {
		return postCode;
	}
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUserIp() {
		return userIp;
	}
	public void setUserIp(String userIp) {
		this.userIp = userIp;
	}
//	public String getUserMac() {
//		return userMac;
//	}
//	public void setUserMac(String userMac) {
//		this.userMac = userMac;
//	}
//	public String getNasIp() {
//		return nasIp;
//	}
//	public void setNasIp(String nasIp) {
//		this.nasIp = nasIp;
//	}
//	public long getNasPort() {
//		return nasPort;
//	}
//	public void setNasPort(long nasPort) {
//		this.nasPort = nasPort;
//	}
	public String getAuthorIp() {
		return authorIp;
	}
	public void setAuthorIp(String authorIp) {
		this.authorIp = authorIp;
	}
	public String getFilterId() {
		return filterId;
	}
	public void setFilterId(String filterId) {
		this.filterId = filterId;
	}
	public String getLoginInfo() {
		return loginInfo;
	}
	public void setLoginInfo(String loginInfo) {
		this.loginInfo = loginInfo;
	}
	public String getUserPrivilege() {
		return userPrivilege;
	}
	public void setUserPrivilege(String userPrivilege) {
		this.userPrivilege = userPrivilege;
	}
	public String getUserVlan() {
		return userVlan;
	}
	public void setUserVlan(String userVlan) {
		this.userVlan = userVlan;
	}
	public String getBaclName() {
		return baclName;
	}
	public void setBaclName(String baclName) {
		this.baclName = baclName;
	}
	public String getSelfServPrem() {
		return selfServPrem;
	}
	public void setSelfServPrem(String selfServPrem) {
		this.selfServPrem = selfServPrem;
	}
	public Date getAutologicDestroyTime() {
		return autologicDestroyTime;
	}
	public void setAutologicDestroyTime(Date autologicDestroyTime) {
		this.autologicDestroyTime = autologicDestroyTime;
	}
	public int getStateFlag() {
		return stateFlag;
	}
	public void setStateFlag(int stateFlag) {
		this.stateFlag = stateFlag;
	}
	public Date getPeriodLimitTime() {
		return periodLimitTime;
	}
	public void setPeriodLimitTime(Date periodLimitTime) {
		this.periodLimitTime = periodLimitTime;
	}
	public Date getNextBillingTime() {
		return nextBillingTime;
	}
	public void setNextBillingTime(Date nextBillingTime) {
		this.nextBillingTime = nextBillingTime;
	}
	public String getAuthorIpV6() {
		return authorIpV6;
	}
	public void setAuthorIpV6(String authorIpV6) {
		this.authorIpV6 = authorIpV6;
	}
	public String getVlanName() {
		return vlanName;
	}
	public void setVlanName(String vlanName) {
		this.vlanName = vlanName;
	}
	public String getPolicyId() {
		return policyId;
	}
	public void setPolicyId(String policyId) {
		this.policyId = policyId;
	}
	public Date getSuspendTime() {
		return suspendTime;
	}
	public void setSuspendTime(Date suspendTime) {
		this.suspendTime = suspendTime;
	}
	public Date getLatestSelfSuspendTime() {
		return latestSelfSuspendTime;
	}
	public void setLatestSelfSuspendTime(Date latestSelfSuspendTime) {
		this.latestSelfSuspendTime = latestSelfSuspendTime;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public int getAccountState() {
		return accountState;
	}
	public void setAccountState(int accountState) {
		this.accountState = accountState;
	}
	public BigDecimal getAccountFee() {
		return accountFee;
	}
	public void setAccountFee(BigDecimal accountFee) {
		this.accountFee = accountFee;
	}
	public BigDecimal getPreAccountFee() {
		return preAccountFee;
	}
	public void setPreAccountFee(BigDecimal preAccountFee) {
		this.preAccountFee = preAccountFee;
	}
	public BigDecimal getOverDraftFee() {
		return overDraftFee;
	}
	public void setOverDraftFee(BigDecimal overDraftFee) {
		this.overDraftFee = overDraftFee;
	}
	public BigDecimal getOverDraftFeeLeft() {
		return overDraftFeeLeft;
	}
	public void setOverDraftFeeLeft(BigDecimal overDraftFeeLeft) {
		this.overDraftFeeLeft = overDraftFeeLeft;
	}
	public boolean getCanOverDraft() {
		return canOverDraft;
	}
	public void setCanOverDraft(boolean canOverDraft) {
		this.canOverDraft = canOverDraft;
	}
	public String getSuVersion() {
		return suVersion;
	}
	public void setSuVersion(String suVersion) {
		this.suVersion = suVersion;
	}
	public int getOnlineState() {
		return onlineState;
	}
	public void setOnlineState(int onlineState) {
		this.onlineState = onlineState;
	}
	public String getField1() {
		return field1;
	}
	public void setField1(String field1) {
		this.field1 = field1;
	}
	public String getField2() {
		return field2;
	}
	public void setField2(String field2) {
		this.field2 = field2;
	}
	public String getField3() {
		return field3;
	}
	public void setField3(String field3) {
		this.field3 = field3;
	}
	public String getField4() {
		return field4;
	}
	public void setField4(String field4) {
		this.field4 = field4;
	}
	public String getField5() {
		return field5;
	}
	public void setField5(String field5) {
		this.field5 = field5;
	}
	public String getField6() {
		return field6;
	}
	public void setField6(String field6) {
		this.field6 = field6;
	}
	public String getField7() {
		return field7;
	}
	public void setField7(String field7) {
		this.field7 = field7;
	}
	public String getField8() {
		return field8;
	}
	public void setField8(String field8) {
		this.field8 = field8;
	}
	public String getField9() {
		return field9;
	}
	public void setField9(String field9) {
		this.field9 = field9;
	}
	public String getField10() {
		return field10;
	}
	public void setField10(String field10) {
		this.field10 = field10;
	}
	public String getField11() {
		return field11;
	}
	public void setField11(String field11) {
		this.field11 = field11;
	}
	public String getField12() {
		return field12;
	}
	public void setField12(String field12) {
		this.field12 = field12;
	}
	public String getField13() {
		return field13;
	}
	public void setField13(String field13) {
		this.field13 = field13;
	}
	public String getField14() {
		return field14;
	}
	public void setField14(String field14) {
		this.field14 = field14;
	}
	public String getField15() {
		return field15;
	}
	public void setField15(String field15) {
		this.field15 = field15;
	}
	public String getField16() {
		return field16;
	}
	public void setField16(String field16) {
		this.field16 = field16;
	}
	public String getField17() {
		return field17;
	}
	public void setField17(String field17) {
		this.field17 = field17;
	}
	public String getField18() {
		return field18;
	}
	public void setField18(String field18) {
		this.field18 = field18;
	}
	public String getField19() {
		return field19;
	}
	public void setField19(String field19) {
		this.field19 = field19;
	}
	public String getField20() {
		return field20;
	}
	public void setField20(String field20) {
		this.field20 = field20;
	}
	public String getReserved0() {
		return reserved0;
	}
	public void setReserved0(String reserved0) {
		this.reserved0 = reserved0;
	}
	public String getReserved1() {
		return reserved1;
	}
	public void setReserved1(String reserved1) {
		this.reserved1 = reserved1;
	}
	public String getReserved2() {
		return reserved2;
	}
	public void setReserved2(String reserved2) {
		this.reserved2 = reserved2;
	}
	public String getReserved3() {
		return reserved3;
	}
	public void setReserved3(String reserved3) {
		this.reserved3 = reserved3;
	}
	public String getReserved4() {
		return reserved4;
	}
	public void setReserved4(String reserved4) {
		this.reserved4 = reserved4;
	}
	public String getReserved5() {
		return reserved5;
	}
	public void setReserved5(String reserved5) {
		this.reserved5 = reserved5;
	}
	public String getReserved6() {
		return reserved6;
	}
	public void setReserved6(String reserved6) {
		this.reserved6 = reserved6;
	}
	public String getReserved7() {
		return reserved7;
	}
	public void setReserved7(String reserved7) {
		this.reserved7 = reserved7;
	}
	public String getReserved8() {
		return reserved8;
	}
	public void setReserved8(String reserved8) {
		this.reserved8 = reserved8;
	}
	public String getReserved9() {
		return reserved9;
	}
	public void setReserved9(String reserved9) {
		this.reserved9 = reserved9;
	}
	public Date getLatelyLogoutTime() {
		return latelyLogoutTime;
	}
	public void setLatelyLogoutTime(Date latelyLogoutTime) {
		this.latelyLogoutTime = latelyLogoutTime;
	}
	public Date getExpireTime() {
		return expireTime;
	}
	public void setExpireTime(Date expireTime) {
		this.expireTime = expireTime;
	}
	public Short getIpv6MulticastOption() {
		return ipv6MulticastOption;
	}
	public void setIpv6MulticastOption(Short ipv6MulticastOption) {
		this.ipv6MulticastOption = ipv6MulticastOption;
	}
	public String getIpv6MulticastAddress() {
		return ipv6MulticastAddress;
	}
	public void setIpv6MulticastAddress(String ipv6MulticastAddress) {
		this.ipv6MulticastAddress = ipv6MulticastAddress;
	}
	public Short getIpv4MulticastOption() {
		return ipv4MulticastOption;
	}
	public void setIpv4MulticastOption(Short ipv4MulticastOption) {
		this.ipv4MulticastOption = ipv4MulticastOption;
	}
	public String getIpv4MulticastAddress() {
		return ipv4MulticastAddress;
	}
	public void setIpv4MulticastAddress(String ipv4MulticastAddress) {
		this.ipv4MulticastAddress = ipv4MulticastAddress;
	}
	public String getOperatorsName() {
		return operatorsName;
	}
	public void setOperatorsName(String operatorsName) {
		this.operatorsName = operatorsName;
	}
	public String getOperatorsUserId() {
		return operatorsUserId;
	}
	public void setOperatorsUserId(String operatorsUserId) {
		this.operatorsUserId = operatorsUserId;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getPeriodStartTime() {
		return periodStartTime;
	}
	public void setPeriodStartTime(Date periodStartTime) {
		this.periodStartTime = periodStartTime;
	}

}
