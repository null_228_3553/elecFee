package com.ruijie.spl.api;

public class QueryInhibitResultV2 extends SamApiBaseResult{
	
	private int total;
	private InhibitInfoV2[] inhibitInfos;
	
	public QueryInhibitResultV2(){
		
	}
	
	public QueryInhibitResultV2(int errorCode,String errorMessage){
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.total=0;
		this.inhibitInfos=null;
	}
	
	public QueryInhibitResultV2(int errorCode,String errorMessage,int total,InhibitInfoV2[] inhibitInfos){
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.total=total;
		this.inhibitInfos=inhibitInfos;
	}
	
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}

	public InhibitInfoV2[] getInhibitInfos() {
		return inhibitInfos;
	}

	public void setInhibitInfos(InhibitInfoV2[] inhibitInfos) {
		this.inhibitInfos = inhibitInfos;
	}
}
