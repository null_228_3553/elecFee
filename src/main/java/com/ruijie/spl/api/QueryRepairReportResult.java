package com.ruijie.spl.api;

public class QueryRepairReportResult  extends SamApiBaseResult{
private int total;
private RepairReportInfo[] repairReports;

public QueryRepairReportResult(){
	
}

public QueryRepairReportResult(int errorCode,String errorMessage){
	this.errorCode=errorCode;
	this.errorMessage=errorMessage;
	this.total=0;
	this.repairReports=null;
}

public QueryRepairReportResult(int errorCode,String errorMessage,int total,RepairReportInfo[] repairReports){
	this.errorCode=errorCode;
	this.errorMessage=errorMessage;
	this.total=total;
	this.repairReports=repairReports;
}

public RepairReportInfo[] getRepairReports() {
	return repairReports;
}
public void setRepairReports(RepairReportInfo[] repairReports) {
	this.repairReports = repairReports;
}
public int getTotal() {
	return total;
}
public void setTotal(int total) {
	this.total = total;
}
}
