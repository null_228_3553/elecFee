package com.ruijie.spl.api;

public class UserFieldInfo {
	private Short fieldType;

	private String fieldName;

	private String fieldDes;

	private Long fieldOrder;

	private Boolean isRequired;

	private Boolean isShowOnSelf;
	
	private Boolean isShowOnAdmin=true;
	
	private String userFieldType;
	
	private Boolean isSelfCanModify;

	private String defaultValue;

	public Short getFieldType() {
		return fieldType;
	}

	public void setFieldType(Short fieldType) {
		this.fieldType = fieldType;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getFieldDes() {
		return fieldDes;
	}

	public void setFieldDes(String fieldDes) {
		this.fieldDes = fieldDes;
	}

	public Long getFieldOrder() {
		return fieldOrder;
	}

	public void setFieldOrder(Long fieldOrder) {
		this.fieldOrder = fieldOrder;
	}

	public Boolean getIsRequired() {
		return isRequired;
	}

	public void setIsRequired(Boolean isRequired) {
		this.isRequired = isRequired;
	}

	public Boolean getIsShowOnSelf() {
		return isShowOnSelf;
	}

	public void setIsShowOnSelf(Boolean isShowOnSelf) {
		this.isShowOnSelf = isShowOnSelf;
	}

	public Boolean getIsShowOnAdmin() {
		return isShowOnAdmin;
	}

	public void setIsShowOnAdmin(Boolean isShowOnAdmin) {
		this.isShowOnAdmin = isShowOnAdmin;
	}

	public String getUserFieldType() {
		return userFieldType;
	}

	public void setUserFieldType(String userFieldType) {
		this.userFieldType = userFieldType;
	}

	public Boolean getIsSelfCanModify() {
		return isSelfCanModify;
	}

	public void setIsSelfCanModify(Boolean isSelfCanModify) {
		this.isSelfCanModify = isSelfCanModify;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

}
