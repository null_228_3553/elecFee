package com.ruijie.spl.api;

public class FindFlowDescInfoResult extends SamApiBaseResult{
	private FlowDescInfo flowDescInfo;
	public FindFlowDescInfoResult(){
		
	}
	public FindFlowDescInfoResult(int errorCode,String errorMessage){
		this.errorCode=errorCode;
		this.errorMessage=errorMessage;
	}

	public FindFlowDescInfoResult(int errorCode,String errorMessage,FlowDescInfo flowDescInfo){
		this.errorCode=errorCode;
		this.errorMessage=errorMessage;
		this.setFlowDescInfo(flowDescInfo);
	}
	public FlowDescInfo getFlowDescInfo() {
		return flowDescInfo;
	}
	public void setFlowDescInfo(FlowDescInfo flowDescInfo) {
		this.flowDescInfo = flowDescInfo;
	}
}