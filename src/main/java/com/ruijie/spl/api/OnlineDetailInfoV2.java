package com.ruijie.spl.api;

import java.math.BigDecimal;
import java.util.Date;

public class OnlineDetailInfoV2 {
	private String userId;
	private String userMac;
	private String usergroupId;
	private String userTemplateId;
	private String suVersion; // 客户端信息
	private String userIpv4;
	private String userIpv6;
	private String nasIp;
	private String nasIpv6;
	private String serviceId;
	private String policyId;
	private String accountId;
	private String wpNasIp;
	private Long wpNasPort;
	private String proxyName;
	private Date loginTime;
	private Date logoutTime;
	private Long onlineSec;
	private String terminateCause;
	private String tunnelClient;
	private String tunnelServer;
	private String apMac; // AP的MAC地址
	private String areaName; // 地区名称
	private String ssid; // 无线SSID
	private Boolean isRoaming = false; // 是否开始了漫游
	private BigDecimal totalTraffic; // 1X流量
	private String serviceSuffix;
	private String accountingRule;
	private String packageName;
	private String timesegmentId;
	private String internetAccessFee;// 每次上网的上网费
	private String reserved0;
	private String reserved1;
	private String reserved2;
	private String reserved3;
	private String reserved4;
	private String reserved5;
	private String reserved6;
	private String reserved7;
	private String reserved8;
	private String reserved9;

	private Short ceVlan;
	private Short peVlan;
	//终端操作系统
	private String terminalOperationSystem;
	//终端类型
	private String terminalTypeDes;
	private String domainName;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserMac() {
		return userMac;
	}

	public void setUserMac(String userMac) {
		this.userMac = userMac;
	}

	public String getUsergroupId() {
		return usergroupId;
	}

	public void setUsergroupId(String usergroupId) {
		this.usergroupId = usergroupId;
	}

	public String getUserTemplateId() {
		return userTemplateId;
	}

	public void setUserTemplateId(String userTemplateId) {
		this.userTemplateId = userTemplateId;
	}

	public String getSuVersion() {
		return suVersion;
	}

	public void setSuVersion(String suVersion) {
		this.suVersion = suVersion;
	}

	public String getUserIpv4() {
		return userIpv4;
	}

	public void setUserIpv4(String userIpv4) {
		this.userIpv4 = userIpv4;
	}

	public String getUserIpv6() {
		return userIpv6;
	}

	public void setUserIpv6(String userIpv6) {
		this.userIpv6 = userIpv6;
	}

	public String getNasIp() {
		return nasIp;
	}

	public void setNasIp(String nasIp) {
		this.nasIp = nasIp;
	}

	public String getNasIpv6() {
		return nasIpv6;
	}

	public void setNasIpv6(String nasIpv6) {
		this.nasIpv6 = nasIpv6;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getPolicyId() {
		return policyId;
	}

	public void setPolicyId(String policyId) {
		this.policyId = policyId;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getWpNasIp() {
		return wpNasIp;
	}

	public void setWpNasIp(String wpNasIp) {
		this.wpNasIp = wpNasIp;
	}

	public Long getWpNasPort() {
		return wpNasPort;
	}

	public void setWpNasPort(Long wpNasPort) {
		this.wpNasPort = wpNasPort;
	}

	public String getProxyName() {
		return proxyName;
	}

	public void setProxyName(String proxyName) {
		this.proxyName = proxyName;
	}

	public Date getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}

	public Date getLogoutTime() {
		return logoutTime;
	}

	public void setLogoutTime(Date logoutTime) {
		this.logoutTime = logoutTime;
	}

	public Long getOnlineSec() {
		return onlineSec;
	}

	public void setOnlineSec(Long onlineSec) {
		this.onlineSec = onlineSec;
	}

	public String getTerminateCause() {
		return terminateCause;
	}

	public void setTerminateCause(String terminateCause) {
		this.terminateCause = terminateCause;
	}

	public String getTunnelClient() {
		return tunnelClient;
	}

	public void setTunnelClient(String tunnelClient) {
		this.tunnelClient = tunnelClient;
	}

	public String getTunnelServer() {
		return tunnelServer;
	}

	public void setTunnelServer(String tunnelServer) {
		this.tunnelServer = tunnelServer;
	}

	public String getApMac() {
		return apMac;
	}

	public void setApMac(String apMac) {
		this.apMac = apMac;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public String getSsid() {
		return ssid;
	}

	public void setSsid(String ssid) {
		this.ssid = ssid;
	}

	public Boolean getIsRoaming() {
		return isRoaming;
	}

	public void setIsRoaming(Boolean isRoaming) {
		this.isRoaming = isRoaming;
	}

	public BigDecimal getTotalTraffic() {
		return totalTraffic;
	}

	public void setTotalTraffic(BigDecimal totalTraffic) {
		this.totalTraffic = totalTraffic;
	}

	public String getServiceSuffix() {
		return serviceSuffix;
	}

	public void setServiceSuffix(String serviceSuffix) {
		this.serviceSuffix = serviceSuffix;
	}

	public String getAccountingRule() {
		return accountingRule;
	}

	public void setAccountingRule(String accountingRule) {
		this.accountingRule = accountingRule;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getTimesegmentId() {
		return timesegmentId;
	}

	public void setTimesegmentId(String timesegmentId) {
		this.timesegmentId = timesegmentId;
	}

	public String getInternetAccessFee() {
		return internetAccessFee;
	}

	public void setInternetAccessFee(String internetAccessFee) {
		this.internetAccessFee = internetAccessFee;
	}

	public String getReserved0() {
		return reserved0;
	}

	public void setReserved0(String reserved0) {
		this.reserved0 = reserved0;
	}

	public String getReserved1() {
		return reserved1;
	}

	public void setReserved1(String reserved1) {
		this.reserved1 = reserved1;
	}

	public String getReserved2() {
		return reserved2;
	}

	public void setReserved2(String reserved2) {
		this.reserved2 = reserved2;
	}

	public String getReserved3() {
		return reserved3;
	}

	public void setReserved3(String reserved3) {
		this.reserved3 = reserved3;
	}

	public String getReserved4() {
		return reserved4;
	}

	public void setReserved4(String reserved4) {
		this.reserved4 = reserved4;
	}

	public String getReserved5() {
		return reserved5;
	}

	public void setReserved5(String reserved5) {
		this.reserved5 = reserved5;
	}

	public String getReserved6() {
		return reserved6;
	}

	public void setReserved6(String reserved6) {
		this.reserved6 = reserved6;
	}

	public String getReserved7() {
		return reserved7;
	}

	public void setReserved7(String reserved7) {
		this.reserved7 = reserved7;
	}

	public String getReserved8() {
		return reserved8;
	}

	public void setReserved8(String reserved8) {
		this.reserved8 = reserved8;
	}

	public String getReserved9() {
		return reserved9;
	}

	public void setReserved9(String reserved9) {
		this.reserved9 = reserved9;
	}

	public Short getCeVlan() {
		return ceVlan;
	}

	public void setCeVlan(Short ceVlan) {
		this.ceVlan = ceVlan;
	}

	public Short getPeVlan() {
		return peVlan;
	}

	public void setPeVlan(Short peVlan) {
		this.peVlan = peVlan;
	}

	public String getTerminalOperationSystem() {
		return terminalOperationSystem;
	}

	public void setTerminalOperationSystem(String terminalOperationSystem) {
		this.terminalOperationSystem = terminalOperationSystem;
	}

	public String getTerminalTypeDes() {
		return terminalTypeDes;
	}

	public void setTerminalTypeDes(String terminalTypeDes) {
		this.terminalTypeDes = terminalTypeDes;
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

}
