package com.ruijie.spl.api;

public class FindAllUserGroupsResult extends SamApiBaseResult{
private GroupLevelInfo[] groupLevelInfo;

public FindAllUserGroupsResult(){
	
}

public FindAllUserGroupsResult(int errorCode,String errorMessage){
	this.errorCode=errorCode;
	this.errorMessage=errorMessage;
	this.groupLevelInfo=null;
}
public FindAllUserGroupsResult(int errorCode,String errorMessage,GroupLevelInfo[] groupLevelInfo){
	this.errorCode=errorCode;
	this.errorMessage=errorMessage;
	this.groupLevelInfo=groupLevelInfo;
}

public GroupLevelInfo[] getGroupLevelInfo() {
	return groupLevelInfo;
}

public void setGroupLevelInfo(GroupLevelInfo[] groupLevelInfo) {
	this.groupLevelInfo = groupLevelInfo;
}

}
