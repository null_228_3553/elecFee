package com.ruijie.spl.api;

public class FindAllBaclNamesResult extends SamApiBaseResult{
	private String[] baclNames;
	public FindAllBaclNamesResult(){
		
	}
	
	public FindAllBaclNamesResult(int errorCode,String errorMessage){
		this.errorCode=errorCode;
		this.errorMessage=errorMessage;
		this.setBaclNames(null);
	}
	
	public FindAllBaclNamesResult(int errorCode,String errorMessage,String[] baclNames){
		this.errorCode=errorCode;
		this.errorMessage=errorMessage;
		this.setBaclNames(baclNames);
	}
	public String[] getBaclNames() {
		return baclNames;
	}
	public void setBaclNames(String[] baclNames) {
		this.baclNames = baclNames;
	}
}
