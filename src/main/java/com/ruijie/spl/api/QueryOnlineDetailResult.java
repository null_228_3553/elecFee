package com.ruijie.spl.api;

public class QueryOnlineDetailResult extends SamApiBaseResult{
	private OnlineDetailInfo[] onlindetailInfo;
	private int total;
	public QueryOnlineDetailResult(){
		
	}
	
	public QueryOnlineDetailResult(int errorCode,String errorMessage){
		this.errorCode=errorCode;
		this.errorMessage=errorMessage;
		this.total=0;
		this.onlindetailInfo=null;
	}
	
	public QueryOnlineDetailResult(int errorCode,String errorMessage,int total,OnlineDetailInfo[] onlindetailInfo){
		this.errorCode=errorCode;
		this.errorMessage=errorMessage;
		this.total=total;
		this.onlindetailInfo=onlindetailInfo;
	}
	public OnlineDetailInfo[] getOnlindetailInfo() {
		return onlindetailInfo;
	}
	public void setOnlindetailInfo(OnlineDetailInfo[] onlindetailInfo) {
		this.onlindetailInfo = onlindetailInfo;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
}
