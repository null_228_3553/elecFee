package com.ruijie.spl.api;

public class ResumeUserResult extends SamApiBaseResult {
private boolean isResumeSuccess;

public ResumeUserResult(){
	
}

public ResumeUserResult(int errorCode,String errorMessage){
	this.errorCode=errorCode;
	this.errorMessage=errorMessage;
}

public ResumeUserResult(int errorCode,String errorMessage,boolean isResumeSuccess){
	this.errorCode=errorCode;
	this.errorMessage=errorMessage;
	this.setResumeSuccess(isResumeSuccess);
}

public boolean isResumeSuccess() {
	return isResumeSuccess;
}

public void setResumeSuccess(boolean isResumeSuccess) {
	this.isResumeSuccess = isResumeSuccess;
}
}
