package com.ruijie.spl.api;

public class QueryInhibitResult extends SamApiBaseResult {
	
	private int total;
	private InhibitInfo[] inhibitInfos;
	
	public QueryInhibitResult(){
		
	}
	
	public QueryInhibitResult(int errorCode,String errorMessage){
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.total=0;
		this.inhibitInfos=null;
	}
	
	public QueryInhibitResult(int errorCode,String errorMessage,int total,InhibitInfo[] inhibitInfos){
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.total=total;
		this.inhibitInfos=inhibitInfos;
	}
	
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}

	public InhibitInfo[] getInhibitInfos() {
		return inhibitInfos;
	}

	public void setInhibitInfos(InhibitInfo[] inhibitInfos) {
		this.inhibitInfos = inhibitInfos;
	}
}
