package com.ruijie.spl.api;


public class FindUserLocationResult extends SamApiBaseResult{
	private String[] locations;
	
	public FindUserLocationResult(){}
	
	public  FindUserLocationResult(int errorCode, String errorMessage){
		super(errorCode,errorMessage);
	}
	
	public  FindUserLocationResult(int errorCode, String errorMessage,String[] locations){
		super(errorCode,errorMessage);
	}

	public String[] getLocations() {
		return locations;
	}

	public void setLocations(String[] locations) {
		this.locations = locations;
	}

}