package com.ruijie.spl.api;

public class VerifyManagerPasswordResult extends SamApiBaseResult {
	private boolean isPasswordCorrect;

	public VerifyManagerPasswordResult() {

	}

	public VerifyManagerPasswordResult(int errorCode, String errorMessage) {
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}
	
	public VerifyManagerPasswordResult(int errorCode, String errorMessage, boolean isPasswordCorrect) {
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.isPasswordCorrect = isPasswordCorrect;
	}

	public boolean isPasswordCorrect() {
		return isPasswordCorrect;
	}

	public void setPasswordCorrect(boolean isPasswordCorrect) {
		this.isPasswordCorrect = isPasswordCorrect;
	}

}
