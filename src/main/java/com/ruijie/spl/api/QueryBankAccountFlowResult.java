package com.ruijie.spl.api;

public class QueryBankAccountFlowResult extends SamApiBaseResult{
	private AccountFlowInfo [] data;
	private int total;
	
	
	public QueryBankAccountFlowResult(){
		
	}
	
	public QueryBankAccountFlowResult(int errorCode,String message){
		this.errorCode=errorCode;
		this.errorMessage=message;
	}
	
	public QueryBankAccountFlowResult(int errorCode,String message,int total,AccountFlowInfo [] data){
		this.errorCode=errorCode;
		this.errorMessage=message;
		this.total=total;
		this.data=data;
	}
	
	public AccountFlowInfo[] getData() {
		return data;
	}
	public void setData(AccountFlowInfo[] data) {
		this.data = data;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
}