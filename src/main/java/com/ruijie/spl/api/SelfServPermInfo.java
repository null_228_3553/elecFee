package com.ruijie.spl.api;



/**
 * NtdflowRecord generated by MyEclipse - Hibernate Tools
 */

public class SelfServPermInfo  {
//     private String userId;
//     private String selfServiceSuspendInterval;
     private String selfServiceName;
//     private String managerName;
//     private String description;
//     private Date createTime;
 	private String reserved0;
 	private String reserved1;
 	private String reserved2;
 	private String reserved3;
 	private String reserved4;
 	private String reserved5;
 	private String reserved6;
 	private String reserved7;
 	private String reserved8;
 	private String reserved9;
//	public String getUserId() {
//		return userId;
//	}
//	public void setUserId(String userId) {
//		this.userId = userId;
//	}
//	public String getSelfServiceSuspendInterval() {
//		return selfServiceSuspendInterval;
//	}
//	public void setSelfServiceSuspendInterval(String selfServiceSuspendInterval) {
//		this.selfServiceSuspendInterval = selfServiceSuspendInterval;
//	}
	public String getSelfServiceName() {
		return selfServiceName;
	}
	public void setSelfServiceName(String selfServiceName) {
		this.selfServiceName = selfServiceName;
	}
//	public String getManagerName() {
//		return managerName;
//	}
//	public void setManagerName(String managerName) {
//		this.managerName = managerName;
//	}
//	public String getDescription() {
//		return description;
//	}
//	public void setDescription(String description) {
//		this.description = description;
//	}
//	public Date getCreateTime() {
//		return createTime;
//	}
//	public void setCreateTime(Date createTime) {
//		this.createTime = createTime;
//	}
	public String getReserved0() {
		return reserved0;
	}
	public void setReserved0(String reserved0) {
		this.reserved0 = reserved0;
	}
	public String getReserved1() {
		return reserved1;
	}
	public void setReserved1(String reserved1) {
		this.reserved1 = reserved1;
	}
	public String getReserved2() {
		return reserved2;
	}
	public void setReserved2(String reserved2) {
		this.reserved2 = reserved2;
	}
	public String getReserved3() {
		return reserved3;
	}
	public void setReserved3(String reserved3) {
		this.reserved3 = reserved3;
	}
	public String getReserved4() {
		return reserved4;
	}
	public void setReserved4(String reserved4) {
		this.reserved4 = reserved4;
	}
	public String getReserved5() {
		return reserved5;
	}
	public void setReserved5(String reserved5) {
		this.reserved5 = reserved5;
	}
	public String getReserved6() {
		return reserved6;
	}
	public void setReserved6(String reserved6) {
		this.reserved6 = reserved6;
	}
	public String getReserved7() {
		return reserved7;
	}
	public void setReserved7(String reserved7) {
		this.reserved7 = reserved7;
	}
	public String getReserved8() {
		return reserved8;
	}
	public void setReserved8(String reserved8) {
		this.reserved8 = reserved8;
	}
	public String getReserved9() {
		return reserved9;
	}
	public void setReserved9(String reserved9) {
		this.reserved9 = reserved9;
	}
	
}