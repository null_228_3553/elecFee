package com.ruijie.spl.api;

import java.math.BigDecimal;
import java.util.Date;


public class QueryUserParams {
	public QueryUserParams(){
		this.reserved0="disableUserIdLikeCondition";
	}
	private String userId;
	private String userName;
	//�߼���ѯ�ֶ���� ww
	//private String accountId;
	//private String webSelfhelpPerId;
	private Integer accountState; 
	private String policyInfoId;
	private String userPackageName;
	private String groupinfoId;
	private String userTemplateName;
	//private int freeAuthen;
	//private String createManagerId;
/*	private Date fromSuspendTime;
	private Date toSuspendTime;
	private Date fromSelfSuspendTime;
	private Date toSelfSuspendTime;*/
	private Date fromCreateTime;
	private Date toCreateTime;
	private Date fromAutoLogicDestroyTime;
	private Date toAutoLogicDestroyTime;
	//private String baclId;	
	//private String areaId;	
	private Integer sex;
	private String email;
	private Integer certificateType;
	private String certificateNo;
	private Integer education;
	private String personalInfo;
	private String telephone;
	private String mobile;
	private String address;
	private String postCode;
	private Long nasPort;
	private String userMac;
	private String nasipV4;
	private String wpNasIp;//ePortal����Ľ�����ip
	private Long wpNasPort;//ePortal����Ľ�������port
	private String ssid;
	private String apMac;
/*	private String gatewayV4;
	private String netmaskV4;
	private String mainDnsV4;
	private String bakDnsV4;*/
	private String useripV6;
	//private String userIpv6LocalLink;
	private String nasipV6;
	//private String gatewayV6;
	//private Integer tempipV6AddrsNum;
	private String authoripV4;
	private Long userVlan;
	private Long ceVlan;
	private Long peVlan;
	private Integer userPrivilege;
	private String filterId;	
	private BigDecimal fromPreacctFee;
	private BigDecimal toPreacctFee;
	private BigDecimal fromAccountFee;	
	private BigDecimal toAccountFee;	
	private String fromUserIpv4;	
	private String toUserIpv4;
	private int limit;
    private int offSet;
	private String field1;
	private String field2;
	private String field3;
	private String field4;
	private String field5;
	private String field6;
	private String field7;
	private String field8;
	private String field9;
	private String field10;
	private String field11;
	private String field12;
	private String field13;
	private String field14;
	private String field15;
	private String field16;
	private String field17;
	private String field18;
	private String field19;
	private String field20;
	private String reserved0;
	private String reserved1;
	private String reserved2;
	private String reserved3;
	private String reserved4;
	private String reserved5;
	private String reserved6;
	private String reserved7;
	private String reserved8;
	private String reserved9;
	
	private String operatorsName; // �û���Ӫ������
	
	private String operatorsUserId; // �û���Ӫ�̴���
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getAccountState() {
		return accountState;
	}
	public void setAccountState(Integer accountState) {
		this.accountState = accountState;
	}
	public String getPolicyInfoId() {
		return policyInfoId;
	}
	public void setPolicyInfoId(String policyInfoId) {
		this.policyInfoId = policyInfoId;
	}
	public String getUserPackageName() {
		return userPackageName;
	}
	public void setUserPackageName(String userPackageName) {
		this.userPackageName = userPackageName;
	}
	public String getGroupinfoId() {
		return groupinfoId;
	}
	public void setGroupinfoId(String groupinfoId) {
		this.groupinfoId = groupinfoId;
	}
	public String getUserTemplateName() {
		return userTemplateName;
	}
	public void setUserTemplateName(String userTemplateName) {
		this.userTemplateName = userTemplateName;
	}

	public Date getFromCreateTime() {
		return fromCreateTime;
	}
	public void setFromCreateTime(Date fromCreateTime) {
		this.fromCreateTime = fromCreateTime;
	}
	public Date getToCreateTime() {
		return toCreateTime;
	}
	public void setToCreateTime(Date toCreateTime) {
		this.toCreateTime = toCreateTime;
	}
	public Date getFromAutoLogicDestroyTime() {
		return fromAutoLogicDestroyTime;
	}
	public void setFromAutoLogicDestroyTime(Date fromAutoLogicDestroyTime) {
		this.fromAutoLogicDestroyTime = fromAutoLogicDestroyTime;
	}
	public Date getToAutoLogicDestroyTime() {
		return toAutoLogicDestroyTime;
	}
	public void setToAutoLogicDestroyTime(Date toAutoLogicDestroyTime) {
		this.toAutoLogicDestroyTime = toAutoLogicDestroyTime;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public String getCertificateNo() {
		return certificateNo;
	}
	public void setCertificateNo(String certificateNo) {
		this.certificateNo = certificateNo;
	}

	public String getPersonalInfo() {
		return personalInfo;
	}
	public void setPersonalInfo(String personalInfo) {
		this.personalInfo = personalInfo;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPostCode() {
		return postCode;
	}
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}
	public Long getNasPort() {
		return nasPort;
	}
	public void setNasPort(Long nasPort) {
		this.nasPort = nasPort;
	}
	public String getUserMac() {
		return userMac;
	}
	public void setUserMac(String userMac) {
		this.userMac = userMac;
	}
	public String getNasipV4() {
		return nasipV4;
	}
	public void setNasipV4(String nasipV4) {
		this.nasipV4 = nasipV4;
	}
	public String getWpNasIp() {
		return wpNasIp;
	}
	public void setWpNasIp(String wpNasIp) {
		this.wpNasIp = wpNasIp;
	}
	public Long getWpNasPort() {
		return wpNasPort;
	}
	public void setWpNasPort(Long wpNasPort) {
		this.wpNasPort = wpNasPort;
	}
	public String getSsid() {
		return ssid;
	}
	public void setSsid(String ssid) {
		this.ssid = ssid;
	}
	public String getApMac() {
		return apMac;
	}
	public void setApMac(String apMac) {
		this.apMac = apMac;
	}

	public String getUseripV6() {
		return useripV6;
	}
	public void setUseripV6(String useripV6) {
		this.useripV6 = useripV6;
	}

	public String getNasipV6() {
		return nasipV6;
	}
	public void setNasipV6(String nasipV6) {
		this.nasipV6 = nasipV6;
	}

	public String getAuthoripV4() {
		return authoripV4;
	}
	public void setAuthoripV4(String authoripV4) {
		this.authoripV4 = authoripV4;
	}
	public Long getUserVlan() {
		return userVlan;
	}
	public void setUserVlan(Long userVlan) {
		this.userVlan = userVlan;
	}
	public Integer getUserPrivilege() {
		return userPrivilege;
	}
	public void setUserPrivilege(Integer userPrivilege) {
		this.userPrivilege = userPrivilege;
	}
	public String getFilterId() {
		return filterId;
	}
	public void setFilterId(String filterId) {
		this.filterId = filterId;
	}
	public BigDecimal getFromPreacctFee() {
		return fromPreacctFee;
	}
	public void setFromPreacctFee(BigDecimal fromPreacctFee) {
		this.fromPreacctFee = fromPreacctFee;
	}
	public BigDecimal getToPreacctFee() {
		return toPreacctFee;
	}
	public void setToPreacctFee(BigDecimal toPreacctFee) {
		this.toPreacctFee = toPreacctFee;
	}
	public BigDecimal getFromAccountFee() {
		return fromAccountFee;
	}
	public void setFromAccountFee(BigDecimal fromAccountFee) {
		this.fromAccountFee = fromAccountFee;
	}
	public BigDecimal getToAccountFee() {
		return toAccountFee;
	}
	public void setToAccountFee(BigDecimal toAccountFee) {
		this.toAccountFee = toAccountFee;
	}
	public String getFromUserIpv4() {
		return fromUserIpv4;
	}
	public void setFromUserIpv4(String fromUserIpv4) {
		this.fromUserIpv4 = fromUserIpv4;
	}
	public String getToUserIpv4() {
		return toUserIpv4;
	}
	public void setToUserIpv4(String toUserIpv4) {
		this.toUserIpv4 = toUserIpv4;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public int getOffSet() {
		return offSet;
	}
	public void setOffSet(int offSet) {
		this.offSet = offSet;
	}
	public String getField1() {
		return field1;
	}
	public void setField1(String field1) {
		this.field1 = field1;
	}
	public String getField2() {
		return field2;
	}
	public void setField2(String field2) {
		this.field2 = field2;
	}
	public String getField3() {
		return field3;
	}
	public void setField3(String field3) {
		this.field3 = field3;
	}
	public String getField4() {
		return field4;
	}
	public void setField4(String field4) {
		this.field4 = field4;
	}
	public String getField5() {
		return field5;
	}
	public void setField5(String field5) {
		this.field5 = field5;
	}
	public String getField6() {
		return field6;
	}
	public void setField6(String field6) {
		this.field6 = field6;
	}
	public String getField7() {
		return field7;
	}
	public void setField7(String field7) {
		this.field7 = field7;
	}
	public String getField8() {
		return field8;
	}
	public void setField8(String field8) {
		this.field8 = field8;
	}
	public String getField9() {
		return field9;
	}
	public void setField9(String field9) {
		this.field9 = field9;
	}
	public String getField10() {
		return field10;
	}
	public void setField10(String field10) {
		this.field10 = field10;
	}
	public String getField11() {
		return field11;
	}
	public void setField11(String field11) {
		this.field11 = field11;
	}
	public String getField12() {
		return field12;
	}
	public void setField12(String field12) {
		this.field12 = field12;
	}
	public String getField13() {
		return field13;
	}
	public void setField13(String field13) {
		this.field13 = field13;
	}
	public String getField14() {
		return field14;
	}
	public void setField14(String field14) {
		this.field14 = field14;
	}
	public String getField15() {
		return field15;
	}
	public void setField15(String field15) {
		this.field15 = field15;
	}
	public String getField16() {
		return field16;
	}
	public void setField16(String field16) {
		this.field16 = field16;
	}
	public String getField17() {
		return field17;
	}
	public void setField17(String field17) {
		this.field17 = field17;
	}
	public String getField18() {
		return field18;
	}
	public void setField18(String field18) {
		this.field18 = field18;
	}
	public String getField19() {
		return field19;
	}
	public void setField19(String field19) {
		this.field19 = field19;
	}
	public String getField20() {
		return field20;
	}
	public void setField20(String field20) {
		this.field20 = field20;
	}
	public String getReserved0() {
		return reserved0;
	}
	public void setReserved0(String reserved0) {
		this.reserved0 = reserved0;
	}
	public String getReserved1() {
		return reserved1;
	}
	public void setReserved1(String reserved1) {
		this.reserved1 = reserved1;
	}
	public String getReserved2() {
		return reserved2;
	}
	public void setReserved2(String reserved2) {
		this.reserved2 = reserved2;
	}
	public String getReserved3() {
		return reserved3;
	}
	public void setReserved3(String reserved3) {
		this.reserved3 = reserved3;
	}
	public String getReserved4() {
		return reserved4;
	}
	public void setReserved4(String reserved4) {
		this.reserved4 = reserved4;
	}
	public String getReserved5() {
		return reserved5;
	}
	public void setReserved5(String reserved5) {
		this.reserved5 = reserved5;
	}
	public String getReserved6() {
		return reserved6;
	}
	public void setReserved6(String reserved6) {
		this.reserved6 = reserved6;
	}
	public String getReserved7() {
		return reserved7;
	}
	public void setReserved7(String reserved7) {
		this.reserved7 = reserved7;
	}
	public String getReserved8() {
		return reserved8;
	}
	public void setReserved8(String reserved8) {
		this.reserved8 = reserved8;
	}
	public String getReserved9() {
		return reserved9;
	}
	public void setReserved9(String reserved9) {
		this.reserved9 = reserved9;
	}
	public Integer getSex() {
		return sex;
	}
	public void setSex(Integer sex) {
		this.sex = sex;
	}
	public Integer getEducation() {
		return education;
	}
	public void setEducation(Integer education) {
		this.education = education;
	}
	public Integer getCertificateType() {
		return certificateType;
	}
	public void setCertificateType(Integer certificateType) {
		this.certificateType = certificateType;
	}
	public String getOperatorsName() {
		return operatorsName;
	}
	public void setOperatorsName(String operatorsName) {
		this.operatorsName = operatorsName;
	}
	public String getOperatorsUserId() {
		return operatorsUserId;
	}
	public void setOperatorsUserId(String operatorsUserId) {
		this.operatorsUserId = operatorsUserId;
	}
	public Long getCeVlan() {
		return ceVlan;
	}
	public void setCeVlan(Long ceVlan) {
		this.ceVlan = ceVlan;
	}
	public Long getPeVlan() {
		return peVlan;
	}
	public void setPeVlan(Long peVlan) {
		this.peVlan = peVlan;
	}
}