package com.ruijie.spl.api;

public class VerifyPeriodStrategyUserResult extends SamApiBaseResult {
	private boolean isPeriodStrategyUser;
	public VerifyPeriodStrategyUserResult() {
		
	}
	
	public VerifyPeriodStrategyUserResult(int errorCode, String errorMessage) {
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.setPeriodUser(false);
	}

	public VerifyPeriodStrategyUserResult(int errorCode, String errorMessage,boolean isPeriodUser ) {
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.isPeriodStrategyUser =isPeriodUser;
	}


	public boolean isPeriodUser() {
		return isPeriodStrategyUser;
	}

	public void setPeriodUser(boolean isPeriodUser) {
		this.isPeriodStrategyUser = isPeriodUser;
	}

}
