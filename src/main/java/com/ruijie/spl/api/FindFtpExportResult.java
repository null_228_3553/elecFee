package com.ruijie.spl.api;

public class FindFtpExportResult extends SamApiBaseResult {
	private String ftpServerIp;
	private Long ftpServerPort;
	private String ftpBakPath;
	private String ftpUser;
	private String[] exportTables;
	public FindFtpExportResult(){
		
	}
	
	public FindFtpExportResult(int errorCode,String errorMessage){
		super(errorCode,errorMessage);
	}
	
	public FindFtpExportResult(int errorCode,String errorMessage,String ftpServerIp,Long ftpServerPort,String ftpBakPath,String ftpUser,String[] exportTables ){
		super(errorCode,errorMessage);
		this.ftpServerIp=ftpServerIp;
		this.ftpServerPort=ftpServerPort;
		this.ftpBakPath=ftpBakPath;
		this.ftpUser=ftpUser;
		this.setExportTables(exportTables);
	}
	public String getFtpServerIp() {
		return ftpServerIp;
	}
	public void setFtpServerIp(String ftpServerIp) {
		this.ftpServerIp = ftpServerIp;
	}
	public Long getFtpServerPort() {
		return ftpServerPort;
	}
	public void setFtpServerPort(Long ftpServerPort) {
		this.ftpServerPort = ftpServerPort;
	}
	public String getFtpBakPath() {
		return ftpBakPath;
	}
	public void setFtpBakPath(String ftpBakPath) {
		this.ftpBakPath = ftpBakPath;
	}
	public String getFtpUser() {
		return ftpUser;
	}
	public void setFtpUser(String ftpUser) {
		this.ftpUser = ftpUser;
	}
	public String[] getExportTables() {
		return exportTables;
	}
	public void setExportTables(String[] exportTables) {
		this.exportTables = exportTables;
	}
}
