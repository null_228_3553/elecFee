package com.ruijie.spl.api;

public class FindSelfServPermResult extends SamApiBaseResult{
	private SelfServPermInfo [] data;
	private int total;
	
	public FindSelfServPermResult(){
		
	}
	
	public FindSelfServPermResult(int errorCode,String message){
		this.errorCode=errorCode;
		this.errorMessage=message;
	}
	
	public SelfServPermInfo[] getData() {
		return data;
	}
	public void setData(SelfServPermInfo[] data) {
		this.data = data;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
}
