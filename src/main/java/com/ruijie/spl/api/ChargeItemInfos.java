package com.ruijie.spl.api;

import java.util.ArrayList;
import java.util.List;

public class ChargeItemInfos {
	
	private String policyId;
	private List<FeeItemInfo> chargeItems = new ArrayList();

	public boolean hasPolicyId(String policyId){
		if(this.policyId==null||this.policyId.isEmpty())
			return false;
		if(this.policyId.equals(policyId))
			return true;
		
		return false;
	}
	//修改华北科大反馈问题
	public boolean getIsUsed(){
		for(FeeItemInfo fi:chargeItems){
			if(fi.getIsUsed()){
				return true;
			}
		}
		return false;
	}
		
	public List<FeeItemInfo> getChargeItems() {
		return chargeItems;
	}

	public void setChargeItems(List<FeeItemInfo> chargeItems) {
		this.chargeItems = chargeItems;
	}

	public String getPolicyId() {
		return policyId;
	}

	public void setPolicyId(String policyId) {
		this.policyId = policyId;
	}




}
