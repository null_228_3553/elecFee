import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;

import com.alibaba.fastjson.JSONObject;

public class ReadDoc {

	public static void main(String[] args) throws IOException {
		List<Course> list=new ArrayList<Course>();
		
		File file=new File("C:\\Users\\qijunhao\\Desktop\\d第六周(1).docx");
		FileInputStream fis = new FileInputStream(file);
        XWPFDocument xdoc = new XWPFDocument(fis);
        
        List<XWPFTable> tables = xdoc.getTables();  
        XWPFTable table=tables.get(0);
        List<XWPFTableRow> rows=table.getRows();
        String day = null;//星期几
        //第一行抬头略过
        for(int i=1;i<rows.size();i++){
        	XWPFTableRow row=rows.get(i);
        	List<XWPFTableCell> cells=row.getTableCells();
        	
        	//星期几为空使用上一个
        	if(!"".equals(cells.get(0).getText())){
        		day=cells.get(0).getText();
        	}
        	
        	//空行不算
        	if("".equals(cells.get(1).getText().trim())){
        		continue;
        	}
        	if(!"".equals(getText(cells.get(3)))){
            	Course course=new ReadDoc.Course();
            	course.setDay(day);
            	course.setCourseName(getText(cells.get(1)));
            	course.setClassName(getText(cells.get(2)));
        		course.setRoomName(getText(cells.get(3)));
        		course.setRemark(getText(cells.get(6)));
        		course.setCourseTime("1To4");
        		list.add(course);
        	}
        	if(!"".equals(getText(cells.get(4)))){
            	Course course=new ReadDoc.Course();
            	course.setDay(day);
            	course.setCourseName(getText(cells.get(1)));
            	course.setClassName(getText(cells.get(2)));
        		course.setRoomName(getText(cells.get(4)));
        		course.setRemark(getText(cells.get(6)));
        		course.setCourseTime("5To8");
        		list.add(course);
        	}else if(!"".equals(getText(cells.get(5)))){
            	Course course=new ReadDoc.Course();
            	course.setDay(day);
            	course.setCourseName(getText(cells.get(1)));
            	course.setClassName(getText(cells.get(2)));
        		course.setRoomName(getText(cells.get(5)));
        		course.setRemark(getText(cells.get(6)));
        		course.setCourseTime("9To12");
        		list.add(course);
        	}
        }
        JSONObject jo=new JSONObject();
        jo.put("title", file.getName());
        jo.put("list", list);
    	System.out.println(jo.toString());
        xdoc.close();
	}
	
	public static String getText(XWPFTableCell cell){
		String text=cell.getText();
		//去除ascii为12288的奇葩空格
		text=text.replace((char)12288+"", "")
				.replace((char)160+"", "").trim();
		//去除回车符
		if(text.indexOf("\n")!=-1){
			text=text.replace("\n", " ");
		}
		return text;
	}
	
	static class Course{
		private String day;
		private String courseName;
		private String className;
		private String courseTime;
		private String roomName;
		private String remark;
		public String getDay() {
			return day;
		}
		public void setDay(String day) {
			this.day = day;
		}
		public String getCourseName() {
			return courseName;
		}
		public void setCourseName(String courseName) {
			this.courseName = courseName;
		}
		public String getClassName() {
			return className;
		}
		public void setClassName(String className) {
			this.className = className;
		}
		public String getRemark() {
			return remark;
		}
		public void setRemark(String remark) {
			this.remark = remark;
		}
		public String getCourseTime() {
			return courseTime;
		}
		public void setCourseTime(String courseTime) {
			this.courseTime = courseTime;
		}
		public String getRoomName() {
			return roomName;
		}
		public void setRoomName(String roomName) {
			this.roomName = roomName;
		}
		@Override
		public String toString() {
			return "Course [day=" + day + ", courseName=" + courseName + ", className=" + className + ", courseTime="
					+ courseTime + ", roomName=" + roomName + ", remark=" + remark + "]";
		}
	}

}

