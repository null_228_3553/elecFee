--微信每日统计
create table DCP_WECHAT.WX_COUNT(
    COUNT_DATE date,
    TOTAL      number,
    INCREASE   number,
    BKS        number,
    YJS        number,
    BSS        number,
    JZG        number,
    JHS        number,
    LXS        number,
    ZKS        number,
    XYW_TOTAL  number,
    XYW_INCREASE number,
    XYW_DAY_MOST number
);
alter table DCP_WECHAT.WX_COUNT
  add constraint PK_WX_COUNT primary key (COUNT_DATE);

-- Add/modify columns 
alter table LOWELECQUEUE add userid VARCHAR2(30);
alter table LOWELECQUEUE add remainelec number;
-- Add/modify columns 
alter table MYDORMITORY add warnswitch varchar2(3) default 'on' not null;
alter table MYDORMITORY add threshold number default 10.0 not null;


create table lowElecQueue(
	id number(9) not null primary key,
  	PROGRAMID varchar2(30) NOT NULL,
  	TXTYQ varchar2(30) NOT NULL,
  	TXTLD varchar2(30) NOT NULL,
 	TXTROOM varchar2(30) NOT NULL,
	mentioned char(1) NOT NULL,
	checkTime timestamp
);

CREATE SEQUENCE lowElecQueueSeq INCREMENT BY 1 START WITH 1 NOMAXvalue NOCYCLE;

create table MYDORMITORY(
  USERID  varchar2(40) NOT NULL,
  PROGRAMID varchar2(30) NOT NULL,
  TXTYQ varchar2(30) NOT NULL,
  TXTLD varchar2(30) NOT NULL,
  TXTROOM varchar2(30) NOT NULL,
  DORMITORYSTATE char(1) DEFAULT 'N' NOT NULL ,
  CREATETIME timestamp 
);

create index idx_dormi on MYDORMITORY(USERID,DORMITORYSTATE);

Create table elecLog(
        id number(9) not null primary key,
        userId varchar2(20) not null,
        trans varchar2(30) not null,
        ip varchar2(30),
        dormitory varchar2(100),
        retMsg varchar2(100),
        success varchar2(10) not null,
        errMsg varchar2(100), 
        qryDate date,
        processTime number(9)
);

CREATE SEQUENCE elecSeq INCREMENT BY 1 START WITH 1 NOMAXvalue NOCYCLE;

create table videoCount
(
  videoName      VARCHAR2(20) not null,
  viewCount      NUMBER(9) 
)
tablespace DCP
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
insert into videoCount values('差旅安全',0);
insert into videoCount values('电子邮件安全安全',0);
insert into videoCount values('恶意软件意识',0);
insert into videoCount values('密码安全',0);
insert into videoCount values('社交安全',0);
insert into videoCount values('网络钓鱼安全',0);
insert into videoCount values('物理安全',0);
insert into videoCount values('移动安全',0);

create table nccremind(
       dutytime date,
       userid varchar(32),
       name varchar(32),
       remind number(1),
       remindTime timestamp
);
create table nccemployee(
       dept varchar(32),
       userid varchar(32),
       name varchar(32),
       mobile varchar(16)
);
insert into nccremind(dutytime,userid,name) values(to_date('2017-01-17','yyyy-mm-dd'),'2016210087','毛文卉');
insert into nccremind(dutytime,userid,name) values(to_date('2017-01-18','yyyy-mm-dd'),'2016210087','毛文卉');
insert into nccremind(dutytime,userid,name) values(to_date('2017-01-18','yyyy-mm-dd'),'2007210123','章勇');
insert into nccremind(dutytime,userid,name) values(to_date('2017-01-19','yyyy-mm-dd'),'2007210123','章勇');
insert into nccremind(dutytime,userid,name) values(to_date('2017-01-20','yyyy-mm-dd'),'2015210231','江林');
insert into nccremind(dutytime,userid,name) values(to_date('2017-01-21','yyyy-mm-dd'),'2015210231','江林');
insert into nccremind(dutytime,userid,name) values(to_date('2017-01-21','yyyy-mm-dd'),'2016210049','李凯');
insert into nccremind(dutytime,userid,name) values(to_date('2017-01-22','yyyy-mm-dd'),'2016210049','李凯');
insert into nccremind(dutytime,userid,name) values(to_date('2017-01-23','yyyy-mm-dd'),'1997010305','李伟明');
insert into nccremind(dutytime,userid,name) values(to_date('2017-01-24','yyyy-mm-dd'),'1997010305','李伟明');
insert into nccremind(dutytime,userid,name) values(to_date('2017-01-24','yyyy-mm-dd'),'2016210155','熊鹰');
insert into nccremind(dutytime,userid,name) values(to_date('2017-01-25','yyyy-mm-dd'),'2016210155','熊鹰');
insert into nccremind(dutytime,userid,name) values(to_date('2017-01-26','yyyy-mm-dd'),'2003010730','郑競力');
insert into nccremind(dutytime,userid,name) values(to_date('2017-01-27','yyyy-mm-dd'),'1999010204','唐九飞');
insert into nccremind(dutytime,userid,name) values(to_date('2017-01-28','yyyy-mm-dd'),'2007210183','张洁卉');
insert into nccremind(dutytime,userid,name) values(to_date('2017-01-29','yyyy-mm-dd'),'0000011150','贺聿志');
insert into nccremind(dutytime,userid,name) values(to_date('2017-01-30','yyyy-mm-dd'),'1996010099','龙涛');
insert into nccremind(dutytime,userid,name) values(to_date('2017-01-31','yyyy-mm-dd'),'2016210198','郑君临');
insert into nccremind(dutytime,userid,name) values(to_date('2017-02-01','yyyy-mm-dd'),'0000014723','杨勇');
insert into nccremind(dutytime,userid,name) values(to_date('2017-02-02','yyyy-mm-dd'),'1999010178','柳斌');
insert into nccremind(dutytime,userid,name) values(to_date('2017-02-03','yyyy-mm-dd'),'1997010153','周丽娟');
insert into nccremind(dutytime,userid,name) values(to_date('2017-02-04','yyyy-mm-dd'),'0070010012','蔡春光');
insert into nccremind(dutytime,userid,name) values(to_date('2017-02-05','yyyy-mm-dd'),'2015210052','雷洲');
insert into nccremind(dutytime,userid,name) values(to_date('2017-02-06','yyyy-mm-dd'),'2013210157','张雪梅');
insert into nccremind(dutytime,userid,name) values(to_date('2017-02-07','yyyy-mm-dd'),'1999010254','王景素');
insert into nccremind(dutytime,userid,name) values(to_date('2017-02-08','yyyy-mm-dd'),'2016210052','戚俊豪');
insert into nccremind(dutytime,userid,name) values(to_date('2017-02-09','yyyy-mm-dd'),'2007210123','章勇');
insert into nccremind(dutytime,userid,name) values(to_date('2017-02-10','yyyy-mm-dd'),'2007210123','章勇');
insert into nccremind(dutytime,userid,name) values(to_date('2017-02-11','yyyy-mm-dd'),'2007210123','章勇');
insert into nccremind(dutytime,userid,name) values(to_date('2017-02-12','yyyy-mm-dd'),'2016210087','毛文卉');

insert into nccemployee values('中心','2003010155','于俊清','13339993818');
insert into nccemployee values('中心','1996010051','李战春','18995621070');
insert into nccemployee values('中心','1998010241','吴驰','18607160370');
insert into nccemployee values('运行部','0000011150','贺聿志','13507181865');
insert into nccemployee values('运行部','1999010178','柳斌','13507181830');
insert into nccemployee values('运行部','2007210123','章勇','13163273382');
insert into nccemployee values('运行部','0000014723','杨勇','13507181870');
insert into nccemployee values('运行部','1997010153','周丽娟','18627104247');
insert into nccemployee values('运行部','2007210183','张洁卉','18627038206');
insert into nccemployee values('运行部','0070010012','蔡春光','13476114361');
insert into nccemployee values('运行部','2015210052','雷洲','13419635358');
insert into nccemployee values('信息部','2003010730','郑競力','18971081527');
insert into nccemployee values('信息部','2016210052','戚俊豪','18672339220');
insert into nccemployee values('信息部','2016210155','熊鹰','15972086302');
insert into nccemployee values('信息部','2016210198','郑君临','13808642538');
insert into nccemployee values('信息部','2016210176','刘晓兰','18672956526');
insert into nccemployee values('数据部','1996010099','龙涛','18602718318');
insert into nccemployee values('数据部','2016210049','李凯','15071030003');
insert into nccemployee values('数据部','2016210087','毛文卉','18672690646');
insert into nccemployee values('用户部','1999010254','王景素','18672751149');
insert into nccemployee values('用户部','2013210157','张雪梅','13429882912');
insert into nccemployee values('用户部','2015210231','江林','18872261241');
insert into nccemployee values('研究部','1993010059','李芝棠','18607155599');
insert into nccemployee values('研究部','1997010305','李伟明','13971032991');
insert into nccemployee values('研究部','2008210160','李冬','18602711250');
insert into nccemployee values('研究部','1999010204','唐九飞','18694058985');
insert into nccemployee values('办公室','0000013560','汪燕','13507181900');
insert into nccemployee values('网络维修','','程晓斌','15827521268');
insert into nccemployee values('网络维修','','李晓峰','18971542896');
insert into nccemployee values('网络维修','','龙跃','13697341956');
insert into nccemployee values('网络维修','','柳彦志','15926303940');
insert into nccemployee values('咨询收费','2014612419','饶琼','18707181859');
insert into nccemployee values('咨询收费','2014612420','张露华','13971081260');
insert into nccemployee values('咨询收费','2014612418','刘慧','15072351081');
insert into nccemployee values('安全值班','','冯师傅','18971172839');
insert into nccemployee values('安全值班','','赵师傅','18872268053');
insert into nccemployee values('同济校区','1998010634','刘波','13035114862');
insert into nccemployee values('同济校区','1975010601','邓洪志','13035114863');
insert into nccemployee values('同济校区','1987010661','李珊','13476839428');
insert into nccemployee values('同济校区','1975010677','陈大庆','13349894738');
insert into nccemployee values('同济校区','1984010539','刘纬','15927591612');
insert into nccemployee values('同济校区','2014612414','陈维','18986039925');
insert into nccemployee values('同济校区','2014612415','张莹','13476262797');