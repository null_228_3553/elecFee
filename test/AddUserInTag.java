import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

//添加性别标签
public class AddUserInTag {
	static String access_token="lE-CY-QHg_YxJoSobg6gMu2cfkH1Lo09RA1XXJXULoVoMoAPWeIKxzOHmA8oXDUGwzWgwcUNLyKcXJkqVeb-C5G_3CMkEDcHE22Imt7wbhmd02Yf8hbVcpeZnwI6okHQUign2G1sYN9AU6TsRd-hsNGJuXMn60t-0RBFVFuvkJ2oANYld2l3VRH2HlORwJhUM9l-FIhbylZsDVJb2YKeAw";
	static String url="https://qyapi.weixin.qq.com/cgi-bin/tag/addtagusers?access_token={0}";
	static int step=1000;//每次查询数
	
	public static void main(String[] args) throws IOException, SQLException, ClassNotFoundException {
		String callUrl=MessageFormat.format(url, access_token);
		Class.forName("oracle.jdbc.OracleDriver") ;
		Connection c=DriverManager.getConnection("jdbc:oracle:thin:@172.16.2.55:1521/ncdb" ,
				"DCP_CORE_PFCT" , "neusoft" ) ;
		
		int total=0;
		String countSql="select count(*) from dcp_wechat.wx_uim_tab";
		PreparedStatement ps=c.prepareStatement(countSql);
		ResultSet rs=ps.executeQuery();
		if(rs.next())
			total=rs.getInt(1);
		
		String sql="select userId,gender,rn from(select userId,gender,rownum rn"
				+ " from dcp_wechat.wx_uim_tab) where rn>=? and rn<?";
		int i=0;
		while(true){
			if(i*step>total) break;
			
			ps=c.prepareStatement(sql);
			ps.setInt(1, i*step);
			ps.setInt(2, (i+1)*step);
			rs=ps.executeQuery();
			List<String> maleList=new ArrayList<String>();
			List<String> femaleList=new ArrayList<String>();
			List<String> unknownList=new ArrayList<String>();
			while(rs.next()){
				String userId=rs.getString(1);
				String gender=rs.getString(2);
				if(gender.equals("1"))
					maleList.add(userId);
				else if(gender.equals("2"))
					femaleList.add(userId);
				else
					unknownList.add(userId);
			}
			
			send(callUrl,maleList,6);//6男士 7女士 8未知
			send(callUrl,femaleList,7);
			send(callUrl,unknownList,8);
			
			i++;
		}
	}
	public static void send(String callUrl,List<String> list,int tag) throws IOException{
		JSONObject jo=new JSONObject();
		JSONArray ja=new JSONArray();
		for(String userId:list){
			ja.add(userId);
		}
		jo.put("tagid",String.valueOf(tag));
		jo.put("userlist",ja);
		String param=jo.toString();
		
		post(callUrl,param);
	}
	
	public static void post(String callUrl, String param) throws IOException{
		URL realUrl = new URL(callUrl);
        // 打开和URL之间的连接
        URLConnection conn = realUrl.openConnection();
        // 设置通用的请求属性
        conn.setRequestProperty("accept", "*/*");
        conn.setRequestProperty("connection", "Keep-Alive");
        conn.setRequestProperty("user-agent",
                "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
        // 发送POST请求必须设置如下两行
        conn.setDoOutput(true);
        conn.setDoInput(true);
        // 获取URLConnection对象对应的输出流
        PrintWriter out = new PrintWriter(conn.getOutputStream());
        // 发送请求参数
        out.print(param);
        // flush输出流的缓冲
        out.flush();
        // 定义BufferedReader输入流来读取URL的响应
        BufferedReader in = new BufferedReader(
                new InputStreamReader(conn.getInputStream()));
        String line;
        String result="";
        while ((line = in.readLine()) != null) {
            result += line;
        }
        System.out.println(result);
	}
	
}
